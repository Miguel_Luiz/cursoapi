using System.Reflection;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Univali.Persistence.DbContexts;


namespace Univali.Api.Extensions;

internal static class StartupHelperExtensions
{
    public static async Task ResetDatabaseAsync(this WebApplication app)
    {
        using var scope = app.Services.CreateScope();

        try
        {
            var customerContext = scope.ServiceProvider.GetService<CustomerContext>();
            if (customerContext != null)
            {
                await customerContext.Database.EnsureDeletedAsync();
                await customerContext.Database.MigrateAsync();
            }

            var publisherContext = scope.ServiceProvider.GetService<PublisherContext>();
            if (publisherContext != null)
            {
                await publisherContext.Database.MigrateAsync();
            }

            var orderContext = scope.ServiceProvider.GetService<OrderContext>();
            if (orderContext != null)
            {
                await orderContext.Database.MigrateAsync();
            }
            var userContext = scope.ServiceProvider.GetService<UserContext>();
            if (userContext != null)
            {
                await userContext.Database.MigrateAsync();
            }
        }
        catch (Exception ex)
        {
            var logger = scope.ServiceProvider.GetRequiredService<ILogger>();
            logger.LogError(ex, "An error occurred while migrating the database.");
        }
    }
}
