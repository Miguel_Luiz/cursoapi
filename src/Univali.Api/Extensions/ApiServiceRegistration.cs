namespace Univali.Api.Extensions;


public static class ApiServiceRegistration{

    public static IServiceCollection AddApiServices(this IServiceCollection services){
        services.AddAuthorization(options => options.AddPolicy("AuthorPolicy", policy => {
            policy.RequireAuthenticatedUser();
            policy.RequireClaim("ClearanceLevel", "Author", "Publisher", "Admin");
        }));

        services.AddAuthorization(options => options.AddPolicy("PublisherPolicy", policy => {
            policy.RequireAuthenticatedUser();
            policy.RequireClaim("ClearanceLevel", "Publisher", "Admin");
        }));

        services.AddAuthorization(options => options.AddPolicy("StudentPolicy", policy => {
            policy.RequireAuthenticatedUser();
            policy.RequireClaim("ClearanceLevel", "Student", "Author", "Admin");
        }));

        services.AddAuthorization(options => options.AddPolicy("CustomerPolicy", policy => {
            policy.RequireAuthenticatedUser();
            policy.RequireClaim("ClearanceLevel", "Customer", "Admin");
        }));

        services.AddAuthorization(options => options.AddPolicy("AdminPolicy", policy => {
            policy.RequireAuthenticatedUser();
            policy.RequireClaim("ClearanceLevel", "Admin");
        }));

        services.AddAuthorization(options =>
        {
            options.AddPolicy("LoginRequired", policy =>
            {
                policy.RequireAuthenticatedUser();
            });

            options.AddPolicy("AdminOnly", policy =>
            {
                policy.RequireRole("Admin");
            });
        });
        return services;
    }

}