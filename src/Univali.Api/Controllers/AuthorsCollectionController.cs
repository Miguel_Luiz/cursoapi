// aula0307
// Retornar DTO
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using Univali.Domain.Entities;
using Univali.Application.Models;
using Univali.Persistence.Repositories;
using Univali.Application.Features.AuthorsCollection;
using MediatR;

namespace Univali.Api.Controllers;

[ApiController]
[Route("api/authors-collection")]
public class AuthorsCollectionController : MainController
{
    private readonly IMediator _mediator;

    public AuthorsCollectionController(
        IMediator mediator
    ) {
        _mediator = mediator;
    }

// pablo1107 - Removi category
    [HttpGet]
    public async Task<ActionResult<IEnumerable<AuthorDto>>> GetAuthorsCollection(
        string? searchQuery = "",
        int pageNumber = 1, 
        int pageSize = 10
    ) {
        if (pageSize < 1 || pageSize > maxPageSize) {
            pageSize = maxPageSize;
        }

        var getAuthorsCollectionResponse = await _mediator.Send(
            new GetAuthorsCollectionQuery(searchQuery, pageNumber, pageSize)
        );

        Response.Headers.Add(
            "X-Pagination",
            JsonSerializer.Serialize(getAuthorsCollectionResponse.Pagination)
        );
        return Ok(getAuthorsCollectionResponse.Authors);
    }

}