using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Univali.Persistence.DbContexts;
using Univali.Domain.Entities;
using Univali.Application.Features.Addresses.Commands.CreateAddress;
using Univali.Application.Features.Addresses.Commands.DeleteAddress;
using Univali.Application.Features.Addresses.Commands.UpdateAddress;
using Univali.Application.Features.Addresses.Queries.GetAddressByIdDetail;
using Univali.Application.Features.Addresses.Queries.GetAddresses;
using Univali.Application.Models;
using Univali.Application.Features.Common;


namespace Univali.Api.Controllers;


[Route("api/customers/{customerId}/addresses")]
[Authorize(Policy = "CustomerPolicy")]
public class AddressesController : MainController
{
    private readonly IMediator _mediator;

    public AddressesController(IMediator mediator)
    {
       _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
    }

    [HttpGet]
    public async  Task<ActionResult<IEnumerable<AddressDto>>> GetAddresses(int customerId)
    {
        var getAddressesDetailQuery = new GetAddressesDetailQuery { CustomerId = customerId };
        var addressesToReturn = await _mediator.Send(getAddressesDetailQuery);

        if(!addressesToReturn.IsSuccess)
            return HandleRequestError(addressesToReturn);

        return Ok(addressesToReturn.Addresses);
    }

    [HttpGet("{addressId}", Name = "GetAddressById")]
    [AllowAnonymous] 
    public async Task<ActionResult<GetAddressByIdDetailDto>> GetAddressById(int customerId, int addressId)
    {
        var getAddressByIdDetailQueryResponse = await _mediator.Send(
            new GetAddressByIdDetailQuery { CustomerId = customerId, AddressId = addressId } 
        );


        if(!getAddressByIdDetailQueryResponse.IsSuccess)
            return HandleRequestError(getAddressByIdDetailQueryResponse);


        return Ok(getAddressByIdDetailQueryResponse.Customer);
    }

    /// <summary>
    /// Create a address for a specific customer
    /// </summary>
    /// <param name="customerId">The id of the address customer</param>
    /// <param name="createAddressCommand">The address to create</param>
    /// <returns>An ActionResult of type Address</returns>
    /// <response code="422">Validation error</response>
    [HttpPost] //Feito por Thomy
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
    public async Task<ActionResult<AddressDto>> CreateAddress(int customerId, CreateAddressCommand createAddressCommand)
    {
        if (customerId != createAddressCommand.CustomerId) return BadRequest();

        var createAddressCommandResponse = await _mediator.Send(createAddressCommand);

        if(!createAddressCommandResponse.IsSuccess)
            return HandleRequestError(createAddressCommandResponse);

        var addressForReturn = createAddressCommandResponse.Address;

        return CreatedAtRoute(
            "GetAddressById",
            new {
                customerId,
                addressId = addressForReturn.Id
            },
            addressForReturn
        );
    }


    [HttpPut("{addressId}")] //Feito por Thomy
    [Authorize(Policy = "AdminPolicy")]
    public async Task<ActionResult> UpdateAddress(int customerId, int addressId, UpdateAddressCommand updateAddressCommand)
    {
        if (customerId != updateAddressCommand.CustomerId || addressId != updateAddressCommand.AddressId)
            return BadRequest();

        var updateAddressCommandResponse = await _mediator.Send(updateAddressCommand);

        if(!updateAddressCommandResponse.IsSuccess)
            return HandleRequestError(updateAddressCommandResponse);

        return NoContent();
    }

    [HttpDelete("{addressId}")] //Feito por Thomy
    [Authorize(Policy = "AdminPolicy")]
    public async Task<ActionResult> DeleteAddress(int customerId, int addressId)
    {
        var deletedAddressCommandResponse = await _mediator.Send(
            new DeleteAddressCommand {CustomerId = customerId, AddressId = addressId}
        );


        if(!deletedAddressCommandResponse.IsSuccess)
            return HandleRequestError(deletedAddressCommandResponse);

        
        return NoContent();
    }
}
