// aula0307
// Retornar DTO
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using Univali.Domain.Entities;
using Univali.Application.Models;
using Univali.Persistence.Repositories;
using Univali.Application.Features.CoursesCollection;
using MediatR;

namespace Univali.Api.Controllers;

[ApiController]
[Route("api/courses-collection")]
public class CoursesCollectionController : MainController
{
    private readonly IMediator _mediator;

    public CoursesCollectionController(
        IMediator mediator
    ) {
        _mediator = mediator;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<CourseDto>>> GetCourses(
        string? category = "",
        string? searchQuery = "",
        int pageNumber = 1, 
        int pageSize = 10
    ) {
        if (pageSize < 1 || pageSize > maxPageSize) {
            pageSize = maxPageSize;
        }

        var getCoursesCollectionResponse = await _mediator.Send(
            new GetCoursesCollectionQuery(category, searchQuery, pageNumber, pageSize)
        );

        Response.Headers.Add(
            "X-Pagination",
            JsonSerializer.Serialize(getCoursesCollectionResponse.Pagination)
        );
        return Ok(getCoursesCollectionResponse.Courses);
    }

}