using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Univali.Application.Features.Orders.Commands.UpdateOrder;
using Univali.Application.Features.Orders.Commands.CreateOrder;
using Univali.Application.Features.Orders.Commands.DeleteOrder;
using Univali.Application.Features.Orders.Queries.GetOrderDetail;
using Univali.Application.Features.Orders.Queries.GetOrderWithReceiptDetail;
using Univali.Application.Features.Orders.Queries.GetOrders;
using Univali.Application.Features.Orders.Queries.GetOrdersWithReceipt;
using Univali.Application.Features.Orders.Queries.GetOrdersWithFilter;
using Univali.Application.Features.Common;

namespace Univali.Api.Controllers;

[ApiController]
[Route("api/customers/{customerId}/orders")]
[Authorize(Policy = "CustomerPolicy")]
public class OrderController : MainController
{
    private readonly IMediator _mediator;

    public OrderController(IMediator mediator)
    {
        _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
    }

    [HttpGet]
    public async Task<ActionResult<GetOrdersDetailQueryResponse>> GetOrders(int customerId)
    {
        var getOrdersDetailQuery = new GetOrdersDetailQuery { CustomerId = customerId };

        var result = await _mediator.Send(getOrdersDetailQuery);

        if(!result.IsSuccess) return HandleRequestError(result);

        return Ok(result.Orders);
    }

    [HttpGet("collection")]
    public async Task<ActionResult<GetOrdersWithFilterDetailQueryResponse>> GetOrdersWithFilter(int customerId, string? itemTittle = "", string? searchQuery = "", int pageNumber = 1, int pageSize = 20)
    {
        if(pageSize > maxPageSize) pageSize = maxPageSize;

        var getOrdersWithFilterDetailQuery = new GetOrdersWithFilterDetailQuery { 
            CustomerId = customerId, ItemTittle = itemTittle!, 
            SearchQuery = searchQuery!, PageNumber = pageNumber, 
            PageSize = pageSize 
        };

        var response = await _mediator.Send(getOrdersWithFilterDetailQuery);

        if(!response.IsSuccess) return HandleRequestError(response);

        return Ok(response.Orders);
    }


    [HttpGet("with-receipt")]
    public async Task<ActionResult<GetOrdersWithReceiptDetailQueryResponse>> GetOrdersWithReceipt(int customerId)
    {
        var getOrdersDetailQuery = new GetOrdersWithReceiptDetailQuery { CustomerId = customerId };

        var result = await _mediator.Send(getOrdersDetailQuery);

        if(!result.IsSuccess) return HandleRequestError(result);

        return Ok(result.Orders);
    }

    [HttpGet("{orderId}", Name = "GetOrderById")]

    public async Task<ActionResult<GetOrderDetailQueryResponse>> GetOrderById(int customerId, int orderId)
    {
        var getOrderByIdDetailQuery = new GetOrderDetailQuery { CustomerId = customerId, OrderId = orderId };

        var result = await _mediator.Send(getOrderByIdDetailQuery);

        if(!result.IsSuccess) return HandleRequestError(result);
        
        return Ok(result.Order);
    }

    [HttpGet("with-receipt/{orderId}")]
    public async Task<ActionResult<GetOrderWithReceiptDetailQueryResponse>> GetOrderWithReceiptById(int customerId, int orderId)
    {
        var getOrderByIdDetailQuery = new GetOrderWithReceiptDetailQuery { CustomerId = customerId, OrderId = orderId };

        var result = await _mediator.Send(getOrderByIdDetailQuery);

        if(!result.IsSuccess) return HandleRequestError(result);

        return Ok(result.Order);
    }

     [HttpPost]
     public async Task<ActionResult<CreateOrderCommandResponse>> CreateOrder(int customerId, CreateOrderCommand createOrderCommand)
     {
        createOrderCommand.CustomerId = customerId;
        var result = await _mediator.Send(createOrderCommand);

        if(!result.IsSuccess) return HandleRequestError(result);

        var orderToReturn = result.Order;

        return CreatedAtRoute(
            "GetOrderById",
            new 
            {
                customerId, 
                orderId = orderToReturn.OrderId 
            },
            orderToReturn
        );
     }

     [HttpPut("{orderId}")]
     public async Task<ActionResult> UpdateOrder(int customerId, int orderId, UpdateOrderCommand updateOrderCommand)
     {
        if (customerId != updateOrderCommand.CustomerId || orderId != updateOrderCommand.OrderId) return BadRequest();

        var result = await _mediator.Send(updateOrderCommand);

        if(!result.IsSuccess) return HandleRequestError(result);

        return NoContent();
     }

    [HttpDelete("{orderId}")]
    public async Task<ActionResult<DeleteOrderCommandResponse>> DeleteOrder(int customerId, int orderId)
    {
        var deleteOrder = new DeleteOrderCommand { CustomerId = customerId, OrderId = orderId };

        var response = await _mediator.Send(deleteOrder);

        if (!response.IsSuccess) return HandleRequestError(response);

        return NoContent();
    }

}