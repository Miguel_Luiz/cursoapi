using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Univali.Application.Features.Courses.Commands;
using Univali.Application.Features.Courses.Queries;
using Univali.Application.Models;

namespace Univali.Api.Controllers;

[ApiController]
[Authorize(Policy = "AuthorPolicy")]
[Route("api/publishers/{publisherId}/courses")]
public class CourseController : MainController 
{
    private readonly IMediator _mediator;

    public CourseController(IMediator mediator) 
    {
        _mediator = mediator;
    }

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [Authorize(Policy = "PublisherPolicy")]
    public async Task<ActionResult<IEnumerable<CourseDto>>> GetAllCoursesWithAuthors(
        int publisherId
    ) {
        var getAllCoursesWithAuthorsQueryResponse = await _mediator.Send(
            new GetAllCoursesWithAuthorsQuery { PublisherId = publisherId }
        );

        if (getAllCoursesWithAuthorsQueryResponse.IsSuccess == false) 
        {
            return HandleRequestError(getAllCoursesWithAuthorsQueryResponse);
        }

        // pablo1107 - retorna o objeto e não a response
        return Ok(getAllCoursesWithAuthorsQueryResponse.Courses);
    }

    [HttpGet("{courseId}", Name = "GetCourseWithAuthors")]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [Authorize(Policy = "StudentPolicy")]
    public async Task<ActionResult<CourseDto>> GetCourseWithAuthors(
        int publisherId,
        int courseId
    ) {        
        var getCourseWithAuthorsQueryResponse = await _mediator.Send(
            new GetCourseWithAuthorsQuery { PublisherId = publisherId, CourseId = courseId }
        );

        if (getCourseWithAuthorsQueryResponse.IsSuccess == false) 
        {
            return HandleRequestError(getCourseWithAuthorsQueryResponse);
        }

        // pablo1107 - retorna o objeto e não a response
        return Ok(getCourseWithAuthorsQueryResponse.Course);
    }

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status201Created)]
    public async Task<ActionResult<CreateCourseDto?>> CreateAsync(
        CreateCourseCommand createCourseCommand,
        int publisherId
    ) {        
        if (publisherId != createCourseCommand.PublisherId) 
        {
            return BadRequest("Erro: PublisherId difere da rota");
        }
        
        var createCourseCommandResponse = await _mediator.Send(createCourseCommand);

        if (createCourseCommandResponse.IsSuccess == false) 
        {
            return HandleRequestError(createCourseCommandResponse);
        }

        return CreatedAtRoute(
            "GetCourseWithAuthors",
            new { 
                courseId = createCourseCommandResponse.Course.CourseId , 
                publisherId = createCourseCommandResponse.Course.Publisher?.PublisherId 
            },
            createCourseCommandResponse.Course
        );
    }

    [HttpPut("{courseId}")]
    [Authorize(Policy = "PublisherPolicy")]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> UpdateAsync(
        int publisherId,
        int courseId, 
        UpdateCourseCommand updateCourseCommand
    ) {
        if(courseId != updateCourseCommand.CourseId
            || publisherId != updateCourseCommand.PublisherId
        ) { 
            // pablo1107 - mensagem do ID inconsistente
            return BadRequest("Erro: ID difere da rota");
        }

        var updateCourseCommandResponse = await _mediator.Send(updateCourseCommand);

        if (updateCourseCommandResponse.IsSuccess == false) 
        {
            return HandleRequestError(updateCourseCommandResponse);
        }

        return NoContent();    
    }

    [HttpDelete("{courseId}")]
    [Authorize(Policy = "PublisherPolicy")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> DeleteAsync(
        int courseId
    ) {
        var deleteCourseCommandResponse = await _mediator.Send(
            new DeleteCourseCommand{CourseId = courseId} 
        );

        if (deleteCourseCommandResponse.IsSuccess == false) 
        {
            return HandleRequestError(deleteCourseCommandResponse);
        }

        return NoContent();
    }
}
