using System.Text.Json;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Univali.Domain.Entities;
using Univali.Application.Features.QuestionsCollection.Queries.GetQuestionsCollectionDetail;

namespace Univali.Api.Controllers;

[Route("api/questions-collection")]
[Authorize]
public class QuestionsCollectionController : MainController
{
    private readonly IMediator _mediator;

    public QuestionsCollectionController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<Question>>> GetQuestions(string? category = "", string? searchQuery = "", int pageNumber = 1, int pageSize = 5)
    {
        if(pageSize > maxPageSize) pageSize = maxPageSize;

        var getQuestionsCollectionDetailQuery = new GetQuestionsCollectionDetailQuery{Category = category, SearchQuery = searchQuery, PageNumber = pageNumber, PageSize = pageSize};

        var questionResponse = await _mediator.Send(getQuestionsCollectionDetailQuery);

        if(!questionResponse.IsSuccess) 
        {
            return HandleRequestError(questionResponse);
        }

        Response.Headers.Add("X-Pagination", JsonSerializer.Serialize(questionResponse.PaginationMetadata));

        return Ok(questionResponse.QuestionsDetailDto);
    }
}