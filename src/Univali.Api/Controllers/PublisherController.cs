using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Univali.Application.Features.Publishers.Commands;
using Univali.Application.Features.Publishers.Commands.CreatePublisher;
using Univali.Application.Features.Publishers.Commands.DeletePublisher;
using Univali.Application.Features.Publishers.Commands.UpdatePublisher;
using Univali.Application.Features.Publishers.Queries;
using Univali.Application.Features.Publishers.Queries.GetPublisher;
using Univali.Application.Models;

namespace Univali.Api.Controllers;

[ApiController]
[Authorize(Policy = "PublisherPolicy")]
[Route("api/publishers")]
public class PublisherController : MainController 
{
    private readonly IMediator _mediator;

    public PublisherController(IMediator mediator) 
    {
        _mediator = mediator;
    }

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<IEnumerable<PublisherDto>>> GetAllPublishersWithCourses() 
    {
        var getAllPublishersWithCoursesQueryResponse = await _mediator.Send(
            new GetAllPublishersWithCoursesQuery()
        );

        if (getAllPublishersWithCoursesQueryResponse.IsSuccess == false) 
        {
            return HandleRequestError(getAllPublishersWithCoursesQueryResponse);
        }

        return Ok(getAllPublishersWithCoursesQueryResponse.Publisher);
    }

    [HttpGet("{publisherId}", Name = "GetPublisherWithCourses")]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [Authorize(Policy = "StudentPolicy")]
    public async Task<ActionResult<PublisherDto>> GetPublisherWithCourses(
        int publisherId
    ) {
        var GetPublisherWithCoursesQueryResponse = await _mediator.Send(
            new GetPublisherWithCoursesQuery{PublisherId = publisherId}
        );

        if (GetPublisherWithCoursesQueryResponse.IsSuccess == false) 
        {
            return HandleRequestError(GetPublisherWithCoursesQueryResponse);
        }

        return Ok(GetPublisherWithCoursesQueryResponse.Publisher);
    }

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [Authorize(Policy = "AdminPolicy")]
    public async Task<ActionResult<Object?>> CreateAsync(
        CreatePublisherCommand createPublisherCommand
    ) {        
        var createPublisherCommandResponse = await _mediator.Send(createPublisherCommand);

        if (createPublisherCommandResponse.IsSuccess == false) 
        {
            return HandleRequestError(createPublisherCommandResponse);
        }

        // return CreatedAtRoute (
        //     "GetPublisherWithCourses",
        //     new { publisherId = createPublisherCommandResponse.Publisher.PublisherId },
        //     createPublisherCommandResponse.Publisher
        // );

        return Ok (createPublisherCommandResponse.Publisher);
    }

    [HttpPut("{publisherId}")]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [Authorize(Policy = "AdminPolicy")]
    public async Task<ActionResult> UpdateAsync(
        int publisherId, 
        UpdatePublisherCommand updatePublisherCommand
    ) {
        //pablo1107 - mensagem do retorno badRequest
        if(publisherId != updatePublisherCommand.PublisherId) { 
            return BadRequest("ID informado no corpo da requisição difere do informado na rota."); 
        }

        var updatedPublisherCommandResponse = await _mediator.Send(updatePublisherCommand);

        if (!updatedPublisherCommandResponse.IsSuccess) 
        {
            return HandleRequestError(updatedPublisherCommandResponse);
        }

         return NoContent();
    }

    [HttpDelete("{publisherId}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [Authorize(Policy = "AdminPolicy")]
    public async Task<ActionResult> DeleteAsync(
        int publisherId
    ) {
        var deletedPublisherCommandResponse = await _mediator.Send(
            new DeletePublisherCommand{PublisherId = publisherId} 
        );

        if (deletedPublisherCommandResponse.IsSuccess == false) 
        {
            return HandleRequestError(deletedPublisherCommandResponse);
        }

        return NoContent();
    }
}
