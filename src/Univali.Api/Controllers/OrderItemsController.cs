using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Univali.Application.Features.Items.Commands.DeleteOrderItem;
using Univali.Application.Features.Items.Queries.GetOrdersItems;
using Univali.Application.Features.OrderItems.Commands.CreateOrderItem;
using Univali.Application.Features.OrderItems.Commands.UpdateOrderItem;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;
using System.Text.Json;
using Univali.Application.Features.Items.Queries.GetOrdersItemsWithFilter;

namespace Univali.Api.Controllers;

[ApiController]
[Route("api/customers/{customerId}/orders/{orderId}/items")]
[Authorize(Policy = "CustomerPolicy")]
public class OrderItemsController : MainController
{
    private readonly IMediator _mediator;

    public OrderItemsController(IMediator mediator)
    {
        _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
    }



    [HttpGet(Name = "GetOrderItems")] //Funcionando.
    public async Task<ActionResult<IEnumerable<GetOrdersItemsDetailDto>>> GetOrderItems(int customerId, int orderId)
    {

        var getOrdersItemsDetailQuery = new GetOrdersItemsDetailQuery { CustomerId = customerId, OrderId = orderId};

        var response = await _mediator.Send(getOrdersItemsDetailQuery);

        if (!response.IsSuccess) return HandleRequestError(response);

        return Ok(response.Items);
    }

    [HttpGet("collection")] //Funcionando.
    public async Task<ActionResult<IEnumerable<GetOrdersItemsWithFilterDto>>> GetOrderItemsWithFilter(int customerId, int orderId, int pageNumber = 1, int pageSize = 5)
    {
        if (pageSize > maxPageSize) pageSize = maxPageSize;

        var getOrdersItemsDetailQuery = new GetOrdersItemsWithFilterQuery { CustomerId = customerId, OrderId = orderId, PageNumber = pageNumber, PageSize = pageSize };

        var response = await _mediator.Send(getOrdersItemsDetailQuery);

        if (!response.IsSuccess) return HandleRequestError(response);

        Response.Headers.Add(
            "X-Pagination",
            JsonSerializer.Serialize(response.PaginationMetadata)
        );

        return Ok(response.Items);
    }

    [HttpPost]
    public async Task<ActionResult<CreateOrderItemCommandResponse>> CreateOrderItem(int customerId, int orderId, CreateOrderItemCommand createOrderItemCommand)
    {
        if(
            customerId != createOrderItemCommand.CustomerId ||
            orderId != createOrderItemCommand.OrderId
        ) return BadRequest();

        var createOrderItemCommandResponse = await _mediator.Send(createOrderItemCommand); //Manda a mensagem com o arquivo escrito para atualizar o customer escolhido.

        if (!createOrderItemCommandResponse.IsSuccess) return HandleRequestError(createOrderItemCommandResponse);

        var itemToReturn = createOrderItemCommandResponse.OrderItem;

        return CreatedAtRoute(
            "GetOrderItems",
            new
            {
                customerId,
                orderId
            },
            itemToReturn
        );
    }

    [HttpPut("{itemId}")]
    public async Task<ActionResult> UpdateOrderItem(int customerId, int orderId, int itemId, UpdateOrderItemCommand updateOrderItemCommand)
    {
        if(
            customerId != updateOrderItemCommand.CustomerId ||
            orderId != updateOrderItemCommand.OrderId ||
            itemId != updateOrderItemCommand.ItemId
        ) return BadRequest();
        
        var updateOrderItemCommandResponse = await _mediator.Send(updateOrderItemCommand); //Manda a mensagem com o arquivo escrito para atualizar o customer escolhido.

        if (!updateOrderItemCommandResponse.IsSuccess) return HandleRequestError(updateOrderItemCommandResponse);
        

        return NoContent(); //Retorna NoContent.
    }


    [HttpDelete("{orderItemId}")] //Funcionando.
    public async Task<ActionResult> DeleteOrderItem(int customerId, int orderId, int orderItemId)
    {
        var deleteOrderItemCommand = new DeleteOrderItemCommand { CustomerId = customerId, OrderId = orderId, ItemId = orderItemId };

        var deleteItemCommandResponse = await _mediator.Send(deleteOrderItemCommand);

        if (!deleteItemCommandResponse.IsSuccess) return HandleRequestError(deleteItemCommandResponse);

        return NoContent();
    }
}