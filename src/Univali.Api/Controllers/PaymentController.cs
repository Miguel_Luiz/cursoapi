using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Univali.Application.Features.Payments.Commands.UpdatePayment;
using Univali.Application.Features.Payments.Commands.DeletePayment;
using Univali.Application.Features.Payments.Commands.CreatePayment;
using Univali.Application.Features.Payments.Queries.GetPayment;

namespace Univali.Api.Controllers;


[ApiController]
[Route("api/customers/{customerId}/orders/{orderId}/payment")]

[Authorize(Policy = "CustomerPolicy")]
public class PaymentController : MainController
{
    private readonly IMediator _mediator;

    public PaymentController(IMediator mediator)
    {
        _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
    }

    [HttpGet(Name = "GetPayment")]
    public async Task<ActionResult<GetPaymentDetailDto>> GetPayment(int customerId, int orderId)
    {
        var getPaymentDetailQuery = new GetPaymentDetailQuery { CustomerId = customerId, OrderId = orderId };

        var getPaymentDetailQueryResponse = await _mediator.Send(getPaymentDetailQuery);

        if (!getPaymentDetailQueryResponse.IsSuccess) return HandleRequestError(getPaymentDetailQueryResponse);

        return Ok(getPaymentDetailQueryResponse.Payment);
    }

    [HttpPost]
    [Authorize(Policy = "AdminPolicy")]
    public async Task<ActionResult<CreatePaymentDto>> CreatePayment(int customerId, int orderId, CreatePaymentCommand createPaymentCommand)
    {
        if(createPaymentCommand == null) return BadRequest(); // string de PaymentType invalida, converter destroi o comando POST

        if(customerId != createPaymentCommand.CustomerId || orderId != createPaymentCommand.OrderId) return BadRequest();

        var createPaymentCommandResponse = await _mediator.Send(createPaymentCommand);

        if (!createPaymentCommandResponse.IsSuccess) return HandleRequestError(createPaymentCommandResponse);


        return CreatedAtRoute(
            "GetPayment",
            new { customerId, orderId },
            createPaymentCommandResponse.Payment
        );
    }

    [HttpPut]
    [Authorize(Policy = "AdminPolicy")]
    public async Task<ActionResult> UpdatePayment(int customerId, int orderId, UpdatePaymentCommand updatePaymentCommand)
    {
        if(updatePaymentCommand == null) return BadRequest(); // string de PaymentType invalida, converter destroi o comando PUT

        if(
            customerId != updatePaymentCommand.CustomerId || 
            orderId != updatePaymentCommand.OrderId
        ) return BadRequest();

        var updatePaymentCommandResponse = await _mediator.Send(updatePaymentCommand);

        if (!updatePaymentCommandResponse.IsSuccess) return HandleRequestError(updatePaymentCommandResponse);

        return NoContent();
    }

    [HttpDelete]
    [Authorize(Policy = "AdminPolicy")]
    public async Task<ActionResult> DeletePayment(int customerId, int orderId)
    {
        var deletePaymentCommand = new DeletePaymentCommand {CustomerId = customerId, OrderId = orderId};

        if(deletePaymentCommand == null) return BadRequest(); // string de PaymentType invalida, converter destroi o comando DELETE

        if(
            customerId != deletePaymentCommand.CustomerId || 
            orderId != deletePaymentCommand.OrderId 
        ) return BadRequest();

        var deletePaymentCommandResponse = await _mediator.Send(deletePaymentCommand);

        if(!deletePaymentCommandResponse.IsSuccess) return HandleRequestError(deletePaymentCommandResponse);

        return NoContent();
    }  
}