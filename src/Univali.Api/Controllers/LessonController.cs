using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Univali.Application.Features.Lessons.Commands.CreateLesson;
using Univali.Application.Features.Lessons.Commands.DeleteLesson;
using Univali.Application.Features.Lessons.Commands.UpdateLesson;
using Univali.Application.Features.Lessons.Queries.GetLessonDetail;


namespace Univali.Api.Controllers;

[Route("api/modules/{moduleId}/lessons")]
[Authorize(Policy = "AuthorPolicy")]
public class LessonsController : MainController
{
    private readonly IMediator _mediator;

    public LessonsController(IMediator mediator, IMapper mapper)
    {
        _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
    }

    [HttpPost]
    public async Task<ActionResult<CreateLessonCommandDto>> CreateLesson(
        int moduleId,
        CreateLessonCommand createLessonCommand
    )
    {
        createLessonCommand.ModuleId = moduleId;
        var createLessonCommandResponse = await _mediator.Send(createLessonCommand);

        if (!createLessonCommandResponse.IsSuccess)
        {
            return HandleRequestError(createLessonCommandResponse);
        }

        return CreatedAtRoute
        (
            "GetLessonById",
            new {moduleId, lessonId = createLessonCommandResponse.Lesson.LessonId },
            createLessonCommandResponse.Lesson
        );
    }
    
    [HttpGet("{lessonId}", Name = "GetLessonById")]
    [Authorize(Policy = "StudentPolicy")]
    public async Task<ActionResult<GetLessonDetailDto>> GetLessonById(int moduleId, int lessonId)
    {
        var getLessonDetailQuery = new GetLessonDetailQuery {LessonId = lessonId, ModuleId =  moduleId};

        var getLessonDetailQueryResponse = await _mediator.Send(getLessonDetailQuery);

        if (!getLessonDetailQueryResponse.IsSuccess)
        {
            return HandleRequestError(getLessonDetailQueryResponse);
        }

        return Ok(getLessonDetailQueryResponse.Lesson);//Miguel
    }
    
    [HttpPut("{lessonId}")]
    [Authorize(Policy = "PublisherPolicy")]

    public async Task<ActionResult> UpdateLesson(int lessonId, int moduleId, UpdateLessonCommand updateLessonCommand)
    {
        // if(moduleId != updateLessonCommand.ModuleId) return BadRequest();
        if(lessonId != updateLessonCommand.LessonId) return BadRequest("Invalid Lesson id ");

        updateLessonCommand.ModuleId = moduleId;

        var updateLessonCommandResponse = await _mediator.Send(updateLessonCommand);

        if(!updateLessonCommandResponse.IsSuccess)
        {
            return HandleRequestError(updateLessonCommandResponse);
        }

        return NoContent();
    }
    
// Delete
    [HttpDelete("{lessonId}")]
    [Authorize(Policy = "PublisherPolicy")]

    public async Task<ActionResult> DeleteLesson(int moduleId, int lessonId)
    {//Miguel novo

        var deleteLessonCommand = new DeleteLessonCommand{LessonId = lessonId, ModuleId = moduleId};

        var deleteLessonCommandResponse = await _mediator.Send(deleteLessonCommand);

        if(!deleteLessonCommandResponse.IsSuccess)
        {
            return HandleRequestError(deleteLessonCommandResponse);
        }

        return NoContent();
    }
}