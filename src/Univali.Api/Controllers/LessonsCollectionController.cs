﻿using System.Text.Json;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Univali.Application.Features.LessonsCollection.Queries;
using Univali.Persistence.Repositories;
using Univali.Application.Models;


namespace Univali.Api.Controllers;

[Route("api/lessons-collection")]
public class LessonsCollectionController : MainController
{
    private readonly IMediator _mediator;
    
    public LessonsCollectionController(IMediator mediator){
        _mediator = mediator;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<LessonDto>>> GetLessonsCollection(
        string title = "", 
        string searchQuery = "",
        int pageNumber = 1, 
        int pageSize = 5
    )
    {
        if (pageSize < 1 || pageSize > maxPageSize) {
            pageSize = maxPageSize;
        }

        var getLessonsCollectionResponse = await _mediator.Send(
            new GetLessonsCollectionQuery(title, searchQuery, pageNumber, pageSize)
        );

        Response.Headers.Add(
            "X-Pagination",
            JsonSerializer.Serialize(getLessonsCollectionResponse.Pagination)
        );
        return Ok(getLessonsCollectionResponse.Lessons);
    }
}