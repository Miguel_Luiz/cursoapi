using System.Text.Json;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Univali.Domain.Entities;
using Univali.Application.Features.Customers.Queries.GetCustomersFilter;
using Univali.Application.Features.Common;

namespace Univali.Api.Controllers;

[Route("api/customers-collection")]
public class CostumerCollectionController : MainController
{
    private readonly IMediator _mediator;

    public CostumerCollectionController(IMediator mediator)
    {
        _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
    }

    [HttpGet]
    public async Task<ActionResult <IEnumerable <Customer>>> GetCustomers(string? type = "", 
    string? searchQuery = "", int pageNumber = 1, int pageSize = 5)
    {
        var getCustomersFilterDetailQuery =
        new GetCustomersFilterDetailQuery
        {
            Type = type,
            SearchQuery = searchQuery,
            PageNumber = pageNumber,
            PageSize = pageSize
        };

        var requestResponse = await _mediator.Send(getCustomersFilterDetailQuery);
        
        if(!requestResponse.IsSuccess) 
            return HandleRequestError(requestResponse);

        var customersToReturn = requestResponse.Customers;
        var paginationMetadata = requestResponse.PaginationMetadata;
        
        Response.Headers.Add("X-Pagination",JsonSerializer.Serialize(paginationMetadata));
        return Ok(customersToReturn);
    }
}