using System.Text.Json;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Univali.Domain.Entities;
using Univali.Application.Features.Addresses.Queries.GetAddressesFilter;
using Univali.Application.Features.Common;

namespace Univali.Api.Controllers;

[Route("api/costumers/{customerId}/addresses-collection")]
public class AddressesCollectionController : MainController
{
    private readonly IMediator _mediator;

    public AddressesCollectionController(IMediator mediator)
    {
        _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
    }



    [HttpGet]
    public async Task<ActionResult <IEnumerable <Address>>> GetAddress(int customerId, string? city = "", 
    string? searchQuery = "", int pageNumber = 1, int pageSize = 2)
    {
        var getAddresesFilterDetailQuery =
        new GetAddresesFilterDetailQuery
        {
            CustomerId = customerId,
            City = city,
            SearchQuery = searchQuery,
            PageNumber = pageNumber,
            PageSize = pageSize
        };

        var requestResponse = await _mediator.Send(getAddresesFilterDetailQuery);
        
        if(!requestResponse.IsSuccess) 
            return HandleRequestError(requestResponse);
        
        var addressToReturn = requestResponse.Addreses;
        var paginationMetadata = requestResponse.PaginationMetadata;

        Response.Headers.Add("X-Pagination",JsonSerializer.Serialize(paginationMetadata));
        return Ok(addressToReturn);
    }
}