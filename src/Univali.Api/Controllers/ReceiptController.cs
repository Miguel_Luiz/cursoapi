using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Univali.Application.Features.Receipts.Queries.GetReceipt;

namespace Univali.Api.Controllers;

[ApiController]
[Route("api/customers/{customerId}/orders/{orderId}/receipt")]
[Authorize(Policy = "CustomerPolicy")]
public class ReceiptController : MainController
{
    private readonly IMediator _mediator;

    public ReceiptController(IMediator mediator)
    {
        _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
    }

    [HttpGet]
    public async Task<ActionResult<GetReceiptDetailDto>> GetReceipt(int customerId, int orderId)
    {
        var getReceiptDetailQuery = new GetReceiptDetailQuery { CustomerId = customerId, OrderId = orderId };

        var getReceiptDetailQueryResponse = await _mediator.Send(getReceiptDetailQuery);

        if (!getReceiptDetailQueryResponse.IsSuccess) return HandleRequestError(getReceiptDetailQueryResponse);

        return Ok(getReceiptDetailQueryResponse.Receipt);
    }
}