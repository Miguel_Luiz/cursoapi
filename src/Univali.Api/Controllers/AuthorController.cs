using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Univali.Application.Features.Authors.Commands;
using Univali.Application.Features.Authors.Commands.CreateAuthor;
using Univali.Application.Features.Authors.Commands.UpdateAuthor;
using Univali.Application.Features.Authors.Queries;
using Univali.Application.Models;

namespace Univali.Api.Controllers;

[ApiController]
[Authorize(Policy = "AuthorPolicy")]
[Route("api/authors")]
public class AuthorController : MainController 
{
    private readonly IMediator _mediator;

    public AuthorController(IMediator mediator) 
    {
        _mediator = mediator;
    }

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [Authorize(Policy = "PublisherPolicy")]
    public async Task<ActionResult<IEnumerable<AuthorDto>>> GetAllAuthorsWithCourses() 
    {
        var getAllAuthorsCommandResponse = await _mediator.Send(
            new GetAllAuthorsWithCoursesQuery()
        );

        if (getAllAuthorsCommandResponse.IsSuccess == false) 
        {
            return HandleRequestError(getAllAuthorsCommandResponse);
        }

        return Ok(getAllAuthorsCommandResponse.Authors);
    }

    [HttpGet("{authorId}", Name = "GetAuthorWithCourses")]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [AllowAnonymous]
    public async Task<ActionResult<AuthorDto>> GetAuthorWithCourses(
        int authorId
    ) {        
        var getAuthorCommandResponse = await _mediator.Send(
            new GetAuthorWithCoursesQuery { AuthorId = authorId }
        );

        if (getAuthorCommandResponse.IsSuccess == false) 
        {
            return HandleRequestError(getAuthorCommandResponse);
        }

        return Ok(getAuthorCommandResponse.Author);
    }

    [HttpPost]
    [Authorize(Policy = "PublisherPolicy")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    public async Task<ActionResult<AuthorDto>> CreateAuthorAsync(
        CreateAuthorCommand createAuthorCommand
    ) {
        var createAuthorCommandResponse = await _mediator.Send(createAuthorCommand);

        if (createAuthorCommandResponse.IsSuccess == false) 
        {
            return HandleRequestError(createAuthorCommandResponse);
        }

        return CreatedAtRoute(
            "GetAuthorWithCourses",
            new { authorId = createAuthorCommandResponse.Author.AuthorId },
            createAuthorCommandResponse.Author
        );
    }

    [HttpPut("{authorId}")]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [Authorize(Policy = "PublisherPolicy")]

    public async Task<ActionResult> UpdateAuthorAsync(
        int authorId, 
        UpdateAuthorCommand updateAuthorCommand
    ) {
        if(authorId != updateAuthorCommand.AuthorId) { 
            // pablo1107 - Mensagem do BadRequest
            return BadRequest("ID do autor informado no corpo da requisição difere do informado na rota."); 
        }

        var updateAuthorCommandResponse = await _mediator.Send(updateAuthorCommand);

        if (updateAuthorCommandResponse.IsSuccess == false) 
        {
            return HandleRequestError(updateAuthorCommandResponse);
        }

        return NoContent();
    }

    [HttpDelete("{authorId}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [Authorize(Policy = "PublisherPolicy")]

    public async Task<ActionResult> DeleteAuthorAsync(
        int authorId
    ) {
        var deletedAuthorCommandResponse = await _mediator.Send( 
            new DeleteAuthorCommand { AuthorId = authorId } 
        );

        if (deletedAuthorCommandResponse.IsSuccess == false) 
        {
            return HandleRequestError(deletedAuthorCommandResponse);
        }

        return NoContent(); // pablo1107 - Mudeu de notFound para NoContent
    }

    
}
