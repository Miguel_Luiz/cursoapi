using System.Text.Json;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Univali.Application.Features.Answers.Commands.CreateAnswer;
using Univali.Application.Features.Answers.Commands.DeleteAnswer;
using Univali.Application.Features.Answers.Commands.UpdateAnswer;
using Univali.Application.Features.Answers.Queries.GetAnswerDetail;
using Univali.Application.Features.Answers.Queries.GetAnswersDetail;

namespace Univali.Api.Controllers;

[Route("api/authors/{authorId}/answers")]
[Authorize(Policy = "StudentPolicy")]
public class AnswersController : MainController
{
    private readonly IMediator _mediator;

    public AnswersController(IMediator mediator)
    {
        _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<GetAnswersDetailDto>>> GetAnswers(int authorId, int pageNumber = 1, int pageSize = 5)
    {
        if(pageSize > maxPageSize) pageSize = maxPageSize;

        var getAnswersDetailQuery = new GetAnswersDetailQuery {AuthorId = authorId, PageNumber = pageNumber, PageSize = pageSize};

        var answersResponse = await _mediator.Send(getAnswersDetailQuery);

        if(!answersResponse.IsSuccess)
        {
            return HandleRequestError(answersResponse);
        }

        Response.Headers.Add("X-Pagination", JsonSerializer.Serialize(answersResponse.paginationMetadata));

        return Ok(answersResponse.AnswersDetailDtos);
    }

    [HttpGet("{answerId}", Name = "GetAnswerById")]
    public async Task<ActionResult<GetAnswerDetailDto>> GetAnswerById(int authorId, int answerId)
    {
        var getAnswerDetailQuery = new GetAnswerDetailQuery {AuthorId = authorId, AnswerId = answerId};

        var answerResponse = await _mediator.Send(getAnswerDetailQuery);

        if(!answerResponse.IsSuccess)
        {
            return HandleRequestError(answerResponse);
        }

        return Ok(answerResponse.Answer);
    }

    [HttpPost]
    [Authorize(Policy = "AuthorPolicy")]
    public async Task<ActionResult<CreateAnswerDto>> CreateAnswer(int authorId, CreateAnswerCommand createAnswerCommand)
    {
        createAnswerCommand.AuthorId = authorId;

        var createAnswerCommandResponse = await _mediator.Send(createAnswerCommand);

        if (!createAnswerCommandResponse.IsSuccess)
        {
            return HandleRequestError(createAnswerCommandResponse);
        }

        return CreatedAtRoute
        (
            "GetAnswerById",
            new {authorId, createAnswerCommandResponse.Answer.AnswerId},
            createAnswerCommandResponse.Answer
        );
    }

    [HttpPut("{answerId}")]
    [Authorize(Policy = "AuthorPolicy")]
    public async Task<ActionResult> UpdateAnswer(int authorId, int answerId, UpdateAnswerCommand updateAnswerCommand)
    {
        if (updateAnswerCommand.AnswerId != answerId) return BadRequest();

        updateAnswerCommand.AuthorId = authorId;

        var updateAnswerCommandResponse = await _mediator.Send(updateAnswerCommand);

        if (!updateAnswerCommandResponse.IsSuccess)
        {
            return HandleRequestError(updateAnswerCommandResponse);
        }

        return NoContent();
    }

    [HttpDelete("{answerId}")]
    [Authorize(Policy = "AuthorPolicy")]
    public async Task<ActionResult> DeleteAnswer(int authorId, int answerId)
    {
        var deleteAnswerCommand = new DeleteAnswerCommand {AnswerId = answerId, AuthorId = authorId};

        var answerResponse = await _mediator.Send(deleteAnswerCommand);

        if (!answerResponse.IsSuccess)
        {
            return  HandleRequestError(answerResponse);
        }

        return NoContent();
    }
}