﻿using Microsoft.AspNetCore.Mvc;
using MediatR;
using System.Text.Json;
using Univali.Application.Models;
using Univali.Persistence.Repositories;
using Univali.Application.Features.ModulesCollection.Queries;
using Univali.Domain.Entities;

namespace Univali.Api.Controllers;


[Route("api/modules-collection")]
public class ModulesCollectionController : MainController
{
    private readonly IMediator _mediator;
    
    public ModulesCollectionController(IMediator mediator){
        _mediator = mediator;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<ModuleDto>>> GetModules(
        string? title = "", 
        string? searchQuery = "",
        int pageNumber = 1, 
        int pageSize = 5
    )
    {
        if (pageSize < 1 || pageSize > maxPageSize) {
            pageSize = maxPageSize;
        }

        var getModulesCollectionResponse = await _mediator.Send(
            new GetModulesCollectionQuery(title, searchQuery, pageNumber, pageSize)
        );

        Response.Headers.Add(
            "X-Pagination",
            JsonSerializer.Serialize(getModulesCollectionResponse.Pagination)
        );

        return Ok(getModulesCollectionResponse.Modules);
    }
}