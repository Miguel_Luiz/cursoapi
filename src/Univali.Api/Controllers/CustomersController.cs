using System.Security.Cryptography.Xml;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore; //Utilização do EntityFramework.
using Univali.Persistence.DbContexts;
using Univali.Domain.Entities; //Acesso as Entidades.
using Univali.Application.Features.Customers.Commands.CreateCustomer; //Acesso ao CQRS do CreateCustomer.
using Univali.Application.Features.Customers.Commands.CreateCustomerWithAddresses; //Acesso ao CQRS do CreateCustomerWithAddresses.
using Univali.Application.Features.Customers.Commands.DeleteCustomer; //Aceso ao CQRS do DeleteCustomer.
using Univali.Application.Features.Customers.Commands.PartiallyUpdateCustomer;
using Univali.Application.Features.Customers.Commands.UpdateCustomer; //Acesso ao CQRS do UpdateCustomer.
using Univali.Application.Features.Customers.Commands.UpdateCustomerWithAddresses; //Acesso ao CQRS do UpdateCustomerWithAddresses.
using Univali.Application.Features.Customers.Queries.GetCustomerByCnpjDetail;
using Univali.Application.Features.Customers.Queries.GetCustomerByCpfDetail; //Acesso ao CQRS do GetCustomerByCpf.
using Univali.Application.Features.Customers.Queries.GetCustomerDetail; //Acesso ao CQRS do GetCustomer.
using Univali.Application.Features.Customers.Queries.GetCustomers; //Acesso ao CQRS do GetCustomers.
using Univali.Application.Features.Customers.Queries.GetCustomersWithAddresses; //Acesso ao CQRS do GetCustomersWithAddresses.
using Univali.Application.Features.Customers.Queries.GetCustomerWithAddressesByIdDetail; //Acesso ao CQRS do GetCustomerWIthAddressesById.
using Univali.Application.Models; //Acesso aos DTOs.
using Univali.Application.Features.Common;

namespace Univali.Api.Controllers;

[Route("api/customers")]
[Authorize(Policy = "CustomerPolicy")]
public class CustomersController : MainController
{
    private readonly IMediator _mediator; //Responsável para acessar a CQRS.

    public CustomersController(IMediator mediator) //Injetor de dependencia.
    {
        _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<GetCustomersDetailDto>>> GetCustomers() //Método para pegar todos os customers sem addresses.
    {
        var getCustomersDetailQuery = new GetCustomersDetailQuery{}; //Criando a request necessária para a CQRS.

        var customersToReturn = await _mediator.Send(getCustomersDetailQuery); //Enviando a "Mensagem" para a CQRS.

        if(!customersToReturn.IsSuccess) 
            return HandleRequestError(customersToReturn);

        return Ok(customersToReturn.Customers); //Retornando Ok e a DTO mapeada.
    }

    [HttpGet("{customerId}", Name = "GetCustomerById")]
    [AllowAnonymous]
    public async Task<ActionResult<GetCustomerDetailDto>> GetCustomerById(int customerId) //Método para pegar o customer com determinado Id .
    {
        var getCustomerDetailQuery = new GetCustomerDetailQuery {CustomerId = customerId}; //Criando a request necessária para o CQRS.

        var customerToReturn = await _mediator.Send(getCustomerDetailQuery); //Envia a "Mensagem" para a CQRS e recebe a Dto ja mapeada.

        if(!customerToReturn.IsSuccess) 
            return HandleRequestError(customerToReturn);

        return Ok(customerToReturn.Customer); //Retorna Ok e a Dto.
    }

    [HttpGet("with-addresses")]
    public async Task<ActionResult<IEnumerable<GetCustomersWithAddressesDetailDto>>> GetCustomersWithAddresses() //Método para pegar todos os customer com seus respectivos addresses.
    {
        var getCustomersWithAddressesDetailQuery = new GetCustomersWithAddressesDetailQuery{}; //Criando a request para o CQRS.

        var customerToReturn = await _mediator.Send(getCustomersWithAddressesDetailQuery); //Enviando a "Mensagem" para o CQRS.

        if(!customerToReturn.IsSuccess) 
            return HandleRequestError(customerToReturn);

      return Ok(customerToReturn.Customers); //Retornando Ok, e a DTO mapeada.
    }

    [HttpGet("cpf/{customerCpf}")]
    [Authorize(Policy = "AdminPolicy")]
    public async Task<ActionResult<GetCustomerByCpfDetailDto>> GetCustomerByCpf(string customerCpf) //Método para pegar o customer com determinado Cpf.
    {
        var getCustomerByCpfDetailQuery = new GetCustomerByCpfDetailQuery{ CustomerCpf = customerCpf }; //Criando a request necessária para o CQRS.

        var customerToReturn = await _mediator.Send(getCustomerByCpfDetailQuery); //Envia a "Mensagem" para a CQRS e recebe a Dto ja mapeada.

        if(!customerToReturn.IsSuccess) 
            return HandleRequestError(customerToReturn);

        return Ok(customerToReturn.Customer); //Retorna Ok e a Dto.
    }

    [HttpGet("cnpj/{customerCnpj}")]
    [Authorize(Policy = "AdminPolicy")]
    public async Task<ActionResult<GetCustomerByCnpjDetailDto>> GetCustomerByCnpj(string customerCnpj) 
    {
        var getCustomerByCnpjDetailQuery = new GetCustomerByCnpjDetailQuery{ CustomerCnpj = customerCnpj }; 

        var customerToReturn = await _mediator.Send(getCustomerByCnpjDetailQuery); 

        if(!customerToReturn.IsSuccess) 
            return HandleRequestError(customerToReturn);

        return Ok(customerToReturn.Customer); 
    }

    [HttpGet("with-addresses/{customerId}", Name = "GetCustomerWithAddressesById")]
    public async Task<ActionResult<GetCustomerWithAddressesByIdDetailDto>> GetCustomerWithAddressesById(int customerId) //Método para pegar um determinado customer com todos seus Addresses.
    {
        var getCustomerWithAddressesByIdDetailQuery = new GetCustomerWithAddressesByIdDetailQuery { CustomerId = customerId }; //Criando a request necessária para o CQRS.

        var customerToReturn = await _mediator.Send(getCustomerWithAddressesByIdDetailQuery); //Enviando a "Mensagem" para o CQRS.

        if(!customerToReturn.IsSuccess) 
            return HandleRequestError(customerToReturn);

        return Ok(customerToReturn.Customer); //Retornará Ok, e a DTO mapeada.
    }

    [HttpPost] //Com FluentValidation
    [AllowAnonymous]
    public async Task<ActionResult<CreateCustomerCommandResponse>> CreateCustomer(CreateCustomerCommand createCustomerCommand) //Método para criar o customer sem Addresses.
    {
        var createCustomerCommandResponse = await _mediator.Send(createCustomerCommand); //Envia a "Mensagem" para o CQRS e recebe a Dto ja mapeada.

        if(!createCustomerCommandResponse.IsSuccess) //Caso a verificação da CQRS seja false.
            return HandleRequestError(createCustomerCommandResponse);

        var customerForReturn = createCustomerCommandResponse.Customer;

        return Ok(customerForReturn);

        // return CreatedAtRoute //Retorna CreateAtRoute e a Dto.
        // (
        //     "GetCustomerById",
        //     new { customerId = customerForReturn.Id },
        //     customerForReturn
        // );
    }

    [HttpPost("with-addresses")] //Com FluentValidation
    public async Task<ActionResult<CreateCustomerWithAddressesCommandResponse>> CreateCustomerWithAddresses(CreateCustomerWithAddressesCommand createCustomerWithAddressesCommand) //Método para adicionar um costumer com addresses no banco de dados.
    {
        var createCustomerWithAddressesCommandResponse = await _mediator.Send(createCustomerWithAddressesCommand); //Enviando a "Mensagem" para o CQRS.

        if(!createCustomerWithAddressesCommandResponse.IsSuccess) //Caso a verificação da CQRS seja false.
            return HandleRequestError(createCustomerWithAddressesCommandResponse);

        var customerForReturn = createCustomerWithAddressesCommandResponse.Customer;

        return CreatedAtRoute //Retorna CreateAtRoute e a Dto.
        (
            "GetCustomerById",
            new { customerId = customerForReturn.Id },
            customerForReturn
        );
    }

    [HttpPut("{customerId}")] //Com FluentValidation
    [Authorize(Policy = "AdminPolicy")]
    public async Task<ActionResult<UpdateCustomerCommandResponse>> UpdateCustomer(int customerId, UpdateCustomerCommand updateCustomerCommand) //Método para atualizar um determinado customer.
    {
        if (customerId != updateCustomerCommand.Id) return BadRequest(); //Caso o Id do link seja diferente do arquivo escrito, retornará BadRequest.

        var updateCustomerCommandResponse = await _mediator.Send(updateCustomerCommand); //Manda a mensagem com o arquivo escrito para atualizar o customer escolhido.
        
        if(!updateCustomerCommandResponse.IsSuccess)
        {
            return HandleRequestError(updateCustomerCommandResponse);
        }

        return NoContent(); //Retorna NoContent.
    }

    [HttpPut("with-addresses/{customerId}")] //Com FluentValidation
    [Authorize(Policy = "AdminPolicy")]
    public async Task<ActionResult> UpdateCustomerWithAddresses(int customerId, UpdateCustomerWithAddressesCommand updateCustomerWithAddressesCommand) //Método para atualizar um determinado customer e seus addresses em relação ao seu id.
    {
        if(customerId != updateCustomerWithAddressesCommand.Id) return BadRequest(); //Caso o valor do link seja diferente do valor do arquivo escrito, retornará BadRequest.

        var updateCustomerWithAddressesCommandResponse = await _mediator.Send(updateCustomerWithAddressesCommand); //Envia a "Mensagem" para o CQRS.

        if(!updateCustomerWithAddressesCommandResponse.IsSuccess)
        {
            return HandleRequestError(updateCustomerWithAddressesCommandResponse);
        }

        return NoContent(); //Retorna NoContent.
    }

    [HttpDelete("{customerId}")]
    [Authorize(Policy = "AdminPolicy")]
    public async Task<ActionResult> DeleteCustomer(int customerId) //Método para deletar um determinado customer.
    {
        var deleteCustomerCommand = new DeleteCustomerCommand { CustomerId = customerId }; //Criando a request necessária para o CQRS. 

        var result = await _mediator.Send(deleteCustomerCommand); //Envia a "Mensagem" para o CQRS.

        if(!result.IsSuccess)
            return HandleRequestError(result);

        return NoContent(); //Retorna NoContent.
    }

    [HttpPatch("{customerId}")]
    [Authorize(Policy = "AdminPolicy")]
    public async Task<ActionResult> PartiallyUpdateCustomer([FromBody] JsonPatchDocument<PartiallyUpdateCustomerCommand> patchDocument, [FromRoute] int customerId)
    {
        var partiallyUpdateCustomerCommand = new PartiallyUpdateCustomerCommand{ Id = customerId, PatchDocument = patchDocument };
        var partiallyUpdateCustomerCommandResponse = await _mediator.Send(partiallyUpdateCustomerCommand);

        if(!partiallyUpdateCustomerCommandResponse.IsSuccess) //Caso a verificação da CQRS seja false.
        {
            return HandleRequestError(partiallyUpdateCustomerCommandResponse);
        }

        return NoContent();
    }
}