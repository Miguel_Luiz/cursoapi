using System.Text.Json;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Univali.Domain.Entities;
using Univali.Application.Features.QuestionAnswers.Queries.GetQuestionAnswerDetail;
using Univali.Application.Features.QuestionAnswers.Queries.GetQuestionAnswersDetail;
using Univali.Application.Features.Common;

namespace Univali.Api.Controllers;

[Route("api/questions/{questionId}/answers")]
[Authorize(Policy = "StudentPolicy")]
public class QuestionAnswersController : MainController
{
    private readonly IMediator _mediator;

    public QuestionAnswersController(IMediator mediator)
    {
        _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<GetQuestionAnswersDetailDto>>> GetQuestionAnswers(int questionId, int pageNumber = 1, int pageSize = 5)
    {
        if(pageSize > maxPageSize) pageSize = maxPageSize;

        var getQuestionAnswersDetailQuery = new GetQuestionAnswersDetailQuery {QuestionId = questionId, PageNumber = pageNumber, PageSize = pageSize};

        var answersResponse = await _mediator.Send(getQuestionAnswersDetailQuery);

        if(!answersResponse.IsSuccess)
        {
            return HandleRequestError(answersResponse);
        }

        Response.Headers.Add("X-Pagination", JsonSerializer.Serialize(answersResponse.paginationMetadata));

        return Ok(answersResponse.QuestionAnswersDetailDtos);
    }

    [HttpGet("{answerId}", Name = "GetQuestionAnswerById")]
    public async Task<ActionResult<GetQuestionAnswerDetailDto>> GetQuestionAnswerById(int questionId, int answerId)
    {
        var getQuestionAnswerDetailQuery = new GetQuestionAnswerDetailQuery {QuestionId = questionId, AnswerId = answerId};

        var answerResponse = await _mediator.Send(getQuestionAnswerDetailQuery);

        if(!answerResponse.IsSuccess)
        {
            return HandleRequestError(answerResponse);
        }

        return Ok(answerResponse.QuestionAnswer);
    }
}