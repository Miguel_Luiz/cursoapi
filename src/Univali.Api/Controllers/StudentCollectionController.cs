using System.Text.Json;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Univali.Application.Features.Students.Queries.GetStudentDetails;
using Univali.Application.Features.StudentsCollection.GetStudentsDetail;
using Univali.Persistence.Repositories;

namespace Univali.Api.Controllers;

[Route("api/student-collection")]
public class StudentCollectionController : MainController
{
    private readonly IMediator _mediator;
    
    public StudentCollectionController(IMediator mediator){
        _mediator = mediator;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<GetStudentDetailsDto>>> GetStudents(string searchQuery = "", int pageNumber = 1, int pageSize = 5)
    {
        if(pageSize > maxPageSize) pageSize = maxPageSize;

        var getStudentsCollectionDetailQuery = new GetStudentsCollectionDetailQuery{SearchQuery = searchQuery, PageNumber = pageNumber, PageSize = pageSize};

        var studentsResponse = await _mediator.Send(getStudentsCollectionDetailQuery);

        Response.Headers.Add("X-Pagination", JsonSerializer.Serialize(studentsResponse.PaginationMetadata));

        return Ok(studentsResponse.StudentsDetailDtos);
    }
}