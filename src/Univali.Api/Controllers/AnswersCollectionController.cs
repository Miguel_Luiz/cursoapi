using System.Text.Json;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Univali.Application.Features.Answers.Queries.GetAnswerDetail;
using Univali.Application.Features.AnswersCollection.GetAnswersDetail;

namespace Univali.Api.Controllers;

[Route("api/answers-collection")]
[Authorize]
public class AnswersCollectionController : MainController
{
    private readonly IMediator _mediator;
    
    public AnswersCollectionController(IMediator mediator){
        _mediator = mediator;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<GetAnswerDetailDto>>> GetAnswers(string searchQuery = "", int pageNumber = 1, int pageSize = 5)
    {
        if(pageSize > maxPageSize) pageSize = maxPageSize;

        var getAnswersCollectionDetailQuery = new GetAnswersCollectionDetailQuery{SearchQuery = searchQuery, PageNumber = pageNumber, PageSize = pageSize};

        var answersResponse = await _mediator.Send(getAnswersCollectionDetailQuery);

        Response.Headers.Add("X-Pagination", JsonSerializer.Serialize(answersResponse.PaginationMetadata));

        return Ok(answersResponse.AnswersDetailDtos);
    }
}