using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Univali.Application.Features.Questions.Commands.CreateQuestion;
using Univali.Application.Features.Questions.Commands.UpdateQuestion;
using Univali.Application.Features.LessonQuestions.Queries.GetLessonQuestionDetail;
using Univali.Application.Features.LessonQuestions.Queries.GetLessonQuestionsDetail;
using Univali.Application.Features.LessonQuestions.Queries.GetLessonQuestionWithAnswersDetail;
using System.Text.Json;


namespace Univali.Api.Controllers;

[Route("api/lessons/{lessonId}/questions")]
[Authorize]
public class LessonQuestionsController : MainController
{
    private readonly IMediator _mediator;

    public LessonQuestionsController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<GetLessonQuestionsDetailDto>>> GetLessonQuestions(
        int lessonId,
        int pageNumber = 1,
        int pageSize = 5
    )
    {
        if(pageSize > maxPageSize) pageSize = maxPageSize;

        var getLessonQuestionsDetailQuery = new GetLessonQuestionsDetailQuery {LessonId = lessonId, PageNumber = pageNumber, PageSize = pageSize};

        var questionsResponse = await _mediator.Send(getLessonQuestionsDetailQuery);

        if(!questionsResponse.IsSuccess)
        {
            return HandleRequestError(questionsResponse);
        }

        Response.Headers.Add("X-Pagination", JsonSerializer.Serialize(questionsResponse.paginationMetadata));

        return Ok(questionsResponse.QuestionsDetailDto);
    }

    [HttpGet("{questionId}", Name = "GetLessonQuestionById")]
    public async Task<ActionResult<GetLessonQuestionDetailDto>> GetLessonQuestionById(
        int lessonId, 
        int questionId
    )
    {
        var getLessonQuestionDetailQuery = new GetLessonQuestionDetailQuery {QuestionId = questionId, LessonId = lessonId};

        var questionResponse = await _mediator.Send(getLessonQuestionDetailQuery);

        if(!questionResponse.IsSuccess)
        {
            return HandleRequestError(questionResponse);
        }

        return Ok(questionResponse.Question);
    }

    [HttpGet("{questionId}/with-answers", Name = "GetLessonQuestionWithAnswersById")]
    public async Task<ActionResult<GetLessonQuestionWithAnswersDetailDto>> GetLessonQuestionWithAnswersById(
        int lessonId, 
        int questionId
    )
    {
        var getLessonQuestionWithAnswersDetailQuery = new GetLessonQuestionWithAnswersDetailQuery {QuestionId = questionId, LessonId = lessonId};

        var questionResponse = await _mediator.Send(getLessonQuestionWithAnswersDetailQuery);

        if(!questionResponse.IsSuccess)
        {
            return HandleRequestError(questionResponse);
        }

        return Ok(questionResponse.Question);
    }
}