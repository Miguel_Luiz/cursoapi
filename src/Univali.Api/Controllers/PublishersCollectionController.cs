using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using Univali.Domain.Entities;
using Univali.Persistence.Repositories;
using Univali.Application.Features.PublishersCollection.Queries;

using MediatR;

namespace Univali.Api.Controllers;

[ApiController]
[Route("api/publishers-collection")]
public class PublishersCollectionController : MainController
{
    private readonly IMediator _mediator;

    public PublishersCollectionController(
        IMediator mediator
    ) {
        _mediator = mediator;
    }

    [Route("legal-publisher")]
    [HttpGet]
    public async Task<ActionResult<IEnumerable<LegalPublisher>>> GetLegalPublisherCollection(
        string name = "",
        string cnpj = "",
        string searchQuery = "",
        int pageNumber = 1, 
        int pageSize = 10
    ) {
        if (pageSize < 1 || pageSize > maxPageSize) {
            pageSize = maxPageSize;
        }

        var getLegalPublisherCollectionResponse = await _mediator.Send(
            new GetLegalPublisherCollectionQuery(name, cnpj, searchQuery, pageNumber, pageSize)
        );

        Response.Headers.Add(
            "X-Pagination",
            JsonSerializer.Serialize(getLegalPublisherCollectionResponse.Pagination)
        );
        return Ok(getLegalPublisherCollectionResponse.Publishers);
    }

    [Route("natural-publisher")]
    [HttpGet]
    public async Task<ActionResult<IEnumerable<NaturalPublisher>>> GetNaturalPublisherCollection(
        string name = "",
        string cpf = "",
        string searchQuery = "",
        int pageNumber = 1, 
        int pageSize = 10
    ) {
        if (pageSize < 1 || pageSize > maxPageSize) {
            pageSize = maxPageSize;
        }

        var getNaturalPublisherCollectionResponse = await _mediator.Send(
            new GetNaturalPublisherCollectionQuery(name, cpf, searchQuery, pageNumber, pageSize)
        );

        Response.Headers.Add(
            "X-Pagination",
            JsonSerializer.Serialize(getNaturalPublisherCollectionResponse.Pagination)
        );
        return Ok(getNaturalPublisherCollectionResponse.Publishers);
    }

}