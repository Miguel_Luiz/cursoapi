using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Univali.Application.Features.Modules.Commands.CreateModule;
using Univali.Application.Features.Modules.Commands.DeleteModule;
using Univali.Application.Features.Modules.Commands.UpdateModule;
using Univali.Application.Features.Modules.Queries.GetModuleDetail;
using Univali.Application.Features.Modules.Queries.GetModulesDetail;
using Univali.Application.Features.Modules.Queries.GetModuleWithLessonsDetail;


namespace Univali.Api.Controllers;

[Route("api/courses/{courseId}/modules")]
[Authorize(Policy = "AuthorPolicy")]
public class ModulesController : MainController
{
    private readonly IMediator _mediator;

    public ModulesController(IMediator mediator, IMapper mapper)
    {
        _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
    }

    [HttpPost]
    public async Task<ActionResult<CreateModuleCommandDto>> CreateModule(
        int courseId,
        CreateModuleCommand createModuleCommand
    )
    {
    // pablo1107 validação
        if(courseId != createModuleCommand.CourseId) { 
            return BadRequest("ID informado no corpo da requisição difere da rota"); 
        }

        var createModuleCommandResponse = await _mediator.Send(createModuleCommand);

        if (!createModuleCommandResponse.IsSuccess)
        {
            return HandleRequestError(createModuleCommandResponse);
        }

        return CreatedAtRoute
        (
            "GetModuleById",
            new {courseId, moduleId = createModuleCommandResponse.Module.ModuleId },
            createModuleCommandResponse.Module
        );

    }

    [HttpGet("{moduleId}", Name = "GetModuleById")]
    [Authorize(Policy = "StudentPolicy")]
    public async Task<ActionResult<GetModuleDetailQueryResponse>> GetModuleById(int courseId, int moduleId)
    {
        var getModuleDetailQuery = new GetModuleDetailQuery { CourseId = courseId, ModuleId = moduleId };

        var getModuleDetailQueryResponse = await _mediator.Send(getModuleDetailQuery);

        if (!getModuleDetailQueryResponse.IsSuccess)
        {
            return HandleRequestError(getModuleDetailQueryResponse);
        }

        return Ok(getModuleDetailQueryResponse.Module);
    }

    [HttpGet("with-lessons/{moduleId}")]
    [Authorize(Policy = "StudentPolicy")]
    public async Task<ActionResult<GetModuleWithLessonsDetailQueryResponse>> GetModuleWithLessonsById(int courseId, int moduleId)
    {
        var getModuleWithLessonsDetailQuery = new GetModuleWithLessonsDetailQuery { CourseId = courseId, ModuleId = moduleId };

        var getModuleWithLessonsDetailQueryResponse = await _mediator.Send(getModuleWithLessonsDetailQuery);

        if (!getModuleWithLessonsDetailQueryResponse.IsSuccess)
        {
            return HandleRequestError(getModuleWithLessonsDetailQueryResponse);
        }

        return Ok(getModuleWithLessonsDetailQueryResponse.Module);
    }

    [HttpGet]
    [Authorize(Policy = "StudentPolicy")]
    public async Task<ActionResult<GetModulesDetailQueryResponse>> GetModules(int courseId) //acho que esse não vai funcionar
    {//eu tirei o IEnumerable pq a List ta dentro do GetOneModuleDetailDto
        var getModulesDetailQuery = new GetModulesDetailQuery { CourseId = courseId};

        var getModulesDetailQueryResponse = await _mediator.Send(getModulesDetailQuery);
        
        if (!getModulesDetailQueryResponse.IsSuccess)
        {
            return HandleRequestError(getModulesDetailQueryResponse);
        }       

        return Ok(getModulesDetailQueryResponse.Modules);
    }

    [HttpDelete("{moduleId}")]
    public async Task<ActionResult> DeleteModule(int courseId, int moduleId)
    {//Miguel novo
        var deleteModuleCommand = new DeleteModuleCommand{CourseId = courseId, ModuleId = moduleId};

        var deleteModuleCommandResponse = await _mediator.Send(deleteModuleCommand);

        if(!deleteModuleCommandResponse.IsSuccess)
        {
            return HandleRequestError(deleteModuleCommandResponse);
        }

        return NoContent();
    }

    [HttpPut("{moduleId}")]
    public async Task<ActionResult> UpdateModule(int courseId, int moduleId, UpdateModuleCommand updateModuleCommand)
    {
    // pablo1107 validação
        if(moduleId != updateModuleCommand.ModuleId
            || courseId != updateModuleCommand.CourseId) { 
            return BadRequest("ID informado no corpo da requisição difere da rota"); 
        }

        updateModuleCommand.CourseId = courseId;

        var updateModuleCommandResponse = await _mediator.Send(updateModuleCommand);

        if(!updateModuleCommandResponse.IsSuccess)
        {
            return HandleRequestError(updateModuleCommandResponse);
        }

        return NoContent();
    }

}