using System.Text.Json;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Univali.Application.Features.Students.Commands.CreateStudent;
using Univali.Application.Features.Students.Commands.DeleteStudent;
using Univali.Application.Features.Students.Commands.UpdateStudent;
using Univali.Application.Features.Students.Queries.GetStudentDetails;
using Univali.Application.Features.Students.Queries.GetStudentsDetails;
using Univali.Application.Models;

namespace Univali.Api.Controllers;

[Route("api/students")]
[Authorize(Policy = "StudentPolicy")]
public class StudentsController : MainController
{
    private readonly IMediator _mediator;

    public StudentsController(IMediator mediator)
    {
        _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
    }

    ///////////////////////////////////////////////////////////////////////////
    // Read
    ///////////////////////////////////////////////////////////////////////////
    [HttpGet]
    public async Task<ActionResult<IEnumerable<GetStudentDetailsDto>>> GetStudents(int pageNumber = 1, int pageSize = 5)
    {
        if(pageSize > maxPageSize) pageSize = maxPageSize;

        var getStudentDetailQuery = new GetStudentsDetailsQuery {PageNumber = pageNumber, PageSize = pageSize};

        var studentResponse = await _mediator.Send(getStudentDetailQuery);

        Response.Headers.Add("X-Pagination", JsonSerializer.Serialize(studentResponse.paginationMetadata));

        return Ok(studentResponse.StudentsDetailsDtos);
    }

    [HttpGet("{studentId}", Name = "GetStudentById")]
    public async Task<ActionResult<GetStudentDetailsDto>> GetStudentByIdAsync(int studentId)
    {
        var getStudentDetailsQuery = new GetStudentDetailsQuery { Id = studentId };

        var studentResponse = await _mediator.Send(getStudentDetailsQuery);

        if (!studentResponse.IsSuccess) return HandleRequestError(studentResponse);

        return Ok(studentResponse.Student);
    }

    // [HttpGet("collection")]
    // //***
    // public async Task<ActionResult<GetStudentCollectionDetailsResponse>> GetStudentCollectionAsync(string? name = "", string? cpf = "", string? searchQuery = "")
    // {
    //     var getStudentCollectionDetailsQuery = new GetStudentCollectionDetailsQuery(name!, cpf!, searchQuery!);

    //     GetStudentCollectionDetailsResponse getStudentCollectionDetailsResponse = await _mediator.Send(getStudentCollectionDetailsQuery);

    //     if (!getStudentCollectionDetailsResponse.IsSuccess) return CheckStatusCode(getStudentCollectionDetailsResponse);

    //     return Ok(getStudentCollectionDetailsResponse.StudentCollection);
    // }

    ///////////////////////////////////////////////////////////////////////////
    // Create
    ///////////////////////////////////////////////////////////////////////////
    [HttpPost]
    [Authorize(Policy = "AdminPolicy")]
    public async Task<ActionResult<StudentDto>> CreateStudent(
        CreateStudentCommand createStudentCommand
        )
    {
        var createStudentCommandResponse = await _mediator.Send(createStudentCommand);

        if (!createStudentCommandResponse.IsSuccess)
        {
            return HandleRequestError(createStudentCommandResponse);
        }
        return CreatedAtRoute
        (
            "GetStudentById",
            new { studentId = createStudentCommandResponse.Student.StudentId },
            createStudentCommandResponse.Student
        );
    }


    ///////////////////////////////////////////////////////////////////////////
    // Update
    ///////////////////////////////////////////////////////////////////////////
    [HttpPut("{studentId}")]
    [Authorize(Policy = "AdminPolicy")]
    public async Task<ActionResult> UpdateStudent(int studentId,
        UpdateStudentCommand updateStudentCommand)
    {
        if (updateStudentCommand.Id != studentId) return BadRequest();

        updateStudentCommand.Id = studentId;

        var updateStudentCommandResponse = await _mediator.Send(updateStudentCommand);

        if (!updateStudentCommandResponse.IsSuccess)
        {
            return HandleRequestError(updateStudentCommandResponse);
        }

        return NoContent();
    }


    ///////////////////////////////////////////////////////////////////////////
    // Delete
    ///////////////////////////////////////////////////////////////////////////
    [HttpDelete("{studentId}")]
    [Authorize(Policy = "AdminPolicy")]

    public async Task<ActionResult> DeleteStudent(int studentId)
    {
        var deleteStudentCommand = new DeleteStudentCommand { Id = studentId };

        var studentResponse = await _mediator.Send(deleteStudentCommand);

        if (!studentResponse.IsSuccess)
        {
            return HandleRequestError(studentResponse);
        }
        
        return NoContent();
    }

}