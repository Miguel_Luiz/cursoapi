using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Univali.Application.Features.Questions.Commands.DeleteQuestion;
using Univali.Application.Features.Questions.Commands.CreateQuestion;
using Univali.Application.Features.Questions.Commands.UpdateQuestion;
using Univali.Application.Features.Questions.Queries.GetQuestionDetail;
using Univali.Application.Features.Questions.Queries.GetQuestionsDetail;
using Univali.Application.Features.Questions.Queries.GetQuestionWithAnswersDetail;
using Microsoft.AspNetCore.JsonPatch;
using Univali.Application.Models;
using System.Text.Json;

namespace Univali.Api.Controllers;

[Route("api/students/{studentId}/questions")]
[Authorize(Policy = "StudentPolicy")]
public class QuestionsController : MainController
{
    private readonly IMediator _mediator;

    public QuestionsController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<GetQuestionsDetailDto>>> GetQuestions(
        int studentId,
        int pageNumber = 1,
        int pageSize = 5
    )
    {
        if(pageSize > maxPageSize) pageSize = maxPageSize;

        var questionResponse = await _mediator.Send(
            new GetQuestionsDetailQuery { 
                StudentId = studentId,
                PageNumber = pageNumber,
                PageSize = pageSize  
            }
        );

        if(!questionResponse.IsSuccess) 
        {
            return HandleRequestError(questionResponse);
        }

        Response.Headers.Add("X-Pagination", JsonSerializer.Serialize(questionResponse.PaginationMetadata));

        return Ok(questionResponse.QuestionsDetailDto);
    }

    [HttpGet("{questionId}", Name = "GetQuestionById")]
    public async Task<ActionResult<GetQuestionDetailDto>> GetQuestionById(
        int studentId, 
        int questionId
    )
    {
        var questionResponse = await _mediator.Send(
            new GetQuestionDetailQuery { 
                QuestionId = questionId, 
                StudentId = studentId 
            }
        );

        if(!questionResponse.IsSuccess)
        {
            return HandleRequestError(questionResponse);
        } 

        return Ok(questionResponse.Question);
    }

    [HttpGet("{questionId}/with-answers", Name = "GetQuestionWithAnswersById")]
    
    public async Task<ActionResult<GetQuestionWithAnswersDetailDto>> GetQuestionWithAnswersById(
        int studentId,
        int questionId
    )
    {
        var questionResponse = await _mediator.Send(
            new GetQuestionWithAnswersDetailQuery { 
                QuestionId = questionId, 
                StudentId = studentId 
            }
        );

        if(!questionResponse.IsSuccess)
        {
            return HandleRequestError(questionResponse);
        } 

        return Ok(questionResponse.Question);
    }

    [HttpPost]
    public async Task<ActionResult<CreateQuestionDto>> CreateQuestion(
        int studentId,
        CreateQuestionCommand createQuestionCommand
    )
    {
        createQuestionCommand.StudentId = studentId;
        createQuestionCommand.CreationDate = DateTime.UtcNow;

        var createQuestionCommandResponse = await _mediator.Send(createQuestionCommand);

        if(!createQuestionCommandResponse.IsSuccess)
        {
            return HandleRequestError(createQuestionCommandResponse);
        }

        return CreatedAtRoute
        (
            "GetQuestionById",
            new {studentId, createQuestionCommandResponse.Question.QuestionId},
            createQuestionCommandResponse.Question
        );
    }

    [HttpPut("{questionId}")]
    public async Task<ActionResult<UpdateQuestionCommandResponse>> UpdateQuestion(
        int studentId,
        int questionId,
        UpdateQuestionCommand updateQuestionCommand
    )
    {
        if (updateQuestionCommand.QuestionId != questionId) return BadRequest();

        updateQuestionCommand.StudentId = studentId;
        updateQuestionCommand.LastUpdate = DateTime.UtcNow;

        var updateQuestionCommandResponse = await _mediator.Send(updateQuestionCommand);

        if(!updateQuestionCommandResponse.IsSuccess)
        {
            return HandleRequestError(updateQuestionCommandResponse);
        }

        return NoContent();
    }

    [HttpDelete("{questionId}")]
    public async Task<ActionResult> DeleteQuestion(
        int studentId,
        int questionId
    )
    {

        var deleteQuestionCommand = new DeleteQuestionCommand{ StudentId = studentId, QuestionId = questionId};

        var questionResponse = await _mediator.Send(deleteQuestionCommand);

        if(!questionResponse.IsSuccess)
        {
            return HandleRequestError(questionResponse);
        }

        return NoContent();
    }
}