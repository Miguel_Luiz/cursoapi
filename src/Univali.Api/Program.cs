// dotnet add package Microsoft.AspNetCore.Authentication
// dotnet add package Microsoft.AspNetCore.Authentication.JwtBearer
// dotnet add package AutoMapper.Extensions.Microsoft.DependencyInjection --version 12.0.1
// dotnet add package Microsoft.AspNetCore.JsonPatch --version 7.0.5
// dotnet add package Npgsql.EntityFrameworkCore.PostgreSQL --version 7.0.4
// dotnet add package Microsoft.EntityFrameworkCore.Tools --version 7.0.4
// dotnet add package Microsoft.Extensions.Logging --version 7.0.0
// dotnet ef --version
// dotnet tool install --global dotnet-ef
// dotnet ef migrations add InitialMigration
// dotnet ef database update
// dotnet add package MediatR --version 12.0.1
// dotnet ef --startup-project ../Univali.Api/ migrations add AddPublisherContext -o Migrations/Publisher --context PublisherContext
// dotnet ef --startup-project ../Univali.Api/ migrations add AddCustomerContext -o Migrations/Customer --context CustomerContext
// dotnet ef database update --context PublisherContext
// dotnet ef database update --context CustomerContext
//  dotnet add package FluentValidation.DependencyInjectionExtensions --version 11.5.2
//  dotnet add package FluentValidation --version 11.5.2



using System.Reflection;
using System.Text;
using System.Text.Json.Serialization;
using Microsoft.IdentityModel.Tokens;
using Univali.Api.Extensions;
using Univali.Application.Extensions;
using Univali.Persistence.Extensions;
using Univali.Api.Configuration;
using Univali.Persistence.DbContexts;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc;

var builder = WebApplication.CreateBuilder(args);

builder.WebHost.ConfigureKestrel(options =>
{
    options.ListenLocalhost(5000);
});

builder.Services.AddApiServices();


//configura a injecao de dependencia
builder.Services.AddControllers()
.AddJsonOptions(opt =>
{
    opt.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
    opt.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
});


//adiciona servicos Scoped do fluent validation (ver StartupHelperExtensions.cs)
builder.Services.AddFluentValidationServices();

builder.Services.AddControllers()
.AddJsonOptions(options => options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);

builder.Services.AddAuthentication("Bearer").AddJwtBearer(options =>
{
    options.TokenValidationParameters = new()
    {
        // Declaramos o que deverá ser validado
        // O tempo de expiração do token é validado automaticamente.
        // Obriga a validação do emissor
        ValidateIssuer = true,
        // Obriga a validação da audiência
        ValidateAudience = true,
        // Obriga a validação da chave de assinatura`
        ValidateIssuerSigningKey = true,

        // Agora declaramos os valores das propriedades que serão validadas
        // Apenas tokens  gerados por esta api serão considerados válidos.
        ValidIssuer = builder.Configuration["Authentication:Issuer"],
        // Apenas tokens desta audiência serão considerados válidos.
        ValidAudience = builder.Configuration["Authentication:Audience"],
        // Apenas tokens com essa assinatura serão considerados válidos.
        IssuerSigningKey = new SymmetricSecurityKey(
            Encoding.UTF8.GetBytes(builder.Configuration["Authentication:SecretKey"]!))
    };
});

builder.Services.AddApplicationServices();
builder.Services.AddPersistenceServices(builder.Configuration);

builder.Services.AddControllers(options =>
{
    options.InputFormatters.Insert(0, MyJPIF.GetJsonPatchInputFormatter());
})    
    .ConfigureApiBehaviorOptions(setupAction =>
       {
           setupAction.SuppressModelStateInvalidFilter = true;
           setupAction.InvalidModelStateResponseFactory = context =>
           {
               // Cria a fábrica de um objeto de detalhes de problema de validação
               var problemDetailsFactory = context.HttpContext.RequestServices
                   .GetRequiredService<ProblemDetailsFactory>();


               // Cria um objeto de detalhes de problema de validação
               var validationProblemDetails = problemDetailsFactory
                   .CreateValidationProblemDetails(
                       context.HttpContext,
                       context.ModelState);


               // Adiciona informações adicionais não adicionadas por padrão
               validationProblemDetails.Detail =
                   "See the errors field for details.";
               validationProblemDetails.Instance =
                   context.HttpContext.Request.Path;


               // Relata respostas do estado de modelo inválido como problemas de validação
               validationProblemDetails.Type =
                   "https://courseunivali.com/modelvalidationproblem";
               validationProblemDetails.Status =
                   StatusCodes.Status422UnprocessableEntity;
               validationProblemDetails.Title =
                   "One or more validation errors occurred.";


               return new UnprocessableEntityObjectResult(
                   validationProblemDetails)
               {
                   ContentTypes = { "application/problem+json" }
               };
           };
       });

// É embutido no ASP.NET Core, expõe informações da API, tipo os endpoints e como interagir com eles.
// É usado internamente pelo Swashbuckle para gerar a especificação OpenAPI
builder.Services.AddEndpointsApiExplorer();
// Registra os serviços que são usados para efetivamente gerar a especificação
builder.Services.AddSwaggerGen(setupAction =>
{
    /*
    Retorna o nome do assembly atual como uma string por reflection

    "Assembly.GetExecutingAssembly()" retorna uma referência para o assembly
    que contém o código que está sendo executado atualmente.

    "GetName()" é chamado na referência do assembly para obter um objeto do tipo AssemblyName,
    que contém informações sobre o assembly, como seu nome, versão, cultura e chave pública.

    "Name" é lido a partir do objeto AssemblyName para obter o nome do assembly como uma string.
    */
    var xmlCommentsFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";

    // "Path.Combine" cria um formato de caminho válido com os parâmetros
    // "AppContext.BaseDirectory" é uma propriedade que retorna o caminho base do diretório em que a aplicação está sendo executada
    var xmlCommentsFullPath = Path.Combine(AppContext.BaseDirectory, xmlCommentsFile);


    // Inclui os comentários XML na documentação do Swagger.
    setupAction.IncludeXmlComments(xmlCommentsFullPath);
});

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

await app.ResetDatabaseAsync();

app.Run();
