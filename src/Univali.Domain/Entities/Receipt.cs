namespace Univali.Domain.Entities;

public class Receipt
{
    public int ReceiptId { get; set; }
    public string CustomerName { get; set; } = string.Empty;
    public string CustomerIdentifier { get; set; } = string.Empty;
    public PaymentType PaymentType { get; set; } // em implementacao completa, pode ser string com Name da classe de pagament especifica
    public DateTime Timestamp { get; set; }
    public decimal TotalPrice { get; set; }
    
    public Order? Order { get; set; }
    public int OrderId { get; set; }
}