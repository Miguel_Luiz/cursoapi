namespace Univali.Domain.Entities;

public class LegalPublisher : Publisher
{
    public string CNPJ {get;set;} = string.Empty;
}