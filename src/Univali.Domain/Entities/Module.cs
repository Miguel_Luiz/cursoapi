namespace Univali.Domain.Entities;

public class Module{
    public int ModuleId { get; set;}
    public string Title {get;set;} = string.Empty;
    public string Description {get;set;} = string.Empty;
    public int Number {get;set;}
    public TimeSpan TotalDuration {get;set;}
    public int LessonAmount {get;set;}
    
    public Course? Course { get; set;}
    public int CourseId { get; set;}

    public List<Lesson> Lessons { get; set;} = new();



    public int AddLessonOnModule()
    {
        return this.LessonAmount + 1;
    }
    
    public int RemoveLessonFromModule()
    {
        return this.LessonAmount - 1;
    }
    
    public TimeSpan CreateModule(List<Lesson> lessons)
    {
        var totalModuleDuration = new TimeSpan();
        foreach(var lesson in lessons)
        {
            totalModuleDuration = totalModuleDuration.Add(lesson.Duration);
        }

        return totalModuleDuration;
    }

    public TimeSpan UpdateModuleOnLessonCreating(TimeSpan lessonDuration)
    {
        var resultingModelDuration = this.TotalDuration.Add(lessonDuration);

        return resultingModelDuration;
    }

    public TimeSpan UpdateModuleOnLessonDeleting(TimeSpan lessonDuration)
    {
        var resultingModelDuration = this.TotalDuration.Subtract(lessonDuration);

        return resultingModelDuration;
    }

    public TimeSpan UpdateModelOnLessonUpdating(TimeSpan oldLessonDuration, TimeSpan newLessonDuration)
    {
        var resultingModelDuration = this.TotalDuration.Subtract(oldLessonDuration);

        resultingModelDuration = resultingModelDuration.Add(newLessonDuration);

        // this.Course.UpdateCourseOnLessonUpdating(oldLesson, newLesson);

        return resultingModelDuration;
    }
}