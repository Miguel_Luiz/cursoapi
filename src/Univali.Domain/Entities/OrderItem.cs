namespace Univali.Domain.Entities;

public class OrderItem
{
    public int OrderItemId { get; set; }
    public string Title { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public decimal Price { get; set; }

    public Order? Order { get; set; }
    public int OrderId { get; set; }
}