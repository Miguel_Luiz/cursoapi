namespace Univali.Domain.Entities;

// abstrair detalhes do pagamento
    // implementacao mais robusta teria uma classe para cada tipo, herdando abstract Payment, 
    // com informacoes especificas do tipo de pagamento
public enum PaymentType { CreditCard, DebitCard, BoletoBancario, PIX }

public class Payment
{
    public int PaymentId { get; set; }
    public PaymentType Type { get; set; }
    public string Description { get; set; } = string.Empty;    
    public Order? Order { get; set; }
    public int OrderId { get; set; }
}
