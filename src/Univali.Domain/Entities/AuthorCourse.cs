namespace Univali.Domain.Entities;

public class AuthorCourse
{
    public int AuthorId { get; set; }
    public int CourseId { get; set; }
    public DateTime CreatedOn { get; set; }
}
