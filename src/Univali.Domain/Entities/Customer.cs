using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Univali.Domain.Entities;

public class Customer
{
    public int Id {get; set;}
    public string Name {get; set;} = string.Empty;

    public int Type { get; }

    public ICollection<Address> Addresses {get; set;} = new List<Address>();

    public ICollection<Order> Orders { get; set; } = new List<Order>();
}