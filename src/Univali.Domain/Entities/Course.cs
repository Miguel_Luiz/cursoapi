namespace Univali.Domain.Entities;

public class Course
{
    public int CourseId {get; set;}
    public string Title {get; set;} = string.Empty;
    public string Description {get; set;} = string.Empty;
    public decimal Price {get; set;}
    public string Category {get; set;} = string.Empty;
    public TimeSpan TotalDuration {get;set;}
    public int ModuleAmount {get;set;}

    public List<Author> Authors {get; set;} = new();

    public List<Module> Modules {get; set;} = new();

    public List<Student> Students {get; set;} = new();
    
    public Publisher? Publisher {get; set;}
    public int PublisherId {get; set;}

    public int AddModuleOnCourse()
    {
        return this.ModuleAmount + 1;
    }
    
    public int RemoveModulefromCourse()
    {
        return this.ModuleAmount - 1;
    }

    public TimeSpan CreateCourse(List<Module> modules)
    {
        var totalCourseDuration = new TimeSpan();
        foreach(var module in modules)
        {
            totalCourseDuration = totalCourseDuration.Add(module.TotalDuration);
        }

        return totalCourseDuration;
    }

    public TimeSpan UpdateCourseOnLessonCreating(TimeSpan lessonDuration)
    {
        var resultingCourseDuration = this.TotalDuration.Add(lessonDuration);

        return resultingCourseDuration;
    }

    public TimeSpan UpdateCourseOnLessonDeleting(TimeSpan lessonDuration)
    {
        var resultingCourseDuration = this.TotalDuration.Subtract(lessonDuration);

        return resultingCourseDuration;
    }

    public TimeSpan UpdateCourseOnLessonUpdating(TimeSpan oldLessonDuration, TimeSpan newLessonDuration)
    {
        var resultingCourseDuration = this.TotalDuration.Subtract(oldLessonDuration);

        resultingCourseDuration = resultingCourseDuration.Add(newLessonDuration);

        return resultingCourseDuration;
    }

    public TimeSpan UpdateCourseOnModuleDeleting(TimeSpan moduleDuration)
    {
        var resultingCourseDuration = this.TotalDuration.Subtract(moduleDuration);

        return resultingCourseDuration;
    }

    public TimeSpan UpdateCourseOnModuleCreating(TimeSpan moduleDuration)
    {
        var resultingCourseDuration = this.TotalDuration.Add(moduleDuration);

        return resultingCourseDuration;
    }
}