namespace Univali.Domain.Entities;

public class CourseStudent
{
    public int CourseId { get; set; }
    public int StudentId { get; set; }
    public DateTime CreatedOn { get; set; }
}
