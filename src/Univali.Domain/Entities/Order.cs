namespace Univali.Domain.Entities;

public class Order
{
    public int OrderId { get; set; }

    public Customer? Customer { get; set; }
    public int CustomerId { get; set; }

    public ICollection<OrderItem> Items { get; set; } = new List<OrderItem>();

    public Payment? Payment { get; set; }

    public Receipt? Receipt { get; set; }



    public bool GenerateReceipt()
    {
        const int NATURAL_CUSTOMER = 2;
        const int LEGAL_CUSTOMER = 3;

        Receipt newReceipt = new();

        if(Customer == null) return false;

        newReceipt.CustomerName = Customer.Name;

        if(Customer.Type == NATURAL_CUSTOMER)
        {
            NaturalCustomer naturalCustomer = (NaturalCustomer) Customer;
            newReceipt.CustomerIdentifier = naturalCustomer.CPF;
        }
        else if(Customer.Type == LEGAL_CUSTOMER)
        {
            LegalCustomer legalCustomer = (LegalCustomer) Customer;
            newReceipt.CustomerIdentifier = legalCustomer.CNPJ;
        }
        else
        {
            return false;
        } 

        if(Payment == null) return false;

        newReceipt.PaymentType = Payment.Type;

        if(Items.Count == 0) return false;

        decimal totalPrice = Items.Select(item => item.Price).Sum();
        newReceipt.TotalPrice = totalPrice;

        newReceipt.Timestamp = DateTime.UtcNow;

        Receipt = newReceipt;
        return true;
    }

    public bool UpdateReceipt()
    {
        const int NATURAL_CUSTOMER = 2;
        const int LEGAL_CUSTOMER = 3;

        if(Receipt == null) return false;

        Receipt updatedReceipt = new(); // dados novos passam por entidade temporaria, permite rollback se etapa do update falhar

        if(Customer == null) return false;

        updatedReceipt.CustomerName = Customer.Name;

        if(Customer.Type == NATURAL_CUSTOMER)
        {
            NaturalCustomer naturalCustomer = (NaturalCustomer) Customer;
            updatedReceipt.CustomerIdentifier = naturalCustomer.CPF;
        }
        else if(Customer.Type == LEGAL_CUSTOMER)
        {
            LegalCustomer legalCustomer = (LegalCustomer) Customer;
            updatedReceipt.CustomerIdentifier = legalCustomer.CNPJ;
        }
        else
        {
            return false;
        } 

        if(Payment == null) return false;

        updatedReceipt.PaymentType = Payment.Type;
        
        decimal totalPrice = Items.Select(item => item.Price).Sum();
        updatedReceipt.TotalPrice = totalPrice;

        updatedReceipt.Timestamp = DateTime.UtcNow;



        Receipt.CustomerName = updatedReceipt.CustomerName;
        Receipt.CustomerIdentifier = updatedReceipt.CustomerIdentifier;
        Receipt.PaymentType = updatedReceipt.PaymentType;
        Receipt.TotalPrice = updatedReceipt.TotalPrice;
        Receipt.Timestamp = updatedReceipt.Timestamp;

        return true;
    }
}