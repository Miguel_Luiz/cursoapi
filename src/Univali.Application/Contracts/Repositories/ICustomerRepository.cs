using Univali.Domain.Entities;
using Univali.Application.Features.Addresses.Commands.UpdateAddress;
using Univali.Application.Features.Common;
using Univali.Application.Features.Customers.Commands.PartiallyUpdateCustomer;
//using Univali.Application.Features.Customers.Commands.PartiallyUpdateCustomer;


using Univali.Application.Features.Customers.Commands.UpdateCustomer;
using Univali.Application.Features.Customers.Commands.UpdateCustomerWithAddresses;
using Univali.Application.Models;


namespace Univali.Application.Contracts.Repositories;

public interface ICustomerRepository
{
    // CUSTOMER GET
    Task<(IEnumerable<Customer>, PaginationMetadata)> GetCustomersAsync(string? category, 
    string? searchQuery, int pageNumber, int pageSize);
    Task<IEnumerable<Customer>> GetCustomersAsync();
    Task<IEnumerable<Customer>> GetCustomersWithAddressesAsync();
    Task<Customer?> GetCustomerByIdAsync(int customerId);
    Task<Customer?> GetCustomerWithAddressesByIdAsync(int customerId);
    Task<Customer?> GetCustomerByCpfAsync(string customerCpf);
    Task<Customer?> GetCustomerByCnpjAsync(string customerCnpj);
    // CUSTOMER POST
    void AddCustomer(Customer customer);
    // CUSTOMER PUT
    void UpdateCustomer(Customer customer, UpdateCustomerCommand newCustomer);
    void UpdateCustomer(Customer customer, UpdateCustomerWithAddressesCommand newCustomer);
    void UpdateCustomer(Customer customer, NaturalCustomer newCustomer);
    void UpdateCustomer(Customer customer, LegalCustomer newCustomer);
    // CUSTOMER DELETE
    void DeleteCustomer(Customer customer);
    // CUSTOMER PATCH
    void PatchCustomer(Customer customer, CustomerForPatchDto newCustomer);
    void PatchCustomer(Customer customer, PartiallyUpdateCustomerCommand newCustomer);


    // ADDRESS GET
    Task<(IEnumerable<Address>?,PaginationMetadata)> GetAddressesAsync(int customerId, string? category, 
    string? searchQuery, int pageNumber, int pageSize);
    Task<IEnumerable<Address>?> GetAddressesAsync(int customerId);
    Task<Address?> GetAddressByIdAsync(int customerId, int addressId);
    // ADDRESS POST
    void AddAddress(Customer customer, Address address);
    // ADDRESS PUT
    void UpdateAddress(Address address, UpdateAddressCommand newAddress);
    // ADDRESS DELETE
    void DeleteAddress(Address address);


    // CONTEXT COMMIT
    Task<bool> SaveChangesAsync(); 
}
