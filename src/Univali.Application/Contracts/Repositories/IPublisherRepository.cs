using Univali.Domain.Entities;
using Univali.Application.Features.Authors.Commands.UpdateAuthor;
using Univali.Application.Features.Common;
using Univali.Application.Features.Publishers.Commands.UpdatePublisher;
using Univali.Application.Features.Courses.Commands;
using Univali.Application.Features.Lessons.Commands.UpdateLesson;

namespace Univali.Application.Contracts.Repositories;

public interface IPublisherRepository
{    
    // PUBLISHER COLLECTION GET // NOVO
    Task<IEnumerable<Publisher>?> GetAllPublishersWithCoursesAsync();
    Task<(IEnumerable<NaturalPublisher>, PaginationMetadata)> GetNaturalPublisherCollectionAsync(
        string? name, string? cpf, string? searchQuery, int pageNumber, int pageSize);
    Task<(IEnumerable<LegalPublisher>, PaginationMetadata)> GetLegalPublisherCollectionAsync(
        string? name, string? cnpj, string? searchQuery, int pageNumber, int pageSize);
    // PUBLISHER GET
    Task<Publisher?> GetPublisherByIdAsync(int publisherId);
    Task<Publisher?> GetPublisherWithCoursesByIdAsync(int publisherId);
    // PUBLISHER POST
    void AddPublisher(Publisher publisher);
    // PUBLISHER PUT
    void UpdatePublisher(Publisher publisher, UpdatePublisherCommand newPublisher);
    // PUBLISHER DELETE
    void DeletePublisher(Publisher publisher);


    // AUTHOR COLLECTION GET // NOVO
    Task<IEnumerable<Author>?> GetAllAuthorsWithCoursesAsync();
    Task<(IEnumerable<Author>, PaginationMetadata)> GetAuthorsCollectionAsync(string? searchQuery, int pageNumber, int pageSize);
    // AUTHOR GET
    Task<Author?> GetAuthorByIdAsync(int authorId);
    Task<Author?> GetAuthorWithCoursesByIdAsync(int authorId);
    Task<Author?> GetAuthorWithAnswersByIdAsync(int authorId);
    // AUTHOR POST
    void AddAuthor(Author author);
    // AUTHOR PUT
    void UpdateAuthor(Author author, UpdateAuthorCommand newAuthor);
    // AUTHOR DELETE
    void DeleteAuthor(Author author);
    // AUTHOR UTILS
    Task<bool> AuthorExistsAsync(int authorId);


    // COURSE COLLECTION GET // NOVO
    Task<IEnumerable<Course>> GetCoursesAsync();
    Task<IEnumerable<Course>?> GetAllCoursesWithAuthorsAsync(int publisherId);
    Task<(IEnumerable<Course>, PaginationMetadata)> GetCoursesCollectionAsync(string? category, string? searchQuery, int pageNumber, int pageSize);
    // COURSE GET
    Task<Course?> GetCourseByIdAsync(int courseId); // NOVO
    Task<Course?> GetCourseByIdAsync(int publisherId, int courseId);
    Task<Course?> GetCourseWithAuthorsByIdAsync(int publisherId, int courseId);
    // COURSE POST
    void AddCourse(Course courseEntity); // NOVO
    void AddCourse(Publisher publisher, Course Course);
    // COURSE PUT
    void UpdateCourse(Course course, UpdateCourseCommand newCourse);
    // COURSE DELETE
    void DeleteCourse(Course Course);
    // COURSE UTILS
    Task<bool> AuthorsExistAsync(IEnumerable<int> authors);
    Task<List <Author>> GetAuthorsAsync(IEnumerable<int> authors);
    

    // MODULE COLLECITON GET // NOVO
    Task<IEnumerable<Module>> GetModulesAsync();
    Task<IEnumerable<Module>?> GetModulesAsync(int courseId);
    Task<(IEnumerable<Module>, PaginationMetadata)> GetModulesAsync(
        string? title, string? searchQuery, int pageNumber, int pageSize
    );
    // MODULE GET
    Task<Module?> GetModuleByIdAsync(int moduleId); // NOVO
    Task<Module?> GetModuleFromCourseByIdAsync(int courseId, int moduleId); // NOVO
    Task<Module?> GetModuleFromCourseWithLessonsByIdAsync(int moduleId, int courseId);
    // MODULE POST // NOVO
    void AddModule(Module module);
    // MODULE PUT
    // MODULE DELETE // NOVO
    void DeleteModule(Module module);
    // MODULE UTILS
    Task<bool> ModuleExistsAsync(int moduleId);


    // LESSON COLLECTION GET
    Task<IEnumerable <Lesson>> GetLessonsAsync();
    Task<IEnumerable<Lesson>?> GetLessonsAsync(int moduleId); // NOVO
    Task<(IEnumerable<Lesson>, PaginationMetadata)> GetLessonsCollectionAsync( // NOVO
            string? title, string? searchQuery, int pageNumber, int pageSize
        );
    // LESSON GET
    Task<Lesson?> GetLessonByIdAsync(int lessonId);
    Task<Lesson?> GetLessonByIdAsync(int moduleId, int lessonId); // NOVO
    Task<Lesson?> GetLessonWithQuestionsByIdAsync(int lessonId);
    // LESSON POST
    void AddLesson(Lesson lesson);
    // LESSON PUT
    void UpdateLesson(Lesson lesson, UpdateLessonCommand newLesson);
    // LESSON DELETE
    void DeleteLesson(Lesson lesson);
    // LESSON UTILS
    Task<bool> LessonExistsAsync(int lessonId);


    // STUDENT COLLECTION GET
    Task<(IEnumerable<Student>, PaginationMetadata)> GetStudentsAsync(int pageNumber, int pageSize);
    Task<(IEnumerable<Student>,PaginationMetadata)> GetStudentsCollectionAsync(string? searchQuery, int pageNumber, int pageSize);
    // STUDENT GET
    Task<Student?> GetStudentByIdAsync(int studentId);
    Task<Student?> GetStudentWithQuestionsByIdAsync(int studentId);
    Task<Student?> GetStudentWithCoursesByIdAsync(int studentId);
    // STUDENT POST
    void AddStudent(Student student);
    // STUDENT PUT
    // STUDENT DELETE
    void DeleteStudent(Student student);
    // STUDENT UTILS
    Task<bool> StudentExistsAsync(int studentId);


    // QUESTION GET
    Task<(IEnumerable<Question>,PaginationMetadata)> GetQuestionsAsync(string? category, string? searchQuery, int pageNumber, int pageSize);
    //Task<(IEnumerable<Question>,PaginationMetadata)> GetQuestionsFromLessonAsync(int lessonId,string? category, string? searchQuery, int pageNumber, int pageSize);
    //Task<(IEnumerable<Question>,PaginationMetadata)> GetQuestionsFromStudentInLessonAsync(int studentId, int lessonId, string? category, string? searchQuery, int pageNumber, int pageSize);
    //Task<(IEnumerable<Question>,PaginationMetadata)> GetLessonQuestionsAsync(int lessonId,string? category, string? searchQuery, int pageNumber, int pageSize);
    Task<(IEnumerable<Question>,PaginationMetadata)> GetQuestionsByLessonIdAsync(int lessonId, int pageNumber, int pageSize);
    public Task<Question?> GetQuestionByIdAsync(int questionId);
    public Task<Question?> GetQuestionWithAnswersByIdAsync(int questionId);
    // QUESTION POST
    public void AddQuestion(Question question);
    // QUESTION PUT
    // QUESTION DELETE
    public void DeleteQuestion(Question question);
    // QUESTION UTILS
    Task<bool> QuestionExistsAsync(int questionId);


    // ANSWER GET
    Task<(IEnumerable<Answer>, PaginationMetadata)> GetAnswersAsync(string? searchQuery, int pageNumber, int pageSize);
    Task<(IEnumerable<Answer>,PaginationMetadata)> GetAnswersByAuthorIdAsync(int authorId, int pageNumber, int pageSize);
    Task<(IEnumerable<Answer>,PaginationMetadata)> GetAnswersByQuestionIdAsync(int questionId, int pageNumber, int pageSize);
    // ANSWER POST
    void AddAnswer(Answer answer);
    // ANSWER PUT
    // ANSWER DELETE
    void DeleteAnswer(Answer answer);
    // ANSWER UTILS
    Task<bool> AnswerExistsAsync(int answerId);


    // CONTEXT COMMIT
    Task<bool> SaveChangesAsync();   
    
  
}