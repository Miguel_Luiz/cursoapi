using Univali.Domain.Entities;
using Univali.Application.Features.Common;
using Univali.Application.Features.Orders.Commands.UpdateOrder;
using Univali.Application.Features.Payments.Commands.UpdatePayment;

namespace Univali.Application.Contracts.Repositories;

public interface IOrderRepository
{
    // CUSTOMER GET
    Task<Customer?> GetCustomerByIdAsync(int customerId);


    // COURSE EXISTS AND GET (NO TRACKING)
    Task<bool> CoursesExistAsync(IEnumerable<int> courses);
    Task<Course?> GetCourseWithoutTrackingByIdAsync(int courseId);
    Task<Course?> GetCourseWithoutTrackingFromItemAsync(OrderItem item);
    Task<List <Course>> GetCoursesWithoutTrackingAsync(IEnumerable<int> courses);


    // ORDER GET
    Task<IEnumerable <Order>?> GetOrdersAsync(int customerId);
    Task<IEnumerable<Order>> GetOrdersWithFilterAsync(int customerId, string? itemTitle, string? searchQuery, int pageNumber, int pageSize);
    Task<IEnumerable <Order>?> GetOrdersWithReceiptAsync(int customerId);
    Task<Order?> GetOrderByIdAsync(int customerId, int orderId);
    Task<Order?> GetOrderWithReceiptByIdAsync(int customerId, int orderId);
    Task<Order?> GetOrderInFullByIdAsync(int customerId, int orderId);
    // ORDER POST
    void AddOrder(Customer customer, Order order);
    // ORDER PUT
    void UpdateOrder(Order order, UpdateOrderCommand newOrder);
    // ORDER DELETE
    void DeleteOrder(Order order);


    // ORDERITEM GET
    Task<IEnumerable<OrderItem>?> GetOrderItemsAsync(int orderId);
    Task<(IEnumerable<OrderItem>?, PaginationMetadata)> GetOrderItemsWithFilterAsync(int orderId, int pageNumber, int pageSize);
    Task<OrderItem?> GetOrderItemByIdAsync(int orderId, int itemId);
    // ORDERITEM POST
    void AddOrderItem(Order order, OrderItem item);
    // ORDERITEM PUT
    void UpdateOrderItem(OrderItem item, Course newItem);
    // ORDERITEM DELETE
    void DeleteOrderItem(OrderItem item);


    // RECEIPT GET
    Task<Receipt?> GetReceiptFromOrderById(int orderId);


    // PAYMENT GET
    Task<Payment?> GetPaymentFromOrderByIdAsync(int paymentId);
    // PAYMENT POST
    void AddPayment(Order order, Payment payment);
    // PAYMENT PUT
    void UpdatePayment(Payment payment, UpdatePaymentCommand newPayment);
    // PAYMENT DELETE
    void DeletePayment(Payment payment);


    // CONTEXT COMMIT
    Task<bool> SaveChangesAsync();
}
