﻿using AutoMapper;
using Univali.Application.Features.Lessons.Commands.CreateLesson;
using Univali.Application.Features.Lessons.Commands.UpdateLesson;
using Univali.Application.Features.Lessons.Queries.GetLessonDetail;
using Univali.Domain.Entities;
using Univali.Application.Models;
using Univali.Application.Features.Modules.Queries.GetModuleWithLessonsDetail;

namespace Univali.Application.Profiles;

public class LessonProfiles : Profile
{
    public LessonProfiles()
    {
        CreateMap<CreateLessonCommand, Lesson>().ReverseMap();
        
        CreateMap<Lesson, CreateLessonCommandDto>().ReverseMap();

        CreateMap<UpdateLessonCommand, Lesson>().ForMember(dest => dest.Duration, opt =>
            opt.MapFrom(src => new TimeSpan(0,src.DurationMinutes,0)));

        CreateMap<GetLessonDetailQuery, GetLessonDetailDto>().ReverseMap();

        CreateMap<Lesson, GetLessonDetailDto>().ReverseMap();

        CreateMap<Lesson, LessonForModuleQueryReturnDto>().ReverseMap();

        CreateMap<CreateLessonCommand, Lesson>().ForMember(dest => dest.Duration, opt =>
            opt.MapFrom(src => new TimeSpan(0,src.DurationMinutes,0)));
        
        CreateMap<Lesson, GetLessonDetailQueryResponse>().ReverseMap();

        CreateMap<Lesson, LessonDto>().ReverseMap();
        
    }
}