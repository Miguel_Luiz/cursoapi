using AutoMapper;
using Univali.Domain.Entities;
using Univali.Application.Features.Answers.Commands.CreateAnswer;
using Univali.Application.Features.Answers.Commands.UpdateAnswer;
using Univali.Application.Features.Answers.Queries.GetAnswerDetail;
using Univali.Application.Features.Answers.Queries.GetAnswersDetail;
using Univali.Application.Features.AnswersCollection.GetAnswersDetail;
using Univali.Application.Features.QuestionAnswers.Queries.GetQuestionAnswerDetail;
using Univali.Application.Features.QuestionAnswers.Queries.GetQuestionAnswersDetail;
using Univali.Application.Models;

namespace Univali.Application.Profiles;

public class AnswerProfile : Profile
{
    public AnswerProfile()
    {
        CreateMap<Answer, AnswerDto>();
        CreateMap<Answer, GetAnswerDetailDto>();
        CreateMap<Answer, GetQuestionAnswersDetailDto>();
        CreateMap<Answer, GetAnswersDetailDto>();
        CreateMap<Answer, GetQuestionAnswerDetailDto>();
        CreateMap<Answer, GetAnswersCollectionDetailDto>();
        CreateMap<Answer, CreateAnswerDto>();
        CreateMap<CreateAnswerCommand, Answer>();
        CreateMap<UpdateAnswerCommand, Answer>();
    }
}