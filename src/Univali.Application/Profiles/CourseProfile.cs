using AutoMapper;
using Univali.Domain.Entities;
using Univali.Application.Features.Courses.Commands;
using Univali.Application.Features.Courses.Queries;
using Univali.Application.Models;

namespace Univali.Application.Profiles;

public class CourseProfile : Profile 
{
    public CourseProfile() 
    {
        CreateMap<UpdateCourseCommand, Course>();

        CreateMap<Course, CreateCourseDto>().ReverseMap();

        CreateMap<CreateCourseCommand, Course>();

        CreateMap<Course, GetCourseWithAuthorsDto>();

        CreateMap<Course, CourseDto>().ReverseMap();

        CreateMap<Course, CourseWithoutAuthorDto>();

        CreateMap<Publisher, PublisherWithoutCoursesDto>();

        CreateMap<Course, CreateCourseDto>()
            .ForMember(dest => dest.Publisher, opt => opt.MapFrom(src => src.Publisher));
    }
}

