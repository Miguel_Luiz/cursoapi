using AutoMapper;
using Univali.Domain.Entities;
using Univali.Application.Features.Receipts.Queries.GetReceipt;

namespace Univali.Application.Profiles;

public class ReceiptProfile : Profile
{
    public ReceiptProfile()
    {
      CreateMap<Receipt, GetReceiptDetailDto>();
      CreateMap<Receipt, GetReceiptDetailDto>();
    }
}