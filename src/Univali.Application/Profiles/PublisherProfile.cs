using AutoMapper;
using Univali.Domain.Entities;
using Univali.Application.Models;
using Univali.Application.Features.Publishers.Commands.CreatePublisher;
using Univali.Application.Features.Publishers.Commands.UpdatePublisher;
using Univali.Application.Features.Publishers.Queries.GetPublisher;

namespace Univali.Application.Profiles;

public class PublisherProfile : Profile 
{
    public PublisherProfile() 
    {
        CreateMap<CreatePublisherCommand, Publisher>();

        CreateMap<CreatePublisherCommand, LegalPublisher>();

        CreateMap<CreatePublisherCommand, NaturalPublisher>();

        CreateMap<CreateLegalPublisherDto, LegalPublisher>().ReverseMap();

        CreateMap<CreateNaturalPublisherDto, NaturalPublisher>().ReverseMap();

        CreateMap<CreatePublisherDto, NaturalPublisher>().ReverseMap();

        CreateMap<CreatePublisherDto, LegalPublisher>().ReverseMap();

        CreateMap<Publisher, CreatePublisherDto>();

        CreateMap<NaturalPublisher, CreatePublisherDto>();

        CreateMap<LegalPublisher, CreatePublisherDto>();

        CreateMap<Publisher, CreateLegalPublisherDto>();

        CreateMap<Publisher, CreateNaturalPublisherDto>();

        CreateMap<UpdatePublisherCommand, Publisher>();

        // CreateMap<Publisher, GetPublisherWithCoursesDto>();

        CreateMap<Publisher, PublisherWithoutCoursesDto>();

        CreateMap<PublisherWithoutCoursesDto, Publisher>();

        CreateMap<LegalPublisher, LegalPublisherDto>();

        CreateMap<NaturalPublisher, NaturalPublisherDto>();

        CreateMap<Publisher, GetPublisherWithCoursesDto>()
            .Include<LegalPublisher, LegalPublisherDto>()
            .Include<NaturalPublisher, NaturalPublisherDto>()
            .ForMember(dest => dest.PublisherId, opt => opt.MapFrom(src => src.PublisherId))
            .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
            .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.GetType().Name))
            .ForMember(dest => dest.Courses, opt => opt.MapFrom(src => src.Courses));

        CreateMap<LegalPublisher, LegalPublisherDto>()
            .ForMember(dest => dest.CNPJ, opt => opt.MapFrom(src => src.CNPJ));

        CreateMap<NaturalPublisher, NaturalPublisherDto>()
            .ForMember(dest => dest.CPF, opt => opt.MapFrom(src => src.CPF));
    }
}

