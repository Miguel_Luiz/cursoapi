using AutoMapper;
using Univali.Domain.Entities;
using Univali.Application.Features.Students.Commands.CreateStudent;
using Univali.Application.Features.StudentsCollection.GetStudentsDetail;

namespace Univali.Application.Profiles;

public class StudentProfile : Profile
{
    public StudentProfile()
    {
        CreateMap<Student, Features.Students.Queries.GetStudentsDetails.GetStudentsDetailsDto>();
        CreateMap<Student, Features.Students.Queries.GetStudentDetails.GetStudentDetailsDto>();
        CreateMap<Student, Features.Students.Commands.CreateStudent.CreateStudentCommand>().ReverseMap();
        CreateMap<Features.Students.Commands.CreateStudent.CreateStudentDto, Student>();
        CreateMap<Features.Students.Commands.UpdateStudent.UpdateStudentCommand, Student>();
        CreateMap<Student, CreateStudentDto>();
        CreateMap<Student, GetStudentsCollectionDetailDto>();

    }
}