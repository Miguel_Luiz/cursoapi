using AutoMapper;
using Univali.Domain.Entities;
using Univali.Application.Features.Authors.Commands.CreateAuthor;
using Univali.Application.Features.Authors.Commands.UpdateAuthor;
using Univali.Application.Features.Authors.Queries;
using Univali.Application.Models;

namespace Univali.Application.Profiles
{
    public class AuthorProfile : Profile
    {
        public AuthorProfile() 
        {
            CreateMap<UpdateAuthorCommand, Author>();

            CreateMap<Author, CreateAuthorDto>();
            
            CreateMap<CreateAuthorCommand, Author>();

            CreateMap<Author, GetAuthorWithCoursesDto>();

            CreateMap<Author, AuthorDto>().ReverseMap();

            CreateMap<Author, GetAuthorWithCoursesDto>();

            CreateMap<Author, AuthorDto>().ReverseMap();
        }
    }
}