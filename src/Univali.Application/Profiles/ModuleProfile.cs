﻿using Univali.Application.Features.Modules.Commands.CreateModule;
using AutoMapper;
using Univali.Application.Features.Modules.Queries.GetModuleDetail;
using Univali.Application.Features.Modules.Queries.GetModulesDetail;
using Univali.Application.Features.Modules.Queries.GetModuleWithLessonsDetail;
using Univali.Application.Features.Modules.Commands.UpdateModule;
using Univali.Domain.Entities;
using Univali.Application.Models;


namespace Univali.Application.Profiles;

public class ModuleProfile : Profile
{
    public ModuleProfile()
    {
        CreateMap<CreateModuleCommand, Module>().ReverseMap();
        CreateMap<CreateModuleCommandDto, Module>().ReverseMap();
        CreateMap<CreateModuleCommandDto, CreateModuleCommand>().ReverseMap();
        CreateMap<Module, GetModuleDetailDto>();
        CreateMap<Module, GetOneModuleDetailDto>();
        CreateMap<Module, GetModuleWithLessonsDetailDto>().ReverseMap();
        CreateMap<UpdateModuleCommand, Module>();
        CreateMap<Module, GetModuleDetailQueryResponse>().ReverseMap();
        CreateMap<Module, GetModulesDetailQueryResponse>().ReverseMap();
        CreateMap<Module, ModuleDto>().ReverseMap();
        CreateMap<Module, GetModuleDetailDto>(); //Miguel
        
    }
}