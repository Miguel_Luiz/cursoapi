using AutoMapper;
using Univali.Domain.Entities;
using Univali.Application.Features.Items.Queries.GetOrdersItems;
using Univali.Application.Features.Items.Queries.GetOrdersItemsWithFilter;
using Univali.Application.Features.OrderItems.Commands.CreateOrderItem;
using Univali.Application.Features.OrderItems.Commands.UpdateOrderItem;
using Univali.Application.Models;


namespace Univali.Application.Profiles;

public class ItemsProfile : Profile
{
    public ItemsProfile()
    {
        CreateMap<OrderItem, GetOrdersItemsDetailDto>();
        CreateMap<OrderItem, GetOrdersItemsWithFilterDto>();
        CreateMap<Course, OrderItem>().ReverseMap();
        CreateMap<CreateOrderItemCommand, OrderItem>().ReverseMap();
        CreateMap<UpdateOrderItemCommand, OrderItem>().ReverseMap();
        CreateMap<OrderItem, OrderItemForCreationDto>().ReverseMap();
        CreateMap<OrderItem, CreateOrderItemDto>().ReverseMap();
    }
}