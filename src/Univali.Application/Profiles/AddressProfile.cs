using AutoMapper;
using Univali.Domain.Entities;
using Univali.Application.Features.Addresses.Commands.CreateAddress;
using Univali.Application.Features.Addresses.Commands.UpdateAddress;
using Univali.Application.Features.Addresses.Queries.GetAddressByIdDetail;
using Univali.Application.Features.Addresses.Queries.GetAddressesFilter;
using Univali.Application.Features.Customers.Queries.GetCustomersWithAddresses;

namespace Univali.Application.Profiles;

public class AddressProfile : Profile
{
    public AddressProfile()
    {
        CreateMap<Address, Models.AddressDto>();
        CreateMap<Models.AddressForUpdateDto, Address>();
        CreateMap<Models.AddressForCreationDto, Address>();
        CreateMap<Models.AddressDto, Address>();
        CreateMap<CreateAddressCommand, Address>();
        CreateMap<Address, CreateAddressCommandResponse>();
        CreateMap<UpdateAddressCommand, Address>();
        CreateMap<Address,GetAddresesFilterDetailDto>().ReverseMap();
        CreateMap<GetCustomersWithAddressesDetailDto, Address>().ReverseMap();
        CreateMap<LegalCustomer,GetAddressByIdDetailDto>().ReverseMap();
    }
}