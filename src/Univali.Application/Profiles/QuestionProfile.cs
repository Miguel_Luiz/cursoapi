using AutoMapper;
using Univali.Domain.Entities;
using Univali.Application.Features.Questions.Commands.CreateQuestion;
using Univali.Application.Features.Questions.Commands.UpdateQuestion;
using Univali.Application.Features.Questions.Queries.GetQuestionDetail;
using Univali.Application.Features.Questions.Queries.GetQuestionsDetail;
using Univali.Application.Features.Questions.Queries.GetQuestionWithAnswersDetail;
using Univali.Application.Features.QuestionsCollection.Queries.GetQuestionsCollectionDetail;
using Univali.Application.Features.LessonQuestions.Queries.GetLessonQuestionDetail;
using Univali.Application.Features.LessonQuestions.Queries.GetLessonQuestionWithAnswersDetail;
using Univali.Application.Features.LessonQuestions.Queries.GetLessonQuestionsDetail;
using Univali.Application.Models;

namespace Univali.Application.Profiles;

public class QuestionProfile : Profile
{
    public QuestionProfile()
    {
        CreateMap<Question, GetQuestionDetailDto>();
        CreateMap<Question, GetLessonQuestionDetailDto>();
        CreateMap<Question, GetLessonQuestionWithAnswersDetailDto>();
        CreateMap<Question, GetLessonQuestionsDetailDto>();
        CreateMap<Question, QuestionForUpdateDto>().ReverseMap();
        CreateMap<Question, GetQuestionsDetailDto>();
        CreateMap<Question, GetQuestionsCollectionDetailDto>().ReverseMap();
        CreateMap<Question, GetQuestionWithAnswersDetailDto>();
        CreateMap<Question, CreateQuestionDto>();
        CreateMap<CreateQuestionCommand, Question>();
        CreateMap<UpdateQuestionCommand, Question>();
    }
}