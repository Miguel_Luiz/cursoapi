using AutoMapper;
using Univali.Domain.Entities;
using Univali.Application.Features.Orders.Commands.UpdateOrder;
using Univali.Application.Features.Orders.Commands.CreateOrder;
using Univali.Application.Features.Orders.Queries.GetOrderDetail;
using Univali.Application.Features.Orders.Queries.GetOrders;
using Univali.Application.Models;
using Univali.Application.Features.Orders.Queries.GetOrdersWithReceipt;
using Univali.Application.Features.Orders.Queries.GetOrderWithReceiptDetail;
using Univali.Application.Features.Orders.Queries.GetOrdersWithFilter;

namespace Univali.Application.Profiles;

public class OrderProfile : Profile
{
  public OrderProfile()
  {
    CreateMap<PaymentTypeDto, PaymentType>().ReverseMap();
    CreateMap<OrderItem, OrderItemDto>().ReverseMap();
    CreateMap<Payment, PaymentDto>().ReverseMap();
    CreateMap<Receipt, ReceiptDto>().ReverseMap();

    CreateMap<GetOrdersDetailDto, Order>();

    CreateMap<CreateOrderDto, Order>();
    CreateMap<OrderItemForCreationDto, OrderItem>();
    CreateMap<PaymentForCreationDto, Payment>();
    CreateMap<ReceiptForCreationDto, Receipt>();
    CreateMap<Order, CreateOrderDto>();

    CreateMap<PaymentTypeDto, PaymentType>().ReverseMap();
    CreateMap<OrderItem, OrderItemDto>().ReverseMap();
    CreateMap<Payment, PaymentDto>().ReverseMap();
    CreateMap<Receipt, ReceiptDto>().ReverseMap();

    CreateMap<Order, GetOrdersDetailDto>();
    CreateMap<Order, GetOrdersWithReceiptDetailDto>();

    CreateMap<Order, GetOrderDetailDto>();
    CreateMap<Order, GetOrderWithReceiptDetailDto>();

    CreateMap<CreateOrderDto, Order>();
    CreateMap<OrderItemForCreationDto, OrderItem>();
    CreateMap<PaymentForCreationDto, Payment>();
    CreateMap<ReceiptForCreationDto, Receipt>();
    CreateMap<Order, CreateOrderDto>();

    CreateMap<CreateOrderCommand, Order>().ReverseMap();
    CreateMap<Order, GetOrdersDetailDto>().ReverseMap();
    CreateMap<Order, GetOrderDetailDto>().ReverseMap();
    CreateMap<UpdateOrderCommand, Order>().ReverseMap();   
    
    CreateMap<Order, GetOrdersWithFilterDetailDto>(); 
  }
}