using AutoMapper;
using Univali.Domain.Entities;
using Univali.Application.Features.Addresses.Commands.CreateAddress;
using Univali.Application.Features.Customers.Commands.CreateCustomer;
using Univali.Application.Features.Customers.Commands.UpdateCustomer;
using Univali.Application.Features.Customers.Commands.UpdateCustomerWithAddresses;
using Univali.Application.Features.Addresses.Queries.GetAddresses;
using Univali.Application.Features.Addresses.Queries.GetAddressByIdDetail;
using Univali.Application.Features.Customers.Queries.GetCustomerByCpfDetail;
using Univali.Application.Features.Customers.Queries.GetCustomerDetail;
using Univali.Application.Features.Customers.Queries.GetCustomerWithAddressesByIdDetail;
using Univali.Application.Models;
using Univali.Application.Features.Customers.Queries.GetCustomers;
using Univali.Application.Features.Customers.Queries.GetCustomersWithAddresses;
using Univali.Application.Features.Customers.Commands.CreateCustomerWithAddresses;
using Univali.Application.Features.Customers.Queries.GetCustomerByCnpjDetail;
using Univali.Application.Features.Customers.Commands.PartiallyUpdateCustomer;
using Univali.Application.Features.Customers.Queries.GetCustomersFilter;

namespace Univali.Application.Profiles;

public class CustomerProfile : Profile
{
    public CustomerProfile()
    {
        // Velhos
        CreateMap<Customer, Models.CustomerDto>();
        CreateMap<Models.CustomerForUpdateDto, Customer>();
        CreateMap<Models.CustomerForCreationDto, Customer>();
        CreateMap<Customer, Models.CustomerWithAddressesDto>();

        // Novos
        CreateMap<Customer, GetCustomerDetailDto>();
        CreateMap<Customer, GetCustomersDetailDto>();
        CreateMap<CreateCustomerCommand, Customer>();
        CreateMap<CreateCustomerWithAddressesCommand, Customer>();
        CreateMap<UpdateCustomerWithAddressesCommand, Customer>();
        CreateMap<Customer, CreateCustomerDto>();
        CreateMap<Customer, CustomerForPatchDto>().ReverseMap();
        CreateMap<UpdateCustomerCommand, Customer>();

        CreateMap<Customer, GetCustomerByCpfDetailDto>();
        CreateMap<Customer, GetCustomerWithAddressesByIdDetailDto>();
        CreateMap<Customer, GetCustomersWithAddressesDetailDto>();
        CreateMap<Customer, CreateCustomerWithAddressesDto>();
        CreateMap<Address, AddressDto>();
        CreateMap<Address, GetAddressByIdDetailDto>();
        CreateMap<CreateAddressCommand, Address>().ReverseMap();
        CreateMap<Address, CreateAddressDto>();
        CreateMap<Address, GetAddressDetailDto>().ReverseMap();
        CreateMap<UpdateCustomerWithAddressesCommand, Address>();

        //TPT
        CreateMap<NaturalCustomer, GetCustomerByCpfDetailDto>();
        CreateMap<LegalCustomer, GetCustomerByCnpjDetailDto>();
        CreateMap<CreateCustomerCommand, LegalCustomer>();
        CreateMap<CreateCustomerCommand, NaturalCustomer>();
        CreateMap<NaturalCustomer,CreateCustomerDto>();
        CreateMap<LegalCustomer,CreateCustomerDto>();
        CreateMap<CreateLegalCustomerDto, LegalCustomer>().ReverseMap();
        CreateMap<CreateNaturalCustomerDto, NaturalCustomer>().ReverseMap();
        CreateMap<CreateCustomerDto, NaturalCustomer>().ReverseMap();
        CreateMap<CreateCustomerDto, LegalCustomer>().ReverseMap();
        CreateMap<Customer, GetCustomerByCnpjDetailDto>();//não é tpt mas implementei junto
        CreateMap<Customer, LegalCustomer>().ReverseMap();
        CreateMap<Customer, NaturalCustomer>().ReverseMap();
        CreateMap<LegalCustomer, UpdateCustomerCommand>().ReverseMap();
        CreateMap<NaturalCustomer, UpdateCustomerCommand>().ReverseMap();
        CreateMap<PartiallyUpdateCustomerCommand, LegalCustomer>().ReverseMap();
        CreateMap<PartiallyUpdateCustomerCommand, NaturalCustomer>().ReverseMap();
        CreateMap<PartiallyUpdateCustomerCommand, Customer>().ReverseMap();
        CreateMap<GetCustomersFilterDetailDto, LegalCustomer>().ReverseMap();
        CreateMap<GetCustomersFilterDetailDto, NaturalCustomer>().ReverseMap();
        CreateMap<GetCustomersFilterDetailDto, Customer>().ReverseMap();

        //TPT ajudando Ritzel
        CreateMap<NaturalCustomer,CreateCustomerWithAddressesDto>().ReverseMap();
        CreateMap<LegalCustomer,CreateCustomerWithAddressesDto>().ReverseMap();
        CreateMap<CreateCustomerWithAddressesCommand, LegalCustomer>().ReverseMap();
        CreateMap<CreateCustomerWithAddressesCommand, NaturalCustomer>().ReverseMap();
        CreateMap<CreateLegalWithAddressesCustomerDto, LegalCustomer>().ReverseMap();
        CreateMap<CreateNaturalCustomerWithAddressesDto, NaturalCustomer>().ReverseMap();
        CreateMap<CreateCustomerWithAddressesDto, NaturalCustomer>().ReverseMap();
        CreateMap<CreateCustomerWithAddressesDto, LegalCustomer>().ReverseMap();
        
        
        //adicionando CPF e CNPJ nos gets
        CreateMap<NaturalCustomer,GetNaturalCustomerDetailDto>();
        CreateMap<LegalCustomer,GetLegalCustomerDetailDto>();
        CreateMap<NaturalCustomer,GetNaturalCustomerWithAddressesByIdDetailDto>();
        CreateMap<LegalCustomer,GetLegalCustomerWithAddressesByIdDetailDto>();
    }
}