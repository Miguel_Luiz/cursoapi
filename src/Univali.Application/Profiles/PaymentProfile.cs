using AutoMapper;
using Univali.Domain.Entities;
using Univali.Application.Features.Payments.Commands.CreatePayment;
using Univali.Application.Features.Payments.Commands.UpdatePayment;
using Univali.Application.Features.Payments.Queries.GetPayment;
using Univali.Application.Models;

namespace Univali.Application.Profiles;

public class PaymentProfile : Profile
{
    public PaymentProfile()
    {
        CreateMap<Payment, GetPaymentDetailDto>();

        CreateMap<Payment, GetPaymentDetailDto>();
        CreateMap<CreatePaymentCommand, Payment>();
        CreateMap<Payment, CreatePaymentCommandResponse>();
        CreateMap<Payment, GetPaymentDetailDto>();
        CreateMap<GetPaymentDetailDto, Payment>();

        CreateMap<Payment, CreatePaymentDto>();
        CreateMap<Payment, PaymentForCreationDto>().ReverseMap();

        CreateMap<Payment, CreatePaymentDto>();
        CreateMap<UpdatePaymentCommand, Payment>();

        CreateMap<PaymentType, PaymentTypeDto>();
    }
}