namespace Univali.Application.Models;

public class CourseWithoutAuthorDto
{
    public int CourseId { get; set; }

    public string Title { get; set; } = string.Empty;

    public string Description { get; set; } = string.Empty;

    public decimal Price { get; set; }

    // pablo1107 - Adicionei category, totalDuration e moduleAmount
    public string Category {get; set;} = string.Empty;

    public TimeSpan TotalDuration {get;set;}
    
    public int ModuleAmount {get;set;}
}
