namespace Univali.Application.Models;


public class AddressForUpdateDto : AddressForManipulationDto
{
   public int Id { get; set; }
}
