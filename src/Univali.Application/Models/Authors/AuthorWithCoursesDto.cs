
namespace Univali.Application.Models;

public class AuthorWithCoursesDto
{
    public int Id {get; set;}
    public string FirstName { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public string Cpf {get; set;} = string.Empty;
    public List<CourseDto> Courses {get; set;} = new();
}