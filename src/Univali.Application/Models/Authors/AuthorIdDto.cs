namespace Univali.Application.Models;

public class AuthorIdDto
{
    public int AuthorId { get; set; }
}