namespace Univali.Application.Models;

public class AuthorForCreationDto
{
    public string FirstName { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public string Cpf {get; set;} = string.Empty;
}