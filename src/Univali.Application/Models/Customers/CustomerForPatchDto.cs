namespace Univali.Application.Models;

public class CustomerForPatchDto : CustomerForManipulationDto
{
    public ICollection<AddressDto> Addresses { get; set; } = new List<AddressDto>();
}