namespace Univali.Application.Models
{
   public class CustomerWithAddressesForUpdateDto
       : CustomerWithAddressesForManipulationDto
   {
       public int Id {get; set;}
   }
}
