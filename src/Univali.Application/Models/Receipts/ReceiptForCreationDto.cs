namespace Univali.Application.Models;

public class ReceiptForCreationDto
{
  public string CustomerName { get; set; } = string.Empty;
  public string CustomerIdentifier { get; set; } = string.Empty;
  public PaymentTypeDto PaymentType { get; set; } // em implementacao completa, pode ser string com Name da classe de pagament especifica
  public DateTime Timestamp { get; set; }
  public decimal TotalPrice { get; set; }
}