namespace Univali.Application.Models;

public class OrderItemDto
{
    public int OrderItemId { get; set; }
    public string Title { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public decimal Price { get; set; }
}