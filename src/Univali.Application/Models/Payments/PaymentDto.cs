namespace Univali.Application.Models;

public enum PaymentTypeDto { CreditCard, DebitCard, BoletoBancario, PIX }

public class PaymentDto
{
    public int PaymentId { get; set; }
    public PaymentTypeDto Type { get; set; }
    public string Description { get; set; } = string.Empty;
}