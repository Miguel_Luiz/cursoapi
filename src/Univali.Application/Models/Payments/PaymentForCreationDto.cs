namespace Univali.Application.Models;

public class PaymentForCreationDto
{
    public PaymentTypeDto Type { get; set; }
    public string Description { get; set; } = string.Empty;
}