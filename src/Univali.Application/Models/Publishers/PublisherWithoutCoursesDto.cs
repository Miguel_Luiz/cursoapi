namespace Univali.Application.Models;

public class PublisherWithoutCoursesDto
{
    public int PublisherId { get; set; }
    public string Name { get; set; } = string.Empty;
}