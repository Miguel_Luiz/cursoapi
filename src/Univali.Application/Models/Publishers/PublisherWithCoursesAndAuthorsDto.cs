namespace Univali.Application.Models;

public class PublisherWithCoursesAndAuthorsDto
{
    public int PublisherId { get; set; }
    public string Name {get; set;} = string.Empty;
    public string CNPJ {get; set;} = string.Empty;
    public List<CourseWithAuthorsDto> Courses {get; set;} = new();
}