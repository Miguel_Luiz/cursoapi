using Univali.Domain.Entities;

namespace Univali.Application.Models;

public class LessonDto
{
    public int LessonId {get;set;}
    public string Title {get;set;} = string.Empty;
    public string Description {get;set;} = string.Empty;
    public TimeSpan Duration {get;set;}
    public int ModuleId {get;set;}
// pablo1107 - removi module
}