using Univali.Domain.Entities;

namespace Univali.Application.Models;

public class ModuleDto
{
    public int ModuleId {get;set;}
    public string Title {get;set;} = string.Empty;
    public string Description {get;set;} = string.Empty;
    public int Number {get;set;}
    public TimeSpan TotalDuration {get;set;}
    public int LessonAmount {get;set;}
}