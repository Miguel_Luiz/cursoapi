using System.Reflection;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using Univali.Application.Features.Addresses.Commands.CreateAddress;
using Univali.Application.Features.Addresses.Commands.DeleteAddress;
using Univali.Application.Features.Addresses.Commands.UpdateAddress;
using Univali.Application.Features.Addresses.Queries.GetAddressByIdDetail;
using Univali.Application.Features.Answers.Commands.CreateAnswer;
using Univali.Application.Features.Answers.Commands.UpdateAnswer;
using Univali.Application.Features.Authors.Commands.CreateAuthor;
using Univali.Application.Features.Authors.Commands.UpdateAuthor;
using Univali.Application.Features.Courses.Commands;
using Univali.Application.Features.Courses.Commands.CreateCourse;
using Univali.Application.Features.Courses.Commands.UpdateCourse;
using Univali.Application.Features.Customers.Commands.CreateCustomer;
using Univali.Application.Features.Customers.Commands.CreateCustomerWithAddresses;
using Univali.Application.Features.Customers.Commands.DeleteCustomer;
using Univali.Application.Features.Customers.Commands.PartiallyUpdateCustomer;
using Univali.Application.Features.Customers.Commands.UpdateCustomer;
using Univali.Application.Features.Customers.Commands.UpdateCustomerWithAddresses;
using Univali.Application.Features.Customers.Queries.GetCustomerByCnpjDetail;
using Univali.Application.Features.Customers.Queries.GetCustomerByCpfDetail;
using Univali.Application.Features.Customers.Queries.GetCustomerDetail;
using Univali.Application.Features.Customers.Queries.GetCustomerWithAddressesByIdDetail;
using Univali.Application.Features.OrderItems.Commands.CreateOrderItem;
using Univali.Application.Features.OrderItems.Commands.UpdateOrderItem;
using Univali.Application.Features.Orders.Commands.CreateOrder;
using Univali.Application.Features.Orders.Commands.UpdateOrder;
using Univali.Application.Features.Payments.Commands.CreatePayment;
using Univali.Application.Features.Payments.Commands.UpdatePayment;
using Univali.Application.Features.Publishers.Commands.CreatePublisher;
using Univali.Application.Features.Publishers.Commands.DeletePublisher;
using Univali.Application.Features.Publishers.Commands.UpdatePublisher;
using Univali.Application.Features.Publishers.Queries.GetPublisher;
using Univali.Application.Features.Questions.Commands.CreateQuestion;
using Univali.Application.Features.Questions.Commands.UpdateQuestion;
using Univali.Application.Features.Students.Commands.CreateStudent;
using Univali.Application.Features.Students.Commands.UpdateStudent;
using Univali.Application.Models;
using Univali.Application.Features.Lessons.Commands.CreateLesson;
using Univali.Application.Features.Lessons.Commands.DeleteLesson;
using Univali.Application.Features.Lessons.Commands.UpdateLesson;
using Univali.Application.Features.Lessons.Queries.GetLessonDetail;
using Univali.Application.Features.Modules.Commands.CreateModule;
using Univali.Application.Features.Modules.Commands.DeleteModule;
using Univali.Application.Features.Modules.Commands.UpdateModule;
using Univali.Application.Features.Modules.Queries.GetModuleDetail;
using Univali.Application.Features.Modules.Queries.GetModulesDetail;
using Univali.Application.Features.Modules.Queries.GetModuleWithLessonsDetail;

namespace Univali.Application.Extensions;

public static class ApplicationServiceRegistration
{
    public static IServiceCollection AddApplicationServices(this IServiceCollection services)
    {
        services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
        services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()));

        services.AddFluentValidationServices();

        return services;
    }

    public static void AddFluentValidationServices(this IServiceCollection services)
    {
        // CUSTOMER COMMANDS
        services.AddScoped<IValidator<CreateCustomerCommand>, CreateCustomerCommandValidator>();
        services.AddScoped<IValidator<UpdateCustomerCommand>, UpdateCustomerCommandValidator>();
        services.AddScoped<IValidator<CreateCustomerWithAddressesCommand>, CreateCustomerWithAddressesCommandValidator>();
        services.AddScoped<IValidator<UpdateCustomerWithAddressesCommand>, UpdateCustomerWithAddressesCommandValidator>();
        services.AddScoped<IValidator<PartiallyUpdateCustomerCommand>, PartiallyUpdateCustomerCommandValidator>();
        services.AddScoped<IValidator<GetCustomerWithAddressesByIdDetailQuery>, GetCustomerWithAddressByIdDetailQueryValidator>();
        services.AddScoped<IValidator<GetCustomerByCpfDetailQuery>, GetCustomerByCpfDetailQueryValidator>();
        services.AddScoped<IValidator<GetCustomerByCnpjDetailQuery>, GetCustomerByCnpjDetailQueryValidator>();
        services.AddScoped<IValidator<GetCustomerDetailQuery>, GetCustomerDetailQueryValidator>();
        services.AddScoped<IValidator<CreateCustomerCommand>, CreateCustomerCommandValidator>();
        services.AddScoped<IValidator<DeleteCustomerCommand>, DeleteCustomerCommandValidator>();
        // ADDRESS COMMANDS
        services.AddScoped<IValidator<CreateAddressCommand>, CreateAddressCommandValidator>();
        services.AddScoped<IValidator<UpdateAddressCommand>, UpdateAddressCommandValidator>();
        services.AddScoped<IValidator<GetAddressByIdDetailQuery>, GetAddressByIdDetailQueryValidator>();
        services.AddScoped<IValidator<DeleteAddressCommand>, DeleteAddressCommandValidator>();


        // PUBLISHER COMMANDS
        services.AddScoped< IValidator<CreatePublisherCommand>, CreatePublisherCommandValidator>();
        services.AddScoped< IValidator<DeletePublisherCommand>, DeletePublisherCommandValidator>();
        services.AddScoped< IValidator<UpdatePublisherCommand>, UpdatePublisherCommandValidator>();
        services.AddScoped< IValidator<GetPublisherWithCoursesQuery>, GetPublisherWithCoursesQueryValidator>();
        // AUTHOR COMMANDS
        services.AddScoped<IValidator<CreateAuthorCommand>, CreateAuthorCommandValidator>();
        services.AddScoped<IValidator<UpdateAuthorCommand>, UpdateAuthorCommandValidator>();
        // COURSE COMMANDS
        services.AddScoped<IValidator<CreateCourseCommand>, CreateCourseCommandValidator>();
        services.AddScoped<IValidator<UpdateCourseCommand>, UpdateCourseCommandValidator>();
        services.AddScoped< IValidator<DeleteCourseCommand>, DeleteCourseCommandValidator>();
        // MODULE COMMANDS
        services.AddScoped< IValidator<CreateModuleCommand>, CreateModuleCommandValidator>();
        services.AddScoped< IValidator<UpdateModuleCommand>, UpdateModuleCommandValidator>();
        services.AddScoped< IValidator<GetModuleDetailQuery>, GetModuleDetailQueryValidator>();
        services.AddScoped< IValidator<GetModulesDetailQuery>, GetModulesDetailQueryValidator>();
        services.AddScoped< IValidator<GetModuleWithLessonsDetailQuery>, GetModuleWithLessonsDetailQueryValidator>();
        services.AddScoped< IValidator<DeleteModuleCommand>, DeleteModuleCommandValidator>();//Miguel Novo
        // LESSON COMMANDS
        services.AddScoped<IValidator<CreateLessonCommand>, CreateLessonCommandValidator>();
        services.AddScoped<IValidator<UpdateLessonCommand>, UpdateLessonCommandValidator>();
        services.AddScoped< IValidator<GetLessonDetailQuery>, GetLessonDetailQueryValidator>();
        services.AddScoped< IValidator<DeleteLessonCommand>, DeleteLessonCommandValidator>();


        // STUDENT COMMANDS
        services.AddScoped<IValidator<CreateStudentCommand>, CreateStudentCommandValidator>();
        services.AddScoped<IValidator<UpdateStudentCommand>, UpdateStudentCommandValidator>();
        // QUESTION COMMANDS
        services.AddScoped<IValidator<CreateQuestionCommand>, CreateQuestionCommandValidator>();
        services.AddScoped<IValidator<UpdateQuestionCommand>, UpdateQuestionCommandValidator>();
        // ANSWER COMMANDS
        services.AddScoped<IValidator<CreateAnswerCommand>, CreateAnswerCommandValidator>();
        services.AddScoped<IValidator<UpdateAnswerCommand>, UpdateAnswerCommandValidator>();
        

        // ORDER COMMANDS
        services.AddScoped<IValidator<CreateOrderCommand>, CreateOrderCommandValidator>();
        services.AddScoped<IValidator<UpdateOrderCommand>, UpdateOrderCommandValidator>();
        // ORDERITEM COMMANDS
        services.AddScoped<IValidator<CreateOrderItemCommand>, CreateOrderItemCommandValidator>();
        services.AddScoped<IValidator<UpdateOrderItemCommand>, UpdateOrderItemCommandValidator>();
        // PAYMENT COMMANDS
        services.AddScoped<IValidator<CreatePaymentCommand>, CreatePaymentCommandValidator>();
        services.AddScoped<IValidator<UpdatePaymentCommand>, UpdatePaymentCommandValidator>();
    }
}