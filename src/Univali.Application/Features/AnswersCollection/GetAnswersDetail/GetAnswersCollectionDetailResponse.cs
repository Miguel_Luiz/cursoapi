
using Univali.Application.Features.Common;

namespace Univali.Application.Features.AnswersCollection.GetAnswersDetail;

public class GetAnswersCollectionDetailResponse : BaseResponse
{
    public IEnumerable<GetAnswersCollectionDetailDto> AnswersDetailDtos {get;set;} = null!;

    public PaginationMetadata PaginationMetadata = null!;
}