using MediatR;

namespace Univali.Application.Features.AnswersCollection.GetAnswersDetail;

public class GetAnswersCollectionDetailQuery : IRequest<GetAnswersCollectionDetailResponse>
{
    public string SearchQuery {get; set;} = string.Empty;
    public int PageNumber {get;set;}
    public int PageSize{get;set;}
}