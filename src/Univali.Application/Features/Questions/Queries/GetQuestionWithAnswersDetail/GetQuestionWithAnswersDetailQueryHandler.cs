using AutoMapper;
using MediatR;
using Univali.Application.Features.Common;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.Questions.Queries.GetQuestionWithAnswersDetail;

public class GetQuestionWithAnswersDetailQueryHandler : IRequestHandler<GetQuestionWithAnswersDetailQuery, GetQuestionWithAnswersDetailResponse>
{
    private readonly IPublisherRepository _questionRepository;
    private readonly IMapper _mapper;

    public GetQuestionWithAnswersDetailQueryHandler(IMapper mapper, IPublisherRepository questionRepository)
    {
        _mapper = mapper;
        _questionRepository = questionRepository;
    }

    public async Task<GetQuestionWithAnswersDetailResponse> Handle(GetQuestionWithAnswersDetailQuery request, CancellationToken cancellationToken)
    {
        GetQuestionWithAnswersDetailResponse getQuestionWithAnswersDetailResponse = new();

        if(! await _questionRepository.StudentExistsAsync(request.StudentId))
        {
            getQuestionWithAnswersDetailResponse.ErrorType = Error.NotFoundProblem;
            getQuestionWithAnswersDetailResponse.Errors.Add("Student", new string[] {"Student Not Found"});
            return getQuestionWithAnswersDetailResponse;
        }

        var questionFromDataBase = await _questionRepository.GetQuestionWithAnswersByIdAsync(request.QuestionId);

        if(questionFromDataBase == null || questionFromDataBase.StudentId != request.StudentId)
        {
            getQuestionWithAnswersDetailResponse.ErrorType = Error.NotFoundProblem;
            getQuestionWithAnswersDetailResponse.Errors.Add("Question", new string[] {"Question Not Found in this student"});
            return getQuestionWithAnswersDetailResponse;
        }

        getQuestionWithAnswersDetailResponse.Question = _mapper.Map<GetQuestionWithAnswersDetailDto>(questionFromDataBase);

        return getQuestionWithAnswersDetailResponse;
    }
}