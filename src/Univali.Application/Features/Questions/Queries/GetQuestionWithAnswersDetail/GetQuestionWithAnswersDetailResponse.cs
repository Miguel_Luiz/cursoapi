using Univali.Application.Features.Common;

namespace Univali.Application.Features.Questions.Queries.GetQuestionWithAnswersDetail;

public class GetQuestionWithAnswersDetailResponse : BaseResponse
{
    public GetQuestionWithAnswersDetailDto Question { get; set; } = new();
}