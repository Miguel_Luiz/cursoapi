using MediatR;

namespace Univali.Application.Features.Questions.Queries.GetQuestionWithAnswersDetail;

public class GetQuestionWithAnswersDetailQuery : IRequest<GetQuestionWithAnswersDetailResponse>
{
        public int QuestionId { get; set; }
        public int StudentId { get; set; }
}