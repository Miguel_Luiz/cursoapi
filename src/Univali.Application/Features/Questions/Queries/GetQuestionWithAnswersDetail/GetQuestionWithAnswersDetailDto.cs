using Univali.Domain.Entities;
using Univali.Application.Models;

namespace Univali.Application.Features.Questions.Queries.GetQuestionWithAnswersDetail;

public class GetQuestionWithAnswersDetailDto 
{
    public int QuestionId { get; set; }
    public string Questioning { get; set; } = string.Empty;
    public DateTime CreationDate { get; set; }
    public DateTime LastUpdate { get; set; }
    public string Category { get; set; } = string.Empty;
    public int LessonId { get; set; }
    public int StudentId { get; set; }
    public List<AnswerDto> Answers{ get; set; } = new();
}