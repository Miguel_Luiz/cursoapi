using MediatR;

namespace Univali.Application.Features.Questions.Queries.GetQuestionDetail;

public class GetQuestionDetailQuery : IRequest<GetQuestionDetailResponse>
{
        public int QuestionId { get; set; }
        public int StudentId { get; set; }
}