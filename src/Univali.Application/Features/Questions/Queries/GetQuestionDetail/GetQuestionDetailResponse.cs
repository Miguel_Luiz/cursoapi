using Univali.Application.Features.Common;

namespace Univali.Application.Features.Questions.Queries.GetQuestionDetail;

public class GetQuestionDetailResponse : BaseResponse
{
    public GetQuestionDetailDto Question { get; set; } = new();
}