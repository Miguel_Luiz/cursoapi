using AutoMapper;
using MediatR;
using Univali.Application.Features.Common;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.Questions.Queries.GetQuestionDetail;

public class GetQuestionDetailQueryHandler : IRequestHandler<GetQuestionDetailQuery, GetQuestionDetailResponse>
{
    private readonly IPublisherRepository _questionRepository;
    private readonly IMapper _mapper;

    public GetQuestionDetailQueryHandler(IMapper mapper, IPublisherRepository questionRepository)
    {
        _mapper = mapper;
        _questionRepository = questionRepository;
    }

    public async Task<GetQuestionDetailResponse> Handle(GetQuestionDetailQuery request, CancellationToken cancellationToken)
    {
        GetQuestionDetailResponse getQuestionDetailResponse = new();

        var studentFromDataBase = await _questionRepository.GetStudentWithQuestionsByIdAsync(request.StudentId);

        if(studentFromDataBase == null)
        {
            getQuestionDetailResponse.ErrorType = Error.NotFoundProblem;
            getQuestionDetailResponse.Errors.Add("Student", new string[] {"Student Not Found"});
            return getQuestionDetailResponse;
        }

        var questionFromDataBase = studentFromDataBase.Questions.FirstOrDefault(q => q.QuestionId == request.QuestionId);

        if(questionFromDataBase == null)
        {
            getQuestionDetailResponse.ErrorType = Error.NotFoundProblem;
            getQuestionDetailResponse.Errors.Add("Question", new string[] {"Question Not Found in this student"});
            return getQuestionDetailResponse;
        }

        getQuestionDetailResponse.Question = _mapper.Map<GetQuestionDetailDto>(questionFromDataBase);

        return getQuestionDetailResponse;
    }
}