using AutoMapper;
using MediatR;
using Univali.Application.Features.Common;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.Questions.Queries.GetQuestionsDetail;

public class GetQuestionsDetailQueryHandler : IRequestHandler<GetQuestionsDetailQuery, GetQuestionsDetailResponse>
{
    private readonly IPublisherRepository _questionRepository;
    private readonly IMapper _mapper;

    public GetQuestionsDetailQueryHandler(IMapper mapper, IPublisherRepository questionRepository)
    {
        _mapper = mapper;
        _questionRepository = questionRepository;
    }

    public async Task<GetQuestionsDetailResponse> Handle(GetQuestionsDetailQuery request, CancellationToken cancellationToken)
    {
        GetQuestionsDetailResponse getQuestionsDetailResponse = new();

        var studentFromDataBase = await _questionRepository.GetStudentWithQuestionsByIdAsync(request.StudentId);

        if(studentFromDataBase == null)
        {
            getQuestionsDetailResponse.Errors.Add("Student", new string[] {"Student Not Found"});
            getQuestionsDetailResponse.ErrorType = Error.NotFoundProblem;
            return getQuestionsDetailResponse;
        }

        var totalItemCount = studentFromDataBase.Questions.Count;

        var questionsFromDatabase = studentFromDataBase.Questions
            .Skip(request.PageSize * (request.PageNumber - 1))
            .Take(request.PageSize);

        getQuestionsDetailResponse.QuestionsDetailDto =  _mapper.Map<IEnumerable<GetQuestionsDetailDto>>(questionsFromDatabase);

        getQuestionsDetailResponse.PaginationMetadata = new PaginationMetadata (totalItemCount, request.PageSize, request.PageNumber);

        return getQuestionsDetailResponse;
    }
}