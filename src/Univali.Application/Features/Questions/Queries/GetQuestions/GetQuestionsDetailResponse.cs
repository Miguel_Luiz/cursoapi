using Univali.Application.Features.Common;

namespace Univali.Application.Features.Questions.Queries.GetQuestionsDetail;

public class GetQuestionsDetailResponse : BaseResponse
{
    public IEnumerable<GetQuestionsDetailDto> QuestionsDetailDto { get; set; } = null!;
    public PaginationMetadata PaginationMetadata{ get; set; } = null!;
}