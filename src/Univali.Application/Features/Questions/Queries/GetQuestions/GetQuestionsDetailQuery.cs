using MediatR;

namespace Univali.Application.Features.Questions.Queries.GetQuestionsDetail;

public class GetQuestionsDetailQuery : IRequest<GetQuestionsDetailResponse>
{
        public int StudentId { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
}