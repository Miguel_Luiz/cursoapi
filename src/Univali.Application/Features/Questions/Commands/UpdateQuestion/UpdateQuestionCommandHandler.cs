using AutoMapper;
using FluentValidation;
using FluentValidation.Results;
using MediatR;
using Univali.Domain.Entities;
using Univali.Application.Features.Common;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.Questions.Commands.UpdateQuestion;

public class UpdateQuestionCommandHandler : IRequestHandler<UpdateQuestionCommand, UpdateQuestionCommandResponse>
{
    private readonly IPublisherRepository _questionRepository;
    private readonly IMapper _mapper;
    private readonly IValidator<UpdateQuestionCommand> _validator;

    public UpdateQuestionCommandHandler(IPublisherRepository questionRepository, IMapper mapper, IValidator<UpdateQuestionCommand> validator)
    {
        _questionRepository = questionRepository;
        _mapper = mapper;
        _validator = validator;
    }

    public async Task<UpdateQuestionCommandResponse> Handle(UpdateQuestionCommand request, CancellationToken cancellationToken)
    {
        UpdateQuestionCommandResponse updateQuestionCommandResponse = new();

        ValidationResult validationResult = _validator.Validate(request);

        if(!validationResult.IsValid)
        {
            updateQuestionCommandResponse.FillErrors(validationResult);
            updateQuestionCommandResponse.ErrorType = Error.ValidationProblem;
            return updateQuestionCommandResponse;
        }

        var studentEntity = await _questionRepository.GetStudentWithQuestionsByIdAsync(request.StudentId);

        if (studentEntity == null)
        {
            updateQuestionCommandResponse.Errors.Add("Student", new string[]{"Question Not Found"});
            updateQuestionCommandResponse.ErrorType = Error.NotFoundProblem;
            return updateQuestionCommandResponse;
        }

        var questionEntity = studentEntity.Questions.FirstOrDefault(s => s.QuestionId == request.QuestionId);

        if (questionEntity == null) 
        {
            updateQuestionCommandResponse.Errors.Add("Question", new string[]{"Question not found in this student"});
            updateQuestionCommandResponse.ErrorType = Error.NotFoundProblem;
            return updateQuestionCommandResponse;
        }

        _mapper.Map(request, questionEntity);

        await _questionRepository.SaveChangesAsync();

        return updateQuestionCommandResponse;
    }
}