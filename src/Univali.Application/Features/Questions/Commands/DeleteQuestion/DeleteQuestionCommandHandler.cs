using AutoMapper;
using MediatR;
using Univali.Application.Features.Common;
using Univali.Application.Contracts.Repositories;


namespace Univali.Application.Features.Questions.Commands.DeleteQuestion;

public class DeleteQuestionCommandHandler : IRequestHandler<DeleteQuestionCommand, DeleteQuestionCommandResponse>
{
    private readonly IPublisherRepository _questionRepository;

    public DeleteQuestionCommandHandler(IPublisherRepository questionRepository)
    {
        _questionRepository = questionRepository;
    }

    public async Task<DeleteQuestionCommandResponse> Handle(DeleteQuestionCommand request, CancellationToken cancellationToken)
    {
        DeleteQuestionCommandResponse deleteQuestionCommandResponse = new();

        var studentEntity = await _questionRepository.GetStudentWithQuestionsByIdAsync(request.StudentId);

        if(studentEntity == null)
        {
            deleteQuestionCommandResponse.Errors.Add("Student", new string[] {"Student Not Found"});
            deleteQuestionCommandResponse.ErrorType = Error.NotFoundProblem;
            return deleteQuestionCommandResponse;
        }

        var questionEntity = studentEntity.Questions.FirstOrDefault(s => s.QuestionId == request.QuestionId);
        
        if(questionEntity == null)
        {
            deleteQuestionCommandResponse.Errors.Add("Question", new string[] {"Question not found in this student"});
            deleteQuestionCommandResponse.ErrorType = Error.NotFoundProblem;
            return deleteQuestionCommandResponse;
        }

        _questionRepository.DeleteQuestion(questionEntity);
        await _questionRepository.SaveChangesAsync();
        return deleteQuestionCommandResponse;
    }
}