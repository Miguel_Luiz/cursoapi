using MediatR;

namespace Univali.Application.Features.Questions.Commands.DeleteQuestion;

public class DeleteQuestionCommand : IRequest<DeleteQuestionCommandResponse>
{
    public int QuestionId { get; set; }
    public int StudentId { get; set; }
}