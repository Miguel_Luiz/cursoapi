using Univali.Application.Features.Common;

namespace Univali.Application.Features.Questions.Commands.CreateQuestion;

public class CreateQuestionCommandResponse : BaseResponse
{
    public CreateQuestionDto Question { get; set; } = default!;
}