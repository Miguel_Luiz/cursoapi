using AutoMapper;
using FluentValidation;
using FluentValidation.Results;
using MediatR;
using Univali.Domain.Entities;
using Univali.Application.Features.Common;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.Questions.Commands.CreateQuestion;

public class CreateQuestionCommandHandler : IRequestHandler<CreateQuestionCommand, CreateQuestionCommandResponse>
{
    private readonly IPublisherRepository _questionRepository;
    private readonly IMapper _mapper;
    private readonly IValidator<CreateQuestionCommand> _validator;

    public CreateQuestionCommandHandler(IPublisherRepository questionRepository, IMapper mapper, IValidator<CreateQuestionCommand> validator)
    {
        _questionRepository = questionRepository;
        _mapper = mapper;
        _validator = validator;
    }

    public async Task<CreateQuestionCommandResponse> Handle(CreateQuestionCommand request, CancellationToken cancellationToken)
    {
        CreateQuestionCommandResponse createQuestionCommandResponse = new();

        ValidationResult validationResult = _validator.Validate(request);

        if(!validationResult.IsValid)
        {
            createQuestionCommandResponse.FillErrors(validationResult);
            createQuestionCommandResponse.ErrorType = Error.ValidationProblem;
            return createQuestionCommandResponse;
        }

        var studentFromDatabase = await _questionRepository.GetStudentWithCoursesByIdAsync(request.StudentId);

        if (studentFromDatabase == null)
        {
            createQuestionCommandResponse.ErrorType = Error.NotFoundProblem;
            createQuestionCommandResponse.Errors.Add("Student",new string[] {"Student Not Found"});
            return createQuestionCommandResponse;
        }

        var lessonFromDatabase = await _questionRepository.GetLessonByIdAsync(request.LessonId);

        var moduleFromDatabase = await _questionRepository.GetModuleByIdAsync(lessonFromDatabase!.ModuleId);

        if(!studentFromDatabase.Courses.Any(c => c.CourseId == moduleFromDatabase!.CourseId))
        {
            createQuestionCommandResponse.ErrorType = Error.NotFoundProblem;
            createQuestionCommandResponse.Errors.Add("Lesson",new string[] {"Lesson Not Found in courses this student"});
            return createQuestionCommandResponse;
        }

        var questionEntity = _mapper.Map<Question>(request);

        _questionRepository.AddQuestion(questionEntity);
        await _questionRepository.SaveChangesAsync();

        createQuestionCommandResponse.Question = _mapper.Map<CreateQuestionDto>(questionEntity);
        return createQuestionCommandResponse;
    }
}