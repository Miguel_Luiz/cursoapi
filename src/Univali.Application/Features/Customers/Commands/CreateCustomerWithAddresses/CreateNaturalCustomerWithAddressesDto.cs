namespace Univali.Application.Features.Customers.Commands.CreateCustomerWithAddresses;

public class CreateNaturalCustomerWithAddressesDto : CreateCustomerWithAddressesDto
{
    public string? CPF {get;set;}

}

