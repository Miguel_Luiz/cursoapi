
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Customers.Commands.CreateCustomerWithAddresses;

public class CreateCustomerWithAddressesCommandResponse : BaseResponse
{
    public CreateCustomerWithAddressesDto Customer {get; set;} = default!;

    public CreateCustomerWithAddressesCommandResponse()
    {
        Errors = new Dictionary<string, string[]>();
    }
}