using System.ComponentModel.DataAnnotations;
using MediatR;
using Univali.Application.Models;
using Univali.Application.ValidationAttributes;

namespace Univali.Application.Features.Customers.Commands.CreateCustomerWithAddresses;

public class CreateCustomerWithAddressesCommand : IRequest<CreateCustomerWithAddressesCommandResponse>
{
  public string Name {get; set;} = string.Empty;
  public string? CPF {get; set;}

  public string? CNPJ {get; set;}
  public List<AddressDto> Addresses { get; set; } = new();
}

