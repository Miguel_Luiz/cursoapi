using Univali.Application.Models;

namespace Univali.Application.Features.Customers.Commands.CreateCustomerWithAddresses;

public class CreateCustomerWithAddressesDto
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public List<AddressDto> Addresses { get; set; } = new();
}

