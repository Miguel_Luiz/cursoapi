using AutoMapper;
using FluentValidation;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Univali.Domain.Entities;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Customers.Commands.CreateCustomerWithAddresses;

// O primeiro parâmetro é o tipo da mensagem
// O segundo parâmetro é o tipo que se espera receber.
public class CreateCustomerWithAddressesCommandHandler: IRequestHandler<CreateCustomerWithAddressesCommand, CreateCustomerWithAddressesCommandResponse>
{
    private readonly ICustomerRepository _customerRepository;
    private readonly IMapper _mapper;
    private readonly IValidator<CreateCustomerWithAddressesCommand> _validator;

    public CreateCustomerWithAddressesCommandHandler(ICustomerRepository customerRepository, IMapper mapper, IValidator<CreateCustomerWithAddressesCommand> validator)
    {
        _customerRepository = customerRepository;
        _mapper = mapper;
        _validator = validator;
    }

    public async Task<CreateCustomerWithAddressesCommandResponse> Handle(CreateCustomerWithAddressesCommand request, CancellationToken cancellationToken)
    {
        CreateCustomerWithAddressesCommandResponse response = new();

        var validationResult = _validator.Validate(request);

            if(!validationResult.IsValid)
            {
                foreach(var error in validationResult.ToDictionary())
                {
                    response.Errors.Add(error.Key, error.Value);
                }

                response.ErrorType = Error.ValidationProblem;
                return response;
            }
         if(!string.IsNullOrEmpty(request.CPF) && !string.IsNullOrEmpty(request.CNPJ))
        {
            string[] error = {"More than one document informed"};
            response.Errors.Add("DocumentType", error);
            response.ErrorType = Error.BadRequestProblem;
            return response;
        }

        if(!string.IsNullOrEmpty(request.CPF))
        {
            var naturalCustomerEntity = _mapper.Map<NaturalCustomer>(request);


            _customerRepository.AddCustomer(naturalCustomerEntity);
            await _customerRepository.SaveChangesAsync();
            response.Customer = _mapper.Map<CreateNaturalCustomerWithAddressesDto>(naturalCustomerEntity);
        }
        else if (!string.IsNullOrEmpty(request.CNPJ))
        {
            var legalCustomerEntity = _mapper.Map<LegalCustomer>(request);

            _customerRepository.AddCustomer(legalCustomerEntity);
            await _customerRepository.SaveChangesAsync();
            response.Customer = _mapper.Map<CreateLegalWithAddressesCustomerDto>(legalCustomerEntity);
        }
        else 
        {
            string[] error = {"No documents  were informed"};
            response.Errors.Add("DocumentType", error);
            response.ErrorType = Error.BadRequestProblem;
            
            return response;
        }

        // var customerEntity = _mapper.Map<Customer>(request);

        // _customerRepository.AddCustomer(customerEntity);
        // await _customerRepository.SaveChangesAsync();

        // response.Customer = _mapper.Map<CreateCustomerWithAddressesDto>(customerEntity);

        return response;
    }
}