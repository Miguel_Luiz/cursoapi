using Univali.Application.Features.Common;

namespace Univali.Application.Features.Customers.Commands.DeleteCustomer;

public class DeleteCustomerCommandResponse: BaseResponse
{

    public DeleteCustomerCommandResponse()
    {
        Errors = new Dictionary<string, string[]>();
    }
}