using FluentValidation;

namespace Univali.Application.Features.Customers.Commands.DeleteCustomer;

public class DeleteCustomerCommandValidator :
    AbstractValidator<DeleteCustomerCommand>
 {
    public DeleteCustomerCommandValidator() 
    {
        RuleFor(a => a.CustomerId)
        .NotEmpty()
        .WithMessage("Fill an Id")      
        .GreaterThan(0)
        .WithMessage("The CustomerId must be greater than zero.");
    }
 }