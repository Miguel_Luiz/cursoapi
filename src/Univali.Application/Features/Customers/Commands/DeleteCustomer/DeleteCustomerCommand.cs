using MediatR;

namespace Univali.Application.Features.Customers.Commands.DeleteCustomer;

public  class DeleteCustomerCommand: IRequest<DeleteCustomerCommandResponse>
{
    public int CustomerId { get; set; }
}