using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Domain.Entities;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Customers.Commands.DeleteCustomer;
public class DeleteCustomerCommandHandler : IRequestHandler<DeleteCustomerCommand, DeleteCustomerCommandResponse>
{
    private readonly ICustomerRepository _customerRepository;
    public readonly IValidator<DeleteCustomerCommand> _validator;

    public DeleteCustomerCommandHandler(ICustomerRepository customerRepository, IValidator<DeleteCustomerCommand> validator)
    {
        _customerRepository = customerRepository;
        _validator = validator;
    }

    public async Task<DeleteCustomerCommandResponse> Handle(DeleteCustomerCommand request, CancellationToken cancellationToken)
    {
       DeleteCustomerCommandResponse response = new();
       var validationResult = _validator.Validate(request);

        if(validationResult.IsValid == false){
            foreach (var error in validationResult.ToDictionary()) 
            {
                response.Errors
                    .Add(error.Key, error.Value);
            }
            response.ErrorType = Error.ValidationProblem;

            return response;
        }

        var customerFromDatabase = await _customerRepository.GetCustomerByIdAsync(request.CustomerId);
        if(customerFromDatabase == null){
            response.Errors.Add(
                "Customer",
                new string[] { $"Customer with id = {request.CustomerId} was not found in the database" }
            );

            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        _customerRepository.DeleteCustomer(customerFromDatabase);

        await _customerRepository.SaveChangesAsync();

        return response;
    }
}