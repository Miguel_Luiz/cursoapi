namespace Univali.Application.Features.Customers.Commands.CreateCustomer;

public class CreateCustomerDto
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
}

