namespace Univali.Application.Features.Customers.Commands.CreateCustomer;

public class CreateLegalCustomerDto : CreateCustomerDto
{
    public string? CNPJ {get;set;}

}

