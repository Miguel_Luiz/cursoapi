namespace Univali.Application.Features.Customers.Commands.CreateCustomer;

public class CreateNaturalCustomerDto : CreateCustomerDto
{
    public string? CPF {get;set;}

}

