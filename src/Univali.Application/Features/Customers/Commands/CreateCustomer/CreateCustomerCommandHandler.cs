using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Domain.Entities;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;


namespace Univali.Application.Features.Customers.Commands.CreateCustomer;

// O primeiro parâmetro é o tipo da mensagem
// O segundo parâmetro é o tipo que se espera receber.
public class CreateCustomerCommandHandler: IRequestHandler<CreateCustomerCommand, CreateCustomerCommandResponse>
{
    private readonly ICustomerRepository _customerRepository;
    private readonly IMapper _mapper;
    private readonly IValidator<CreateCustomerCommand> _validator;

    public CreateCustomerCommandHandler(ICustomerRepository customerRepository, IMapper mapper, IValidator<CreateCustomerCommand> validator)
    {
        _customerRepository = customerRepository;
        _mapper = mapper;
        _validator = validator;
    }

    public async Task<CreateCustomerCommandResponse> Handle(CreateCustomerCommand request, CancellationToken cancellationToken)
    {
        CreateCustomerCommandResponse response = new();

        var validationResult = _validator.Validate(request);

        if(!validationResult.IsValid)
        {
            foreach(var error in validationResult.ToDictionary())
            {
                response.Errors.Add(error.Key, error.Value);
            }

            response.ErrorType = Error.ValidationProblem;
            return response;
        }



        if(!string.IsNullOrEmpty(request.CPF) && !string.IsNullOrEmpty(request.CNPJ))
        {
            string[] error = {"More than one document informed"};
            response.Errors.Add("DocumentType", error);
            response.ErrorType = Error.BadRequestProblem;
            return response;
        }

        if(!string.IsNullOrEmpty(request.CPF))
        {
            var customerEntity = _mapper.Map<NaturalCustomer>(request);


            _customerRepository.AddCustomer(customerEntity);
            await _customerRepository.SaveChangesAsync();
            response.Customer = _mapper.Map<CreateNaturalCustomerDto>(customerEntity);
        }
        else if (!string.IsNullOrEmpty(request.CNPJ))
        {
            var customerEntity = _mapper.Map<LegalCustomer>(request);

            _customerRepository.AddCustomer(customerEntity);
            await _customerRepository.SaveChangesAsync();
            response.Customer = _mapper.Map<CreateLegalCustomerDto>(customerEntity);
        }
        else 
        {
            string[] error = {"No documents  were informed"};
            response.Errors.Add("DocumentType", error);
            response.ErrorType = Error.BadRequestProblem;
            return response;
        }
        //var publisherEntity = _mapper.Map<Publisher>(createPublisherCommand);

        
    
        return response;

        // var customerEntity = _mapper.Map<Customer>(request);

        // _customerRepository.AddCustomer(customerEntity);
        // await _customerRepository.SaveChangesAsync();

        // response.Customer = _mapper.Map<CreateCustomerDto>(customerEntity);
        // return response;
    }
}