using Univali.Application.Features.Common;

namespace Univali.Application.Features.Customers.Commands.CreateCustomer;

public class CreateCustomerCommandResponse : BaseResponse
{
    public CreateCustomerDto Customer {get; set;} = default!;

    public CreateCustomerCommandResponse()
    {
        Errors = new Dictionary<string, string[]>();
    }
}