using FluentValidation;

namespace Univali.Application.Features.Customers.Commands.PartiallyUpdateCustomer;

public class PartiallyUpdateCustomerCommandValidator : AbstractValidator<PartiallyUpdateCustomerCommand>
{
    public PartiallyUpdateCustomerCommandValidator()
    {
        RuleFor(c => c.Id)
            .NotEmpty() //Não pode ser um valor vazio ou null.
                .WithMessage("You should fill out an Id") //Caso seja vazio ou nulo aparece a mensagem.
            .GreaterThan(0) //O valor deve ser maior que 0.
                .WithMessage("The Id should be greater than 0"); //Caso seja menor que 0 aparece a mensagem.

        RuleFor(c => c.Name)
            .MaximumLength(50) //Máximo de caracteres permitido
                .WithMessage("The Name shouldn't have more than 50 characteres"); //Caso tenha mais de 50 aparece essa mensagem

        RuleFor(c => c.CPF)
            .Length(11) //O tamanho deve ser 11. 
                .WithMessage("The CPF should have 11 characters") //Caso não seja 11, aparecerá essa mensagem.
            .Must(ValidateCPF) //Validação do Cpf.
                .When(c => c.CPF != null, ApplyConditionTo.CurrentValidator) 
                .WithMessage("The CPf should be valid number"); //Caso o CPF não seja válido, aparecerá essa mensagem

        RuleFor(c => c.CNPJ)
            .Length(14) //O tamanho deve ser 14.
                .WithMessage("The CNPJ should have 14 characters") //Caso não seja 14, aparecerá essa mensagem.
            .Must(ValidateCNPJ) //Validação do CNPJ.
                .When(c => c.CNPJ != null, ApplyConditionTo.CurrentValidator)
                .WithMessage("The CNPJ should be valid number"); //Caso o CNPJ não seja válido, aparecerá essa mensagem.
    }

    private bool ValidateCNPJ(string cnpj) // GPT
    {
        cnpj = cnpj.Replace(".", "").Replace("/", "").Replace("-", "");
        if (cnpj.Length != 14) {
            return false;
        }
        int[] multiplicators1 = { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
        int[] multiplicators2 = { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
        int sum = 0;
        for (int i = 0; i < 12; i++) {
            sum += int.Parse(cnpj[i].ToString()) * multiplicators1[i];
        }
        int remainder = sum % 11;
        int verificationDigit1 = remainder < 2 ? 0 : 11 - remainder;
        if (int.Parse(cnpj[12].ToString()) != verificationDigit1) {
            return false;
        }
        sum = 0;
        for (int i = 0; i < 13; i++) {
            sum += int.Parse(cnpj[i].ToString()) * multiplicators2[i];
        }
        remainder = sum % 11;
        int verificationDigit2 = remainder < 2 ? 0 : 11 - remainder;
        if (int.Parse(cnpj[13].ToString()) != verificationDigit2) {
            return false;
        }
        return true;
    }

    private bool ValidateCPF(string cpf) //Código de validação do CPF.
    {
        // Remove non-numeric characters
        cpf = cpf.Replace(".", "").Replace("-", "");

        // Check if it has 11 digits
        if (cpf.Length != 11)
        {
            return false;
        }

        // Check if all digits are the same
        bool allDigitsEqual = true;
        for (int i = 1; i < cpf.Length; i++)
        {
            if (cpf[i] != cpf[0])
            {
                allDigitsEqual = false;
                break;
            }
        }
        if (allDigitsEqual)
        {
            return false;
        }

        // Check first verification digit
        int sum = 0;
        for (int i = 0; i < 9; i++)
        {
            sum += int.Parse(cpf[i].ToString()) * (10 - i);
        }
        int remainder = sum % 11;
        int verificationDigit1 = remainder < 2 ? 0 : 11 - remainder;
        if (int.Parse(cpf[9].ToString()) != verificationDigit1)
        {
            return false;
        }

        // Check second verification digit
        sum = 0;
        for (int i = 0; i < 10; i++)
        {
            sum += int.Parse(cpf[i].ToString()) * (11 - i);
        }
        remainder = sum % 11;
        int verificationDigit2 = remainder < 2 ? 0 : 11 - remainder;
        if (int.Parse(cpf[10].ToString()) != verificationDigit2)
        {
            return false;
        }

        return true;
    }
}