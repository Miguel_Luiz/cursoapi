using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.JsonPatch;
using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Domain.Entities;
using Univali.Application.Models;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Customers.Commands.PartiallyUpdateCustomer;

// O primeiro parâmetro é o tipo da mensagem
// O segundo parâmetro é o tipo que se espera receber.
public class PartiallyUpdateCustomerCommandHandler: IRequestHandler<PartiallyUpdateCustomerCommand, PartiallyUpdateCustomerCommandResponse>
{
    private readonly ICustomerRepository _customerRepository;
    private readonly IMapper _mapper;
    private readonly IValidator<PartiallyUpdateCustomerCommand> _validator;

    public PartiallyUpdateCustomerCommandHandler(ICustomerRepository customerRepository, IMapper mapper, IValidator<PartiallyUpdateCustomerCommand> validator)
    {
        _customerRepository = customerRepository;
        _mapper = mapper;
        _validator = validator;
    }

    public async Task<PartiallyUpdateCustomerCommandResponse> Handle(PartiallyUpdateCustomerCommand request, CancellationToken cancellationToken)
    {
        PartiallyUpdateCustomerCommandResponse response = new();

        var validationResultForRequest = _validator.Validate(request);

        if(!validationResultForRequest.IsValid)
        {
            foreach(var error in validationResultForRequest.ToDictionary())
            {
                response.Errors.Add(error.Key, error.Value);
            }

            response.ErrorType = Error.ValidationProblem;
            return response;
        }

        if(!string.IsNullOrEmpty(request.CPF) && !string.IsNullOrEmpty(request.CNPJ))
        {
            string[] error = {"More than one document informed"};
            response.Errors.Add("DocumentType", error);
            response.ErrorType = Error.BadRequestProblem;
            return response;
        }

        var customerFromDatabase = await _customerRepository.GetCustomerByIdAsync(request.Id);

        if(customerFromDatabase == null)
        {
            string[] error = {$"Customer with id = {request.Id} was not found in the database"};
            response.Errors.Add("Customer", error);
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var operationsPaths = request.PatchDocument.Operations.Select(operation => operation.path).ToList();

        if (customerFromDatabase is LegalCustomer && !VerifyDocumentTypeCnpj(operationsPaths))
        {
            string[] error = {"Document type is not valid for this customer"};
            response.Errors.Add("DocumentType", error);
            response.ErrorType = Error.BadRequestProblem;
            return response;
        }

        if (customerFromDatabase is NaturalCustomer && !VerifyDocumentTypeCpf(operationsPaths))
        {
            string[] error = {"Document type is not valid for this customer"};
            response.Errors.Add("DocumentType", error);
            response.ErrorType = Error.BadRequestProblem;
            return response;
        }

        var customerToPatch = _mapper.Map<PartiallyUpdateCustomerCommand>(customerFromDatabase); 

        request.PatchDocument.ApplyTo(customerToPatch);

        var validationResult = _validator.Validate(customerToPatch);

        if(!validationResult.IsValid)
        {
            foreach(var error in validationResult.ToDictionary())
            {
                response.Errors.Add(error.Key, error.Value);
            }

            response.ErrorType = Error.ValidationProblem;
            return response;
        } 

        _customerRepository.PatchCustomer(customerFromDatabase, customerToPatch);
        await _customerRepository.SaveChangesAsync();

        return response;
    }

    private bool VerifyDocumentTypeCnpj(List<string> operationsPaths)
    {
        foreach(var path in operationsPaths)
            if(path.ToLower().Contains("cnpj")) 
                return true;

        return false;
    }

    private bool VerifyDocumentTypeCpf(List<string> operationsPaths)
    {
        foreach(var path in operationsPaths)
            if(path.ToLower().Contains("cpf"))
                return true;

        return false;
    }
}