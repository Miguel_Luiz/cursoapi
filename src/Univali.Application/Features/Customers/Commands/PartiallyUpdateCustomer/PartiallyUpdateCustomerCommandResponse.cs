using Univali.Application.Features.Common;

namespace Univali.Application.Features.Customers.Commands.PartiallyUpdateCustomer;

public class PartiallyUpdateCustomerCommandResponse : BaseResponse
{

    public PartiallyUpdateCustomerCommandResponse()
    {
        Errors = new Dictionary<string, string[]>();
    }
}