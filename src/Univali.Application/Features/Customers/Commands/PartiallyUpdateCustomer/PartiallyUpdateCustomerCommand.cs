using MediatR;
using Microsoft.AspNetCore.JsonPatch;
using Univali.Domain.Entities;
using Univali.Application.Models;

namespace Univali.Application.Features.Customers.Commands.PartiallyUpdateCustomer;

public class PartiallyUpdateCustomerCommand : IRequest<PartiallyUpdateCustomerCommandResponse>
{
  public int Id { get; set; }
  public string Name {get; set;} = string.Empty;
  public string? CPF {get; set;}
  public string? CNPJ {get; set;}
  public ICollection<Address> Addresses {get; set;} = new List<Address>();
  public ICollection<Order> Orders { get; set; } = new List<Order>();
  public JsonPatchDocument<PartiallyUpdateCustomerCommand> PatchDocument { get; set; } = null!;
}

