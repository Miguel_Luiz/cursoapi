using Univali.Application.Features.Common;

namespace Univali.Application.Features.Customers.Commands.UpdateCustomer;

public class UpdateCustomerCommandResponse : BaseResponse
{
    public UpdateCustomerCommandResponse()
    {
        Errors = new Dictionary<string, string[]>();
    }
}