using System.ComponentModel.DataAnnotations;
using MediatR;
using Univali.Application.ValidationAttributes;

namespace Univali.Application.Features.Customers.Commands.UpdateCustomer;

public  class UpdateCustomerCommand: IRequest<UpdateCustomerCommandResponse>
{
    public int Id {get; set;}

    public string Name { get; set; } = string.Empty;

    public string? CPF { get; set; }

    public string? CNPJ {get;set;}
}