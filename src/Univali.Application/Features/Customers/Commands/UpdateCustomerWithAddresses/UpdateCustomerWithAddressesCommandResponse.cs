using Univali.Application.Features.Common;

namespace Univali.Application.Features.Customers.Commands.UpdateCustomerWithAddresses;

public class UpdateCustomerWithAddressesCommandResponse : BaseResponse
{
    public UpdateCustomerWithAddressesCommandResponse()
    {
        Errors = new Dictionary<string, string[]>();
    }
}