using MediatR;
using Univali.Application.Models;

namespace Univali.Application.Features.Customers.Commands.UpdateCustomerWithAddresses;

public  class UpdateCustomerWithAddressesCommand: IRequest<UpdateCustomerWithAddressesCommandResponse>
{
  public int Id {get; set;}

    public string Name { get; set; } = string.Empty;

    public string ?CPF { get; set; }

    public string ?CNPJ {get;set;}
  public List<AddressDto> Addresses { get; set; } = new();
}