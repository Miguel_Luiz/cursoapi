using FluentValidation;
using Univali.Application.Models;

namespace Univali.Application.Features.Customers.Commands.UpdateCustomerWithAddresses;

public class UpdateCustomerWithAddressesCommandValidator : AbstractValidator<UpdateCustomerWithAddressesCommand>
{
    public UpdateCustomerWithAddressesCommandValidator()
    {
        RuleFor(c => c.Name)
            .NotEmpty() //Não pode ser um valor vazio ou null.
                .WithMessage("You should fill out a Name") //Caso seja vazio ou nulo aparece a mensagem.
            .MaximumLength(50) //Máximo de caracteres permitido
                .WithMessage("The {PropertyName} shouldn't have more than 50 characteres"); //Caso tenha mais de 50 aparece essa mensagem

        RuleFor(c => c.CPF)
            .Cascade(CascadeMode.Stop)
            .Length(11) //O tamanho deve ser 11. 
                .WithMessage("The CPF should have 11 characters") //Caso não seja 11, aparecerá essa mensagem.
            .Must(ValidateCPF) //Validação do Cpf.
                .When(c => !string.IsNullOrEmpty(c.CPF)) 
                .WithMessage("The CPf should be valid number"); //Caso o CPF não seja válido, aparecerá essa mensagem
        RuleFor(c => c.CNPJ)
            .Cascade(CascadeMode.Stop)
            .Length(14)
            .WithMessage("CNPJ must have 14 characters")
            .Must(ValidateCNPJ)
            .When(c => !string.IsNullOrEmpty(c.CNPJ))
            .WithMessage("CNPJ must be valid");
    }

    private bool ValidateCPF(string cpf) //Código de validação do CPF.
    {
        // Remove non-numeric characters
        cpf = cpf.Replace(".", "").Replace("-", "");

        // Check if it has 11 digits
        if (cpf.Length != 11)
        {
            return false;
        }

        // Check if all digits are the same
        bool allDigitsEqual = true;
        for (int i = 1; i < cpf.Length; i++)
        {
            if (cpf[i] != cpf[0])
            {
                allDigitsEqual = false;
                break;
            }
        }
        if (allDigitsEqual)
        {
            return false;
        }

        // Check first verification digit
        int sum = 0;
        for (int i = 0; i < 9; i++)
        {
            sum += int.Parse(cpf[i].ToString()) * (10 - i);
        }
        int remainder = sum % 11;
        int verificationDigit1 = remainder < 2 ? 0 : 11 - remainder;
        if (int.Parse(cpf[9].ToString()) != verificationDigit1)
        {
            return false;
        }

        // Check second verification digit
        sum = 0;
        for (int i = 0; i < 10; i++)
        {
            sum += int.Parse(cpf[i].ToString()) * (11 - i);
        }
        remainder = sum % 11;
        int verificationDigit2 = remainder < 2 ? 0 : 11 - remainder;
        if (int.Parse(cpf[10].ToString()) != verificationDigit2)
        {
            return false;
        }

        return true;
    }
    private bool ValidateCNPJ(string cnpj) // GPT
    {
        cnpj = cnpj.Replace(".", "").Replace("/", "").Replace("-", "");
        if (cnpj.Length != 14) {
            return false;
        }
        int[] multiplicators1 = { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
        int[] multiplicators2 = { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
        int sum = 0;
        for (int i = 0; i < 12; i++) {
            sum += int.Parse(cnpj[i].ToString()) * multiplicators1[i];
        }
        int remainder = sum % 11;
        int verificationDigit1 = remainder < 2 ? 0 : 11 - remainder;
        if (int.Parse(cnpj[12].ToString()) != verificationDigit1) {
            return false;
        }
        sum = 0;
        for (int i = 0; i < 13; i++) {
            sum += int.Parse(cnpj[i].ToString()) * multiplicators2[i];
        }
        remainder = sum % 11;
        int verificationDigit2 = remainder < 2 ? 0 : 11 - remainder;
        if (int.Parse(cnpj[13].ToString()) != verificationDigit2) {
            return false;
        }
        return true;
    }
}

public class AddressDtoValidator : AbstractValidator<AddressDto>
{
    public AddressDtoValidator()
    {
        RuleFor(a => a.City)
        .NotEmpty()
        .WithMessage("You should fill out a City")
        .MaximumLength(50)
        .WithMessage("The {PropertyName} shouldn't have more than 50 characteres");

        RuleFor(a => a.Street)
        .NotEmpty()
        .WithMessage("You should fill out a Street")
        .MaximumLength(50)
        .WithMessage("The {PropertyName} shouldn't have more than 50 characteres");
    }
}