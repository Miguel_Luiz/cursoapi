using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Domain.Entities;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Customers.Commands.UpdateCustomerWithAddresses;

public class UpdateCustomerWithAddressesCommandHandler : IRequestHandler<UpdateCustomerWithAddressesCommand, UpdateCustomerWithAddressesCommandResponse>
{
    private readonly ICustomerRepository _customerRepository;
    private readonly IMapper _mapper;
    private readonly IValidator<UpdateCustomerWithAddressesCommand> _validator;

    public UpdateCustomerWithAddressesCommandHandler(ICustomerRepository customerRepository, IMapper mapper, IValidator<UpdateCustomerWithAddressesCommand> validator)
    {
        _customerRepository = customerRepository;
        _mapper = mapper;
        _validator = validator;
    }

    public async Task<UpdateCustomerWithAddressesCommandResponse> Handle(UpdateCustomerWithAddressesCommand request, CancellationToken cancellationToken)
    {
        UpdateCustomerWithAddressesCommandResponse response = new();

        var validationResult = _validator.Validate(request);

        if(!validationResult.IsValid)
        {
            foreach(var error in validationResult.ToDictionary())
            {
                response.Errors.Add(error.Key, error.Value);
            }

            response.ErrorType = Error.ValidationProblem;
            return response;
        }

        if(!string.IsNullOrEmpty(request.CPF) && !string.IsNullOrEmpty(request.CNPJ))
        {
            string[] error = {"More than one document informed"};
            response.Errors.Add("DocumentType", error);
            response.ErrorType = Error.BadRequestProblem;
            return response;
        }

         var customerFromDatabase = await _customerRepository.GetCustomerByIdAsync(request.Id);

        if(customerFromDatabase == null) 
        {
            response.Errors.Add(
                "Publisher",
                new string[] { $"Customer with id = {request.Id} was not found in the database." }
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }
        else if (customerFromDatabase is LegalCustomer legalCustomer) {
            if (string.IsNullOrEmpty(request.CNPJ)) {
                string[] error = { "No CNPJ informed" };
                response.Errors.Add("DocumentType", error);
                response.ErrorType = Error.BadRequestProblem;
                return response;
            }

        }
        else if (customerFromDatabase is NaturalCustomer naturalCustomer) {
            if (string.IsNullOrEmpty(request.CPF)) {
                string[] error = { "No CPF informed" };
                response.Errors.Add("DocumentType", error);
                response.ErrorType = Error.BadRequestProblem;
                return response;
            }
        }

        _customerRepository.UpdateCustomer(customerFromDatabase, request);
        await _customerRepository.SaveChangesAsync();

        return response;
    }
}