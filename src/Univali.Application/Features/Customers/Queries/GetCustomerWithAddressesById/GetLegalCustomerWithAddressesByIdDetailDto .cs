using Univali.Application.Models;

namespace Univali.Application.Features.Customers.Queries.GetCustomerWithAddressesByIdDetail;

public class GetLegalCustomerWithAddressesByIdDetailDto : GetCustomerWithAddressesByIdDetailDto
{
  public string? CNPJ{ get; set; }
}
