// BRANCH: AULA
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Customers.Queries.GetCustomerWithAddressesByIdDetail;

public class GetCustomerWithAddressByIdQueryResponse : BaseResponse
{


    public GetCustomerWithAddressesByIdDetailDto Customer {get; set;} = default!;

    public GetCustomerWithAddressByIdQueryResponse()
    {
        Errors = new Dictionary<string, string[]>();
    }
}