using Univali.Application.Models;

namespace Univali.Application.Features.Customers.Queries.GetCustomerWithAddressesByIdDetail;

public class GetCustomerWithAddressesByIdDetailDto
{
  public int Id { get; set; }
  public string Name { get; set; } = string.Empty;
  // public string Cpf { get; set; } = string.Empty;

  public List<AddressDto> Addresses { get; set; } = new();
}
