using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Domain.Entities;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Customers.Queries.GetCustomerWithAddressesByIdDetail;

public class GetCustomerWithAddressByIdDetailQueryHandler : 
    IRequestHandler<GetCustomerWithAddressesByIdDetailQuery, GetCustomerWithAddressByIdQueryResponse?>
{
    private readonly ICustomerRepository _customerRepository;

    private readonly IMapper _mapper;

    public readonly IValidator<GetCustomerWithAddressesByIdDetailQuery> _validator; 

    public GetCustomerWithAddressByIdDetailQueryHandler(
        ICustomerRepository customerRepository, 
        IMapper mapper,
        IValidator<GetCustomerWithAddressesByIdDetailQuery> validator
    ) {
        _customerRepository = customerRepository;

        _mapper = mapper;

        _validator = validator;
    }

    public async Task<GetCustomerWithAddressByIdQueryResponse?> Handle(
        GetCustomerWithAddressesByIdDetailQuery getCustomerWithAddressByIdDetailQuery, 
        CancellationToken cancellationToken
    ) {
        GetCustomerWithAddressByIdQueryResponse response = new();

        var validationResult = _validator.Validate(getCustomerWithAddressByIdDetailQuery);

        if (validationResult.IsValid == false) 
        {
            foreach (var error in validationResult.ToDictionary()) 
            {
                response.Errors
                    .Add(error.Key, error.Value);
            }

            response.ErrorType = Error.ValidationProblem;
            return response;
        }

        var customerFromDatabase = await _customerRepository
            .GetCustomerWithAddressesByIdAsync(
                getCustomerWithAddressByIdDetailQuery.CustomerId
            );
        
        if (customerFromDatabase == null) { 
            response.Errors.Add(
                "Customer",
                new string[] { $"Customer with id = {getCustomerWithAddressByIdDetailQuery.CustomerId} was not found in the database" }
            );

            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        if(customerFromDatabase is NaturalCustomer)
        {
            response.Customer = _mapper.Map<GetNaturalCustomerWithAddressesByIdDetailDto>(customerFromDatabase);
        }
        if(customerFromDatabase is LegalCustomer)
        {
            response.Customer = _mapper.Map<GetLegalCustomerWithAddressesByIdDetailDto>(customerFromDatabase);
        }


        // response.Customer = _mapper
        //     .Map<GetCustomerWithAddressesByIdDetailDto>(customerFromDatabase);

        return response;
    }
}