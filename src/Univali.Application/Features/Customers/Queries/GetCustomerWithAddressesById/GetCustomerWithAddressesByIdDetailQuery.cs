using MediatR;

namespace Univali.Application.Features.Customers.Queries.GetCustomerWithAddressesByIdDetail;

public class GetCustomerWithAddressesByIdDetailQuery : IRequest<GetCustomerWithAddressByIdQueryResponse>
{
  public int CustomerId { get; set; }
}