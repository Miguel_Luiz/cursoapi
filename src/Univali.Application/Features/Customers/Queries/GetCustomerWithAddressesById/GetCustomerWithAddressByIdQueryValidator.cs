// BRANCH: AULA
using FluentValidation;

namespace Univali.Application.Features.Customers.Queries.GetCustomerWithAddressesByIdDetail;

public class GetCustomerWithAddressByIdDetailQueryValidator :
    AbstractValidator<GetCustomerWithAddressesByIdDetailQuery>
 {
    public GetCustomerWithAddressByIdDetailQueryValidator() 
    {
        RuleFor(c => c.CustomerId)
        .NotEmpty()
        .WithMessage("Fill a Id")      
        .GreaterThan(0)
        .WithMessage("The CustomerId must be greater than zero.");
    }
 }