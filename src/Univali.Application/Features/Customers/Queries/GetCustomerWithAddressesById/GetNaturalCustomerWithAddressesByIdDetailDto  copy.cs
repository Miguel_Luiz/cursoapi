using Univali.Application.Models;

namespace Univali.Application.Features.Customers.Queries.GetCustomerWithAddressesByIdDetail;

public class GetNaturalCustomerWithAddressesByIdDetailDto : GetCustomerWithAddressesByIdDetailDto
{
  public string? CPF{ get; set; }
}
