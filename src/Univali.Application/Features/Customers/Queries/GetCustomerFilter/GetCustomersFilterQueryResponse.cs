using Univali.Application.Features.Common;

namespace Univali.Application.Features.Customers.Queries.GetCustomersFilter;

public class GetCustomersFilterDetailQueryResponse : BaseResponse
{
    public IEnumerable<GetCustomersFilterDetailDto> Customers{ get; set; } = default!;
    public PaginationMetadata? PaginationMetadata { get; set; }
    public GetCustomersFilterDetailQueryResponse()
    {
        Errors = new Dictionary<string, string[]>();
    }
}