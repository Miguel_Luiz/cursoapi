namespace Univali.Application.Features.Customers.Queries.GetCustomersFilter;

public class GetCustomersFilterDetailDto 
{
  public int Id { get; set; }
  public string Name { get; set; } = string.Empty;
}
