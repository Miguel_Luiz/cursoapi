using MediatR;

namespace Univali.Application.Features.Customers.Queries.GetCustomersFilter;

public class GetCustomersFilterDetailQuery : IRequest<GetCustomersFilterDetailQueryResponse>
{
    public string? Type { get; set; }
    public string? SearchQuery { get; set; }
    public int PageNumber { get; set; }
    public int PageSize { get; set; }
}