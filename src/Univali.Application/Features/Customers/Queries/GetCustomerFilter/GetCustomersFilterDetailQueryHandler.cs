using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Customers.Queries.GetCustomersFilter;

public class GetCustomersFilterDetailQueryHandler : IRequestHandler<GetCustomersFilterDetailQuery,GetCustomersFilterDetailQueryResponse>
{
    private readonly ICustomerRepository _customerRepository;
    private readonly IMapper _mapper;

    public GetCustomersFilterDetailQueryHandler(ICustomerRepository customerRepository, IMapper mapper)
    {
        _customerRepository = customerRepository;
        _mapper = mapper;
    }

    public async Task<GetCustomersFilterDetailQueryResponse> Handle(GetCustomersFilterDetailQuery request, CancellationToken cancellationToken)
    {

        GetCustomersFilterDetailQueryResponse response = new();

        var (costumerToReturn, paginationMetadata) = await _customerRepository.GetCustomersAsync(request.Type, request.SearchQuery, request.PageNumber,request.PageSize);
        if(costumerToReturn == null || !costumerToReturn.Any())
        {
            response.Errors.Add(
                "Customers",
                new string[] { "No customers were found in the database." }
            );

            
            response.ErrorType = Error.NotFoundProblem;
            return response;

        }

        response.Customers = _mapper.Map<IEnumerable<GetCustomersFilterDetailDto>>(costumerToReturn);
        response.PaginationMetadata = paginationMetadata;

        return response;
    }
}