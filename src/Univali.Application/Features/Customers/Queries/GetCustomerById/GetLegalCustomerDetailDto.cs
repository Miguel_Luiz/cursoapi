namespace Univali.Application.Features.Customers.Queries.GetCustomerDetail;


public class GetLegalCustomerDetailDto : GetCustomerDetailDto
{
    public string? CNPJ{get; set;}
}

