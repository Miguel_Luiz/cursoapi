namespace Univali.Application.Features.Customers.Queries.GetCustomerDetail;


public class GetCustomerDetailDto
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
}

