using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Domain.Entities;
using Univali.Application.Features.Common;


namespace Univali.Application.Features.Customers.Queries.GetCustomerDetail;

public class GetCustomerDetailQueryHandler : IRequestHandler<GetCustomerDetailQuery, GetCustomerDetailQueryResponse>
{
    private readonly ICustomerRepository _customerRepository;
    private readonly IMapper _mapper;
    public readonly IValidator<GetCustomerDetailQuery> _validator;

    public GetCustomerDetailQueryHandler(ICustomerRepository customerRepository, IMapper mapper, IValidator<GetCustomerDetailQuery> validator)
    {
        _customerRepository = customerRepository;
        _mapper = mapper;
        _validator = validator;
    }

    public async Task<GetCustomerDetailQueryResponse> Handle(GetCustomerDetailQuery request, CancellationToken cancellationToken)
    {
        GetCustomerDetailQueryResponse response = new();

        var validationResult = _validator.Validate(request);

        if (validationResult.IsValid == false) 
        {
            foreach (var error in validationResult.ToDictionary()) 
            {
                response.Errors
                    .Add(error.Key, error.Value);
            }

            response.ErrorType = Error.ValidationProblem;
            return response;
        }

        var customerFromDatabase = await _customerRepository.GetCustomerByIdAsync(request.CustomerId);

        if (customerFromDatabase == null)
        {
            response.Errors.Add(
                "Customer",
                new string[] { $"Customer with Id = {request.CustomerId} was not found in the database" }
            );

            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        if(customerFromDatabase is NaturalCustomer)
        {

            response.Customer = _mapper.Map<GetNaturalCustomerDetailDto>(customerFromDatabase);
        }
        if(customerFromDatabase is LegalCustomer)
        {
            response.Customer = _mapper.Map<GetLegalCustomerDetailDto>(customerFromDatabase);
        }


        // response.Customer = _mapper.Map<GetCustomerDetailDto>(customerFromDatabase);

        return response;
    }
}