// BRANCH: AULA
using FluentValidation;

namespace Univali.Application.Features.Customers.Queries.GetCustomerDetail;

public class GetCustomerDetailQueryValidator :
    AbstractValidator<GetCustomerDetailQuery>
 {
    public GetCustomerDetailQueryValidator() 
    {
        RuleFor(c => c.CustomerId)
        .NotEmpty()
        .WithMessage("Fill a Id")      
        .GreaterThan(0)
        .WithMessage("The CustomerId must be greater than zero.");
    }
 }