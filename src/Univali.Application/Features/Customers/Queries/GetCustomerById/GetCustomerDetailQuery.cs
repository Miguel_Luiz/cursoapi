using MediatR;

namespace Univali.Application.Features.Customers.Queries.GetCustomerDetail;

public class GetCustomerDetailQuery : IRequest<GetCustomerDetailQueryResponse>
{
    public int CustomerId {get; set;}
}