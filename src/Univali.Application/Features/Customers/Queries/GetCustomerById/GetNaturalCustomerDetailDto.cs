namespace Univali.Application.Features.Customers.Queries.GetCustomerDetail;


public class GetNaturalCustomerDetailDto : GetCustomerDetailDto
{
    public string? CPF{get; set;}
}

