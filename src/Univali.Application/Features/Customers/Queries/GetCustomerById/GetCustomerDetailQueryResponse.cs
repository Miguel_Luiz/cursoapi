

using Univali.Application.Features.Common;

namespace Univali.Application.Features.Customers.Queries.GetCustomerDetail;

public class GetCustomerDetailQueryResponse : BaseResponse
{
    public GetCustomerDetailDto Customer {get; set;} = new();

    public GetCustomerDetailQueryResponse()
    {
        Errors = new Dictionary<string, string[]>();
    }
}