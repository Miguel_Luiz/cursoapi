using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Customers.Queries.GetCustomerByCnpjDetail;

public class GetCustomerByCnpjDetailQueryHandler : IRequestHandler<GetCustomerByCnpjDetailQuery, GetCustomerByCnpjDetailQueryResponse>
{
    private readonly ICustomerRepository _customerRepository;
    private readonly IMapper _mapper;
        public readonly IValidator<GetCustomerByCnpjDetailQuery> _validator;


    public GetCustomerByCnpjDetailQueryHandler(ICustomerRepository customerRepository, IMapper mapper, IValidator<GetCustomerByCnpjDetailQuery> validator)
    {
        _customerRepository = customerRepository;
        _mapper = mapper;
        _validator = validator;
    }

    public async Task<GetCustomerByCnpjDetailQueryResponse> Handle(
        GetCustomerByCnpjDetailQuery request, CancellationToken cancellationToken)
    {

        GetCustomerByCnpjDetailQueryResponse response = new();

        var validationResult = _validator.Validate(request);

            if (validationResult.IsValid == false) 
        {
            foreach (var error in validationResult.ToDictionary()) 
            {
                response.Errors
                    .Add(error.Key, error.Value);
            }

            response.ErrorType = Error.ValidationProblem;
            return response;
        }

        var customerFromDatabase = await _customerRepository.GetCustomerByCnpjAsync(request.CustomerCnpj);

        if (customerFromDatabase == null)
        {
            response.Errors.Add(
                "Customer",
                new string[] { $"Customer with cnpj = {request.CustomerCnpj} was not found in the database" }
            );

            response.ErrorType = Error.NotFoundProblem;
            return response;
        }


        response.Customer = _mapper.Map<GetCustomerByCnpjDetailDto>(customerFromDatabase);

        return response;
    }
}