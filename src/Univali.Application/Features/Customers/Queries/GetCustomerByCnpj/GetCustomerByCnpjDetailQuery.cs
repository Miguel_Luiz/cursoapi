using MediatR;

namespace Univali.Application.Features.Customers.Queries.GetCustomerByCnpjDetail;

public class GetCustomerByCnpjDetailQuery : IRequest<GetCustomerByCnpjDetailQueryResponse>
{
  public string CustomerCnpj { get; set; } = string.Empty;
}