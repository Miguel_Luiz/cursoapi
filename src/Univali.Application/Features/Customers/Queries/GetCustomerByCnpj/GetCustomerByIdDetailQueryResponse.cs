using Univali.Application.Features.Common;

namespace Univali.Application.Features.Customers.Queries.GetCustomerByCnpjDetail;

public class GetCustomerByCnpjDetailQueryResponse : BaseResponse
{
    public GetCustomerByCnpjDetailDto Customer {get; set;} = new();

    public GetCustomerByCnpjDetailQueryResponse()
    {
        Errors = new Dictionary<string, string[]>();
    }
}