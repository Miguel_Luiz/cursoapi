namespace Univali.Application.Features.Customers.Queries.GetCustomerByCnpjDetail;

public class GetCustomerByCnpjDetailDto
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string CNPJ { get; set; } = string.Empty;
}
