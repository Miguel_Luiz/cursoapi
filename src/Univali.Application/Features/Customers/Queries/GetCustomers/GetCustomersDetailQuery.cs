using MediatR;

namespace Univali.Application.Features.Customers.Queries.GetCustomers;

public class GetCustomersDetailQuery : IRequest<GetCustomersDetailQueryResponse>
{

}