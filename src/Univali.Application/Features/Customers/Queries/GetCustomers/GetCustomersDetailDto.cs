namespace Univali.Application.Features.Customers.Queries.GetCustomers;

public class GetCustomersDetailDto 
{
  public int Id { get; set; }
  public string Name { get; set; } = string.Empty;
}
