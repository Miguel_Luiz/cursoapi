using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Customers.Queries.GetCustomers;

public class GetOrdersItemsDetailQueryHandler : IRequestHandler<GetCustomersDetailQuery,GetCustomersDetailQueryResponse>
{
    private readonly ICustomerRepository _customerRepository;
    private readonly IMapper _mapper;

    public GetOrdersItemsDetailQueryHandler(ICustomerRepository customerRepository, IMapper mapper)
    {
        _customerRepository = customerRepository;
        _mapper = mapper;
    }

    public async Task<GetCustomersDetailQueryResponse> Handle(GetCustomersDetailQuery request, CancellationToken cancellationToken)
    {

        GetCustomersDetailQueryResponse response = new();

        var customersFromDatabase = await _customerRepository.GetCustomersAsync();

        if(customersFromDatabase == null || !customersFromDatabase.Any())
        {
            response.Errors.Add(
                "Customers",
                new string[] { "No customers found in the database." }
            );

            
            response.ErrorType = Error.NotFoundProblem;
            return response;

        }

        response.Customers = _mapper.Map<List<GetCustomersDetailDto>>(customersFromDatabase);


        return response;


        // var customerFromDatabase = await _customerRepository.GetCustomersAsync();

        // return _mapper.Map<IEnumerable<GetCustomersDetailQueryResponse>>(customerFromDatabase);
    }
}