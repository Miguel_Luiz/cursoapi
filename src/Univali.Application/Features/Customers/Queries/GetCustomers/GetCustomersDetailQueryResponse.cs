namespace Univali.Application.Features.Customers.Queries.GetCustomers;

public class GetCustomersDetailQueryResponse : Common.BaseResponse
{
    public List<GetCustomersDetailDto> Customers{ get; set; } = default!;

    public GetCustomersDetailQueryResponse()
    {
        Errors = new Dictionary<string, string[]>();
    }
}