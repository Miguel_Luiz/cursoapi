namespace Univali.Application.Features.Customers.Queries.GetCustomerByCpfDetail;

public class GetCustomerByCpfDetailDto
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string CPF { get; set; } = string.Empty;
}
