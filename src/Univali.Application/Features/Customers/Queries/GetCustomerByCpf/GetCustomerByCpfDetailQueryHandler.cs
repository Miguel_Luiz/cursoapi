using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Customers.Queries.GetCustomerByCpfDetail;

public class GetCustomerByCpfDetailQueryHandler : IRequestHandler<GetCustomerByCpfDetailQuery, GetCustomerByCpfDetailQueryResponse>
{
    private readonly ICustomerRepository _customerRepository;
    private readonly IMapper _mapper;
    public readonly IValidator<GetCustomerByCpfDetailQuery> _validator;


    public GetCustomerByCpfDetailQueryHandler(ICustomerRepository customerRepository, IMapper mapper, IValidator<GetCustomerByCpfDetailQuery> validator)
    {
        _customerRepository = customerRepository;
        _mapper = mapper;
        _validator = validator;
    }

    public async Task<GetCustomerByCpfDetailQueryResponse> Handle(
        GetCustomerByCpfDetailQuery request, CancellationToken cancellationToken)
    {

        GetCustomerByCpfDetailQueryResponse response = new();

        var validationResult = _validator.Validate(request);

            if (validationResult.IsValid == false) 
        {
            foreach (var error in validationResult.ToDictionary()) 
            {
                response.Errors
                    .Add(error.Key, error.Value);
            }

            response.ErrorType = Error.ValidationProblem;
            return response;
        }

        var customerFromDatabase = await _customerRepository.GetCustomerByCpfAsync(request.CustomerCpf);

        if (customerFromDatabase == null)
        {
            response.Errors.Add(
                "Customer",
                new string[] { $"Customer with cpf = {request.CustomerCpf} was not found in the database" }
            );

            response.ErrorType = Error.NotFoundProblem;
            return response;
        }


        response.Customer = _mapper.Map<GetCustomerByCpfDetailDto>(customerFromDatabase);

        return response;
    }
}