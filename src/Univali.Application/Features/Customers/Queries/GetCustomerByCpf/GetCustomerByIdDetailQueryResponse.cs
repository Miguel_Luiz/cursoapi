using Univali.Application.Features.Common;

namespace Univali.Application.Features.Customers.Queries.GetCustomerByCpfDetail;

public class GetCustomerByCpfDetailQueryResponse : BaseResponse
{
    public GetCustomerByCpfDetailDto Customer {get; set;} = new();

    public GetCustomerByCpfDetailQueryResponse()
    {
        Errors = new Dictionary<string, string[]>();
    }
}