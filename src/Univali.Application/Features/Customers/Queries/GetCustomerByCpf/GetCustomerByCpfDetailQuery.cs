using MediatR;

namespace Univali.Application.Features.Customers.Queries.GetCustomerByCpfDetail;

public class GetCustomerByCpfDetailQuery : IRequest<GetCustomerByCpfDetailQueryResponse>
{
  public string CustomerCpf { get; set; } = string.Empty;
}