using Univali.Application.Features.Common;

namespace Univali.Application.Features.Customers.Queries.GetCustomersWithAddresses;

public class GetCustomersWithAddressDetailQueryResponse : BaseResponse
{
    public List<GetCustomersWithAddressesDetailDto> Customers{ get; set; } = default!;

    public GetCustomersWithAddressDetailQueryResponse()
    {
        Errors = new Dictionary<string, string[]>();
    }
}