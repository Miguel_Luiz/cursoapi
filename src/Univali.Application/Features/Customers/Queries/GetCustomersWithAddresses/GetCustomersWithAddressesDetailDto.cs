using Univali.Application.Models;

namespace Univali.Application.Features.Customers.Queries.GetCustomersWithAddresses;

public class GetCustomersWithAddressesDetailDto 
{
  public int Id { get; set; }
  public string Name { get; set; } = string.Empty;
  public List<AddressDto> Addresses { get; set; } = new();
}
