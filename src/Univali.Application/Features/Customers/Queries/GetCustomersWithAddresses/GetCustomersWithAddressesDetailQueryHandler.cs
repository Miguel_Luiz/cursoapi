using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Customers.Queries.GetCustomersWithAddresses;

public class GetCustomersWithAddressesDetailQueryHandler : IRequestHandler<GetCustomersWithAddressesDetailQuery, GetCustomersWithAddressDetailQueryResponse>
{
    private readonly ICustomerRepository _customerRepository;
    private readonly IMapper _mapper;

    public GetCustomersWithAddressesDetailQueryHandler(ICustomerRepository customerRepository, IMapper mapper)
    {
        _customerRepository = customerRepository;
        _mapper = mapper;
    }

    public async Task<GetCustomersWithAddressDetailQueryResponse> Handle(GetCustomersWithAddressesDetailQuery request, CancellationToken cancellationToken)
    {
        GetCustomersWithAddressDetailQueryResponse response = new();
        
        var customerFromDatabase = await _customerRepository.GetCustomersWithAddressesAsync();

         if(customerFromDatabase == null || customerFromDatabase.Count() == 0)
        {
            response.Errors.Add(
                "Customers",
                new string[] { "No customers found in the database." }
            );
            
            
            response.ErrorType = Error.NotFoundProblem;
            return response;

        }

         response.Customers = _mapper.Map<List<GetCustomersWithAddressesDetailDto>>(customerFromDatabase);

        return response;
    }
}