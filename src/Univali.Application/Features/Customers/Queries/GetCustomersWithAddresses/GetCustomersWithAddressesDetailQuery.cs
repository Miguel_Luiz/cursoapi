using MediatR;

namespace Univali.Application.Features.Customers.Queries.GetCustomersWithAddresses;

public class GetCustomersWithAddressesDetailQuery : IRequest<GetCustomersWithAddressDetailQueryResponse>
{

}