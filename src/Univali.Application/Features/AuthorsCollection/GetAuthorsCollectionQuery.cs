// pablo1107 - Removi category
using MediatR;

namespace Univali.Application.Features.AuthorsCollection;

public class GetAuthorsCollectionQuery : IRequest<GetAuthorsCollectionResponse>
{
    public GetAuthorsCollectionQuery(string? searchQuery, int pageNumber, int pageSize)
    {
        SearchQuery = searchQuery; 
        PageNumber = pageNumber;
        PageSize = pageSize;
    }

    public string? SearchQuery { get; set; }

    public int PageNumber { get; set; }

    public int PageSize { get; set; }
}