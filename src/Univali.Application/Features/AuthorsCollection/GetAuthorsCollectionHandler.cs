using AutoMapper;
using MediatR;
using Univali.Application.Models;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.AuthorsCollection;

public class GetAuthorsCollectionHandler : IRequestHandler<GetAuthorsCollectionQuery, GetAuthorsCollectionResponse>
{
    private readonly IPublisherRepository _publisherRepository;

    private readonly IMapper _mapper;

    public GetAuthorsCollectionHandler(IPublisherRepository publisherRepository, IMapper mapper)
    {
        _publisherRepository = publisherRepository;
        _mapper = mapper;
    }

    public async Task<GetAuthorsCollectionResponse> Handle(
        GetAuthorsCollectionQuery getAuthorsCollectionQuery,
        CancellationToken cancellationToken
    )
    {
        var (authors, paginationMetadata) = await _publisherRepository
             .GetAuthorsCollectionAsync(
                 getAuthorsCollectionQuery.SearchQuery,
                 getAuthorsCollectionQuery.PageNumber,
                 getAuthorsCollectionQuery.PageSize
             );

        GetAuthorsCollectionResponse getAuthorsCollectionResponse = new();

        getAuthorsCollectionResponse.Authors = _mapper.Map<List<AuthorDto>>(authors);

        getAuthorsCollectionResponse.Pagination = paginationMetadata;

        return getAuthorsCollectionResponse;
    }
}