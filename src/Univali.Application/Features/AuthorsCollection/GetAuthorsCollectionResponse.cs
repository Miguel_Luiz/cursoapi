using Univali.Application.Features.Common;
using Univali.Application.Models;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.AuthorsCollection;

public class GetAuthorsCollectionResponse : BaseResponse
{
    public List<AuthorDto> Authors {get; set;} = new();

    public PaginationMetadata? Pagination {get; set;}
}