using MediatR;

namespace Univali.Application.Features.Payments.Queries.GetPayment;

public class GetPaymentDetailQuery : IRequest<GetPaymentDetailQueryResponse>
{
    public int CustomerId { get; set; }
    public int OrderId { get; set; }
}