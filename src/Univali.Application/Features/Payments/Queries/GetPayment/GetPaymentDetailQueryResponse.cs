using Univali.Application.Features.Common;

namespace Univali.Application.Features.Payments.Queries.GetPayment;

public class GetPaymentDetailQueryResponse : BaseResponse
{
    public GetPaymentDetailDto Payment {get; set;} = default!;
}