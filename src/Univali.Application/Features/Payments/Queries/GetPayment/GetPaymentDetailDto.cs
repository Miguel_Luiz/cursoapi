using Univali.Application.Models;

namespace Univali.Application.Features.Payments.Queries.GetPayment;

public class GetPaymentDetailDto 
{
  public int PaymentId { get; set; }
  public PaymentType Type { get; set; }
  public string Description { get; set; } = string.Empty;
  
  public enum PaymentType { CreditCard, DebitCard, BoletoBancario, PIX }
}
