using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Payments.Queries.GetPayment;

public class GetPaymentDetailQueryHandler : IRequestHandler<GetPaymentDetailQuery, GetPaymentDetailQueryResponse>
{
    private readonly IMapper _mapper;
    private readonly IOrderRepository _orderRepository;

    public GetPaymentDetailQueryHandler(IOrderRepository orderRepository, IMapper mapper)
    {
        _orderRepository = orderRepository;
        _mapper = mapper;
    }

    public async Task<GetPaymentDetailQueryResponse> Handle(GetPaymentDetailQuery request, CancellationToken cancellationToken)
    {
        GetPaymentDetailQueryResponse response = new();

        var orderFromDatabase = await _orderRepository.GetOrderByIdAsync(request.CustomerId, request.OrderId);

        if(orderFromDatabase == null)
        {
            response.Errors.Add(
                "Order",
                new string[] { $"Order with id = {request.OrderId} for customer with id = {request.CustomerId} was not found in the database"}
            );

            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var paymentFromDatabase = await _orderRepository.GetPaymentFromOrderByIdAsync(request.OrderId);

        if(paymentFromDatabase == null)
        {
            response.Errors.Add(
                "Payment",
                new string[] { $"Payment of order with id = {request.OrderId} for customer with id = {request.CustomerId} was not found in the database"}
            );

            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        response.Payment = _mapper.Map<GetPaymentDetailDto>(paymentFromDatabase);
        return response;
    }
}