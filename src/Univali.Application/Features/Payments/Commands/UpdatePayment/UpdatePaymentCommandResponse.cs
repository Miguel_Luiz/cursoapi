using Univali.Application.Features.Common;

namespace Univali.Application.Features.Payments.Commands.UpdatePayment;

public class UpdatePaymentCommandResponse : BaseResponse
{
}