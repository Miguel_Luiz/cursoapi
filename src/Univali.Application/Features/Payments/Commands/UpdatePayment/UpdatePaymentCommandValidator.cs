using FluentValidation;
namespace Univali.Application.Features.Payments.Commands.UpdatePayment;

public class UpdatePaymentCommandValidator : AbstractValidator<UpdatePaymentCommand>
{
    public UpdatePaymentCommandValidator()
    {
        RuleFor(p => p.Type)
            .NotNull()
            .IsInEnum();
    }
}