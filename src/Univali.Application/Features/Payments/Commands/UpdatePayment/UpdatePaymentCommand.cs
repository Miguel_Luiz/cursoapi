using MediatR;
using Univali.Application.Models;

namespace Univali.Application.Features.Payments.Commands.UpdatePayment;

public  class UpdatePaymentCommand : IRequest<UpdatePaymentCommandResponse>
{
    public int CustomerId { get; set; }
    public int OrderId { get; set; }
    public PaymentTypeDto? Type { get; set; } = null;
    public string Description { get; set; } = string.Empty;
}