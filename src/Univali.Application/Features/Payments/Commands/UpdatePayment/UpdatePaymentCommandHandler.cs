using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Payments.Commands.UpdatePayment;

public class UpdatePaymentCommandHandler : IRequestHandler<UpdatePaymentCommand, UpdatePaymentCommandResponse>
{
    private readonly IOrderRepository _orderRepository;
    private readonly IMapper _mapper;
    private readonly IValidator<UpdatePaymentCommand> _validator;

    public UpdatePaymentCommandHandler(IOrderRepository orderRepository, IMapper mapper, IValidator<UpdatePaymentCommand> validator)
    {
        _orderRepository = orderRepository;
        _mapper = mapper;
        _validator = validator;
    }

    public async Task<UpdatePaymentCommandResponse> Handle(UpdatePaymentCommand request, CancellationToken cancellationToken)
    {
        UpdatePaymentCommandResponse response = new();
        
        var validationResult = _validator.Validate(request);

        if(!validationResult.IsValid)
        {
            foreach(var error in validationResult.ToDictionary())
            {
                response.Errors.Add(error.Key, error.Value);
            }

            response.ErrorType = Error.ValidationProblem;
            return response;
        }

        var orderFromDatabase = await _orderRepository.GetOrderInFullByIdAsync(request.CustomerId, request.OrderId);

        if(orderFromDatabase == null)
        {
            response.Errors.Add(
                "Order",
                new string[] { $"Order with id = {request.OrderId} for customer with id = {request.CustomerId} was not found in the database"}
            );

            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var paymentFromOrder = orderFromDatabase.Payment;

        if(paymentFromOrder == null)
        {
            response.Errors.Add(
                "Payment",
                new string[] { $"Payment of order with id = {request.OrderId} for customer with id = {request.CustomerId} was not found in the database"}
            );

            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        _orderRepository.UpdatePayment(paymentFromOrder, request);

        if(!orderFromDatabase.GenerateReceipt()) 
        {
            response.Errors.Add(
                "Receipt",
                new string[] { $"Receipt could not be generated successfully"}
            );

            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        await _orderRepository.SaveChangesAsync();

        return response;
    }
}