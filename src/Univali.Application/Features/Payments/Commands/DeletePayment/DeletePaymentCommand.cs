using MediatR;

namespace Univali.Application.Features.Payments.Commands.DeletePayment;

public  class DeletePaymentCommand: IRequest<DeletePaymentCommandResponse>
{
    public int CustomerId { get; set; }
    public int OrderId { get; set; }
}