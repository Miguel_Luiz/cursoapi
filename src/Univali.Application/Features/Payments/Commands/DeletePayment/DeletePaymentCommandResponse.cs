using Univali.Application.Features.Common;

namespace Univali.Application.Features.Payments.Commands.DeletePayment;

public class DeletePaymentCommandResponse : BaseResponse
{
}