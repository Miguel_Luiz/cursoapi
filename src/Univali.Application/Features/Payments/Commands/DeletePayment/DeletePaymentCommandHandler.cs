using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Payments.Commands.DeletePayment;
public class DeletePaymentCommandHandler : IRequestHandler<DeletePaymentCommand, DeletePaymentCommandResponse>
{
    private readonly IOrderRepository _orderRepository;
    private readonly IMapper _mapper;

    public DeletePaymentCommandHandler(IOrderRepository orderRepository, IMapper mapper)
    {
        _orderRepository = orderRepository;
        _mapper = mapper;
    }

    public async Task<DeletePaymentCommandResponse> Handle(DeletePaymentCommand request, CancellationToken cancellationToken)
    {
        DeletePaymentCommandResponse response = new();

        var orderFromDatabase = await _orderRepository.GetOrderByIdAsync(request.CustomerId, request.OrderId);

        if(orderFromDatabase == null)
        {
            response.Errors.Add(
                "Order",
                new string[] { $"Order with id = {request.OrderId} for customer with id = {request.CustomerId} was not found in the database"}
            );

            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var paymentFromDatabase = await _orderRepository.GetPaymentFromOrderByIdAsync(request.OrderId);

        if(paymentFromDatabase == null)
        {
            response.Errors.Add(
                "Payment",
                new string[] { $"Payment of order with id = {request.OrderId} for customer with id = {request.CustomerId} was not found in the database"}
            );

            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        _orderRepository.DeletePayment(paymentFromDatabase);
        await _orderRepository.SaveChangesAsync();

        return response;
    }
}