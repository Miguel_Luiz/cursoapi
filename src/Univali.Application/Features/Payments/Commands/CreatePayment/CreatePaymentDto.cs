using Univali.Application.Models;

namespace Univali.Application.Features.Payments.Commands.CreatePayment;

public class CreatePaymentDto
{
    public int PaymentId { get; set; }
    public PaymentTypeDto Type { get; set; }
    public string Description { get; set; } = string.Empty;
}

