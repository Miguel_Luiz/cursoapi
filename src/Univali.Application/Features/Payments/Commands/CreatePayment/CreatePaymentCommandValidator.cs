using FluentValidation;
namespace Univali.Application.Features.Payments.Commands.CreatePayment;

public class CreatePaymentCommandValidator : AbstractValidator<CreatePaymentCommand>
{
    public CreatePaymentCommandValidator()
    {
        RuleFor(p => p.Type)
            .NotNull()
            .IsInEnum();
    }
}