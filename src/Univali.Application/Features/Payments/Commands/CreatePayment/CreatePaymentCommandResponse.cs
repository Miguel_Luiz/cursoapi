using Univali.Application.Features.Common;

namespace Univali.Application.Features.Payments.Commands.CreatePayment;

public class CreatePaymentCommandResponse : BaseResponse
{
    public CreatePaymentDto Payment {get; set;} = default!;
}