using System.ComponentModel.DataAnnotations;
using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Domain.Entities;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Payments.Commands.CreatePayment;

// O primeiro parâmetro é o tipo da mensagem
// O segundo parâmetro é o tipo que se espera receber.
public class CreatePaymentCommandHandler: IRequestHandler<CreatePaymentCommand, CreatePaymentCommandResponse>
{
    private readonly IOrderRepository _orderRepository;
    private readonly IMapper _mapper;
    private readonly IValidator<CreatePaymentCommand> _validator;

    public CreatePaymentCommandHandler(IOrderRepository orderRepository, IMapper mapper, IValidator<CreatePaymentCommand> validator)
    {
        _orderRepository = orderRepository;
        _mapper = mapper;
        _validator = validator;
    }

    public async Task<CreatePaymentCommandResponse> Handle(CreatePaymentCommand request, CancellationToken cancellationToken)
    {
        CreatePaymentCommandResponse response = new();
        
        var validationResult = _validator.Validate(request);

        if(!validationResult.IsValid)
        {
            foreach(var error in validationResult.ToDictionary())
            {
                response.Errors.Add(error.Key, error.Value);
            }

            response.ErrorType = Error.ValidationProblem;
            return response;
        }

        var orderFromDatabase = await _orderRepository.GetOrderInFullByIdAsync(request.CustomerId, request.OrderId);

        if(orderFromDatabase == null)
        {
            response.Errors.Add(
                "Order",
                new string[] { $"Order with id = {request.OrderId} for customer with id = {request.CustomerId} was not found in the database"}
            );

            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var paymentEntity = _mapper.Map<Payment>(request);

        _orderRepository.AddPayment(orderFromDatabase, paymentEntity);

        if(!orderFromDatabase.GenerateReceipt()) 
        {
            response.Errors.Add(
                "Receipt",
                new string[] { $"Receipt could not be generated successfully"}
            );

            response.ErrorType = Error.NotFoundProblem;
            return response;
        }
        
        await _orderRepository.SaveChangesAsync();

        var paymentToReturn = _mapper.Map<CreatePaymentDto>(paymentEntity);
        response.Payment = paymentToReturn;

        return response;
    }
}