using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.QuestionAnswers.Queries.GetQuestionAnswerDetail;

public class GetQuestionAnswerDetailQueryHandler : IRequestHandler<GetQuestionAnswerDetailQuery, GetQuestionAnswerDetailResponse>
{
    private readonly IPublisherRepository _publisherRepository;
    private readonly IMapper _mapper;

    public GetQuestionAnswerDetailQueryHandler(IPublisherRepository publisherRepository, IMapper mapper)
    {
        _publisherRepository = publisherRepository;
        _mapper = mapper;
    }

    public async Task<GetQuestionAnswerDetailResponse> Handle(GetQuestionAnswerDetailQuery request, CancellationToken cancellationToken)
    {
        var getQuestionAnswerDetailResponse = new GetQuestionAnswerDetailResponse();

        var questionFromDatabase = await _publisherRepository.GetQuestionWithAnswersByIdAsync(request.QuestionId);

        if(questionFromDatabase == null)
        {
            getQuestionAnswerDetailResponse.ErrorType = Error.NotFoundProblem;
            getQuestionAnswerDetailResponse.Errors.Add("Question", new string[] {"Question Not Found"});
            return getQuestionAnswerDetailResponse;
        }

        var answerFromDatabase = questionFromDatabase.Answers.FirstOrDefault(a => a.AnswerId == request.AnswerId);

        if(answerFromDatabase == null)
        {
            getQuestionAnswerDetailResponse.ErrorType = Error.NotFoundProblem;
            getQuestionAnswerDetailResponse.Errors.Add("Answer", new string[] {"Answer not found in this question"});
            return getQuestionAnswerDetailResponse;
        }

        getQuestionAnswerDetailResponse.QuestionAnswer =  _mapper.Map<GetQuestionAnswerDetailDto>(answerFromDatabase);

        return getQuestionAnswerDetailResponse;
    }
}