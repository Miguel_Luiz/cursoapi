namespace Univali.Application.Features.QuestionAnswers.Queries.GetQuestionAnswerDetail;

public class GetQuestionAnswerDetailDto
{
    public int AnswerId {get; set;}
    public string Body {get; set;} = string.Empty;
    public int QuestionId {get; set;}
    public int AuthorId {get; set;}
}