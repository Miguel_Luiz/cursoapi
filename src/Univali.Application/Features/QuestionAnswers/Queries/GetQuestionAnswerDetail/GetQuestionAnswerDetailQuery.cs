using MediatR;

namespace Univali.Application.Features.QuestionAnswers.Queries.GetQuestionAnswerDetail;

public class GetQuestionAnswerDetailQuery : IRequest<GetQuestionAnswerDetailResponse>
{
    public int QuestionId {get; set;}
    public int AnswerId {get; set;}
}