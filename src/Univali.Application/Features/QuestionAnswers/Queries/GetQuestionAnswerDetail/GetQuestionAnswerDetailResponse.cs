using Univali.Application.Features.Common;

namespace Univali.Application.Features.QuestionAnswers.Queries.GetQuestionAnswerDetail;

public class GetQuestionAnswerDetailResponse : BaseResponse
{
    public GetQuestionAnswerDetailDto QuestionAnswer {get;set;} = new();
}