namespace Univali.Application.Features.QuestionAnswers.Queries.GetQuestionAnswersDetail;

public class GetQuestionAnswersDetailDto
{
    public int AnswerId {get; set;}
    public string Body {get; set;} = string.Empty;
    public int QuestionId {get; set;}
    public int AuthorId {get; set;}
}