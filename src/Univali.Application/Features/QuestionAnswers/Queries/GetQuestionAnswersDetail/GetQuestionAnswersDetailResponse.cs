using Univali.Application.Features.Common;

namespace Univali.Application.Features.QuestionAnswers.Queries.GetQuestionAnswersDetail;

public class GetQuestionAnswersDetailResponse : BaseResponse
{
    public IEnumerable<GetQuestionAnswersDetailDto> QuestionAnswersDetailDtos {get;set;} = null!;

    public PaginationMetadata paginationMetadata = null!;
}