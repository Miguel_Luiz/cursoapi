using MediatR;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.QuestionAnswers.Queries.GetQuestionAnswersDetail;

public class GetQuestionAnswersDetailQuery : IRequest<GetQuestionAnswersDetailResponse>
{
    public int QuestionId {get; set;}
    public int PageNumber {get;set;}
    public int PageSize{get;set;}
}