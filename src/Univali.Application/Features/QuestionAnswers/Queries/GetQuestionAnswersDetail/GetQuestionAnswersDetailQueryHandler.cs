using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.QuestionAnswers.Queries.GetQuestionAnswersDetail;

public class GetQuestionAnswersDetailQueryHandler : IRequestHandler<GetQuestionAnswersDetailQuery, GetQuestionAnswersDetailResponse>
{
    private readonly IPublisherRepository _publisherRepository;
    private readonly IMapper _mapper;

    public GetQuestionAnswersDetailQueryHandler(IPublisherRepository publisherRepository, IMapper mapper)
    {
        _publisherRepository = publisherRepository;
        _mapper = mapper;
    }

    public async Task<GetQuestionAnswersDetailResponse> Handle(GetQuestionAnswersDetailQuery request, CancellationToken cancellationToken)
    {
        GetQuestionAnswersDetailResponse getQuestionAnswersDetailResponse = new();

        if(! await _publisherRepository.QuestionExistsAsync(request.QuestionId))
        {
            getQuestionAnswersDetailResponse.Errors.Add("Question", new string[] {"Question Not Found"});
            getQuestionAnswersDetailResponse.ErrorType = Error.NotFoundProblem;
            return getQuestionAnswersDetailResponse;
        }

        var (answersFromDatabase, paginationMetadata)  = await _publisherRepository.GetAnswersByQuestionIdAsync(request.QuestionId, request.PageNumber, request.PageSize);

        getQuestionAnswersDetailResponse.QuestionAnswersDetailDtos =  _mapper.Map<IEnumerable<GetQuestionAnswersDetailDto>>(answersFromDatabase);

        getQuestionAnswersDetailResponse.paginationMetadata = paginationMetadata;

        return getQuestionAnswersDetailResponse;
    }
}