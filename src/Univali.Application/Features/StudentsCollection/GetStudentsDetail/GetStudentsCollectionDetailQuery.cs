using MediatR;

namespace Univali.Application.Features.StudentsCollection.GetStudentsDetail;

public class GetStudentsCollectionDetailQuery : IRequest<GetStudentsCollectionDetailResponse>
{
    public string SearchQuery {get; set;} = string.Empty;
    public int PageNumber {get;set;}
    public int PageSize{get;set;}
}