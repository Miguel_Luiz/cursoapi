using Univali.Application.Features.Common;
using Univali.Application.Contracts.Repositories;


namespace Univali.Application.Features.StudentsCollection.GetStudentsDetail;

public class GetStudentsCollectionDetailResponse : BaseResponse
{
    public IEnumerable<GetStudentsCollectionDetailDto> StudentsDetailDtos {get;set;} = null!;

    public PaginationMetadata PaginationMetadata = null!;
}