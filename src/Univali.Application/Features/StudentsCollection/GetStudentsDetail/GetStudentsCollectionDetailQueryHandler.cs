using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.StudentsCollection.GetStudentsDetail;

public class GetStudentsCollectionDetailQueryHandler : IRequestHandler<GetStudentsCollectionDetailQuery, GetStudentsCollectionDetailResponse>
{
    private readonly IPublisherRepository _publisherRepository;
    private readonly IMapper _mapper;

    public GetStudentsCollectionDetailQueryHandler(IPublisherRepository publisherRepository, IMapper mapper)
    {
        _publisherRepository = publisherRepository;
        _mapper = mapper;
    }

    public async Task<GetStudentsCollectionDetailResponse> Handle(GetStudentsCollectionDetailQuery request, CancellationToken cancellationToken)
    {
        GetStudentsCollectionDetailResponse getStudentsCollectionDetailResponse = new();

        var (studentsFromDatabase, paginationMetadata) = await _publisherRepository.GetStudentsCollectionAsync(request.SearchQuery, request.PageNumber, request.PageSize);

        getStudentsCollectionDetailResponse.PaginationMetadata = paginationMetadata;

        getStudentsCollectionDetailResponse.StudentsDetailDtos = _mapper.Map<IEnumerable<GetStudentsCollectionDetailDto>>(studentsFromDatabase);

        return getStudentsCollectionDetailResponse;
    }
}