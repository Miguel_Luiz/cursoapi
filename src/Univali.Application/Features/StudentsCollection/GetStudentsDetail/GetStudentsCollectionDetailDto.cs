namespace Univali.Application.Features.StudentsCollection.GetStudentsDetail;

public class GetStudentsCollectionDetailDto
{
    public int StudentId {get; set;}
    public string Name {get; set;} = string.Empty;
}