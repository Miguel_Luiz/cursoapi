using MediatR;

namespace Univali.Application.Features.LessonQuestions.Queries.GetLessonQuestionDetail;

public class GetLessonQuestionDetailQuery : IRequest<GetLessonQuestionDetailResponse>
{
        public int QuestionId { get; set; }
        public int LessonId { get; set; }
}