using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.LessonQuestions.Queries.GetLessonQuestionDetail;

public class GetLessonQuestionDetailQueryHandler : IRequestHandler<GetLessonQuestionDetailQuery, GetLessonQuestionDetailResponse>
{
    private readonly IPublisherRepository _questionRepository;
    private readonly IMapper _mapper;

    public GetLessonQuestionDetailQueryHandler(IMapper mapper, IPublisherRepository questionRepository)
    {
        _mapper = mapper;
        _questionRepository = questionRepository;
    }

    public async Task<GetLessonQuestionDetailResponse> Handle(GetLessonQuestionDetailQuery request, CancellationToken cancellationToken)
    {
        GetLessonQuestionDetailResponse getLessonQuestionDetailResponse = new();

        var lessonFromDataBase = await _questionRepository.GetLessonWithQuestionsByIdAsync(request.LessonId);

        if(lessonFromDataBase == null)
        {
            getLessonQuestionDetailResponse.ErrorType = Error.NotFoundProblem;
            getLessonQuestionDetailResponse.Errors.Add("Lesson", new string[] {"Lesson Not Found"});
            return getLessonQuestionDetailResponse;
        }

        var questionFromDataBase = lessonFromDataBase.Questions.FirstOrDefault(q => q.QuestionId == request.QuestionId);

        if(questionFromDataBase == null)
        {
            getLessonQuestionDetailResponse.ErrorType = Error.NotFoundProblem;
            getLessonQuestionDetailResponse.Errors.Add("Question", new string[] {"Question Not Found in this lesson"});
            return getLessonQuestionDetailResponse;
        }

        getLessonQuestionDetailResponse.Question = _mapper.Map<GetLessonQuestionDetailDto>(questionFromDataBase);

        return getLessonQuestionDetailResponse;
    }
}