using Univali.Application.Features.Common;

namespace Univali.Application.Features.LessonQuestions.Queries.GetLessonQuestionDetail;

public class GetLessonQuestionDetailResponse : BaseResponse
{
    public GetLessonQuestionDetailDto Question{ get; set; } = new();
}