using Univali.Domain.Entities;
using Univali.Application.Models;

namespace Univali.Application.Features.LessonQuestions.Queries.GetLessonQuestionWithAnswersDetail;

public class GetLessonQuestionWithAnswersDetailDto 
{
    public int QuestionId { get; set; }
    public string Questioning { get; set; } = string.Empty;
    public DateTime CreationDate { get; set; }
    public DateTime LastUpdate {get; set;}
    public string Category { get; set; } = string.Empty;
    public int StudentId { get; set; }
    public List<AnswerDto> Answers{ get; set; } = new();
}