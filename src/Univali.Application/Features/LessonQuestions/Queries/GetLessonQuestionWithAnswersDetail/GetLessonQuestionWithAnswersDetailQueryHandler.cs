using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.LessonQuestions.Queries.GetLessonQuestionWithAnswersDetail;

public class GetLessonQuestionWithAnswersDetailQueryHandler : IRequestHandler<GetLessonQuestionWithAnswersDetailQuery, GetLessonQuestionWithAnswersDetailResponse>
{
    private readonly IPublisherRepository _questionRepository;
    private readonly IMapper _mapper;

    public GetLessonQuestionWithAnswersDetailQueryHandler(IMapper mapper, IPublisherRepository questionRepository)
    {
        _mapper = mapper;
        _questionRepository = questionRepository;
    }

    public async Task<GetLessonQuestionWithAnswersDetailResponse> Handle(GetLessonQuestionWithAnswersDetailQuery request, CancellationToken cancellationToken)
    {
        GetLessonQuestionWithAnswersDetailResponse getLessonQuestionWithAnswersDetailResponse = new();

        if(! await _questionRepository.LessonExistsAsync(request.LessonId))
        {
            getLessonQuestionWithAnswersDetailResponse.ErrorType = Error.NotFoundProblem;
            getLessonQuestionWithAnswersDetailResponse.Errors.Add("Lesson", new string[] {"Lesson Not Found"});
            return getLessonQuestionWithAnswersDetailResponse;
        }

        var questionFromDataBase = await _questionRepository.GetQuestionWithAnswersByIdAsync(request.QuestionId);

        if(questionFromDataBase == null || questionFromDataBase.LessonId != request.LessonId)
        {
            getLessonQuestionWithAnswersDetailResponse.ErrorType = Error.NotFoundProblem;
            getLessonQuestionWithAnswersDetailResponse.Errors.Add("Question", new string[] {"Question Not Found in this lesson"});
            return getLessonQuestionWithAnswersDetailResponse;
        }

        getLessonQuestionWithAnswersDetailResponse.Question = _mapper.Map<GetLessonQuestionWithAnswersDetailDto>(questionFromDataBase);

        return getLessonQuestionWithAnswersDetailResponse;
    }
}