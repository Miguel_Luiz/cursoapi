using MediatR;

namespace Univali.Application.Features.LessonQuestions.Queries.GetLessonQuestionWithAnswersDetail;

public class GetLessonQuestionWithAnswersDetailQuery : IRequest<GetLessonQuestionWithAnswersDetailResponse>
{
        public int QuestionId { get; set; }
        public int LessonId { get; set; }
}