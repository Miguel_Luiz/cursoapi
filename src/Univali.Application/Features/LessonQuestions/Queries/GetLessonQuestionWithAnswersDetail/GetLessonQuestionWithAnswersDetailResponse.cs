using Univali.Application.Features.Common;

namespace Univali.Application.Features.LessonQuestions.Queries.GetLessonQuestionWithAnswersDetail;

public class GetLessonQuestionWithAnswersDetailResponse : BaseResponse
{
    public GetLessonQuestionWithAnswersDetailDto Question {get;set;} = new();
}