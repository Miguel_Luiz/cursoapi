using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.LessonQuestions.Queries.GetLessonQuestionsDetail;

public class GetLessonQuestionsDetailQueryHandler : IRequestHandler<GetLessonQuestionsDetailQuery, GetLessonQuestionsDetailResponse>
{
    private readonly IPublisherRepository _questionRepository;
    private readonly IMapper _mapper;

    public GetLessonQuestionsDetailQueryHandler(IMapper mapper, IPublisherRepository questionRepository)
    {
        _mapper = mapper;
        _questionRepository = questionRepository;
    }

    public async Task<GetLessonQuestionsDetailResponse> Handle(GetLessonQuestionsDetailQuery request, CancellationToken cancellationToken)
    {
        GetLessonQuestionsDetailResponse getLessonQuestionsDetailResponse = new();

        if(! await _questionRepository.LessonExistsAsync(request.LessonId))
        {
            getLessonQuestionsDetailResponse.Errors.Add("Lesson", new string[] {"Lesson Not Found"});
            getLessonQuestionsDetailResponse.ErrorType = Error.NotFoundProblem;
            return getLessonQuestionsDetailResponse;
        }

        var (questionsFromDatabase, paginationMetadata)  = await _questionRepository.GetQuestionsByLessonIdAsync(request.LessonId, request.PageNumber, request.PageSize);

        getLessonQuestionsDetailResponse.QuestionsDetailDto = _mapper.Map<IEnumerable<GetLessonQuestionsDetailDto>>(questionsFromDatabase);

        getLessonQuestionsDetailResponse.paginationMetadata = paginationMetadata;

        return getLessonQuestionsDetailResponse;
    }
}