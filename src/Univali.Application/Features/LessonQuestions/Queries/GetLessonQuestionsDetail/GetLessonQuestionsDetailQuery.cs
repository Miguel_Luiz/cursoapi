using MediatR;

namespace Univali.Application.Features.LessonQuestions.Queries.GetLessonQuestionsDetail;

public class GetLessonQuestionsDetailQuery : IRequest<GetLessonQuestionsDetailResponse>
{
        public int LessonId { get; set; }
        public int PageNumber {get;set;}
        public int PageSize{get;set;}
}