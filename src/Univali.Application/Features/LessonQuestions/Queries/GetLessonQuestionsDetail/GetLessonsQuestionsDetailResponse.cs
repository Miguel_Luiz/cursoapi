using Univali.Application.Features.Common;

namespace Univali.Application.Features.LessonQuestions.Queries.GetLessonQuestionsDetail;

public class GetLessonQuestionsDetailResponse : BaseResponse
{
    public IEnumerable<GetLessonQuestionsDetailDto> QuestionsDetailDto {get;set;} = null!;

    public PaginationMetadata paginationMetadata = null!;
}