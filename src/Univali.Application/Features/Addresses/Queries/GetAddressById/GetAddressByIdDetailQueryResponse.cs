using Univali.Application.Features.Common;

namespace Univali.Application.Features.Addresses.Queries.GetAddressByIdDetail;


public class GetAddressByIdDetailQueryResponse : BaseResponse
{


    public GetAddressByIdDetailDto Customer {get; set;} = new();

    public GetAddressByIdDetailQueryResponse()
    {
        Errors = new Dictionary<string, string[]>();
    }
}