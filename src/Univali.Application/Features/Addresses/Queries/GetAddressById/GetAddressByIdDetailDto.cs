namespace Univali.Application.Features.Addresses.Queries.GetAddressByIdDetail;


public class GetAddressByIdDetailDto
{
   public int Id { get; set; }
   public string Street { get; set; } = string.Empty;
   public string City { get; set; } = string.Empty;
}

