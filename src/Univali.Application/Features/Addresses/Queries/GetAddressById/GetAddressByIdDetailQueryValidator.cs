
using System.Data;
using FluentValidation;

namespace Univali.Application.Features.Addresses.Queries.GetAddressByIdDetail;

public class GetAddressByIdDetailQueryValidator :
    AbstractValidator<GetAddressByIdDetailQuery>
 {
    public GetAddressByIdDetailQueryValidator() 
    {
        RuleFor(c => c.CustomerId)
        .NotEmpty()
        .WithMessage("Fill a Id")      
        .GreaterThan(0)
        .WithMessage("The CustomerId must be greater than zero.");

        RuleFor(a => a.AddressId)
        .NotEmpty()
        .WithMessage("Fill a Id")      
        .GreaterThan(0)
        .WithMessage("The AddressId must be greater than zero.");
        
    }
 }