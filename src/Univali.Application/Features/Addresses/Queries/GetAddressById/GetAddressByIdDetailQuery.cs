using MediatR;

namespace Univali.Application.Features.Addresses.Queries.GetAddressByIdDetail;

public class GetAddressByIdDetailQuery : IRequest<GetAddressByIdDetailQueryResponse>
{
    public int CustomerId { get; set; }
    public int AddressId { get; set; }
}