using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Domain.Entities;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;


namespace Univali.Application.Features.Addresses.Queries.GetAddressByIdDetail;

public class GetAddressByIdDetailQueryHandler : 
    IRequestHandler<GetAddressByIdDetailQuery, GetAddressByIdDetailQueryResponse>
{
    private readonly ICustomerRepository _customerRepository;

    private readonly IMapper _mapper;

    public readonly IValidator<GetAddressByIdDetailQuery> _validator;

    public GetAddressByIdDetailQueryHandler(
        ICustomerRepository customerRepository, 
        IMapper mapper,
        IValidator<GetAddressByIdDetailQuery> validator
    ) {
        _customerRepository = customerRepository;

        _mapper = mapper;

        _validator = validator;
    }

    public async Task<GetAddressByIdDetailQueryResponse> Handle(
        GetAddressByIdDetailQuery getAddressByIdDetailQuery, 
        CancellationToken cancellationToken
    ) {

        GetAddressByIdDetailQueryResponse response = new();

        var validationResult = _validator.Validate(getAddressByIdDetailQuery);

        if (validationResult.IsValid == false) 
        {
            foreach (var error in validationResult.ToDictionary()) 
            {
                response.Errors
                    .Add(error.Key, error.Value);
            }

            response.ErrorType = Error.ValidationProblem;
            return response;
        }

        var customerFromDatabase = await _customerRepository.GetCustomerByIdAsync(getAddressByIdDetailQuery.CustomerId);

        if (customerFromDatabase == null){

            response.Errors.Add(
                "Customer",
                new string[] { $"Customer with id = {getAddressByIdDetailQuery.CustomerId} was not found in the database" }
            );

            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var addressFromDatabase = await _customerRepository
            .GetAddressByIdAsync(
                getAddressByIdDetailQuery.CustomerId, 
                getAddressByIdDetailQuery.AddressId
            );

        if (addressFromDatabase == null){

            response.Errors.Add(
                "Address",
                new string[] { $"Address with id = {getAddressByIdDetailQuery.AddressId} was not found in the database" }
            );

            response.ErrorType = Error.NotFoundProblem;
            return response;
        }


        response.Customer = _mapper
            .Map<GetAddressByIdDetailDto>(addressFromDatabase);
        
        return response;
        
        //return _mapper.Map<GetAddressDetailDto>(customerFromDatabase);
    }
}