using Univali.Application.Features.Common;

namespace Univali.Application.Features.Addresses.Queries.GetAddressesFilter;

public class GetAddresesFilterDetailQueryResponse : BaseResponse
{
    public IEnumerable<GetAddresesFilterDetailDto> Addreses{ get; set; } = default!;
    public PaginationMetadata? PaginationMetadata { get; set; }
    public GetAddresesFilterDetailQueryResponse()
    {
        Errors = new Dictionary<string, string[]>();
    }
}