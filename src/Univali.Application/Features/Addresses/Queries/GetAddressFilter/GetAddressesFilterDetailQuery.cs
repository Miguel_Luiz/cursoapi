using MediatR;

namespace Univali.Application.Features.Addresses.Queries.GetAddressesFilter;

public class GetAddresesFilterDetailQuery : IRequest<GetAddresesFilterDetailQueryResponse>
{
    public int CustomerId { get; set; }
    public string? City { get; set; }
    public string? SearchQuery { get; set; }
    public int PageNumber { get; set; }
    public int PageSize { get; set; }
}