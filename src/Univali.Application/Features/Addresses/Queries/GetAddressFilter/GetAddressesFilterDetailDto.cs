namespace Univali.Application.Features.Addresses.Queries.GetAddressesFilter;

public class GetAddresesFilterDetailDto 
{
  public int Id { get; set; }
  public string Street { get; set; } = string.Empty;
  public string City { get; set; } = string.Empty;
}