using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Addresses.Queries.GetAddressesFilter;

public class GetAddresesFilterDetailQueryHandler : IRequestHandler<GetAddresesFilterDetailQuery,GetAddresesFilterDetailQueryResponse>
{
    private readonly ICustomerRepository _customerRepository;
    private readonly IMapper _mapper;

    public GetAddresesFilterDetailQueryHandler(ICustomerRepository customerRepository, IMapper mapper)
    {
        _customerRepository = customerRepository;
        _mapper = mapper;
    }

    public async Task<GetAddresesFilterDetailQueryResponse> Handle(GetAddresesFilterDetailQuery request, CancellationToken cancellationToken)
    {

        GetAddresesFilterDetailQueryResponse response = new();

        if (await _customerRepository.GetCustomerByIdAsync(request.CustomerId) == null)
        {
            response.Errors.Add(
                "Customer",
                new string[] { $"Customer with id = {request.CustomerId} was not found in the database." }
            );

            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var (addressToReturn, paginationMetadata) = await _customerRepository.GetAddressesAsync
        (request.CustomerId, request.City, request.SearchQuery, request.PageNumber, request.PageSize);
        
        if(addressToReturn == null || !addressToReturn.Any())
        {
            response.Errors.Add(
                "Addreses",
                new string[] { $"No Addreses for Customer with id = {request.CustomerId} were found in the database." }
            );

            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        response.Addreses = _mapper.Map<IEnumerable<GetAddresesFilterDetailDto>>(addressToReturn);
        response.PaginationMetadata = paginationMetadata;

        return response;
    }
}