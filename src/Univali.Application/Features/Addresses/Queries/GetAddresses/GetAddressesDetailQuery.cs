using MediatR;
using Univali.Domain.Entities;

namespace Univali.Application.Features.Addresses.Queries.GetAddresses;

public class GetAddressesDetailQuery : IRequest<GetAddressesDetailQueryResponse> 
{ 
    public int CustomerId { get; set; }
}