using Univali.Application.Features.Common;

namespace Univali.Application.Features.Addresses.Queries.GetAddresses;

public class GetAddressesDetailQueryResponse : BaseResponse
{
    public List<GetAddressDetailDto> Addresses{ get; set; } = default!;

    public GetAddressesDetailQueryResponse()
    {
        Errors = new Dictionary<string, string[]>();
    }
}