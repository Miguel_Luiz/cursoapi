
using AutoMapper;
using MediatR;
using Univali.Domain.Entities;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;


namespace Univali.Application.Features.Addresses.Queries.GetAddresses;

public class GetAddressesDetailQueryHandler : IRequestHandler<GetAddressesDetailQuery,GetAddressesDetailQueryResponse>
{
    private readonly ICustomerRepository _customerRepository;
    private readonly IMapper _mapper;

    public GetAddressesDetailQueryHandler(ICustomerRepository customerRepository, IMapper mapper)
    {
        _customerRepository = customerRepository;
        _mapper = mapper;
    }

    public async Task<GetAddressesDetailQueryResponse> Handle(GetAddressesDetailQuery request, CancellationToken cancellationToken)
    {
        GetAddressesDetailQueryResponse response = new();

        var customerFromDatabase = await _customerRepository.GetCustomerByIdAsync(request.CustomerId);

        if(customerFromDatabase == null)
        {
            response.Errors.Add(
                "Customer",
                new string[] { $"Customer with id = {request.CustomerId} was not found in the database." }
            );
            
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var addressesFromDatabase = await _customerRepository.GetAddressesAsync(request.CustomerId);

        if(addressesFromDatabase == null || !addressesFromDatabase.Any())
        {
            response.Errors.Add(
                "Addresses",
                new string[] { $"No addresses for customer with id = {request.CustomerId} were found in the database." }
            );
            
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        response.Addresses = _mapper.Map<List<GetAddressDetailDto>>(addressesFromDatabase);


        return response;
        
    }
}