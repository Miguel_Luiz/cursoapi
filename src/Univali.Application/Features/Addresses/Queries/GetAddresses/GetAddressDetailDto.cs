namespace Univali.Application.Features.Addresses.Queries.GetAddresses;

public class GetAddressDetailDto
{
    public int Id { get; set; }
    public string Street { get; set; } = string.Empty;
    public string City { get; set; } = string.Empty;
}

