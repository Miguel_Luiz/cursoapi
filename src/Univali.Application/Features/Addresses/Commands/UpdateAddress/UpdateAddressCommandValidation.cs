using FluentValidation;
using MediatR;
using Univali.Application.Features.Customers.Queries.GetCustomerDetail;

namespace Univali.Application.Features.Addresses.Commands.UpdateAddress;

public class UpdateAddressCommandValidator : AbstractValidator<UpdateAddressCommand>
{
    public UpdateAddressCommandValidator()
    {
        RuleFor(c => c.Street)
            .NotEmpty()
                .WithMessage("You should fill out a Street")
            .MaximumLength(50)
                .WithMessage("The Street should have 50 characters");

        RuleFor(c => c.City)
            .NotEmpty()
                .WithMessage("You should fill out a City")
            .MaximumLength(50)
                .WithMessage("The City should have 50 characters");

        RuleFor(c => c.CustomerId)
            .NotEmpty()
                .WithMessage("You should fill out a CustomerId");
    }
   
}