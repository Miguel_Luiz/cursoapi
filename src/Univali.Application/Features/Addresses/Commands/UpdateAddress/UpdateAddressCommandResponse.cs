using Univali.Application.Features.Common;

namespace Univali.Application.Features.Addresses.Commands.UpdateAddress;

public class UpdateAddressCommandResponse : BaseResponse
{
    public UpdateAddressCommandResponse()
    {
        Errors = new Dictionary<string, string[]>();
    }
}
