using MediatR;

namespace Univali.Application.Features.Addresses.Commands.UpdateAddress;

public class UpdateAddressCommand: IRequest <UpdateAddressCommandResponse>
{
    public int AddressId { get; set; }
    public string Street { get; set; } = string.Empty;
    public string City { get; set; } = string.Empty;
    public int CustomerId {get; set;}
}