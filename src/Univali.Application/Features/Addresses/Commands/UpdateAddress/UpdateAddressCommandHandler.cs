using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Domain.Entities;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;


namespace Univali.Application.Features.Addresses.Commands.UpdateAddress;

// O primeiro parâmetro é o tipo da mensagem
// O segundo parâmetro é o tipo que se espera receber.
public class UpdateAddressCommandHandler: IRequestHandler<UpdateAddressCommand, UpdateAddressCommandResponse>
{
    private readonly ICustomerRepository _customerRepository;
    private readonly IMapper _mapper;
    private readonly IValidator<UpdateAddressCommand> _validator;

    public UpdateAddressCommandHandler(ICustomerRepository customerRepository, IMapper mapper, IValidator<UpdateAddressCommand> validator)
    {
        _customerRepository = customerRepository ?? throw new ArgumentNullException(nameof(customerRepository));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        _validator = validator ?? throw new ArgumentNullException(nameof(validator));
    }

    public async Task<UpdateAddressCommandResponse> Handle(UpdateAddressCommand request, CancellationToken cancellationToken)
    {
        UpdateAddressCommandResponse response = new();

        var validationResult = _validator.Validate(request);

        if(!validationResult.IsValid)
        {
            foreach(var error in validationResult.ToDictionary())
            {
                response.Errors.Add(error.Key, error.Value);
            }

            response.ErrorType = Error.ValidationProblem;
            return response;
        }

        var customerFromDatabase = await _customerRepository.GetCustomerByIdAsync(request.CustomerId);

        if(customerFromDatabase == null)
        {
            response.Errors.Add(
                "Customer",
                new string[] { $"Customer with id = {request.CustomerId} was not found in the database" }
            );
            
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var addressFromDatabase = await _customerRepository.GetAddressByIdAsync(request.CustomerId, request.AddressId);

        if(addressFromDatabase == null)
        {
            response.Errors.Add(
                "Address",
                new string[] { $"Address with id = {request.AddressId} was not found in the database" }
            );
            
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        _customerRepository.UpdateAddress(addressFromDatabase, request);
        await _customerRepository.SaveChangesAsync();

        return response;
    }
}