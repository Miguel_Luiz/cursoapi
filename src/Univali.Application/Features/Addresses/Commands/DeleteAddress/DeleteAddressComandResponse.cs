using Univali.Application.Features.Common;

namespace Univali.Application.Features.Addresses.Commands.DeleteAddress;

public class DeleteAddressCommandResponse: BaseResponse
{

    public DeleteAddressCommandResponse()
    {
        Errors = new Dictionary<string, string[]>();
    }
}