using MediatR;

namespace Univali.Application.Features.Addresses.Commands.DeleteAddress;

public class DeleteAddressCommand : IRequest<DeleteAddressCommandResponse>
{
    public int CustomerId { get; set; }
    public int AddressId { get; set; }
}