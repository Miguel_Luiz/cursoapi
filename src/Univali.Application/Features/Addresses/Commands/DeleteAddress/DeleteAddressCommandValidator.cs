using FluentValidation;

namespace Univali.Application.Features.Addresses.Commands.DeleteAddress;

public class DeleteAddressCommandValidator :
    AbstractValidator<DeleteAddressCommand>
 {
    public DeleteAddressCommandValidator() 
    {
        RuleFor(a => a.AddressId)
        .NotEmpty()
        .WithMessage("Fill an Id")      
        .GreaterThan(0)
        .WithMessage("The AddressId must be greater than zero.");
    }
 }