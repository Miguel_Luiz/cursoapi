using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Domain.Entities;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Addresses.Commands.DeleteAddress;

public class DeleteAddressCommandHandler : 
    IRequestHandler<DeleteAddressCommand,DeleteAddressCommandResponse>
{
    private readonly ICustomerRepository _customerRepository;
    public readonly IValidator<DeleteAddressCommand> _validator;

    public DeleteAddressCommandHandler(
        ICustomerRepository customerRepository,
        IValidator<DeleteAddressCommand> validator
    ) {
        _customerRepository = customerRepository;
        _validator = validator;
    }

    public async Task<DeleteAddressCommandResponse> Handle(
        DeleteAddressCommand deleteAddressCommand, 
        CancellationToken cancellationToken
    ) {
        DeleteAddressCommandResponse response = new ();

        var validationResult = _validator.Validate(deleteAddressCommand);

        if (validationResult.IsValid == false) 
        {
            foreach (var error in validationResult.ToDictionary()) 
            {
                response.Errors
                    .Add(error.Key, error.Value);
            }

            response.ErrorType = Error.ValidationProblem;

            return response;
        } 

        var addressFromDatabase = await _customerRepository.GetAddressByIdAsync(deleteAddressCommand.CustomerId,deleteAddressCommand.AddressId);

        if (addressFromDatabase == null) 
        { 
            response.Errors.Add(
                "Address",
                new string[] { $"Address with id = {deleteAddressCommand.AddressId} was not found in the database" }
            );

            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        _customerRepository.DeleteAddress(addressFromDatabase);

        await _customerRepository.SaveChangesAsync();

        return response;
    }
}