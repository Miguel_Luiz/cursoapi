using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Domain.Entities;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;


namespace Univali.Application.Features.Addresses.Commands.CreateAddress;

// O primeiro parâmetro é o tipo da mensagem
// O segundo parâmetro é o tipo que se espera receber.
public class CreateAddressCommandHandler: IRequestHandler<CreateAddressCommand, CreateAddressCommandResponse>
{
    private readonly ICustomerRepository _customerRepository;
    private readonly IMapper _mapper;
    private readonly IValidator<CreateAddressCommand> _validator;

    public CreateAddressCommandHandler(ICustomerRepository customerRepository, IMapper mapper, IValidator<CreateAddressCommand> validator)
    {
        _customerRepository = customerRepository; //?? throw new ArgumentNullException(nameof(customerRepository));
        _mapper = mapper; //?? throw new ArgumentNullException(nameof(mapper));
        _validator = validator; //?? throw new ArgumentNullException(nameof(validator));
    }

    public async Task<CreateAddressCommandResponse> Handle(CreateAddressCommand request, CancellationToken cancellationToken)
    {
        CreateAddressCommandResponse createAddressCommandResponse = new();

        var validationResult = _validator.Validate(request);

        if(!validationResult.IsValid)
        {

            foreach(var error in validationResult.ToDictionary())
            {
                createAddressCommandResponse.Errors.Add(error.Key, error.Value);
            }

            createAddressCommandResponse.ErrorType = Error.ValidationProblem;

            return createAddressCommandResponse;
        }

        var customerFromDatabase = await _customerRepository.GetCustomerByIdAsync(request.CustomerId);
        if(customerFromDatabase == null)
        {
            string[] errors = { $"Customer with id = {request.CustomerId} was not found in the database" };
            createAddressCommandResponse.Errors.Add("Customer", errors);
            createAddressCommandResponse.ErrorType = Error.NotFoundProblem;
            return createAddressCommandResponse;
        }

        var newAddress = _mapper.Map<Address>(request);

        _customerRepository.AddAddress(customerFromDatabase, newAddress);
        await _customerRepository.SaveChangesAsync();

        createAddressCommandResponse.Address = _mapper.Map<CreateAddressDto>(newAddress);
        

        return createAddressCommandResponse;
    }
}