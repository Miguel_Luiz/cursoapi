using Univali.Application.Features.Common;

namespace Univali.Application.Features.Addresses.Commands.CreateAddress;

public class CreateAddressCommandResponse : BaseResponse
{
    public CreateAddressDto Address { get; set; } = default!;

    public CreateAddressCommandResponse()
    {
        Errors = new Dictionary<string, string[]>();
    }
}