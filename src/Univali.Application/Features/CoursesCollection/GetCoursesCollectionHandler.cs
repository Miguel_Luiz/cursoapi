using AutoMapper;
using MediatR;
using Univali.Application.Models;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.CoursesCollection;

public class GetCoursesCollectionHandler : IRequestHandler<GetCoursesCollectionQuery, GetCoursesCollectionResponse>
{
    private readonly IPublisherRepository _publisherRepository;

    private readonly IMapper _mapper;

    public GetCoursesCollectionHandler(IPublisherRepository publisherRepository, IMapper mapper)
    {
        _publisherRepository = publisherRepository;
        _mapper = mapper;
    }

    public async Task<GetCoursesCollectionResponse> Handle(
        GetCoursesCollectionQuery getCoursesCollectionQuery,
        CancellationToken cancellationToken
    )
    {
        var (courses, paginationMetadata) = await _publisherRepository
             .GetCoursesCollectionAsync(
                 getCoursesCollectionQuery.Category,
                 getCoursesCollectionQuery.SearchQuery,
                 getCoursesCollectionQuery.PageNumber,
                 getCoursesCollectionQuery.PageSize
             );

        var getCoursesCollectionResponse = new GetCoursesCollectionResponse();

        getCoursesCollectionResponse.Courses = _mapper.Map<List<CourseDto>>(courses);

        getCoursesCollectionResponse.Pagination = paginationMetadata;

        return getCoursesCollectionResponse;
    }
}