using Univali.Application.Features.Common;
using Univali.Application.Models;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.CoursesCollection;

public class GetCoursesCollectionResponse : BaseResponse
{
    public List<CourseDto> Courses {get; set;} = new();

    public PaginationMetadata? Pagination {get; set;}
}