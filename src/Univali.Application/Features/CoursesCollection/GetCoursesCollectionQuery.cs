using MediatR;

namespace Univali.Application.Features.CoursesCollection;

public class GetCoursesCollectionQuery : IRequest<GetCoursesCollectionResponse>
{
    public GetCoursesCollectionQuery(string? category, string? searchQuery, int pageNumber, int pageSize)
    {
        Category = category;
        SearchQuery = searchQuery; 
        PageNumber = pageNumber;
        PageSize = pageSize;
    }

    public string? Category { get; set; }
    public string? SearchQuery { get; set; }
    public int PageNumber { get; set; }
    public int PageSize { get; set; }
}