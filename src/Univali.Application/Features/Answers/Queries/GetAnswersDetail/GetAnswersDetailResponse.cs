using Univali.Application.Features.Common;

namespace Univali.Application.Features.Answers.Queries.GetAnswersDetail;

public class GetAnswersDetailResponse : BaseResponse
{
    public IEnumerable<GetAnswersDetailDto> AnswersDetailDtos {get;set;} = null!;

    public PaginationMetadata paginationMetadata = null!;
}