using MediatR;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Answers.Queries.GetAnswersDetail;

public class GetAnswersDetailQuery : IRequest<GetAnswersDetailResponse>
{
    public int AuthorId {get; set;}
    public int PageNumber {get;set;}
    public int PageSize{get;set;}
}