using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Answers.Queries.GetAnswersDetail;

public class GetAnswersDetailQueryHandler : IRequestHandler<GetAnswersDetailQuery, GetAnswersDetailResponse>
{
    private readonly IPublisherRepository _publisherRepository;
    private readonly IMapper _mapper;

    public GetAnswersDetailQueryHandler(IPublisherRepository publisherRepository, IMapper mapper)
    {
        _publisherRepository = publisherRepository;
        _mapper = mapper;
    }

    public async Task<GetAnswersDetailResponse> Handle(GetAnswersDetailQuery request, CancellationToken cancellationToken)
    {
        GetAnswersDetailResponse getAnswersDetailResponse = new();

        if(! await _publisherRepository.AuthorExistsAsync(request.AuthorId))
        {
            getAnswersDetailResponse.Errors.Add("Author", new string[] {"Author Not Found"});
            getAnswersDetailResponse.ErrorType = Error.NotFoundProblem;
            return getAnswersDetailResponse;
        }

        var (answersFromDatabase, paginationMetadata)  = await _publisherRepository.GetAnswersByAuthorIdAsync(request.AuthorId, request.PageNumber, request.PageSize);

        getAnswersDetailResponse.AnswersDetailDtos =  _mapper.Map<IEnumerable<GetAnswersDetailDto>>(answersFromDatabase);

        getAnswersDetailResponse.paginationMetadata = paginationMetadata;

        return getAnswersDetailResponse;
    }
}