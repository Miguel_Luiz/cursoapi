using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Answers.Queries.GetAnswerDetail;

public class GetAnswerDetailQueryHandler : IRequestHandler<GetAnswerDetailQuery, GetAnswerDetailResponse>
{
    private readonly IPublisherRepository _publisherRepository;
    private readonly IMapper _mapper;

    public GetAnswerDetailQueryHandler(IPublisherRepository publisherRepository, IMapper mapper)
    {
        _publisherRepository = publisherRepository;
        _mapper = mapper;
    }

    public async Task<GetAnswerDetailResponse> Handle(GetAnswerDetailQuery request, CancellationToken cancellationToken)
    {
        var getAnswerDetailResponse = new GetAnswerDetailResponse();

        var authorFromDatabase = await _publisherRepository.GetAuthorWithAnswersByIdAsync(request.AuthorId);

        if(authorFromDatabase == null)
        {
            getAnswerDetailResponse.ErrorType = Error.NotFoundProblem;
            getAnswerDetailResponse.Errors.Add("Author", new string[] {"Author Not Found"});
            return getAnswerDetailResponse;
        }

        var answerFromDatabase = authorFromDatabase.Answers.FirstOrDefault(a => a.AnswerId == request.AnswerId);

        if(answerFromDatabase == null)
        {
            getAnswerDetailResponse.ErrorType = Error.NotFoundProblem;
            getAnswerDetailResponse.Errors.Add("Answer", new string[] {"Answer not found in this author"});
            return getAnswerDetailResponse;
        }

        getAnswerDetailResponse.Answer =  _mapper.Map<GetAnswerDetailDto>(answerFromDatabase);

        return getAnswerDetailResponse;
    }
}