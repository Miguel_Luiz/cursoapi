using Univali.Application.Features.Common;

namespace Univali.Application.Features.Answers.Queries.GetAnswerDetail;

public class GetAnswerDetailResponse : BaseResponse
{
    public GetAnswerDetailDto Answer {get;set;} = new();
}