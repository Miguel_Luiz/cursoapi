using MediatR;

namespace Univali.Application.Features.Answers.Queries.GetAnswerDetail;

public class GetAnswerDetailQuery : IRequest<GetAnswerDetailResponse>
{
    public int AuthorId {get; set;}
    public int AnswerId {get; set;}
}