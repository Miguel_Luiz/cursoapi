using MediatR;

namespace Univali.Application.Features.Answers.Commands.DeleteAnswer;

public class DeleteAnswerCommand : IRequest<DeleteAnswerCommandResponse>
{
    public int AnswerId {get; set;}
    public int AuthorId {get; set;}
}