using MediatR;

namespace Univali.Application.Features.Answers.Commands.UpdateAnswer;

public class UpdateAnswerCommand : IRequest<UpdateAnswerCommandResponse>
{
    public int AnswerId {get; set;}
    public string Body {get; set;} = string.Empty;
    public int AuthorId {get; set;}
}