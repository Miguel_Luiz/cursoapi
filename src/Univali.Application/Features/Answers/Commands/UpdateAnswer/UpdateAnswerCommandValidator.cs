using FluentValidation;

namespace Univali.Application.Features.Answers.Commands.UpdateAnswer;

public class UpdateAnswerCommandValidator : AbstractValidator<UpdateAnswerCommand>
{
    public UpdateAnswerCommandValidator()
    {
        RuleFor(a => a.AnswerId)
        .NotEmpty()
        .WithMessage("You should fill out an AnswerId");

        RuleFor(a => a.Body)
        .NotEmpty()
        .WithMessage("You should fill out a Body");
    }
}