using AutoMapper;
using FluentValidation;
using FluentValidation.Results;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Answers.Commands.UpdateAnswer;

public class UpdateAnswerCommandHandler : IRequestHandler<UpdateAnswerCommand, UpdateAnswerCommandResponse>
{
    private readonly IPublisherRepository _repository;
    private readonly IMapper _mapper;
    private readonly IValidator<UpdateAnswerCommand> _validator;

    public UpdateAnswerCommandHandler(IPublisherRepository repository, IMapper mapper, IValidator<UpdateAnswerCommand> validator)
    {
        _repository = repository;
        _mapper = mapper;
        _validator = validator;
    }

    public async Task<UpdateAnswerCommandResponse> Handle(UpdateAnswerCommand request, CancellationToken cancellationToken)
    {
        UpdateAnswerCommandResponse updateAnswerCommandResponse = new();

        ValidationResult validationResult = _validator.Validate(request);

        if (!validationResult.IsValid)
        {
            updateAnswerCommandResponse.FillErrors(validationResult);
            updateAnswerCommandResponse.ErrorType = Error.ValidationProblem;
            return updateAnswerCommandResponse;
        }

        var authorFromDatabase = await _repository.GetAuthorWithAnswersByIdAsync(request.AuthorId);

        if (authorFromDatabase == null)
        {
            updateAnswerCommandResponse.Errors.Add("Author", new string[]{"Author Not Found"});
            updateAnswerCommandResponse.ErrorType = Error.NotFoundProblem;
            return updateAnswerCommandResponse;
        }

        var answerFromDatabase = authorFromDatabase.Answers.FirstOrDefault(a => a.AnswerId == request.AnswerId);

        if (answerFromDatabase == null) 
        {
            updateAnswerCommandResponse.Errors.Add("Answer", new string[]{"Answer not found in this author"});
            updateAnswerCommandResponse.ErrorType = Error.NotFoundProblem;
            return updateAnswerCommandResponse;
        }    

        _mapper.Map(request, answerFromDatabase);
        
        await _repository.SaveChangesAsync();

        return updateAnswerCommandResponse;
    }
}