using Univali.Application.Features.Common;

namespace Univali.Application.Features.Answers.Commands.CreateAnswer;

public class CreateAnswerCommandResponse : BaseResponse
{
    public CreateAnswerDto Answer { get; set;} = default!;
}