using AutoMapper;
using FluentValidation;
using FluentValidation.Results;
using MediatR;
using Univali.Domain.Entities;
using Univali.Application.Features.Common;
using Univali.Application.Contracts.Repositories;


namespace Univali.Application.Features.Answers.Commands.CreateAnswer;

public class CreateAnswerCommandHandler : IRequestHandler<CreateAnswerCommand, CreateAnswerCommandResponse>
{
    private readonly IPublisherRepository _repository;
    private readonly IMapper _mapper;
    private readonly IValidator<CreateAnswerCommand> _validator;

    public CreateAnswerCommandHandler(IPublisherRepository repository, IMapper mapper, IValidator<CreateAnswerCommand> validator)
    {
        _repository = repository;
        _mapper = mapper;
        _validator = validator;
    }

    public async Task<CreateAnswerCommandResponse> Handle(CreateAnswerCommand request, CancellationToken cancellationToken)
    {
        CreateAnswerCommandResponse createAnswerCommandResponse = new();

        ValidationResult validationResult = _validator.Validate(request);

        if (!validationResult.IsValid)
        {
            createAnswerCommandResponse.FillErrors(validationResult);
            createAnswerCommandResponse.ErrorType = Error.ValidationProblem;
            return createAnswerCommandResponse;
        }

        var authorFromDatabase = await _repository.GetAuthorWithCoursesByIdAsync(request.AuthorId);

        if (authorFromDatabase == null)
        {
            createAnswerCommandResponse.ErrorType = Error.NotFoundProblem;
            createAnswerCommandResponse.Errors.Add("Author",new string[] {"Author Not Found"});
            return createAnswerCommandResponse;
        }

        var questionFromDatabase = await _repository.GetQuestionByIdAsync(request.QuestionId);

        if (questionFromDatabase == null)
        {
            createAnswerCommandResponse.ErrorType = Error.NotFoundProblem;
            createAnswerCommandResponse.Errors.Add("Question",new string[] {"Question Not Found"});
            return createAnswerCommandResponse;
        }

        var lessonFromDatabase = await _repository.GetLessonByIdAsync(questionFromDatabase.LessonId);

        var moduleFromDatabase = await _repository.GetModuleByIdAsync(lessonFromDatabase!.ModuleId);

        if(!authorFromDatabase.Courses.Any(c => c.CourseId == moduleFromDatabase!.CourseId))
        {
            createAnswerCommandResponse.ErrorType = Error.NotFoundProblem;
            createAnswerCommandResponse.Errors.Add("Question",new string[] {"Question Not Found in this author"});
            return createAnswerCommandResponse;
        }

        var answerEntity = _mapper.Map<Answer>(request);

        _repository.AddAnswer(answerEntity);
        await _repository.SaveChangesAsync();

        createAnswerCommandResponse.Answer = _mapper.Map<CreateAnswerDto>(answerEntity);
        return createAnswerCommandResponse;
    }
}