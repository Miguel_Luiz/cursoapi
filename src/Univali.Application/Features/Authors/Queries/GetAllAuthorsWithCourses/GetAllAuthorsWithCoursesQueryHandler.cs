using AutoMapper;
using MediatR;
using Univali.Application.Features.Common;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.Authors.Queries
{
    public class GetAllAuthorsWithCoursesQueryHandler :
        IRequestHandler<GetAllAuthorsWithCoursesQuery, GetAllAuthorsWithCoursesQueryResponse>
    {
        private readonly IPublisherRepository _publisherRepository;

        private readonly IMapper _mapper;

        public GetAllAuthorsWithCoursesQueryHandler(
            IPublisherRepository publisherRepository,
            IMapper mapper
        )
        {
            _publisherRepository = publisherRepository;

            _mapper = mapper;
        }

        public async Task<GetAllAuthorsWithCoursesQueryResponse> Handle(GetAllAuthorsWithCoursesQuery getAllAuthorsWithCoursesQuery, CancellationToken cancellationToken)
        {
            GetAllAuthorsWithCoursesQueryResponse getAllAuthorsWithCoursesQueryResponse = new();

            var authorsFromDatabase = await _publisherRepository
                .GetAllAuthorsWithCoursesAsync();

            if (authorsFromDatabase == null)
            {
                getAllAuthorsWithCoursesQueryResponse.Errors.Add(
                    "Authors",
                    new string[] { $"No Authors were found in the database." }
                );
                getAllAuthorsWithCoursesQueryResponse.ErrorType = Error.NotFoundProblem;

                return getAllAuthorsWithCoursesQueryResponse;
            }

            var authorsWithCoursesDto = _mapper
                .Map<List<GetAuthorWithCoursesDto>>(authorsFromDatabase);

            getAllAuthorsWithCoursesQueryResponse.Authors = authorsWithCoursesDto;
            return getAllAuthorsWithCoursesQueryResponse;
        }
    }
}
