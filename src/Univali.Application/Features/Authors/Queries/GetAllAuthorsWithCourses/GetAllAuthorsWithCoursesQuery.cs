using MediatR;

namespace Univali.Application.Features.Authors.Queries;

public class GetAllAuthorsWithCoursesQuery : IRequest<GetAllAuthorsWithCoursesQueryResponse>
{
}