// BRANCH: AULA
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Authors.Queries;

public class GetAllAuthorsWithCoursesQueryResponse : BaseResponse
{
    public List<GetAuthorWithCoursesDto> Authors {get; set;} = new();
}