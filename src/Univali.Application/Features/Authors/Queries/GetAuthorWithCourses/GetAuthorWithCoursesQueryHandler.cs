using AutoMapper;
using MediatR;
using Univali.Application.Features.Authors.Queries;
using Univali.Application.Features.Common;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.Authors.Authors
{
    public class GetAuthorWithCoursesQueryHandler : 
        IRequestHandler<GetAuthorWithCoursesQuery, GetAuthorWithCoursesQueryResponse>
    {
        private readonly IPublisherRepository _publisherRepository;
        
        private readonly IMapper _mapper;

        public GetAuthorWithCoursesQueryHandler(
            IPublisherRepository publisherRepository, 
            IMapper mapper
        ) {
            _publisherRepository = publisherRepository;

            _mapper = mapper;
        }

        public async Task<GetAuthorWithCoursesQueryResponse> Handle(
            GetAuthorWithCoursesQuery getAuthorWithCoursesQuery, 
            CancellationToken cancellationToken
        ) {
            GetAuthorWithCoursesQueryResponse getAuthorWithCoursesQueryResponse = new();

            var authorFromDatabase = await _publisherRepository
                .GetAuthorWithCoursesByIdAsync(getAuthorWithCoursesQuery.AuthorId);

            if (authorFromDatabase == null) 
            { 
                getAuthorWithCoursesQueryResponse.Errors.Add(
                    "Authors",
                    new string[] { $"Author with id = {getAuthorWithCoursesQuery.AuthorId} was not found in the database." }
                );
                getAuthorWithCoursesQueryResponse.ErrorType = Error.NotFoundProblem;

                return getAuthorWithCoursesQueryResponse; 
            }

            var authorWithCoursesDto = _mapper
                .Map<GetAuthorWithCoursesDto>(authorFromDatabase);
            
            getAuthorWithCoursesQueryResponse.Author = authorWithCoursesDto;

            return getAuthorWithCoursesQueryResponse;
        }
    }
}
