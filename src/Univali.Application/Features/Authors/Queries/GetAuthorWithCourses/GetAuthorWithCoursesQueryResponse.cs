// BRANCH: AULA
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Authors.Queries;

public class GetAuthorWithCoursesQueryResponse : BaseResponse
{
    public GetAuthorWithCoursesDto Author {get; set;} = new();
}