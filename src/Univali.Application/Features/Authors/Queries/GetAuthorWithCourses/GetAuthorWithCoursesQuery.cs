using System.ComponentModel.DataAnnotations;
using MediatR;

namespace Univali.Application.Features.Authors.Queries;

public class GetAuthorWithCoursesQuery : IRequest<GetAuthorWithCoursesQueryResponse>
{
    [Required(ErrorMessage = "Error: AuthorId is required")]
    public int AuthorId { get; set; }
}