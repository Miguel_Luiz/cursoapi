using MediatR;
using Univali.Application.Features.Common;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.Authors.Commands;

public class DeleteAuthorCommandHandler : 
    IRequestHandler<DeleteAuthorCommand, DeleteAuthorCommandResponse>
{
    private readonly IPublisherRepository _publisherRepository;

    public DeleteAuthorCommandHandler(
        IPublisherRepository publisherRepository
    ) {
        _publisherRepository = publisherRepository;
    }

    public async Task<DeleteAuthorCommandResponse> Handle(
        DeleteAuthorCommand deleteAuthorCommand, 
        CancellationToken cancellationToken
    ) {
        DeleteAuthorCommandResponse deleteAuthorCommandResponse = new();

        var authorFromDatabase = await _publisherRepository
            .GetAuthorByIdAsync(deleteAuthorCommand.AuthorId);

        if (authorFromDatabase == null) 
        {
            deleteAuthorCommandResponse.Errors.Add(
                "Author",
                new string[] { $"Author with id = {deleteAuthorCommand.AuthorId} was not found in the database." }
            );
            deleteAuthorCommandResponse.ErrorType = Error.NotFoundProblem;

            return deleteAuthorCommandResponse; 
        }

        _publisherRepository.DeleteAuthor(authorFromDatabase);

        await _publisherRepository.SaveChangesAsync();

        return deleteAuthorCommandResponse;
    }
}
