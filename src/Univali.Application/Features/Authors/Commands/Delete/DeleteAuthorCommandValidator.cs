// BRANCH: AULA
using FluentValidation;

namespace Univali.Application.Features.Authors.Commands;

public class DeleteAuthorCommandValidator : AbstractValidator<DeleteAuthorCommand>
{
    public DeleteAuthorCommandValidator() 
    {
        RuleFor(c => c.AuthorId)
        .NotEmpty()
        .WithMessage("Fill a Id")      
        .GreaterThan(0)
        .WithMessage("The AuthorId must be greater than zero.");
    }
 }