using Univali.Application.Features.Common;

namespace Univali.Application.Features.Authors.Commands;

public class DeleteAuthorCommandResponse : BaseResponse
{
    public DeleteAuthorCommandResponse()
    {
        Errors = new Dictionary<string, string[]>();
    }
}