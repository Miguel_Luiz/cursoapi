using System.ComponentModel.DataAnnotations;
using MediatR;

namespace Univali.Application.Features.Authors.Commands;

public class DeleteAuthorCommand : IRequest<DeleteAuthorCommandResponse>
{
    [Required(ErrorMessage = "Error: AuthorId is required")]
    public int AuthorId { get; set; }
}

