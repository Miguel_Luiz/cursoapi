using Univali.Application.Features.Common;

namespace Univali.Application.Features.Authors.Commands.CreateAuthor;

public class CreateAuthorCommandResponse : BaseResponse
{
    public CreateAuthorDto Author { get; set;} = default!;
}