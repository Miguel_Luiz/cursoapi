using Univali.Application.Features.Common;

namespace Univali.Application.Features.Authors.Commands.UpdateAuthor;

public class UpdateAuthorCommandResponse : BaseResponse
{
}