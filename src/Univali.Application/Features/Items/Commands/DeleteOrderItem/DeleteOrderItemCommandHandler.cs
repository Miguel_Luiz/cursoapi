using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Items.Commands.DeleteOrderItem;
public class DeleteOrderItemCommandHandler : IRequestHandler<DeleteOrderItemCommand, DeleteOrderItemCommandResponse>
{
    private readonly IOrderRepository _orderRepository;
    private readonly IMapper _mapper;

    public DeleteOrderItemCommandHandler(IOrderRepository orderRepository, IMapper mapper)
    {
        _orderRepository = orderRepository;
        _mapper = mapper;
    }

    public async Task<DeleteOrderItemCommandResponse> Handle(DeleteOrderItemCommand request, CancellationToken cancellationToken)
    {
        var response = new DeleteOrderItemCommandResponse();

        var customerFromDatabase = await _orderRepository.GetCustomerByIdAsync(request.CustomerId);

        if(customerFromDatabase == null)
        {
            response.Errors.Add(
                "Customer",
                new string[] {$"Customer with id  = {request.CustomerId} was not found in the database"}
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var orderFromDatabase = await _orderRepository.GetOrderInFullByIdAsync(request.CustomerId, request.OrderId);

        if(orderFromDatabase == null)
        {
            response.Errors.Add(
                "Order",
                new string[] {$"Order with id  = {request.OrderId} for customer with id = {request.CustomerId} was not found in the database"}
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var itemFromDatabase = await _orderRepository.GetOrderItemByIdAsync(request.OrderId, request.ItemId);

        if(itemFromDatabase == null)
        {
            response.Errors.Add(
                "Item",
                new string[] {$"Item with id  = {request.ItemId} for order with id = {request.OrderId} was not found in the database"}
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        orderFromDatabase.Items.Remove(itemFromDatabase);

        if(!orderFromDatabase.UpdateReceipt())
        {
            response.Errors.Add(
                "Receipt",
                new string[] {$"Unable to generate receipt for item with id = {request.ItemId}"}
            );
            response.ErrorType = Error.ValidationProblem;
            return response;
        };

        await _orderRepository.SaveChangesAsync();

        return response;
    }
}