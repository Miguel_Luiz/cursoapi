using MediatR;

namespace Univali.Application.Features.Items.Commands.DeleteOrderItem;

public  class DeleteOrderItemCommand: IRequest<DeleteOrderItemCommandResponse>
{
    public int CustomerId { get; set; }
    public int OrderId { get; set; }
    public int ItemId { get; set; }
}