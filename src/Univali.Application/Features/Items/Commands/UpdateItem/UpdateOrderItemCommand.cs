using MediatR;

namespace Univali.Application.Features.OrderItems.Commands.UpdateOrderItem;

public  class UpdateOrderItemCommand: IRequest<UpdateOrderItemCommandResponse>
{
    public int CustomerId { get; set; }
    public int OrderId { get; set; }
    public int ItemId { get; set; }
    public int CourseId { get; set; }
}