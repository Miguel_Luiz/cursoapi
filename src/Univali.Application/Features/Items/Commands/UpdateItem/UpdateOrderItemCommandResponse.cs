using Univali.Application.Features.Common;

namespace Univali.Application.Features.OrderItems.Commands.UpdateOrderItem;

public class UpdateOrderItemCommandResponse : BaseResponse
{ }