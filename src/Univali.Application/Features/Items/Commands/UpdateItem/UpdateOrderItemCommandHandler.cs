using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Domain.Entities;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.OrderItems.Commands.UpdateOrderItem;

public class UpdateOrderItemCommandHandler : IRequestHandler<UpdateOrderItemCommand, UpdateOrderItemCommandResponse>
{
    private readonly IOrderRepository _orderRepository;
    private readonly IMapper _mapper;
    private readonly IValidator<UpdateOrderItemCommand> _validator;

    public UpdateOrderItemCommandHandler(IOrderRepository orderRepository, IMapper mapper, IValidator<UpdateOrderItemCommand> validator)
    {
        _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        _validator = validator ?? throw new ArgumentNullException(nameof(validator));
    }

    public async Task<UpdateOrderItemCommandResponse> Handle(UpdateOrderItemCommand request, CancellationToken cancellationToken)
    {
        UpdateOrderItemCommandResponse response = new();

        var validationResult = _validator.Validate(request);

        if(!validationResult.IsValid)
        {
            foreach(var error in validationResult.ToDictionary())
            {
                response.Errors.Add(error.Key, error.Value);
            }

            response.ErrorType = Error.ValidationProblem;
            return response;
        }

        var customerFromDatabase = await _orderRepository.GetCustomerByIdAsync(request.CustomerId);

        if(customerFromDatabase == null)
        {
            response.Errors.Add(
                "Customer",
                new string[] {$"Customer with id  = {request.CustomerId} was not found in the database"}
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var orderFromDatabase = await _orderRepository.GetOrderInFullByIdAsync(request.CustomerId, request.OrderId);

        if(orderFromDatabase == null)
        {
            response.Errors.Add(
                "Order",
                new string[] { $"Order with id = {request.OrderId} for customer with id = {request.CustomerId} was not found in the database" }
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var itemFromDatabase = await _orderRepository.GetOrderItemByIdAsync(request.OrderId, request.ItemId);

        if(itemFromDatabase == null)
        {
            response.Errors.Add(
                "Item",
                new string[] {$"Item with id  = {request.ItemId} was not found in the database"}
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var itemSnapshot = _mapper.Map<Course>(itemFromDatabase);

        var courseFromDatabase = await _orderRepository.GetCourseWithoutTrackingByIdAsync(request.CourseId);

        if(courseFromDatabase == null)
        {
            response.Errors.Add(
                "Course",
                new string[] {$"Course with id  = {request.CourseId} was not found in the database"}
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        _orderRepository.UpdateOrderItem(itemFromDatabase, courseFromDatabase);
        await _orderRepository.SaveChangesAsync();

        if(!orderFromDatabase.UpdateReceipt())
        {
            _orderRepository.UpdateOrderItem(itemFromDatabase, itemSnapshot); // rollback devido ao SaveChanges anterior
            await _orderRepository.SaveChangesAsync();

            response.Errors.Add(
                "Receipt",
                new string[] {$"Unable to update receipt for order with id = {request.OrderId}"}
            );
            response.ErrorType = Error.ValidationProblem;
            return response;
        }

        await _orderRepository.SaveChangesAsync();

        return response;
    }
}