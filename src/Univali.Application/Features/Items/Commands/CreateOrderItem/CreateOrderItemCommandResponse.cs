using Univali.Application.Features.Common;


namespace Univali.Application.Features.OrderItems.Commands.CreateOrderItem;
public class CreateOrderItemCommandResponse : BaseResponse
{
    // public bool IsValidationProblem { get; set; } = false;
    public CreateOrderItemDto OrderItem { get; set; } = default!;
}