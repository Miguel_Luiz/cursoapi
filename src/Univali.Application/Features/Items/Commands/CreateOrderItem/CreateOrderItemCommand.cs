using MediatR;

namespace Univali.Application.Features.OrderItems.Commands.CreateOrderItem;

public  class CreateOrderItemCommand: IRequest<CreateOrderItemCommandResponse>
{
    public int CustomerId { get; set; }
    public int OrderId { get; set; }
    public int CourseId { get; set; }
}