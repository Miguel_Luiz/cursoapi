using Univali.Application.Features.Common;

namespace Univali.Application.Features.Items.Queries.GetOrdersItemsWithFilter;

public class GetOrdersItemsWithFilterDto 
{
  public int OrderItemId { get; set; }
  public string Title { get; set; } = string.Empty;
  public string Description { get; set; } = string.Empty;
  public decimal Price { get; set; }
  public int OrderId { get; set; }
}
