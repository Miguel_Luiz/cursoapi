using Univali.Application.Features.Common;

namespace Univali.Application.Features.Items.Queries.GetOrdersItemsWithFilter;


public class GetOrdersItemsWithFilterQueryResponse : BaseResponse
{
    public IEnumerable<GetOrdersItemsWithFilterDto> Items { get; set; } = default!;
    public PaginationMetadata? PaginationMetadata {get; set;}
}