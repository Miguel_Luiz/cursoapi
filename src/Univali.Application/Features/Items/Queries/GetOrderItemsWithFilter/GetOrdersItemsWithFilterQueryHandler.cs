using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Items.Queries.GetOrdersItemsWithFilter;

public class GetOrdersItemsWithFilterQueryHandler : IRequestHandler<GetOrdersItemsWithFilterQuery, GetOrdersItemsWithFilterQueryResponse>
{
    private readonly IOrderRepository _orderRepository;
    private readonly IMapper _mapper;

    public GetOrdersItemsWithFilterQueryHandler(IOrderRepository orderRepository, IMapper mapper)
    {
        _orderRepository = orderRepository;
        _mapper = mapper;
    }

    public async Task<GetOrdersItemsWithFilterQueryResponse> Handle(GetOrdersItemsWithFilterQuery request, CancellationToken cancellationToken)
    {
        var response = new GetOrdersItemsWithFilterQueryResponse();

        var customerFromDatabase = await _orderRepository.GetCustomerByIdAsync(request.CustomerId);

        if(customerFromDatabase == null)
        {
            response.Errors.Add(
                "Customer",
                new string[] {$"Customer with id  = {request.CustomerId} was not found in the database"}
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var orderFromDatabase = await _orderRepository.GetOrderInFullByIdAsync(request.CustomerId, request.OrderId);

        if(orderFromDatabase == null)
        {
            response.Errors.Add(
                "Order",
                new string[] { $"Order with id = {request.OrderId} for customer with id = {request.CustomerId} was not found in the database" }
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var (itemsFromDatabase, paginationMetadata) = await _orderRepository.GetOrderItemsWithFilterAsync(request.OrderId, request.PageNumber, request.PageSize);

        // ADICIONADO VALIDAÇÃO NUMERO DE PÁGINAS
        if(request.PageNumber > paginationMetadata.TotalPageCount)
        {
            response.Errors.Add(
                "Page",
                new string[] {$"The page number {request.PageNumber} does not exist for this order. Last page is {paginationMetadata.TotalPageCount}"}
            );
            response.ErrorType = Error.BadRequestProblem;
            return response;
        }

        if(itemsFromDatabase == null) 
        {
            response.Errors.Add(
                "Items",
                new string[] {$"No items in the order with id = {request.OrderId} were found in the database"}
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var itemsToReturn = _mapper.Map<IEnumerable<GetOrdersItemsWithFilterDto>>(itemsFromDatabase);

        response.Items = itemsToReturn;
        response.PaginationMetadata = paginationMetadata;

        return response;
    }
}