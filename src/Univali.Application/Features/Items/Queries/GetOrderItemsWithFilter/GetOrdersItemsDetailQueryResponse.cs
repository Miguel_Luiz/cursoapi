using Univali.Application.Features.Common;

namespace Univali.Application.Features.Items.Queries.GetOrdersItems;


public class GetOrdersItemsDetailQueryResponse : BaseResponse
{
    public IEnumerable<GetOrdersItemsDetailDto> Items { get; set; } = default!;
    public PaginationMetadata? PaginationMetadata {get; set;}
}