using MediatR;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Items.Queries.GetOrdersItemsWithFilter;

public class GetOrdersItemsWithFilterQuery : IRequest<GetOrdersItemsWithFilterQueryResponse>
{
    public int CustomerId { get; set; }
    public int OrderId { get; set; }
    public int PageNumber { get; set; }
    public int PageSize { get; set; }
}