using MediatR;

namespace Univali.Application.Features.Items.Queries.GetOrdersItems;

public class GetOrdersItemsDetailQuery : IRequest<GetOrdersItemsDetailResponse>
{
    public int CustomerId { get; set; }
    public int OrderId { get; set; }
}