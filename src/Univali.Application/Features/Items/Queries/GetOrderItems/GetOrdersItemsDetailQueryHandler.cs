using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Items.Queries.GetOrdersItems;

public class GetOrdersItemsDetailQueryHandler : IRequestHandler<GetOrdersItemsDetailQuery, GetOrdersItemsDetailResponse>
{
    private readonly IOrderRepository _orderRepository;
    private readonly IMapper _mapper;

    public GetOrdersItemsDetailQueryHandler(IOrderRepository orderRepository, IMapper mapper)
    {
        _orderRepository = orderRepository;
        _mapper = mapper;
    }

    public async Task<GetOrdersItemsDetailResponse> Handle(GetOrdersItemsDetailQuery request, CancellationToken cancellationToken)
    {
        var response = new GetOrdersItemsDetailResponse();

        var customerFromDatabase = await _orderRepository.GetCustomerByIdAsync(request.CustomerId);

        if(customerFromDatabase == null)
        {
            response.Errors.Add(
                "Customer",
                new string[] {$"Customer with id  = {request.CustomerId} was not found in the database"}
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var orderFromDatabase = await _orderRepository.GetOrderInFullByIdAsync(request.CustomerId, request.OrderId);

        var itemsFromDatabase = await _orderRepository.GetOrderItemsAsync(request.OrderId);

        if(itemsFromDatabase == null) 
        {
            response.Errors.Add(
                "Items",
                new string[] {$"No items in the order with id = {request.OrderId} were found in the database"}
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        if(itemsFromDatabase == null) return null!;

        var itemsToReturn = _mapper.Map<IEnumerable<GetOrdersItemsDetailDto>>(itemsFromDatabase);

        response.Items = itemsToReturn;

        return response;
    }
}