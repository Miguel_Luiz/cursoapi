using Univali.Application.Features.Common;

namespace Univali.Application.Features.Items.Queries.GetOrdersItems;

public class GetOrdersItemsDetailResponse : BaseResponse
{
    public IEnumerable<GetOrdersItemsDetailDto> Items { get; set; } = default!;
}