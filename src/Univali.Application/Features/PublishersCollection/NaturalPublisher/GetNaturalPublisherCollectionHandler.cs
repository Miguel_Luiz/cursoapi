using AutoMapper;
using MediatR;
using Univali.Domain.Entities;
using Univali.Application.Features.Publishers.Queries.GetPublisher;
using Univali.Application.Models;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.PublishersCollection.Queries;

public class GetNaturalPublisherCollectionHandler : 
    IRequestHandler<GetNaturalPublisherCollectionQuery, GetNaturalPublisherCollectionResponse>
{
    private readonly IPublisherRepository _publisherRepository;
    
    private readonly IMapper _mapper;

    public GetNaturalPublisherCollectionHandler(
        IPublisherRepository publisherRepository, 
        IMapper mapper
    ) {
        _publisherRepository = publisherRepository;

        _mapper = mapper;
    }

    public async Task<GetNaturalPublisherCollectionResponse> Handle(
        GetNaturalPublisherCollectionQuery getNaturalPublisherCollectionQuery, 
        CancellationToken cancellationToken
    ) {
       var (publishers, paginationMetadata) = await _publisherRepository
            .GetNaturalPublisherCollectionAsync(
                getNaturalPublisherCollectionQuery.Name,
                getNaturalPublisherCollectionQuery.CPF,
                getNaturalPublisherCollectionQuery.SearchQuery, 
                getNaturalPublisherCollectionQuery.PageNumber, 
                getNaturalPublisherCollectionQuery.PageSize
            );

       var getNaturalPublisherCollectionResponse = new GetNaturalPublisherCollectionResponse();

       getNaturalPublisherCollectionResponse.Publishers = _mapper.Map<List<NaturalPublisherDto>>(publishers);

       getNaturalPublisherCollectionResponse.Pagination = paginationMetadata;

       return getNaturalPublisherCollectionResponse;
    }
}