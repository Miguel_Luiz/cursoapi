using MediatR;

namespace Univali.Application.Features.PublishersCollection.Queries;

public class GetNaturalPublisherCollectionQuery : 
    IRequest<GetNaturalPublisherCollectionResponse>
{
    public GetNaturalPublisherCollectionQuery(
        string name,
        string cpf,
        string searchQuery, 
        int pageNumber, 
        int pageSize
    ) {
        Name = name; 
        CPF = cpf; 
        SearchQuery = searchQuery; 
        PageNumber = pageNumber;
        PageSize = pageSize;
    }

    public string Name { get; set; }

    public string CPF { get; set; }

    public string SearchQuery { get; set; }

    public int PageNumber { get; set; }

    public int PageSize { get; set; }
}