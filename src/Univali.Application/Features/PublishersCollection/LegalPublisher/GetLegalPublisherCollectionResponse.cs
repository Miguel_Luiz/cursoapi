using Univali.Application.Features.Common;
using Univali.Application.Features.Publishers.Queries.GetPublisher;
using Univali.Application.Models;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.PublishersCollection.Queries;

public class GetLegalPublisherCollectionResponse : BaseResponse
{
    public List<LegalPublisherDto> Publishers { get; set; } = new();

    public PaginationMetadata? Pagination {get; set;}
}