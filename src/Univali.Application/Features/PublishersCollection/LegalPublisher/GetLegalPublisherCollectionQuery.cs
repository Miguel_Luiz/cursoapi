using MediatR;

namespace Univali.Application.Features.PublishersCollection.Queries;

public class GetLegalPublisherCollectionQuery : 
    IRequest<GetLegalPublisherCollectionResponse>
{
    public GetLegalPublisherCollectionQuery(
        string name,
        string cnpj,
        string searchQuery, 
        int pageNumber, 
        int pageSize
    ) {
        Name = name; 
        CNPJ = cnpj; 
        SearchQuery = searchQuery; 
        PageNumber = pageNumber;
        PageSize = pageSize;
    }
    public string Name { get; set; }

    public string CNPJ { get; set; }

    public string SearchQuery { get; set; }

    public int PageNumber { get; set; }

    public int PageSize { get; set; }
}