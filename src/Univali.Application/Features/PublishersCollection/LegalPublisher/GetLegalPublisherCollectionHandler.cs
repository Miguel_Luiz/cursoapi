using AutoMapper;
using MediatR;
using Univali.Domain.Entities;
using Univali.Application.Features.Publishers.Queries.GetPublisher;
using Univali.Application.Models;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.PublishersCollection.Queries;

public class GetLegalPublisherCollectionHandler : 
    IRequestHandler<GetLegalPublisherCollectionQuery, GetLegalPublisherCollectionResponse>
{
    private readonly IPublisherRepository _publisherRepository;
    
    private readonly IMapper _mapper;

    public GetLegalPublisherCollectionHandler(
        IPublisherRepository publisherRepository, 
        IMapper mapper
    ) {
        _publisherRepository = publisherRepository;

        _mapper = mapper;
    }

    public async Task<GetLegalPublisherCollectionResponse> Handle(
        GetLegalPublisherCollectionQuery getLegalPublisherCollectionQuery, 
        CancellationToken cancellationToken
    ) {
       var (publishers, paginationMetadata) = await _publisherRepository
            .GetLegalPublisherCollectionAsync(
                getLegalPublisherCollectionQuery.Name,
                getLegalPublisherCollectionQuery.CNPJ,
                getLegalPublisherCollectionQuery.SearchQuery, 
                getLegalPublisherCollectionQuery.PageNumber, 
                getLegalPublisherCollectionQuery.PageSize
            );

       var getLegalPublisherCollectionResponse = new GetLegalPublisherCollectionResponse();

       getLegalPublisherCollectionResponse.Publishers = _mapper.Map<List<LegalPublisherDto>>(publishers);

       getLegalPublisherCollectionResponse.Pagination = paginationMetadata;

       return getLegalPublisherCollectionResponse;
    }
}