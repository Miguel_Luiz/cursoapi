using FluentValidation;
using Univali.Application.Features.Lessons.Commands.CreateLesson;

namespace Univali.Application.Features.Modules.Commands.DeleteModule;

public class DeleteModuleCommandValidator : AbstractValidator<DeleteModuleCommand>
{
    public DeleteModuleCommandValidator()
    {
        RuleFor(c => c.CourseId)
        .NotEmpty()
        .WithMessage("Fill a CourseId")      
        .GreaterThan(0)
        .WithMessage("The CourseId must be greater than zero.");
    
    
        RuleFor(c => c.ModuleId)
        .NotEmpty()
        .WithMessage("Fill a ModuleId")      
        .GreaterThan(0)
        .WithMessage("The ModuleId must be greater than zero.");
    }
        
}