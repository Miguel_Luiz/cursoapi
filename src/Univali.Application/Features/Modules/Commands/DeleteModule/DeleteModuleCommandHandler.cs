using FluentValidation;
using MediatR;
using Univali.Application.Features.Common;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.Modules.Commands.DeleteModule;

public class DeleteModuleCommandHandler : IRequestHandler<DeleteModuleCommand, DeleteModuleCommandResponse>
{
    private readonly IPublisherRepository _publisherRepository;
    private readonly IValidator <DeleteModuleCommand> _validator;

    public DeleteModuleCommandHandler(IPublisherRepository publisherRepository, IValidator <DeleteModuleCommand> validator)
    {
        _publisherRepository = publisherRepository ?? throw new ArgumentNullException(nameof(publisherRepository));
        _validator = validator ?? throw new ArgumentNullException(nameof(validator));
    }

    public async Task<DeleteModuleCommandResponse> Handle(DeleteModuleCommand request, CancellationToken cancellationToken)
    {//Miguel novo(deve ter uns bagulho que eu mudei e não coloquei o novo na frente, desculpa)
        DeleteModuleCommandResponse deleteModuleCommandResponse = new();

        var validationResult = _validator.Validate(request);

        if(!validationResult.IsValid)
        {
            foreach(var error in validationResult.ToDictionary())
            {
                deleteModuleCommandResponse.Errors.Add(error.Key, error.Value);
            }

            deleteModuleCommandResponse.ErrorType = Error.ValidationProblem;
            return deleteModuleCommandResponse;
        }

        var courseEntity = await _publisherRepository.GetCourseByIdAsync(request.CourseId);

        if (courseEntity == null)
        {
            deleteModuleCommandResponse.Errors.Add("Course", new string[]{$"The course with id = {request.CourseId} was not found in the database"});
            deleteModuleCommandResponse.ErrorType = Error.NotFoundProblem;
            return deleteModuleCommandResponse;
        }

        var moduleEntity = await _publisherRepository.GetModuleByIdAsync(request.ModuleId);

        if (moduleEntity == null)
        {
            deleteModuleCommandResponse.Errors.Add("Module", new string[]{$"The module with id = {request.ModuleId} was not found in the database"});
            deleteModuleCommandResponse.ErrorType = Error.NotFoundProblem;
            return deleteModuleCommandResponse;
        }

       
        courseEntity.TotalDuration = courseEntity.UpdateCourseOnModuleDeleting(moduleEntity.TotalDuration);
        
        courseEntity.ModuleAmount = courseEntity.RemoveModulefromCourse(); // courseEntity.ModuleAmount = courseEntity.ModuleAmount - 1;
        
        _publisherRepository.DeleteModule(moduleEntity);

        await _publisherRepository.SaveChangesAsync();
        
        return deleteModuleCommandResponse;
    }
}