using MediatR;

namespace Univali.Application.Features.Modules.Commands.DeleteModule;

public class DeleteModuleCommand : IRequest<DeleteModuleCommandResponse>
{
    public int ModuleId {get;set;}
    public int CourseId {get;set;}
}