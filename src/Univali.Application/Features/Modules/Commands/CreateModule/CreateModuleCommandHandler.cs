﻿using AutoMapper;
using MediatR;
using FluentValidation;
using Univali.Domain.Entities;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;


namespace Univali.Application.Features.Modules.Commands.CreateModule;

public class CreateModuleCommandHandler: IRequestHandler<CreateModuleCommand, CreateModuleCommandResponse>
{
    private readonly IPublisherRepository _publisherRepository;
    private readonly IMapper _mapper;
    private readonly IValidator <CreateModuleCommand> _validator;

    public CreateModuleCommandHandler(IPublisherRepository publisherRepository, IMapper mapper, IValidator<CreateModuleCommand> validator)
    {
        _publisherRepository = publisherRepository;
        _mapper = mapper;
        _validator = validator;
    }

       public async Task<CreateModuleCommandResponse> Handle(CreateModuleCommand request, CancellationToken cancellationToken)
    {
        CreateModuleCommandResponse createModuleCommandResponse =  new();

        var validationResult = _validator.Validate(request);

        if(!validationResult.IsValid)
        {
            foreach(var error in validationResult.ToDictionary())
            {
                createModuleCommandResponse.Errors.Add(error.Key, error.Value);
            }

            createModuleCommandResponse.ErrorType = Error.ValidationProblem;
            return createModuleCommandResponse;
        }

        var course = await _publisherRepository.GetCourseByIdAsync(request.CourseId);//Miguel
        if(course == null) //Miguel
        {
            createModuleCommandResponse.Errors.Add("Course", new string[]{$"The course with id = {request.CourseId} was not found in the database"});
            createModuleCommandResponse.ErrorType = Error.NotFoundProblem;
            return createModuleCommandResponse;
        }

        var moduleEntity = _mapper.Map<Module>(request);

        var courseEntity = await _publisherRepository.GetCourseByIdAsync(request.CourseId);
        courseEntity!.TotalDuration = courseEntity.UpdateCourseOnModuleCreating(moduleEntity.TotalDuration);
        
        courseEntity!.ModuleAmount = courseEntity.AddModuleOnCourse(); // courseEntity.ModuleAmount = courseEntity.ModuleAmount + 1;
        
        _publisherRepository.AddModule(moduleEntity);
        await _publisherRepository.SaveChangesAsync();

        createModuleCommandResponse.Module = _mapper.Map<CreateModuleCommandDto>(moduleEntity);
        return createModuleCommandResponse;
    }
}