﻿using FluentValidation;
using Univali.Application.Features.Modules.Commands.CreateModule;

namespace Univali.Application.Features.Modules.Commands.CreateModule;

public class CreateModuleCommandValidator : AbstractValidator<CreateModuleCommand>
{
    public CreateModuleCommandValidator()
    {
        RuleFor(m => m.Title)
            .NotEmpty()
            .WithMessage("You sould Fill Out a Title")
            .MaximumLength(100)
            .WithMessage("The Title shouldn't have more than 100 characters");

// pablo1207 - validação de number maior que zero
        RuleFor(m => m.Number)
            .NotNull()
            .WithMessage("You sould Fill Out a Module Number")
            .GreaterThan(0)
            .WithMessage("The number property must be greater than zero.");
        
        RuleFor(m => m.Description)
            .NotEmpty()
            .WithMessage("You sould Fill Out a Description");
    }
}
