﻿
namespace Univali.Application.Features.Modules.Commands.CreateModule;
public class CreateModuleCommandDto
{
    public int ModuleId { get; set; }
    public string Title { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public int Number { get; set; }
//pablo1107 - removi totalDuration
    public int CourseId { get; set; }
}