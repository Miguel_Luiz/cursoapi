﻿using MediatR;

namespace Univali.Application.Features.Modules.Commands.CreateModule;

public class CreateModuleCommand : IRequest<CreateModuleCommandResponse>
{
    public int ModuleId { get; set; }//Miguel
    public string Title {get; set;} = string.Empty;
    public string Description {get; set;} = string.Empty;
    public int Number { get; set; }
    public int CourseId { get; set; }
}