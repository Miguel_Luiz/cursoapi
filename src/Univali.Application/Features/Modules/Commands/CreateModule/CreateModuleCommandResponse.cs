﻿using Univali.Application.Features.Common;

namespace Univali.Application.Features.Modules.Commands.CreateModule;

public class CreateModuleCommandResponse : BaseResponse
{
    public CreateModuleCommandDto Module { get; set; } = default!;
}