﻿namespace Univali.Application.Features.Modules.Commands.CreateModule;

public class LessonForModuleCreationDto
{
    public int Id { get; set; }
    public string Title { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public int DurationMinutes { get; set; }
}