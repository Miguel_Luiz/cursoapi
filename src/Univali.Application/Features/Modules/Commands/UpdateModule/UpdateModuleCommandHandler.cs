using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Application.Features.Common;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.Modules.Commands.UpdateModule;

public class UpdateModuleCommandHandler : IRequestHandler<UpdateModuleCommand, UpdateModuleCommandResponse>
{
    private readonly IMapper _mapper; 
    private readonly IPublisherRepository _publisherRepository;
    private readonly IValidator<UpdateModuleCommand> _validator;

    public UpdateModuleCommandHandler(IMapper mapper, IPublisherRepository publisherRepository, IValidator<UpdateModuleCommand> validator)
    {
        _publisherRepository = publisherRepository ?? throw new ArgumentNullException(nameof(publisherRepository));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        _validator = validator ?? throw new ArgumentNullException(nameof(validator));
    }

    public async Task<UpdateModuleCommandResponse> Handle(UpdateModuleCommand request, CancellationToken cancellationToken)
    {
        UpdateModuleCommandResponse updateModuleCommandResponse = new();

        var validationResult = _validator.Validate(request);

        if(!validationResult.IsValid)
        {
            foreach(var error in validationResult.ToDictionary())
            {
                updateModuleCommandResponse.Errors.Add(error.Key, error.Value);
            }

            updateModuleCommandResponse.ErrorType = Error.ValidationProblem;
            return updateModuleCommandResponse;
        }
        
        var courseEntity = await _publisherRepository.GetCourseByIdAsync(request.CourseId);
        if(courseEntity == null) 
        {
            updateModuleCommandResponse.Errors.Add("Course", new string[]{$"The course with id = {request.CourseId} was not found in the database"});
            updateModuleCommandResponse.ErrorType = Error.NotFoundProblem;
            return updateModuleCommandResponse;
        }
        
        var moduleEntity = await _publisherRepository.GetModuleFromCourseByIdAsync(request.CourseId, request.ModuleId);
        
        if(moduleEntity == null)
        {
            updateModuleCommandResponse.Errors.Add("Module", new string[]{$"The module with id = {request.ModuleId} was not found in the database"});
            updateModuleCommandResponse.ErrorType = Error.NotFoundProblem;
            return updateModuleCommandResponse;
        }
        
        _mapper.Map(request, moduleEntity);

        await _publisherRepository.SaveChangesAsync();

        return updateModuleCommandResponse;
    }
}