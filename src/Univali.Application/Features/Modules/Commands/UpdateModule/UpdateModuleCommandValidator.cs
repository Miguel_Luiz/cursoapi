using FluentValidation;

namespace Univali.Application.Features.Modules.Commands.UpdateModule;

public class UpdateModuleCommandValidator : AbstractValidator<UpdateModuleCommand>
{
    public UpdateModuleCommandValidator()
    {
        RuleFor(m => m.Title)
            .NotEmpty()
            .WithMessage("You sould Fill Out a Title")
            .MaximumLength(100)
            .WithMessage("The Title shouldn't have more than 100 characters");

// pablo1207 - validação de number maior que zero
        RuleFor(m => m.Number)
            .NotNull()
            .WithMessage("You sould Fill Out a Module Number")
            .GreaterThan(0)
            .WithMessage("The number property must be greater than zero.");
        
        RuleFor(m => m.Description)
            .NotEmpty()
            .WithMessage("You sould Fill Out a Description");
    }
}
