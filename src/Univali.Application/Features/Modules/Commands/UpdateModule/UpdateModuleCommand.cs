using MediatR;

namespace Univali.Application.Features.Modules.Commands.UpdateModule;

public class UpdateModuleCommand : IRequest<UpdateModuleCommandResponse>
{
    public int ModuleId {get;set;}
    public string Title {get; set;} = string.Empty;
    public string Description {get; set;} = string.Empty;
    public int Number {get; set;}
    public int CourseId {get; set;}
}
