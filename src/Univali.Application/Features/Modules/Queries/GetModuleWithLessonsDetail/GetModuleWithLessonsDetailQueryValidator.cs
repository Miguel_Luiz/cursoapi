using FluentValidation;

namespace Univali.Application.Features.Modules.Queries.GetModuleWithLessonsDetail;


public class GetModuleWithLessonsDetailQueryValidator :
    AbstractValidator<GetModuleWithLessonsDetailQuery>
 {
    public GetModuleWithLessonsDetailQueryValidator() 
    {//Miguel
        RuleFor(c => c.ModuleId)
        .NotEmpty()
        .WithMessage("Fill a ModuleId")      
        .GreaterThan(0)
        .WithMessage("The ModuleId must be greater than zero.");

        RuleFor(c => c.CourseId)
        .NotEmpty()
        .WithMessage("Fill a CourseId")      
        .GreaterThan(0)
        .WithMessage("The CourseId must be greater than zero.");
    }
 }