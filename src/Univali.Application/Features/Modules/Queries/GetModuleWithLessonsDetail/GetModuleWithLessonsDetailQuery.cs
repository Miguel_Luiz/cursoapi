﻿using MediatR;

namespace Univali.Application.Features.Modules.Queries.GetModuleWithLessonsDetail;

public class GetModuleWithLessonsDetailQuery : IRequest<GetModuleWithLessonsDetailQueryResponse>
{
    public int CourseId { get; set; }
    public int ModuleId { get; set; }
}