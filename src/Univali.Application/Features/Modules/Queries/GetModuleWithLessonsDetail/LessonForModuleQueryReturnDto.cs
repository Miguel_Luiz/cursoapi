namespace Univali.Application.Features.Modules.Queries.GetModuleWithLessonsDetail;


public class LessonForModuleQueryReturnDto
{
    public int LessonId { get; set; }
    public string Title { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public TimeSpan Duration { get; set; }    
    
}