﻿using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Application.Features.Common;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.Modules.Queries.GetModuleWithLessonsDetail;

public class GetModuleWithLessonsDetailQueryHandler : IRequestHandler<GetModuleWithLessonsDetailQuery, GetModuleWithLessonsDetailQueryResponse>
{
    private readonly IPublisherRepository _publisherRepository;
    private readonly IMapper _mapper;
    private readonly IValidator <GetModuleWithLessonsDetailQuery> _validator;


    public GetModuleWithLessonsDetailQueryHandler(IPublisherRepository publisherRepository, IMapper mapper, IValidator <GetModuleWithLessonsDetailQuery> validator)
    {
        _publisherRepository = publisherRepository;
        _mapper = mapper;
        _validator = validator;
    }

    public async Task<GetModuleWithLessonsDetailQueryResponse> Handle(GetModuleWithLessonsDetailQuery request, CancellationToken cancellationToken)
    {//Miguel novo
        GetModuleWithLessonsDetailQueryResponse getModuleWithLessonsDetailQueryResponse = new();

        var validationResult = _validator.Validate(request);

        if(!validationResult.IsValid)
        {
            foreach(var error in validationResult.ToDictionary())
            {
                getModuleWithLessonsDetailQueryResponse.Errors.Add(error.Key, error.Value);
            }

            getModuleWithLessonsDetailQueryResponse.ErrorType = Error.ValidationProblem;
            return getModuleWithLessonsDetailQueryResponse;
        }

        if(await _publisherRepository.GetCourseByIdAsync(request.CourseId) == null) 
        {
            getModuleWithLessonsDetailQueryResponse.Errors.Add("Course", new string[]{$"The course with id = {request.CourseId} was not found in the database"});
            getModuleWithLessonsDetailQueryResponse.ErrorType = Error.NotFoundProblem;
            return getModuleWithLessonsDetailQueryResponse;
        }
        
        var moduleFromDatabase = await _publisherRepository.GetModuleFromCourseWithLessonsByIdAsync(request.CourseId, request.ModuleId);

        if (moduleFromDatabase == null)
        {
            // pablo1207 - na mensagem abaixo exibia o courseId ao invés do moduleId
            getModuleWithLessonsDetailQueryResponse.Errors.Add("Module", new string[]{$"The module with id = {request.ModuleId} was not found in the database"});
            getModuleWithLessonsDetailQueryResponse.ErrorType = Error.NotFoundProblem;
            return getModuleWithLessonsDetailQueryResponse;
        }

        getModuleWithLessonsDetailQueryResponse.Module = _mapper.Map<GetModuleWithLessonsDetailDto>(moduleFromDatabase);
        return getModuleWithLessonsDetailQueryResponse;
    }
}