using Univali.Application.Features.Common;

namespace Univali.Application.Features.Modules.Queries.GetModuleWithLessonsDetail;

public class GetModuleWithLessonsDetailQueryResponse : BaseResponse
{
    public GetModuleWithLessonsDetailDto Module {get; set;} = default!;
}