﻿using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Application.Features.Common;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.Modules.Queries.GetModuleDetail;

public class GetModuleDetailQueryHandler : IRequestHandler<GetModuleDetailQuery, GetModuleDetailQueryResponse>
{
    private readonly IPublisherRepository _publisherRepository;
    private readonly IMapper _mapper;
    private readonly IValidator <GetModuleDetailQuery> _validator;


    public GetModuleDetailQueryHandler(IPublisherRepository publisherRepository, IMapper mapper, IValidator <GetModuleDetailQuery> validator)
    {
        _publisherRepository = publisherRepository;
        _mapper = mapper;
        _validator = validator;
    }

    public async Task<GetModuleDetailQueryResponse> Handle(GetModuleDetailQuery request, CancellationToken cancellationToken)
    {//Miguel novo
        GetModuleDetailQueryResponse getModuleDetailQueryResponse = new();

        var validationResult = _validator.Validate(request);

        if(!validationResult.IsValid)
        {
            foreach(var error in validationResult.ToDictionary())
            {
                getModuleDetailQueryResponse.Errors.Add(error.Key, error.Value);
            }

            getModuleDetailQueryResponse.ErrorType = Error.ValidationProblem;
            return getModuleDetailQueryResponse;
        }

        if(await _publisherRepository.GetCourseByIdAsync(request.CourseId) == null) 
        {
            getModuleDetailQueryResponse.Errors.Add("Course", new string[]{$"The course with id = {request.CourseId} was not found in the database"});
            getModuleDetailQueryResponse.ErrorType = Error.NotFoundProblem;
            return getModuleDetailQueryResponse;
        }

        var moduleFromDatabase = await _publisherRepository.GetModuleFromCourseByIdAsync(request.CourseId, request.ModuleId);

        if(moduleFromDatabase == null) 
        {
            getModuleDetailQueryResponse.Errors.Add("Module", new string[]{$"The module with id = {request.ModuleId} was not found in the database"});
            getModuleDetailQueryResponse.ErrorType = Error.NotFoundProblem;
            return getModuleDetailQueryResponse;
        }

        
        getModuleDetailQueryResponse.Module = _mapper.Map<GetModuleDetailDto>(moduleFromDatabase);
       
        return getModuleDetailQueryResponse;
    }
}