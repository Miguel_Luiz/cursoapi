using Univali.Application.Features.Common;

namespace Univali.Application.Features.Modules.Queries.GetModuleDetail;


public class GetModuleDetailQueryResponse : BaseResponse
{
    public GetModuleDetailDto Module {get; set;} = default!;
}