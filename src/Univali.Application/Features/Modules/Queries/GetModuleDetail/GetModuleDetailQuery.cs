﻿using MediatR;

namespace Univali.Application.Features.Modules.Queries.GetModuleDetail;

public class GetModuleDetailQuery : IRequest<GetModuleDetailQueryResponse>
{
    public int CourseId { get; set; }
    public int ModuleId { get; set; }
}