﻿namespace Univali.Application.Features.Modules.Queries.GetModuleDetail;

public class GetModuleDetailDto
{
    public int ModuleId {get; set;}
    public string Title { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public int Number { get; set; }
    public TimeSpan TotalDuration {get;set;}
}