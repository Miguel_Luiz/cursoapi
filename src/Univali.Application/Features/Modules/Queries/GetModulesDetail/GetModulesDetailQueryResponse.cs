using Univali.Application.Features.Common;

namespace Univali.Application.Features.Modules.Queries.GetModulesDetail;

public class GetModulesDetailQueryResponse : BaseResponse
{
    public List<GetOneModuleDetailDto> Modules {get; set;} = new();
}