using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Application.Features.Common;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.Modules.Queries.GetModulesDetail;

public class GetModulesDetailQueryHandler : IRequestHandler<GetModulesDetailQuery, GetModulesDetailQueryResponse>
{
    private readonly IPublisherRepository _publisherRepository;
    private readonly IMapper _mapper;
    private readonly IValidator <GetModulesDetailQuery> _validator;

   public GetModulesDetailQueryHandler(
    IPublisherRepository publisherRepository, 
    IMapper mapper, 
    IValidator <GetModulesDetailQuery> validator)
    {
        _publisherRepository = publisherRepository;
        _mapper = mapper;
        _validator = validator;
    }

public async Task<GetModulesDetailQueryResponse> Handle(GetModulesDetailQuery request, CancellationToken cancellationToken)
    {//Miguel
        GetModulesDetailQueryResponse getModulesDetailQueryResponse = new();

        var validationResult = _validator.Validate(request);

        if(!validationResult.IsValid)
        {
            foreach(var error in validationResult.ToDictionary())
            {
                getModulesDetailQueryResponse.Errors.Add(error.Key, error.Value);
            }

            getModulesDetailQueryResponse.ErrorType = Error.ValidationProblem;
            return getModulesDetailQueryResponse;
        }

        var course = await _publisherRepository.GetCourseByIdAsync(request.CourseId);
        if(course == null) 
        {
            getModulesDetailQueryResponse.Errors.Add("Course", new string[]{$"The course with id = {request.CourseId} was not found in the database"});
            getModulesDetailQueryResponse.ErrorType = Error.NotFoundProblem;
            return getModulesDetailQueryResponse;
        }

        var modulesFromDatabase = await _publisherRepository.GetModulesAsync(request.CourseId);

        if (modulesFromDatabase!.Count() == 0)
        {
            getModulesDetailQueryResponse.Errors.Add("Module", new string[]{"No modules were found in the requested course"});
            getModulesDetailQueryResponse.ErrorType = Error.NotFoundProblem;
            return getModulesDetailQueryResponse;
        }
        
        getModulesDetailQueryResponse.Modules = _mapper.Map<List<GetOneModuleDetailDto>>(modulesFromDatabase);
        
        return getModulesDetailQueryResponse;
    }
}