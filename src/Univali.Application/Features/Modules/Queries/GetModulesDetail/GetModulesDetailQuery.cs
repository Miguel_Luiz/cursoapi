using MediatR;

namespace Univali.Application.Features.Modules.Queries.GetModulesDetail;

public class GetModulesDetailQuery : IRequest<GetModulesDetailQueryResponse>
{
    public int CourseId { get; set; }
}