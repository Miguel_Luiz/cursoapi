using FluentValidation;

namespace Univali.Application.Features.Modules.Queries.GetModulesDetail;

public class GetModulesDetailQueryValidator :
    AbstractValidator<GetModulesDetailQuery>
 {
    public GetModulesDetailQueryValidator() 
    {//Miguel
        RuleFor(c => c.CourseId)
        .NotEmpty()
        .WithMessage("Fill a CourseId")      
        .GreaterThan(0)
        .WithMessage("The CourseId must be greater than zero.");
    }
}