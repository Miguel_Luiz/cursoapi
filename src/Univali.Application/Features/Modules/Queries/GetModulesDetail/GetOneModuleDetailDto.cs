namespace Univali.Application.Features.Modules.Queries.GetModulesDetail;

public class GetOneModuleDetailDto
{
    public int ModuleId {get; set;}
    public string Title { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public int Number { get; set; }
    public TimeSpan TotalDuration {get;set;}
}
