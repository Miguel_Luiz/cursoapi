using Univali.Application.Features.Common;
using Univali.Application.Contracts.Repositories;


namespace Univali.Application.Features.Students.Queries.GetStudentsDetails;

public class GetStudentsDetailsResponse : BaseResponse
{
    public IEnumerable<GetStudentsDetailsDto> StudentsDetailsDtos { get; set; } = default!;
    
    public PaginationMetadata paginationMetadata = null!;
}