using MediatR;

namespace Univali.Application.Features.Students.Queries.GetStudentsDetails;

public class GetStudentsDetailsQuery : IRequest<GetStudentsDetailsResponse>
{
    public int StudentId { get; set; }
    public int PageNumber { get; set; }
    public int PageSize { get; set; }
}