using AutoMapper;
using MediatR;
using Univali.Domain.Entities;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;


namespace Univali.Application.Features.Students.Queries.GetStudentsDetails;

public class GetStudentsDetailQueryHandler : IRequestHandler<GetStudentsDetailsQuery, GetStudentsDetailsResponse>
{
    private readonly IPublisherRepository _publisherRepository;
    private readonly IMapper _mapper;

    public GetStudentsDetailQueryHandler(IPublisherRepository publisherRepository, IMapper mapper)
    {
        _publisherRepository = publisherRepository;
        _mapper = mapper;
    }

    public async Task<GetStudentsDetailsResponse> Handle(GetStudentsDetailsQuery request, CancellationToken cancellationToken)
    {
        GetStudentsDetailsResponse getStudentsDetailsResponse = new();

         var (studentsFromDatabase, paginationMetadata) = await _publisherRepository.GetStudentsAsync(request.PageNumber, request.PageSize);

        getStudentsDetailsResponse.StudentsDetailsDtos = _mapper.Map<IEnumerable<GetStudentsDetailsDto>>(studentsFromDatabase);

        getStudentsDetailsResponse.paginationMetadata = paginationMetadata;

        return getStudentsDetailsResponse;
    }
}