using Univali.Application.Features.Common;

namespace Univali.Application.Features.Students.Queries.GetStudentDetails;

public class GetStudentDetailsResponse : BaseResponse
{
    public GetStudentDetailsDto Student {get; set;} = default!;
}