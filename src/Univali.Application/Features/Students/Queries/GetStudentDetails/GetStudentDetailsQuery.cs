using MediatR;

namespace Univali.Application.Features.Students.Queries.GetStudentDetails;

public class GetStudentDetailsQuery : IRequest<GetStudentDetailsResponse> 
{
    public int Id {get; set;}
}