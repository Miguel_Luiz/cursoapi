using AutoMapper;
using MediatR;
using Univali.Domain.Entities;
using Univali.Application.Features.Common;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.Students.Queries.GetStudentDetails;

public class GetStudentDetailQueryHandler : IRequestHandler<GetStudentDetailsQuery, GetStudentDetailsResponse>
{
    private readonly IPublisherRepository _publisherRepository;
    private readonly IMapper _mapper;

    public GetStudentDetailQueryHandler(IPublisherRepository publisherRepository, IMapper mapper)
    {
        _publisherRepository = publisherRepository;
        _mapper = mapper;
    }

    public async Task<GetStudentDetailsResponse> Handle(GetStudentDetailsQuery request, CancellationToken cancellationToken)
    {
        GetStudentDetailsResponse getStudentsDetailsResponse = new();

        var studentFromDatabase = await _publisherRepository.GetStudentByIdAsync(request.Id);

        if(studentFromDatabase == null)
        {
            getStudentsDetailsResponse.ErrorType = Error.NotFoundProblem;
            getStudentsDetailsResponse.Errors.Add("Student", new string[] { "Student Not Found" });
            return getStudentsDetailsResponse;
        }

        getStudentsDetailsResponse.Student = _mapper.Map<GetStudentDetailsDto>(studentFromDatabase);
        
        return getStudentsDetailsResponse;
    }
}