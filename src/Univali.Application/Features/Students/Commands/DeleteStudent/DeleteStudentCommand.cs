using System.ComponentModel.DataAnnotations;
using MediatR;

namespace Univali.Application.Features.Students.Commands.DeleteStudent;

// IRequest<> transforma a classe em uma Mensagem
// DeleteCustomerDto é o tipo que este comando espera receber de volta
public class DeleteStudentCommand : IRequest<DeleteStudentCommandResponse>
{
    public int Id {get; set;}
}