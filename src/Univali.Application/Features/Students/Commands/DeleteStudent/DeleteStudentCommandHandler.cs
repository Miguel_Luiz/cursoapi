using AutoMapper;
using MediatR;
using Univali.Application.Features.Common;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.Students.Commands.DeleteStudent;

// O primeiro parâmetro é o tipo da mensagem
// O segundo parâmetro é o tipo que se espera receber.
public class DeleteStudentCommandHandler : IRequestHandler<DeleteStudentCommand, DeleteStudentCommandResponse>
{
    private readonly IPublisherRepository _studentRepository;
    private readonly IMapper _mapper;

    public DeleteStudentCommandHandler(IPublisherRepository studentRepository, IMapper mapper)
    {
        _studentRepository = studentRepository;
        _mapper = mapper;
    }

    public async Task<DeleteStudentCommandResponse> Handle(DeleteStudentCommand request, CancellationToken cancellationToken)
    {
        DeleteStudentCommandResponse deleteStudentCommandResponse = new();

        var studentFromDatabase = await _studentRepository.GetStudentByIdAsync(request.Id);

        if (studentFromDatabase == null)
        {
            deleteStudentCommandResponse.Errors.Add("Student", new string[] { "Student Not Found" });
            deleteStudentCommandResponse.ErrorType = Error.NotFoundProblem;
            return deleteStudentCommandResponse;
        }

        _studentRepository.DeleteStudent(studentFromDatabase);
        await _studentRepository.SaveChangesAsync();
        return deleteStudentCommandResponse;
    }
}