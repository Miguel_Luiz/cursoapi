using Univali.Application.Features.Common;

namespace Univali.Application.Features.Students.Commands.CreateStudent;

public class CreateStudentCommandResponse : BaseResponse
{
    public CreateStudentDto Student {get; set;} = default!;
}