using Univali.Application.Features.Common;
using Univali.Application.Features.Students.Commands.CreateStudent;

namespace Univali.Application.Features.Students.Commands.UpdateStudent;

public class UpdateStudentCommandResponse : BaseResponse
{
}