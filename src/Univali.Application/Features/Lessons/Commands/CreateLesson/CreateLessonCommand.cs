﻿using MediatR;

namespace Univali.Application.Features.Lessons.Commands.CreateLesson;

public class CreateLessonCommand : IRequest<CreateLessonCommandResponse>
{
    public int LessonId { get; set; }//Miguel
    public string Title {get; set;} = string.Empty;
    public string Description {get; set;} = string.Empty;
    public int DurationMinutes { get; set; }
    public int ModuleId { get; set; }
}