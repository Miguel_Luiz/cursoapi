﻿using AutoMapper;
using MediatR;
using FluentValidation;
using Univali.Domain.Entities;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Lessons.Commands.CreateLesson;

public class CreateLessonCommandHandler: IRequestHandler<CreateLessonCommand, CreateLessonCommandResponse>
{
    private readonly IPublisherRepository _publisherRepository;
    private readonly IMapper _mapper;
    private readonly IValidator <CreateLessonCommand> _validator;

    public CreateLessonCommandHandler(IPublisherRepository publisherRepository, IMapper mapper, IValidator<CreateLessonCommand> validator)
    {
        _publisherRepository = publisherRepository;
        _mapper = mapper;
        _validator = validator;
    }

    public async Task<CreateLessonCommandResponse> Handle(CreateLessonCommand request, CancellationToken cancellationToken)
    {
        CreateLessonCommandResponse createLessonCommandResponse =  new();

        var validationResult = _validator.Validate(request);

        if(!validationResult.IsValid)
        {
            foreach(var error in validationResult.ToDictionary())
            {
                createLessonCommandResponse.Errors.Add(error.Key, error.Value);
            }

            createLessonCommandResponse.ErrorType = Error.ValidationProblem;
            return createLessonCommandResponse;
        }

        var lessonEntity = _mapper.Map<Lesson>(request);

        var moduleEntity = await _publisherRepository.GetModuleByIdAsync(request.ModuleId);
        
        if (moduleEntity == null)
        {
                
            createLessonCommandResponse.Errors.Add("Module", new string[] { $"Module with id = {request.ModuleId} was not found in the database." });
            createLessonCommandResponse.ErrorType = Error.NotFoundProblem;
            return createLessonCommandResponse;
        }

        moduleEntity!.TotalDuration = moduleEntity.UpdateModuleOnLessonCreating(lessonEntity.Duration);
        moduleEntity.LessonAmount = moduleEntity.AddLessonOnModule(); // moduleEntity.LessonAmount = moduleEntity.LessonAmount + 1;

        var courseEntity = await _publisherRepository.GetCourseByIdAsync(moduleEntity.CourseId);
        
        if (courseEntity == null)
        {
            createLessonCommandResponse.Errors.Add("Course", new string[] { $"Course with id = {moduleEntity.CourseId} was not found in the database." });
            createLessonCommandResponse.ErrorType = Error.NotFoundProblem;
            return createLessonCommandResponse;
        }
        
        courseEntity!.TotalDuration = courseEntity.UpdateCourseOnLessonCreating(lessonEntity.Duration);
        
        _publisherRepository.AddLesson(lessonEntity);
        
        await _publisherRepository.SaveChangesAsync();

        createLessonCommandResponse.Lesson = _mapper.Map<CreateLessonCommandDto>(lessonEntity);

        return createLessonCommandResponse;
    }
}