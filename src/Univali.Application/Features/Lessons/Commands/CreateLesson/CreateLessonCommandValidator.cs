﻿using FluentValidation;
using Univali.Application.Features.Lessons.Commands.CreateLesson;

namespace Univali.Application.Features.Lessons.Commands.CreateLesson;

public class CreateLessonCommandValidator : AbstractValidator<CreateLessonCommand>
{
    public CreateLessonCommandValidator()
    {
        RuleFor(l => l.Title)
            .NotEmpty()
            .WithMessage("You sould Fill Out a Title")
            .MaximumLength(100)
            .WithMessage("The {PropertyTitle} shouldn't have more than 100 characters");

        RuleFor(l => l.Description)
            .NotEmpty()
            .WithMessage("You sould Fill Out a Description");
//pablo1107 - validação greater than
        RuleFor(l => l.DurationMinutes)
            .NotNull()
            .WithMessage("You sould Fill Out a Minutes Duration")
            .GreaterThan(0)
            .WithMessage("The DurationMinutes must be greater than zero.");
    }
}
