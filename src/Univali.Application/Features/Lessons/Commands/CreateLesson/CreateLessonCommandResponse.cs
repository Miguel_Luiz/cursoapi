﻿using Univali.Application.Features.Common;

namespace Univali.Application.Features.Lessons.Commands.CreateLesson;

public class CreateLessonCommandResponse : BaseResponse
{
    public CreateLessonCommandDto Lesson { get; set; } = default!;
}