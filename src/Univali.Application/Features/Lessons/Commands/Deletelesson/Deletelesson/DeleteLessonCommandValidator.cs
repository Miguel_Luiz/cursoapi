using FluentValidation;
using Univali.Application.Features.Lessons.Commands.CreateLesson;

namespace Univali.Application.Features.Lessons.Commands.DeleteLesson;

public class DeleteLessonCommandValidator : AbstractValidator<DeleteLessonCommand>
{
    public DeleteLessonCommandValidator()
    {
        RuleFor(c => c.LessonId)
        .NotEmpty()
        .WithMessage("Fill a LessonId")      
        .GreaterThan(0)
        .WithMessage("The LessonId must be greater than zero.");
    
    
        RuleFor(c => c.ModuleId)
        .NotEmpty()
        .WithMessage("Fill a ModuleId")      
        .GreaterThan(0)
        .WithMessage("The ModuleId must be greater than zero.");
    }
        
}