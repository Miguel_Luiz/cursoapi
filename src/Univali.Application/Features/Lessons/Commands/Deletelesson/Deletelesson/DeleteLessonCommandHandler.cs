﻿using FluentValidation;
using MediatR;
using Univali.Application.Features.Common;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.Lessons.Commands.DeleteLesson;

public class DeleteLessonCommandHandler : IRequestHandler<DeleteLessonCommand, DeleteLessonCommandResponse>
{
    private readonly IPublisherRepository _publisherRepository;
    private readonly IValidator <DeleteLessonCommand> _validator;

    public DeleteLessonCommandHandler(IPublisherRepository publisherRepository, IValidator <DeleteLessonCommand> validator)
    {
        _publisherRepository = publisherRepository ?? throw new ArgumentNullException(nameof(publisherRepository));
        _validator = validator ?? throw new ArgumentNullException(nameof(validator));
    }

    public async Task<DeleteLessonCommandResponse> Handle(DeleteLessonCommand request, CancellationToken cancellationToken)
    {//Miguel
        DeleteLessonCommandResponse deleteLessonCommandResponse = new();

        var validationResult = _validator.Validate(request);

        if(!validationResult.IsValid)
        {
            foreach(var error in validationResult.ToDictionary())
            {
                deleteLessonCommandResponse.Errors.Add(error.Key, error.Value);
            }

            deleteLessonCommandResponse.ErrorType = Error.ValidationProblem;
            return deleteLessonCommandResponse;
        }

        var moduleEntity = await _publisherRepository.GetModuleByIdAsync(request.ModuleId);

        if (moduleEntity == null)
        {
            deleteLessonCommandResponse.Errors.Add("Module", new string[] { $"Module with id = {request.ModuleId} was not found in the database." });
            deleteLessonCommandResponse.ErrorType = Error.NotFoundProblem;
            return deleteLessonCommandResponse;
        }

        var lessonEntity = await _publisherRepository.GetLessonByIdAsync(request.ModuleId, request.LessonId);

        if (lessonEntity == null)
        {
            deleteLessonCommandResponse.Errors.Add("Lesson", new string[] { $"Lesson with id = {request.LessonId} was not found in the database." });
            deleteLessonCommandResponse.ErrorType = Error.NotFoundProblem;
            return deleteLessonCommandResponse;
        }

        moduleEntity!.TotalDuration = moduleEntity.UpdateModuleOnLessonDeleting(lessonEntity!.Duration);
        moduleEntity.LessonAmount = moduleEntity.RemoveLessonFromModule(); // moduleEntity.LessonAmount = moduleEntity.LessonAmount - 1;

        var courseEntity = await _publisherRepository.GetCourseByIdAsync(moduleEntity.CourseId);

        if (courseEntity == null)
        {
            deleteLessonCommandResponse.Errors.Add("Course", new string[] { $"Course with id = {moduleEntity.CourseId} was not found in the database." });
            deleteLessonCommandResponse.ErrorType = Error.NotFoundProblem;
            return deleteLessonCommandResponse;
        }

        courseEntity!.TotalDuration = courseEntity.UpdateCourseOnLessonDeleting(lessonEntity.Duration);
        
        _publisherRepository.DeleteLesson(lessonEntity);

        await _publisherRepository.SaveChangesAsync();

        return deleteLessonCommandResponse;
    }
}