﻿using MediatR;

namespace Univali.Application.Features.Lessons.Commands.DeleteLesson;

public class DeleteLessonCommand : IRequest<DeleteLessonCommandResponse>
{
    public int LessonId { get; set; } 
    public int ModuleId { get; set; }
}