﻿using System.Data;
using FluentValidation;

namespace Univali.Application.Features.Lessons.Commands.UpdateLesson;

public class UpdateLessonCommandValidator : AbstractValidator<UpdateLessonCommand>
{
    public UpdateLessonCommandValidator()
    {
        RuleFor(l => l.Title)
            .NotEmpty()
            .WithMessage("You should fill out a Title")
            .MaximumLength(100)
            .WithMessage("The Title shouldn't have more than 100 characteres");

        RuleFor(l => l.Description)
            .NotEmpty()
            .WithMessage("You should fill out a Description");

//pablo1107 - validação greater than
        RuleFor((l => l.DurationMinutes))
            .NotNull()
            .WithMessage("You should fill out a duration in minutes")
            .GreaterThan(0)
            .WithMessage("The DurationMinutes must be greater than zero.");
    }
}