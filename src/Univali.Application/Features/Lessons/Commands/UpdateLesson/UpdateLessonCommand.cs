﻿using MediatR;

namespace Univali.Application.Features.Lessons.Commands.UpdateLesson;

public class UpdateLessonCommand : IRequest<UpdateLessonCommandResponse>
{
    public int LessonId { get; set; }
//pablo1107 - removi id
    public string Title { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public int DurationMinutes { get; set; }
    public int ModuleId { get; set; }
}