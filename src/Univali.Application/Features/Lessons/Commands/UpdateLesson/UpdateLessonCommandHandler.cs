﻿using AutoMapper;
using MediatR;
using FluentValidation;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Lessons.Commands.UpdateLesson;

public class UpdateLessonCommandHandler : IRequestHandler<UpdateLessonCommand, UpdateLessonCommandResponse>
{
    private readonly IMapper _mapper; 
    private readonly IPublisherRepository _publisherRepository;
    private readonly IValidator<UpdateLessonCommand> _validator;

    public UpdateLessonCommandHandler(IMapper mapper, IPublisherRepository publisherRepository, IValidator<UpdateLessonCommand> validator)
    {
        _publisherRepository = publisherRepository ?? throw new ArgumentNullException(nameof(publisherRepository));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        _validator = validator ?? throw new ArgumentNullException(nameof(validator));
    }

    public async Task<UpdateLessonCommandResponse> Handle(UpdateLessonCommand request, CancellationToken cancellationToken)
    {
        UpdateLessonCommandResponse updateLessonCommandResponse = new();

        var validationResult = _validator.Validate(request);

        if(!validationResult.IsValid)
        {
            foreach(var error in validationResult.ToDictionary())
            {
                updateLessonCommandResponse.Errors.Add(error.Key, error.Value);
            }

            updateLessonCommandResponse.ErrorType = Error.ValidationProblem;
            return updateLessonCommandResponse;
        }
        
        var moduleEntity = await _publisherRepository.GetModuleByIdAsync(request.ModuleId);
        
        if (moduleEntity == null) {
            updateLessonCommandResponse.Errors.Add("Module", new string[] { $"Module with id = {request.ModuleId} was not found in the database." });
            updateLessonCommandResponse.ErrorType = Error.NotFoundProblem;
            return updateLessonCommandResponse;
        }
        
        var lessonEntity = await _publisherRepository.GetLessonByIdAsync(request.ModuleId, request.LessonId);
        
        if(lessonEntity == null)
        {
            updateLessonCommandResponse.Errors.Add("Lesson", new string[] { $"Lesson with id = {request.LessonId} was not found in the database." });
            updateLessonCommandResponse.ErrorType = Error.NotFoundProblem;
            return updateLessonCommandResponse;
        }

        // var duration = new TimeOnly(request.DurationHours, request.DurationMinutes);
        // _publisherRepository.UpdateLessonParents(lessonEntity, moduleEntity!, duration);

        moduleEntity!.TotalDuration = moduleEntity.UpdateModelOnLessonUpdating(lessonEntity.Duration, new TimeSpan(0,request.DurationMinutes,0));

        var courseEntity = await _publisherRepository.GetCourseByIdAsync(moduleEntity.CourseId);
        
        if(courseEntity == null)
        {
            updateLessonCommandResponse.Errors.Add("Course", new string[] { $"Course with id = {moduleEntity.CourseId} was not found in the database." });
            updateLessonCommandResponse.ErrorType = Error.NotFoundProblem;
            return updateLessonCommandResponse;
        }

        courseEntity!.TotalDuration = courseEntity.UpdateCourseOnLessonUpdating(lessonEntity.Duration, new TimeSpan(0,request.DurationMinutes,0));

        _mapper.Map(request, lessonEntity);
        
        await _publisherRepository.SaveChangesAsync();

        return updateLessonCommandResponse;
    }
}