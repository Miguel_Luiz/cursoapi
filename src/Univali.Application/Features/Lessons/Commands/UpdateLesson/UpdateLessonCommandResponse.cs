﻿using Univali.Application.Features.Common;

namespace Univali.Application.Features.Lessons.Commands.UpdateLesson;

public class UpdateLessonCommandResponse : BaseResponse
{
}