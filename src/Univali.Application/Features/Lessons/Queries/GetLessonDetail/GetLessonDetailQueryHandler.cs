﻿using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Application.Features.Common;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.Lessons.Queries.GetLessonDetail;

public class GetLessonDetailQueryHandler : IRequestHandler<GetLessonDetailQuery, GetLessonDetailQueryResponse>
{
    private readonly IPublisherRepository _publisherRepository;
    private readonly IMapper _mapper;
    private readonly IValidator <GetLessonDetailQuery> _validator;//coloquei o validator Miguel


    public GetLessonDetailQueryHandler(
        IPublisherRepository publisherRepository, 
        IMapper mapper,
        IValidator <GetLessonDetailQuery> validator
    ) {
        _publisherRepository = publisherRepository;
        _mapper = mapper;
        _validator = validator;
    }

    public async Task<GetLessonDetailQueryResponse> Handle(GetLessonDetailQuery request, CancellationToken cancellationToken)
    {//mudei coisas aqui Miguel
        GetLessonDetailQueryResponse getLessonDetailQueryResponse = new();

        var validationResult = _validator.Validate(request);

        if(!validationResult.IsValid)
        {
            foreach(var error in validationResult.ToDictionary())
            {
                getLessonDetailQueryResponse.Errors.Add(error.Key, error.Value);
            }

            getLessonDetailQueryResponse.ErrorType = Error.ValidationProblem;
            return getLessonDetailQueryResponse;
        }

        var module = await _publisherRepository.GetModuleByIdAsync(request.ModuleId);

        if (module == null)
        {
            getLessonDetailQueryResponse.Errors.Add("Module", new string[] { $"Module with id = {request.ModuleId} was not found in the database." });
            getLessonDetailQueryResponse.ErrorType = Error.NotFoundProblem;
            return getLessonDetailQueryResponse;
        }

        var lessonFromDatabase = await _publisherRepository.GetLessonByIdAsync(request.ModuleId, request.LessonId);
        
        if (lessonFromDatabase == null)
        {
            getLessonDetailQueryResponse.Errors.Add("Lesson", new string[] { $"Lesson with id = {request.LessonId} was not found in the database." });
            getLessonDetailQueryResponse.ErrorType = Error.NotFoundProblem;
            return getLessonDetailQueryResponse;
        }

        getLessonDetailQueryResponse.Lesson = _mapper.Map<GetLessonDetailDto>(lessonFromDatabase);

        return getLessonDetailQueryResponse;
    }
}