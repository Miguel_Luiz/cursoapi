using FluentValidation;

namespace Univali.Application.Features.Lessons.Queries.GetLessonDetail;

public class GetLessonDetailQueryValidator :
    AbstractValidator<GetLessonDetailQuery>
 {
    public GetLessonDetailQueryValidator() 
    {//Miguel
        RuleFor(c => c.LessonId)
        .NotEmpty()
        .WithMessage("Fill a LessonId")      
        .GreaterThan(0)
        .WithMessage("The LessonId must be greater than zero.");
    
    
        RuleFor(c => c.ModuleId)
        .NotEmpty()
        .WithMessage("Fill a ModuleId")      
        .GreaterThan(0)
        .WithMessage("The ModuleId must be greater than zero.");
    }
 }