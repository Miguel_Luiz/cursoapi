﻿using MediatR;

namespace Univali.Application.Features.Lessons.Queries.GetLessonDetail;

public class GetLessonDetailQuery : IRequest<GetLessonDetailQueryResponse>
{
    public int LessonId { get; set; }
    public int ModuleId { get; set; }
}