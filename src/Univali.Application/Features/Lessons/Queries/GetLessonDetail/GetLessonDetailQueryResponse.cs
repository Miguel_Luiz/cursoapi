using Univali.Application.Features.Common;

namespace Univali.Application.Features.Lessons.Queries.GetLessonDetail;

public class GetLessonDetailQueryResponse : BaseResponse
{
    public GetLessonDetailDto Lesson {get; set;} = default!;//modificado Miguel
}