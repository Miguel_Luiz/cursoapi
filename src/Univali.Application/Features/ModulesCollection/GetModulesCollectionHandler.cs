using AutoMapper;
using MediatR;
using Univali.Domain.Entities;
using Univali.Application.Models;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.ModulesCollection.Queries;

public class GetModulesCollectionHandler : 
    IRequestHandler<GetModulesCollectionQuery, GetModulesCollectionResponse>
{
    private readonly IPublisherRepository _publisherRepository;
    
    private readonly IMapper _mapper;

    public GetModulesCollectionHandler(
        IPublisherRepository publisherRepository, 
        IMapper mapper
    ) {
        _publisherRepository = publisherRepository;

        _mapper = mapper;
    }

    public async Task<GetModulesCollectionResponse> Handle(
        GetModulesCollectionQuery getModulesCollectionQuery, 
        CancellationToken cancellationToken
    ) {
       var (modules, paginationMetadata) = await _publisherRepository
            .GetModulesAsync(
                getModulesCollectionQuery.Title, 
                getModulesCollectionQuery.SearchQuery, 
                getModulesCollectionQuery.PageNumber, 
                getModulesCollectionQuery.PageSize
            );

       var getModulesCollectionResponse = new GetModulesCollectionResponse();

       getModulesCollectionResponse.Modules = _mapper.Map<List<ModuleDto>>(modules);

       getModulesCollectionResponse.Pagination = paginationMetadata;

       return getModulesCollectionResponse;
    }
}