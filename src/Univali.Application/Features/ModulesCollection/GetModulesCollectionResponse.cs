using Univali.Application.Features.Common;
using Univali.Application.Models;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.ModulesCollection.Queries;

public class GetModulesCollectionResponse : BaseResponse
{
    public List<ModuleDto> Modules {get; set;} = new();

    public PaginationMetadata? Pagination {get; set;}
}