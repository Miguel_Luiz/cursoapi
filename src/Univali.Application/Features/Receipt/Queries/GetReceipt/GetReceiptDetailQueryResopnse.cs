using Univali.Application.Features.Common;

namespace Univali.Application.Features.Receipts.Queries.GetReceipt;

public class GetReceiptDetailQueryResponse : BaseResponse
{
    public GetReceiptDetailDto Receipt { get; set; } = default!;
}