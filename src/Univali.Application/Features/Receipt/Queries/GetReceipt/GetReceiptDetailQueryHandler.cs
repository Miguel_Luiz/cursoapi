using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Receipts.Queries.GetReceipt;

public class GetReceiptDetailQueryHandler : IRequestHandler<GetReceiptDetailQuery, GetReceiptDetailQueryResponse>
{
    private readonly IMapper _mapper;
    private readonly IOrderRepository _orderRepository;

    public GetReceiptDetailQueryHandler(IOrderRepository orderRepository, IMapper mapper)
    {
        _orderRepository = orderRepository;
        _mapper = mapper;
    }

    public async Task<GetReceiptDetailQueryResponse> Handle(GetReceiptDetailQuery request, CancellationToken cancellationToken)
    {
        GetReceiptDetailQueryResponse response = new();

        var customerFromDatabase = await _orderRepository.GetCustomerByIdAsync(request.CustomerId);

        if(customerFromDatabase == null)
        {
            response.Errors.Add(
                "Customer",
                new string[] {$"Customer with id  = {request.CustomerId} was not found in the database"}
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var orderFromDatabase = await _orderRepository.GetOrderByIdAsync(request.CustomerId, request.OrderId);

        if (orderFromDatabase == null)
        {
            response.Errors.Add(
                "Order",
                new string[] { $"Order with id = {request.OrderId} for customer with id = {request.CustomerId} was not found in the database" }
            );

            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var receiptFromDatabase = await _orderRepository.GetReceiptFromOrderById(request.OrderId);

        if (receiptFromDatabase == null)
        {
            response.Errors.Add(
                "Receipt",
                new string[] { $"Receipt of order with id = {request.OrderId} for customer with id = {request.CustomerId} was not found in the database" }
            );

            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        response.Receipt = _mapper.Map<GetReceiptDetailDto>(receiptFromDatabase);
        return response;
    }
}