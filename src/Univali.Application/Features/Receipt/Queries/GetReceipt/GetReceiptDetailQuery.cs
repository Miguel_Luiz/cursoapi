using MediatR;

namespace Univali.Application.Features.Receipts.Queries.GetReceipt;

public class GetReceiptDetailQuery : IRequest<GetReceiptDetailQueryResponse>
{
  public int CustomerId { get; set; }
  public int OrderId { get; set; }
}