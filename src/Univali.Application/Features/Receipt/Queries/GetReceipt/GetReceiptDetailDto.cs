using Univali.Domain.Entities;
using Univali.Application.Models;

namespace Univali.Application.Features.Receipts.Queries.GetReceipt;

public class GetReceiptDetailDto 
{
  public int ReceiptId { get; set; }
  public string CustomerName { get; set; } = string.Empty;
  public string CustomerIdentifier { get; set; } = string.Empty;
  public PaymentType PaymentType { get; set; } // em implementacao completa, pode ser string com Name da classe de pagament especifica
  public DateTime Timestamp { get; set; }
  public decimal TotalPrice { get; set; }
}
