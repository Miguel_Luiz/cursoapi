using AutoMapper;
using MediatR;
using Univali.Application.Features.Common;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.QuestionsCollection.Queries.GetQuestionsCollectionDetail;

public class GetQuestionsCollectionDetailQueryHandler : IRequestHandler<GetQuestionsCollectionDetailQuery, GetQuestionsCollectionDetailResponse>
{
    private readonly IPublisherRepository _questionRepository;
    private readonly IMapper _mapper;

    public GetQuestionsCollectionDetailQueryHandler(IMapper mapper, IPublisherRepository questionRepository)
    {
        _mapper = mapper;
        _questionRepository = questionRepository;
    }

    public async Task<GetQuestionsCollectionDetailResponse> Handle(GetQuestionsCollectionDetailQuery request, CancellationToken cancellationToken)
    {
        GetQuestionsCollectionDetailResponse getQuestionsCollectionDetailResponse = new();

        var (questionFromDatabase, paginationMetadata) = await _questionRepository.GetQuestionsAsync(request.Category, request.SearchQuery, request.PageNumber, request.PageSize);

        getQuestionsCollectionDetailResponse.PaginationMetadata = paginationMetadata;

        getQuestionsCollectionDetailResponse.QuestionsDetailDto = _mapper.Map<IEnumerable<GetQuestionsCollectionDetailDto>>(questionFromDatabase);

        return getQuestionsCollectionDetailResponse;
    }
}