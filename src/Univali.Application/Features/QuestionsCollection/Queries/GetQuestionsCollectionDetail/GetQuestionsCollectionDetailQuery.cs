using MediatR;

namespace Univali.Application.Features.QuestionsCollection.Queries.GetQuestionsCollectionDetail;

public class GetQuestionsCollectionDetailQuery : IRequest<GetQuestionsCollectionDetailResponse>
{
        public string? Category { get; set; } = string.Empty;
        public string? SearchQuery { get; set;} = string.Empty;
        public int PageNumber {get;set;}
        public int PageSize{get;set;}
}