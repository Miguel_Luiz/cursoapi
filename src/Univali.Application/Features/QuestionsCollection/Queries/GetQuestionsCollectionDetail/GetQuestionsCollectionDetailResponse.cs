using Univali.Application.Features.Common;

namespace Univali.Application.Features.QuestionsCollection.Queries.GetQuestionsCollectionDetail;

public class GetQuestionsCollectionDetailResponse : BaseResponse
{
    public IEnumerable<GetQuestionsCollectionDetailDto> QuestionsDetailDto {get;set;} = null!;

    public PaginationMetadata PaginationMetadata = null!;
}