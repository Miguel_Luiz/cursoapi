using MediatR;

namespace Univali.Application.Features.Publishers.Queries.GetPublisher;

public class GetAllPublishersWithCoursesQuery 
    : IRequest<GetAllPublishersWithCoursesQueryResponse>
{
}