using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Domain.Entities;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Publishers.Queries.GetPublisher;

public class GetAllPublishersWithCoursesQueryHandler : 
    IRequestHandler<GetAllPublishersWithCoursesQuery, GetAllPublishersWithCoursesQueryResponse>
{
    private readonly IPublisherRepository _publisherRepository;

    private readonly IMapper _mapper;

    public GetAllPublishersWithCoursesQueryHandler(
        IPublisherRepository publisherRepository, 
        IMapper mapper
    ) {
        _publisherRepository = publisherRepository;

        _mapper = mapper;
    }

    public async Task<GetAllPublishersWithCoursesQueryResponse> Handle(
        GetAllPublishersWithCoursesQuery getAllPublishersWithCoursesQuery, 
        CancellationToken cancellationToken
    ) {
        GetAllPublishersWithCoursesQueryResponse getAllPublishersWithCoursesQueryResponse = new();

        List<object> publishersToReturn = new();

        var publishersFromDatabase = await _publisherRepository
            .GetAllPublishersWithCoursesAsync();

        if (publishersFromDatabase == null) { 
            getAllPublishersWithCoursesQueryResponse.Errors.Add(
                "Publisher",
                new string[] { $"No Publishers were not found in the database." }
            );
            getAllPublishersWithCoursesQueryResponse.ErrorType = Error.NotFoundProblem;

            return getAllPublishersWithCoursesQueryResponse;
        }

        foreach (Publisher publisher in publishersFromDatabase)
        {
            if(publisher is LegalPublisher legalPublisher)
            {
                var legalPublisherToReturn = _mapper.Map<LegalPublisherDto>(legalPublisher);
                publishersToReturn.Add(legalPublisherToReturn);
            }
            else if (publisher is NaturalPublisher naturalPublisher)
            {
                var naturalPublisherToReturn = _mapper.Map<NaturalPublisherDto>(naturalPublisher);
                publishersToReturn.Add(naturalPublisherToReturn);
            }
        }
        getAllPublishersWithCoursesQueryResponse.Publisher = publishersToReturn;
        return getAllPublishersWithCoursesQueryResponse;
    }
}

