// BRANCH: AULA
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Publishers.Queries.GetPublisher;

public class GetAllPublishersWithCoursesQueryResponse : BaseResponse
{
    public List<object> Publisher {get; set;} = new();

    public GetAllPublishersWithCoursesQueryResponse()
    {
        Errors = new Dictionary<string, string[]>();
    }
}