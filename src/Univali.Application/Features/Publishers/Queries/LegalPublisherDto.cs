using Univali.Application.Models;

namespace Univali.Application.Features.Publishers.Queries.GetPublisher;

    public class LegalPublisherDto : GetPublisherWithCoursesDto
    {
        public string CNPJ { get; set; } = string.Empty;

    }
