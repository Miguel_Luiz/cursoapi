// BRANCH: AULA
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Publishers.Queries.GetPublisher;

public class GetPublisherWithCoursesQueryResponse : BaseResponse
{
    public GetPublisherWithCoursesDto Publisher {get; set;} = default!;

    public GetPublisherWithCoursesQueryResponse()
    {
        Errors = new Dictionary<string, string[]>();
    }
}