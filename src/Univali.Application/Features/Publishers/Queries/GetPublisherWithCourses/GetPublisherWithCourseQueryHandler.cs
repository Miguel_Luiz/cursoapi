using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Application.Features.Publishers.Queries;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Publishers.Queries.GetPublisher;

public class GetPublisherWithCoursesQueryHandler : 
    IRequestHandler<GetPublisherWithCoursesQuery, GetPublisherWithCoursesQueryResponse>
{
    private readonly IPublisherRepository _publisherRepository;

    private readonly IMapper _mapper;

    public readonly IValidator<GetPublisherWithCoursesQuery> _validator;

    public GetPublisherWithCoursesQueryHandler(
        IPublisherRepository publisherRepository, 
        IMapper mapper,
        IValidator<GetPublisherWithCoursesQuery> validator
    ) {
        _publisherRepository = publisherRepository;

        _mapper = mapper;

        _validator = validator;
    }

 public async Task<GetPublisherWithCoursesQueryResponse> Handle(
        GetPublisherWithCoursesQuery getPublisherWithCoursesQuery, 
        CancellationToken cancellationToken
    ) {
        GetPublisherWithCoursesQueryResponse getPublisherWithCoursesQueryResponse = new();

        var validationResult = _validator.Validate(getPublisherWithCoursesQuery);

        if (validationResult.IsValid == false) 
        {
            foreach (var error in validationResult.ToDictionary()) 
            {
                getPublisherWithCoursesQueryResponse.Errors
                    .Add(error.Key, error.Value);
            }

            getPublisherWithCoursesQueryResponse.ErrorType = Error.ValidationProblem;
            return getPublisherWithCoursesQueryResponse;
        }

        var publisherFromDatabase = await _publisherRepository
            .GetPublisherWithCoursesByIdAsync(getPublisherWithCoursesQuery.PublisherId);

        if (publisherFromDatabase == null) { 
            getPublisherWithCoursesQueryResponse.Errors.Add(
                "Publisher",
                new string[] { $"Publisher with id = {getPublisherWithCoursesQuery.PublisherId} was not found in the database." }
            );
            getPublisherWithCoursesQueryResponse.ErrorType = Error.NotFoundProblem;
            return getPublisherWithCoursesQueryResponse;
        }

        getPublisherWithCoursesQueryResponse.Publisher = _mapper
            .Map<GetPublisherWithCoursesDto>(publisherFromDatabase);
        
        return getPublisherWithCoursesQueryResponse;
    }
}

