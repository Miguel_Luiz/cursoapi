using Univali.Application.Models;

namespace Univali.Application.Features.Publishers.Queries.GetPublisher;

    public class NaturalPublisherDto : GetPublisherWithCoursesDto
    {
        public string CPF { get; set; } = string.Empty;

    }
