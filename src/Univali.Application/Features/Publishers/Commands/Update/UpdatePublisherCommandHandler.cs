using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;
using Univali.Domain.Entities;

namespace Univali.Application.Features.Publishers.Commands.UpdatePublisher;

public class UpdatePublisherCommandHandler :
    IRequestHandler<UpdatePublisherCommand, UpdatePublisherCommandResponse>
{
    private readonly IPublisherRepository _publisherRepository;

    private readonly IMapper _mapper;

    public readonly IValidator<UpdatePublisherCommand> _validator;

    public UpdatePublisherCommandHandler(
        IPublisherRepository publisherRepository,
        IMapper mapper,
        IValidator<UpdatePublisherCommand> validator
    )
    {
        _publisherRepository = publisherRepository;

        _mapper = mapper;

        _validator = validator;
    }

    public async Task<UpdatePublisherCommandResponse> Handle(
        UpdatePublisherCommand updatePublisherCommand,
        CancellationToken cancellationToken
    )
    {
        UpdatePublisherCommandResponse updatePublisherCommandResponse = new();

        var validationResult = _validator.Validate(updatePublisherCommand);

        if (validationResult.IsValid == false)
        {
            foreach (var error in validationResult.ToDictionary())
            {
                updatePublisherCommandResponse.Errors
                    .Add(error.Key, error.Value);
            }

            updatePublisherCommandResponse.ErrorType = Error.ValidationProblem;
            return updatePublisherCommandResponse;
        }

        if (!string.IsNullOrEmpty(updatePublisherCommand.CPF) && !string.IsNullOrEmpty(updatePublisherCommand.CNPJ))
        {
            string[] error = { "More than one document informed" };
            updatePublisherCommandResponse.Errors.Add("DocumentType", error);

            updatePublisherCommandResponse.ErrorType = Error.BadRequestProblem;
            return updatePublisherCommandResponse;
        }

        var publisherFromDatabase = await _publisherRepository
            .GetPublisherByIdAsync(updatePublisherCommand.PublisherId);

        if (publisherFromDatabase == null)
        {
            updatePublisherCommandResponse.Errors.Add(
                "Publisher",
                new string[] { $"Publisher with id = {updatePublisherCommand.PublisherId} was not found in the database." }
            );
            updatePublisherCommandResponse.ErrorType = Error.NotFoundProblem;

            return updatePublisherCommandResponse;
        } 
        else if (publisherFromDatabase is LegalPublisher legalPublisher) {
            if (string.IsNullOrEmpty(updatePublisherCommand.CNPJ)) {
                string[] error = { "No CNPJ informed" };
                updatePublisherCommandResponse.Errors.Add("DocumentType", error);

                updatePublisherCommandResponse.ErrorType = Error.BadRequestProblem;
                return updatePublisherCommandResponse;
            }

        }
        else if (publisherFromDatabase is NaturalPublisher naturalPublisher) {
            if (string.IsNullOrEmpty(updatePublisherCommand.CPF)) {
                string[] error = { "No CPF informed" };
                updatePublisherCommandResponse.Errors.Add("DocumentType", error);
                
                updatePublisherCommandResponse.ErrorType = Error.BadRequestProblem;
                return updatePublisherCommandResponse;
            }
        }

        _mapper.Map(updatePublisherCommand, publisherFromDatabase);

        await _publisherRepository.SaveChangesAsync();

        return updatePublisherCommandResponse;
    }
}
