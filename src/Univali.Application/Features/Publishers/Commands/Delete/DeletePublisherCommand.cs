using MediatR;

namespace Univali.Application.Features.Publishers.Commands.DeletePublisher;

public class DeletePublisherCommand : IRequest<DeletePublisherCommandResponse>
{
    public int PublisherId { get; set; }
}

