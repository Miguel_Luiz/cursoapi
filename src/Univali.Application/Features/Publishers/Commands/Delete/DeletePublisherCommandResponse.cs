// BRANCH: AULA
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Publishers.Commands.DeletePublisher;

public class DeletePublisherCommandResponse : BaseResponse
{
    public DeletePublisherCommandResponse()
    {
        Errors = new Dictionary<string, string[]>();
    }
}