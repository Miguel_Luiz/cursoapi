using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Publishers.Commands.DeletePublisher;

public class DeletePublisherCommandHandler : 
    IRequestHandler<DeletePublisherCommand, DeletePublisherCommandResponse>
{
    private readonly IPublisherRepository _publisherRepository;

    public readonly IValidator<DeletePublisherCommand> _validator;

    public DeletePublisherCommandHandler(
        IPublisherRepository publisherRepository, 
        IMapper mapper,
        IValidator<DeletePublisherCommand> validator
    ) {
        _publisherRepository = publisherRepository;

        _validator = validator;
    }

    public async Task<DeletePublisherCommandResponse> Handle(
        DeletePublisherCommand deletePublisherCommand, 
        CancellationToken cancellationToken
    ) {
        DeletePublisherCommandResponse deletePublisherCommandResponse = new ();

        var validationResult = _validator.Validate(deletePublisherCommand);

        if (validationResult.IsValid == false) 
        {
            foreach (var error in validationResult.ToDictionary()) 
            {
                deletePublisherCommandResponse.Errors
                    .Add(error.Key, error.Value);
            }

            deletePublisherCommandResponse.ErrorType = Error.ValidationProblem;
            return deletePublisherCommandResponse;
        } 

        var publisherFromDatabase = await _publisherRepository
            .GetPublisherByIdAsync(deletePublisherCommand.PublisherId);

        if (publisherFromDatabase == null) 
        {
            deletePublisherCommandResponse.Errors.Add(
                "Publisher",
                new string[] { $"Publisher with id = {deletePublisherCommand.PublisherId} was not found in the database." }
            );
            deletePublisherCommandResponse.ErrorType = Error.NotFoundProblem;

            return deletePublisherCommandResponse;
        }

        _publisherRepository.DeletePublisher(publisherFromDatabase);

        await _publisherRepository.SaveChangesAsync();

        return deletePublisherCommandResponse;
    }
}
