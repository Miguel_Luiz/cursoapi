namespace Univali.Application.Features.Publishers.Commands.CreatePublisher;

public class CreatePublisherDto
{
    public int PublisherId {get; set;}

    public string Name {get; set;} = string.Empty;
}

