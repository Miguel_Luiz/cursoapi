// BRANCH: AULA
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Publishers.Commands.CreatePublisher;

public class CreatePublisherCommandResponse : BaseResponse
{
    public CreatePublisherDto Publisher {get; set;} = default!;

    public CreatePublisherCommandResponse()
    {
        Errors = new Dictionary<string, string[]>();
    }
}