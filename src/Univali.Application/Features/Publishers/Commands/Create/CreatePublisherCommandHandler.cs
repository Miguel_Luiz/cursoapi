using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Domain.Entities;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Publishers.Commands.CreatePublisher;

public class CreatePublisherCommandHandler : 
    IRequestHandler<CreatePublisherCommand, CreatePublisherCommandResponse>
{
    private readonly IPublisherRepository _publisherRepository;

    private readonly IMapper _mapper;

    public readonly IValidator<CreatePublisherCommand> _validator;

    public CreatePublisherCommandHandler(
        IPublisherRepository publisherRepository, 
        IMapper mapper,
        IValidator<CreatePublisherCommand> validator
    ) {
        _publisherRepository = publisherRepository;

        _mapper = mapper;
       
        _validator = validator;
    }

    public async Task<CreatePublisherCommandResponse> Handle(
        CreatePublisherCommand createPublisherCommand, 
        CancellationToken cancellationToken
    ) {
        CreatePublisherCommandResponse createPublisherCommandResponse = new();

        var validationResult = _validator.Validate(createPublisherCommand);

        if (validationResult.IsValid == false) 
        {
            foreach (var error in validationResult.ToDictionary()) 
            {
                createPublisherCommandResponse.Errors
                    .Add(error.Key, error.Value);
            }

            createPublisherCommandResponse.ErrorType = Error.ValidationProblem;
            return createPublisherCommandResponse;
        }

        if(!string.IsNullOrEmpty(createPublisherCommand.CPF) && !string.IsNullOrEmpty(createPublisherCommand.CNPJ))
        {
            string[] error = {"More than one document informed"};
            createPublisherCommandResponse.Errors.Add("DocumentType", error);
            
            createPublisherCommandResponse.ErrorType = Error.BadRequestProblem;
            return createPublisherCommandResponse;
        }

        if(!string.IsNullOrEmpty(createPublisherCommand.CPF))
        {
            var publisherEntity = _mapper.Map<NaturalPublisher>(createPublisherCommand);


            _publisherRepository.AddPublisher(publisherEntity);
            await _publisherRepository.SaveChangesAsync();
            createPublisherCommandResponse.Publisher = _mapper.Map<CreateNaturalPublisherDto>(publisherEntity);
        }
        else if (!string.IsNullOrEmpty(createPublisherCommand.CNPJ))
        {
            var publisherEntity = _mapper.Map<LegalPublisher>(createPublisherCommand);

            _publisherRepository.AddPublisher(publisherEntity);
            await _publisherRepository.SaveChangesAsync();
            createPublisherCommandResponse.Publisher = _mapper.Map<CreateLegalPublisherDto>(publisherEntity);
        }
        else 
        {
            string[] error = {"No documents were informed"};
            createPublisherCommandResponse.Errors.Add("DocumentType", error);
            
            createPublisherCommandResponse.ErrorType = Error.BadRequestProblem;
            return createPublisherCommandResponse;
        }
        //var publisherEntity = _mapper.Map<Publisher>(createPublisherCommand);

        
    
        return createPublisherCommandResponse;
    }
}

