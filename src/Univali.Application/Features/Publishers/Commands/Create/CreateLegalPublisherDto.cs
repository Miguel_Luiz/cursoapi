namespace Univali.Application.Features.Publishers.Commands.CreatePublisher;

public class CreateLegalPublisherDto : CreatePublisherDto
{
    public string CNPJ {get; set;} = string.Empty;    
}