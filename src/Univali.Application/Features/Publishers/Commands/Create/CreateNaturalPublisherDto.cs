namespace Univali.Application.Features.Publishers.Commands.CreatePublisher;

public class CreateNaturalPublisherDto : CreatePublisherDto
{
    public string CPF {get;set;} = string.Empty;
}