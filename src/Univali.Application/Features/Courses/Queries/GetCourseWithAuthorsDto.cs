using Univali.Application.Models;

namespace Univali.Application.Features.Courses.Queries
{
    public class GetCourseWithAuthorsDto
    {
        public int CourseId { get; set; }

        public string Title { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

// pablo1107 - Removido publisher, adicionado category, totalDuration e ModuleAmount
        public string? Category { get; set; }

        public TimeSpan TotalDuration {get;set;}

        public int ModuleAmount {get;set;}

        public decimal Price { get; set; }

        public List<AuthorDto> Authors { get; set; } = new List<AuthorDto>();
    }
}
