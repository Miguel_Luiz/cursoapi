using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Courses.Queries
{
    public class GetCourseWithAuthorsQueryHandler : 
        IRequestHandler<GetCourseWithAuthorsQuery, GetCourseWithAuthorsQueryResponse>
    {
        private readonly IPublisherRepository _publisherRepository;
        
        private readonly IMapper _mapper;

        public GetCourseWithAuthorsQueryHandler(
            IPublisherRepository publisherRepository, 
            IMapper mapper
        ) {
            _publisherRepository = publisherRepository;

            _mapper = mapper;
        }

// pablo1107 - validação de publisher not found
        public async Task<GetCourseWithAuthorsQueryResponse> Handle(
            GetCourseWithAuthorsQuery getCourseWithAuthorsQuery, 
            CancellationToken cancellationToken
        ) {
            GetCourseWithAuthorsQueryResponse getCourseWithAuthorsQueryResponse = new ();

            var publisherFromDatabase = await _publisherRepository.GetPublisherByIdAsync(getCourseWithAuthorsQuery.PublisherId);
            
            if (publisherFromDatabase == null) 
            {
                // pablo1207 - alterei a mensagem de erro para seguir o padrão
                getCourseWithAuthorsQueryResponse.Errors.Add(
                    "Publisher",
                    new string[]{$"The publisher with id = {getCourseWithAuthorsQuery.PublisherId} was not found in the database"}
                );
                getCourseWithAuthorsQueryResponse.ErrorType = Error.NotFoundProblem;
                
                return getCourseWithAuthorsQueryResponse;
            }

            var courseFromDatabase = await _publisherRepository
                .GetCourseWithAuthorsByIdAsync(
                    getCourseWithAuthorsQuery.PublisherId, 
                    getCourseWithAuthorsQuery.CourseId
                );

            if (courseFromDatabase == null)
            {
                // pablo1207 - alterei a mensagem de erro para seguir o padrão
                getCourseWithAuthorsQueryResponse.Errors.Add(
                    "Course",
                    new string[] {$"No course was found for publisher with id = {getCourseWithAuthorsQuery.PublisherId}" }
                );
                getCourseWithAuthorsQueryResponse.ErrorType = Error.NotFoundProblem;
                
                return getCourseWithAuthorsQueryResponse;
            }

            getCourseWithAuthorsQueryResponse.Course = _mapper.Map<GetCourseWithAuthorsDto>(courseFromDatabase);
            
            return getCourseWithAuthorsQueryResponse;
        }
    }
}
