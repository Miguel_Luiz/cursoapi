// BRANCH: AULA
using Univali.Domain.Entities;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Courses.Queries;

public class GetCourseWithAuthorsQueryResponse : BaseResponse
{
    public GetCourseWithAuthorsDto Course { get; set; } = default!;
}