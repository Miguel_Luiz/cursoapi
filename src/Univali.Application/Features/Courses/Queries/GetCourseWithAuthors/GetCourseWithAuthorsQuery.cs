using System.ComponentModel.DataAnnotations;
using MediatR;

namespace Univali.Application.Features.Courses.Queries;

public class GetCourseWithAuthorsQuery : IRequest<GetCourseWithAuthorsQueryResponse>
{
    [Required(ErrorMessage = "Error: PublisherId is required")]
    public int PublisherId { get; set; }

    [Required(ErrorMessage = "Error: CourseId is required")]
    public int CourseId { get; set; }
}