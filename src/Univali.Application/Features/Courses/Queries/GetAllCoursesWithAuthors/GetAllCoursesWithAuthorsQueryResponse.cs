// BRANCH: AULA
using Univali.Domain.Entities;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Courses.Queries;

public class GetAllCoursesWithAuthorsQueryResponse : BaseResponse
{
    public List<GetCourseWithAuthorsDto> Courses { get; set; } = default!;
}