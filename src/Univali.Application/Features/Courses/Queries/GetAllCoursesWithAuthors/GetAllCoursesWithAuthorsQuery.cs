using MediatR;

namespace Univali.Application.Features.Courses.Queries;

public class GetAllCoursesWithAuthorsQuery : IRequest<GetAllCoursesWithAuthorsQueryResponse>
{
    public int PublisherId { get; set; }
}