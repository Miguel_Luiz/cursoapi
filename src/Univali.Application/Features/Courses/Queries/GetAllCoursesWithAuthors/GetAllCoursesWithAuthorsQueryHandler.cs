using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Courses.Queries
{
    public class GetAllCoursesWithAuthorsQueryHandler : 
        IRequestHandler<GetAllCoursesWithAuthorsQuery, GetAllCoursesWithAuthorsQueryResponse>
    {
        private readonly IPublisherRepository _publisherRepository;
        
        private readonly IMapper _mapper;

        public GetAllCoursesWithAuthorsQueryHandler(
            IPublisherRepository publisherRepository, 
            IMapper mapper
        ) {
            _publisherRepository = publisherRepository;
            
            _mapper = mapper;
        }

// pablo1107 - Validação de publisher inexistente
        public async Task<GetAllCoursesWithAuthorsQueryResponse> Handle(
            GetAllCoursesWithAuthorsQuery getAllCoursesWithAuthorsQuery, 
            CancellationToken cancellationToken
        ) {
            GetAllCoursesWithAuthorsQueryResponse getAllCoursesWithAuthorsQueryResponse = new ();

            var publisherFromDatabase = await _publisherRepository.GetPublisherByIdAsync(getAllCoursesWithAuthorsQuery.PublisherId);
            
            if (publisherFromDatabase == null) 
            {
                // pablo1207 - alterei a mensagem de erro para seguir o padrão
                getAllCoursesWithAuthorsQueryResponse.Errors.Add(
                    "Publisher",
                    new string[]{$"The publisher with id = {getAllCoursesWithAuthorsQuery.PublisherId} was not found in the database"}
                );
                getAllCoursesWithAuthorsQueryResponse.ErrorType = Error.NotFoundProblem;
                
                return getAllCoursesWithAuthorsQueryResponse;
            }

            var coursesFromDatabase = await _publisherRepository
                .GetAllCoursesWithAuthorsAsync(getAllCoursesWithAuthorsQuery.PublisherId);

            if (coursesFromDatabase == null || !coursesFromDatabase.Any())
            {
                // pablo1207 - alterei a mensagem de erro para seguir o padrão
                getAllCoursesWithAuthorsQueryResponse.Errors.Add(
                    "Course",
                    new string[] {$"No course was found for publisher with id = {getAllCoursesWithAuthorsQuery.PublisherId}" }
                );
                getAllCoursesWithAuthorsQueryResponse.ErrorType = Error.NotFoundProblem;
                
                return getAllCoursesWithAuthorsQueryResponse;
            }

            getAllCoursesWithAuthorsQueryResponse.Courses = _mapper.Map<List<GetCourseWithAuthorsDto>>(coursesFromDatabase);
            
            return getAllCoursesWithAuthorsQueryResponse;
        }
    }
}
