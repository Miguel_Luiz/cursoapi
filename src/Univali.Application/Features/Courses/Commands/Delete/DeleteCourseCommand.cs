using System.ComponentModel.DataAnnotations;
using MediatR;
using Univali.Application.Features.Publishers.Commands.DeletePublisher;

namespace Univali.Application.Features.Courses.Commands;

public class DeleteCourseCommand : IRequest<DeleteCourseCommandResponse>
{
    public int CourseId { get; set; }
}

