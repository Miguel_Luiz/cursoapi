using MediatR;
using Univali.Application.Features.Courses.Commands.UpdateCourse;

namespace Univali.Application.Features.Courses.Commands;

public class UpdateCourseCommand : IRequest<UpdateCourseCommandResponse>
{
    public int CourseId { get; set; }

    public string Title { get; set; } = string.Empty;

    public string Description { get; set; } = string.Empty;

// pablo1107 - adicionei category
    public string Category { get; set; } = string.Empty;

    public decimal Price { get; set; }

    public int PublisherId {get; set;}
}

