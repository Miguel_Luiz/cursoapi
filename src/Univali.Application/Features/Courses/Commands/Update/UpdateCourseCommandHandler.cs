using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Application.Features.Courses.Commands.UpdateCourse;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Courses.Commands;

public class UpdateCourseCommandHandler :
    IRequestHandler<UpdateCourseCommand, UpdateCourseCommandResponse>
{
    private readonly IPublisherRepository _publisherRepository;

    private readonly IMapper _mapper;

    private readonly IValidator<UpdateCourseCommand> _validator;

    public UpdateCourseCommandHandler(
        IPublisherRepository publisherRepository,
        IMapper mapper, IValidator<UpdateCourseCommand> validator)
    {
        _publisherRepository = publisherRepository;

        _mapper = mapper;

        _validator = validator;
    }

    public async Task<UpdateCourseCommandResponse> Handle(
        UpdateCourseCommand request,
        CancellationToken cancellationToken)
    {
        UpdateCourseCommandResponse updateCourseCommandResponse = new();

        var validationResult = _validator.Validate(request);

        if (!validationResult.IsValid)
        {
            foreach (var error in validationResult.ToDictionary())
            {
                updateCourseCommandResponse.Errors.Add(error.Key, error.Value);
            }

            updateCourseCommandResponse.ErrorType = Error.ValidationProblem;
            return updateCourseCommandResponse;
        }
// pablo1107 - validação publisher not found

        var publisherFromDatabase = await _publisherRepository.GetPublisherByIdAsync(request.PublisherId);

        if (publisherFromDatabase == null)
        {
            updateCourseCommandResponse.Errors.Add(
                "Publisher",
                new string[] { $"Publisher with id = {request.PublisherId} was not found in the database." }
            );
            updateCourseCommandResponse.ErrorType = Error.NotFoundProblem;
            return updateCourseCommandResponse;
        } 

        var courseEntity = await _publisherRepository.GetCourseByIdAsync(request.CourseId);

        if (courseEntity == null)
        {
            updateCourseCommandResponse.Errors.Add(
                "Course",
                new string[] { $"Course with id = {request.CourseId} was not found in the database." }
            );
            
            updateCourseCommandResponse.ErrorType = Error.NotFoundProblem;
            
            return updateCourseCommandResponse;
        }

        _mapper.Map(request, courseEntity);

        await _publisherRepository.SaveChangesAsync();

        return updateCourseCommandResponse;
    }
}