using Univali.Application.Features.Common;

namespace Univali.Application.Features.Courses.Commands.CreateCourse;

public class CreateCourseCommandResponse : BaseResponse
{  
    public CreateCourseDto Course {get; set;} = default!;

}