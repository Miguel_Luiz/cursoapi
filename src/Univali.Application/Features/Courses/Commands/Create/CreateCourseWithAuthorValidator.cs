using FluentValidation;
using Univali.Domain.Entities;
using Univali.Application.Models;

namespace Univali.Application.Features.Courses.Commands.CreateCourseWithAuthor;

public class CreateCourseWithAuthorValidator : AbstractValidator<AuthorDto>
{
    public CreateCourseWithAuthorValidator()
    {
        RuleFor(a => a.FirstName)
            .Empty();

        RuleFor(a => a.LastName)
            .Empty();

        RuleFor(a => a.AuthorId)    
            .NotEmpty()
            .WithMessage("Fill a Id")      
            .GreaterThan(0)
            .WithMessage("The AuthorId must be greater than zero.");
    }
}