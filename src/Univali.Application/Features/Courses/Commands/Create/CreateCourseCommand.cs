using MediatR;
using Univali.Application.Features.Courses.Commands.CreateCourse;
using Univali.Application.Models;

namespace Univali.Application.Features.Courses.Commands;

public class CreateCourseCommand : IRequest<CreateCourseCommandResponse>
{
    public string Title { get; set; } = string.Empty;

    public string Description { get; set; } = string.Empty;

// pablo1107 - adicionei category
    public string Category { get; set; } = string.Empty;

    public decimal Price { get; set; }

    public List<AuthorDto> Authors { get; set; } = new();

    public int PublisherId { get; set; }
}

