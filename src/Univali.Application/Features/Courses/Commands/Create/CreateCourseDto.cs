using Univali.Domain.Entities;
using Univali.Application.Models;

namespace Univali.Application.Features.Courses.Commands;

public class CreateCourseDto
{
    public int CourseId { get; set; }

    public string Title { get; set; } = string.Empty;

    public string Description { get; set; } = string.Empty;

// pablo1107 - adicionei category e removi authors
    public string Category { get; set; } = string.Empty;

    public decimal Price { get; set; }

    public PublisherWithoutCoursesDto Publisher {get; set;} = default!;
}
