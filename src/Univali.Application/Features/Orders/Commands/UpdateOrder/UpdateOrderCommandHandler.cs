using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Domain.Entities;
using Univali.Application.Features.Common;


namespace Univali.Application.Features.Orders.Commands.UpdateOrder;

public class UpdateOrderCommandHandler : IRequestHandler<UpdateOrderCommand, UpdateOrderCommandResponse>
{
    private readonly IOrderRepository _orderRepository;
    private readonly IMapper _mapper;
    private readonly IValidator<UpdateOrderCommand> _validator;

    public UpdateOrderCommandHandler(IOrderRepository orderRepository, IMapper mapper, IValidator<UpdateOrderCommand> validator)
    {
        _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        _validator = validator ?? throw new ArgumentNullException(nameof(validator));
    }

    public async Task<UpdateOrderCommandResponse> Handle(UpdateOrderCommand request, CancellationToken cancellationToken)
    {
        UpdateOrderCommandResponse response = new();

        var customerFromDatabse = _orderRepository.GetCustomerByIdAsync(request.CustomerId);

        if(customerFromDatabse == null)
        {
            response.Errors.Add(
                "Customer",
                new string[]{$"Customer with id = {request.CustomerId} wasn't found in database"}
            );

            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var validationResult = _validator.Validate(request);

        if(!validationResult.IsValid)
        {
            foreach(var error in validationResult.ToDictionary())
            {
                response.Errors.Add(error.Key, error.Value);
            }

            response.ErrorType = Error.ValidationProblem;
            return response;
        }

        var orderFromDatabase = await _orderRepository.GetOrderInFullByIdAsync(request.CustomerId, request.OrderId);

        if(orderFromDatabase == null)
        {
            response.Errors.Add(
                "Order",
                new string[]{$"Order with id = {request.OrderId} wasn't found in database"}
            );

            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var orderSnapshot = _mapper.Map<UpdateOrderCommand>(orderFromDatabase);

        List<int> courseIds = request.Items
            .Select(i => i.CourseId)
            .ToList();
            
        if(!await _orderRepository.CoursesExistAsync(courseIds))
        {
            response.Errors.Add(
                "Courses",
                new string[] {$"One of the Courses Ids was not found in the database"}
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var courses = await _orderRepository.GetCoursesWithoutTrackingAsync(courseIds);

        _orderRepository.UpdateOrder(orderFromDatabase, request);
        orderFromDatabase.Items = _mapper.Map<IEnumerable<OrderItem>>(courses).ToList();
        await _orderRepository.SaveChangesAsync();

        if(!orderFromDatabase.UpdateReceipt()) 
        {
            _orderRepository.UpdateOrder(orderFromDatabase, orderSnapshot); // rollback devido ao SaveChanges anterior
            await _orderRepository.SaveChangesAsync();

            response.Errors.Add(
                "Receipt",
                new string[]{$"Error generating receipt"}
            );

            response.ErrorType = Error.ValidationProblem; // nao eh bem um erro de validacao, mas UnprocessableEntity cabe aqui entao deixa baixo
            return response;
        }

        await _orderRepository.SaveChangesAsync();

        return response;
    }
}