using Univali.Application.Features.Common;

namespace Univali.Application.Features.Orders.Commands.UpdateOrder;

public class UpdateOrderCommandResponse : BaseResponse
{ }