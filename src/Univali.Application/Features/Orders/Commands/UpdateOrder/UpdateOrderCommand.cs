using System.ComponentModel.DataAnnotations;
using MediatR;
using Univali.Application.Models;
using Univali.Application.ValidationAttributes;

namespace Univali.Application.Features.Orders.Commands.UpdateOrder;

public  class UpdateOrderCommand: IRequest<UpdateOrderCommandResponse>
{
    public int OrderId { get; set; }

    public int CustomerId { get; set; }

    public ICollection<OrderItemForCreationDto> Items { get; set; } = new List<OrderItemForCreationDto>();

    public PaymentForCreationDto Payment { get; set; } = new();
}