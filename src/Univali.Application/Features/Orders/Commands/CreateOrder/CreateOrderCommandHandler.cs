using AutoMapper;
using FluentValidation;
using MediatR;
using Univali.Domain.Entities;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Application.Features.Orders.Commands.CreateOrder;

// O primeiro parâmetro é o tipo da mensagem
// O segundo parâmetro é o tipo que se espera receber.
public class CreateOrderCommandHandler: IRequestHandler<CreateOrderCommand, CreateOrderCommandResponse>
{
    private readonly IOrderRepository _orderRepository;
    private readonly IMapper _mapper;
    private readonly IValidator<CreateOrderCommand> _validator;

    public CreateOrderCommandHandler(IOrderRepository orderRepository, IMapper mapper, IValidator<CreateOrderCommand> validator)
    {
        _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        _validator = validator ?? throw new ArgumentNullException(nameof(validator));
    }

    public async Task<CreateOrderCommandResponse> Handle(CreateOrderCommand request, CancellationToken cancellationToken)
    {
        CreateOrderCommandResponse response = new();
        
        var validationResult = _validator.Validate(request);

        if(!validationResult.IsValid)
        {
            foreach(var error in validationResult.ToDictionary())
            {
                response.Errors.Add(error.Key, error.Value);
            }

            response.ErrorType = Error.ValidationProblem;
            return response;
        }

        var orderEntity = _mapper.Map<Order>(request);

        List<int> courseIds = request.Items
            .Select(i => i.CourseId)
            .ToList();

        if(!await _orderRepository.CoursesExistAsync(courseIds))
        {
            response.Errors.Add(
                "Courses",
                new string[] {$"One of the Courses Ids was not found in the database"}
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var courses = await _orderRepository.GetCoursesWithoutTrackingAsync(courseIds);
        orderEntity.Items = _mapper.Map<IEnumerable <OrderItem>>(courses).ToList();

        var customerEntity = await _orderRepository.GetCustomerByIdAsync(request.CustomerId);

        if(customerEntity == null)
        {
            response.Errors.Add(
                "Customer",
                new string[] {$"Customer with id  = {request.CustomerId} was not found in the database"}
            );

            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        _orderRepository.AddOrder(customerEntity, orderEntity);
        await _orderRepository.SaveChangesAsync();

        if(!orderEntity.GenerateReceipt()) 
        {
            _orderRepository.DeleteOrder(orderEntity);
            await _orderRepository.SaveChangesAsync();

            response.Errors.Add(
                "Receipt",
                new string[]{$"Error generating receipt"}
            );

            response.ErrorType = Error.ValidationProblem;
            return response;
        }
        
        await _orderRepository.SaveChangesAsync();

        var orderToReturn = _mapper.Map<CreateOrderDto>(orderEntity);
        response.Order = orderToReturn;

        return response;
  }
}