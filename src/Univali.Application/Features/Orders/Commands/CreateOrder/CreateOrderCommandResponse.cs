using Univali.Application.Features.Common;

namespace Univali.Application.Features.Orders.Commands.CreateOrder;

public class CreateOrderCommandResponse : BaseResponse
{
    public CreateOrderDto Order { get; set; } = default!;
}