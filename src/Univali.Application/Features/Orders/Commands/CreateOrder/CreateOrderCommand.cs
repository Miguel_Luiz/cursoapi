using MediatR;
using Univali.Application.Models;

namespace Univali.Application.Features.Orders.Commands.CreateOrder;

public class CreateOrderCommand : IRequest<CreateOrderCommandResponse>
{
    public int CustomerId { get; set; }
    public ICollection<OrderItemForCreationDto> Items { get; set; } = new List<OrderItemForCreationDto>();
    public PaymentForCreationDto Payment { get; set; } = new();
}


