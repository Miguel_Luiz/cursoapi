using FluentValidation;
namespace Univali.Application.Features.Orders.Commands.CreateOrder;

public class CreateOrderCommandValidator : AbstractValidator<CreateOrderCommand>
{
    public CreateOrderCommandValidator()
    {
        /* Order apenas tem relacoes, nao possuindo nenhum dado por si so.
           Por isso, as validacoes sao feitas com consultas no banco de dados,
           verificando se as entidades relacionadas existem.
           Esse tipo de validacao nao cabe ao Validator da entrada, logo essa classe fica vazia*/
    }
}
