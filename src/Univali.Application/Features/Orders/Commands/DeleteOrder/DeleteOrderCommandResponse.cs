using Univali.Application.Features.Common;

namespace Univali.Application.Features.Orders.Commands.DeleteOrder;

public class DeleteOrderCommandResponse : BaseResponse
{ }