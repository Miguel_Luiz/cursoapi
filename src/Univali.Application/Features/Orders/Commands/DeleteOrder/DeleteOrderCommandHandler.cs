using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;


namespace Univali.Application.Features.Orders.Commands.DeleteOrder;
public class DeleteOrderCommandHandler : IRequestHandler<DeleteOrderCommand, DeleteOrderCommandResponse>
{
    private readonly IOrderRepository _orderRepository;
    private readonly IMapper _mapper;

    public DeleteOrderCommandHandler(IOrderRepository orderRepository, IMapper mapper)
    {
        _orderRepository = orderRepository;
        _mapper = mapper;
    }

    public async Task<DeleteOrderCommandResponse> Handle(DeleteOrderCommand request, CancellationToken cancellationToken)
    {
        DeleteOrderCommandResponse response = new();

        var customerFromDatabase = await _orderRepository.GetCustomerByIdAsync(request.CustomerId);

        if(customerFromDatabase == null)
        {
            response.Errors.Add(
                "Customer",
                new string[] {$"Customer with id  = {request.CustomerId} was not found in the database"}
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var orderFromDatabase = await _orderRepository.GetOrderByIdAsync(request.CustomerId, request.OrderId);

        if(orderFromDatabase == null){
            response.Errors.Add(
                "Orders",
                new string[] {
                    $"No one order in customer with id = {request.CustomerId} was found"
                }
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        _orderRepository.DeleteOrder(orderFromDatabase);

        await _orderRepository.SaveChangesAsync();

        return response;
    }
}