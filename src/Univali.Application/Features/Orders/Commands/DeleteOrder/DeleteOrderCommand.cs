using MediatR;

namespace Univali.Application.Features.Orders.Commands.DeleteOrder;

public  class DeleteOrderCommand: IRequest<DeleteOrderCommandResponse>
{
    public int CustomerId {get; set;}
    public int OrderId { get; set; }
}