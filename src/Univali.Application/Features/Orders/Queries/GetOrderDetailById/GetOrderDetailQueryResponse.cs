using Univali.Application.Features.Common;


namespace Univali.Application.Features.Orders.Queries.GetOrderDetail;

public class GetOrderDetailQueryResponse : BaseResponse
{
    public GetOrderDetailDto Order { get; set; } = default!;
}