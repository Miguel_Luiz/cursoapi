using MediatR;

namespace Univali.Application.Features.Orders.Queries.GetOrderDetail;

public class GetOrderDetailQuery : IRequest<GetOrderDetailQueryResponse>
{
    public int OrderId {get; set;}
    public int CustomerId {get; set;}
}