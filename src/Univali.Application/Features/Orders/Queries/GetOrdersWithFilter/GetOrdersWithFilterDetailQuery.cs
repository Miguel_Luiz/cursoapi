using MediatR;

namespace Univali.Application.Features.Orders.Queries.GetOrdersWithFilter;

public class GetOrdersWithFilterDetailQuery : IRequest<GetOrdersWithFilterDetailQueryResponse>
{
    public int CustomerId { get; set; }
    public string ItemTittle { get; set; } = string.Empty;
    public string SearchQuery { get; set; } = string.Empty;
    public int PageNumber { get; set; }
    public int PageSize { get; set; }
}