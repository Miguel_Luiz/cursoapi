using Univali.Application.Features.Common;


namespace Univali.Application.Features.Orders.Queries.GetOrdersWithFilter;

public class GetOrdersWithFilterDetailQueryResponse : BaseResponse
{
    public IEnumerable<GetOrdersWithFilterDetailDto> Orders { get; set; } = default!;
}