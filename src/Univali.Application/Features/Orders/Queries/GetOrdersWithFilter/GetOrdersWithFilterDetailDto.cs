using Univali.Application.Models;

namespace Univali.Application.Features.Orders.Queries.GetOrdersWithFilter;

public class GetOrdersWithFilterDetailDto 
{
    public int OrderId { get; set; }
    public ICollection<OrderItemDto> Items { get; set; } = new List<OrderItemDto>();
    public PaymentDto Payment { get; set; } = new();
}
