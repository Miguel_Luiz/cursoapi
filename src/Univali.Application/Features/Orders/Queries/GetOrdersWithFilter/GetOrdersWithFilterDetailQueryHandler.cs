using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;


namespace Univali.Application.Features.Orders.Queries.GetOrdersWithFilter;

public class GetOrdersWithFilterDetailQueryHandler : IRequestHandler<GetOrdersWithFilterDetailQuery, GetOrdersWithFilterDetailQueryResponse>
{
    private readonly IMapper _mapper;
    private readonly IOrderRepository _orderRepository;

    public GetOrdersWithFilterDetailQueryHandler(IOrderRepository orderRepository, IMapper mapper)
    {
        _orderRepository = orderRepository;
        _mapper = mapper;
    }

    public async Task<GetOrdersWithFilterDetailQueryResponse> Handle(GetOrdersWithFilterDetailQuery request, CancellationToken cancellationToken)
    {
        GetOrdersWithFilterDetailQueryResponse response = new();

        var customerFromDatabase = await _orderRepository.GetCustomerByIdAsync(request.CustomerId);

        if(customerFromDatabase == null)
        {
            response.Errors.Add(
                "Customer",
                new string[] {$"Customer with id  = {request.CustomerId} was not found in the database"}
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var ordersFromDatabase = await _orderRepository.GetOrdersWithFilterAsync(
            request.CustomerId, request.ItemTittle, request.SearchQuery, request.PageNumber, request.PageSize
        );

        if(ordersFromDatabase == null)
        {
            response.Errors.Add(
                "Orders",
                new string[] {
                    $"No one order in customer with id = {request.CustomerId}, ItemTittle = {request.ItemTittle} or SerchQuery = {request.SearchQuery} was found"
                }
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }
            
        response.Orders = _mapper.Map<IEnumerable<GetOrdersWithFilterDetailDto>>(ordersFromDatabase).ToList();

        return response;
    }
}