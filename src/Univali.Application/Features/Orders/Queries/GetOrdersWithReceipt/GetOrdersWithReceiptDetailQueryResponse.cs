using Univali.Application.Features.Common;


namespace Univali.Application.Features.Orders.Queries.GetOrdersWithReceipt;

public class GetOrdersWithReceiptDetailQueryResponse : BaseResponse
{
    public IEnumerable<GetOrdersWithReceiptDetailDto> Orders { get; set; } = default!;
}