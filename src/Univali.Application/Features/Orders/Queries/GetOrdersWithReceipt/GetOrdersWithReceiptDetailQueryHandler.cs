using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;


namespace Univali.Application.Features.Orders.Queries.GetOrdersWithReceipt;

public class GetOrdersWithReceiptDetailQueryHandler : IRequestHandler<GetOrdersWithReceiptDetailQuery, GetOrdersWithReceiptDetailQueryResponse>
{
    private readonly IMapper _mapper;
    private readonly IOrderRepository _orderRepository;

    public GetOrdersWithReceiptDetailQueryHandler(IOrderRepository orderRepository, IMapper mapper)
    {
        _orderRepository = orderRepository;
        _mapper = mapper;
    }

    public async Task<GetOrdersWithReceiptDetailQueryResponse> Handle(GetOrdersWithReceiptDetailQuery request, CancellationToken cancellationToken)
    {
        GetOrdersWithReceiptDetailQueryResponse response = new();

        var customerFromDatabase = await _orderRepository.GetCustomerByIdAsync(request.CustomerId);

        if(customerFromDatabase == null)
        {
            response.Errors.Add(
                "Customer",
                new string[] {$"Customer with id  = {request.CustomerId} was not found in the database"}
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var ordersFromDatabase = await _orderRepository.GetOrdersWithReceiptAsync(request.CustomerId);

        if(ordersFromDatabase == null)
        {
             response.Errors.Add(
                "Orders with receipt",
                new string[] {$"No one order in customer with id = {request.CustomerId} was found"}
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }
            
        response.Orders = _mapper.Map<IEnumerable<GetOrdersWithReceiptDetailDto>>(ordersFromDatabase).ToList();

        return response;
    }
}