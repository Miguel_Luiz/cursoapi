using MediatR;

namespace Univali.Application.Features.Orders.Queries.GetOrdersWithReceipt;

public class GetOrdersWithReceiptDetailQuery : IRequest<GetOrdersWithReceiptDetailQueryResponse>
{
    public int CustomerId { get; set; }
}