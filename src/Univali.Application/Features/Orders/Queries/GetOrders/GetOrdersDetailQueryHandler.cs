using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;


namespace Univali.Application.Features.Orders.Queries.GetOrders;

public class GetOrdersDetailQueryHandler : IRequestHandler<GetOrdersDetailQuery, GetOrdersDetailQueryResponse>
{
    private readonly IMapper _mapper;
    private readonly IOrderRepository _orderRepository;

    public GetOrdersDetailQueryHandler(IOrderRepository orderRepository, IMapper mapper)
    {
        _orderRepository = orderRepository;
        _mapper = mapper;
    }

    public async Task<GetOrdersDetailQueryResponse> Handle(GetOrdersDetailQuery request, CancellationToken cancellationToken)
    {
        GetOrdersDetailQueryResponse response = new();

        var customerFromDatabase = await _orderRepository.GetCustomerByIdAsync(request.CustomerId);

        if(customerFromDatabase == null)
        {
            response.Errors.Add(
                "Customer",
                new string[] {$"Customer with id  = {request.CustomerId} was not found in the database"}
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }


        var ordersFromDatabase = await _orderRepository.GetOrdersAsync(request.CustomerId);

        if(ordersFromDatabase == null)
        {
            response.Errors.Add(
                "Orders",
                new string[] {$"No one order in customer with id = {request.CustomerId} was found"}
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }
            
        response.Orders = _mapper.Map<IEnumerable<GetOrdersDetailDto>>(ordersFromDatabase).ToList();

        return response;
    }
}