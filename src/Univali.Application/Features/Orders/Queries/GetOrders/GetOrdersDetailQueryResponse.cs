using Univali.Application.Features.Common;


namespace Univali.Application.Features.Orders.Queries.GetOrders;

public class GetOrdersDetailQueryResponse : BaseResponse
{
    public IEnumerable<GetOrdersDetailDto> Orders { get; set; } = default!;
}