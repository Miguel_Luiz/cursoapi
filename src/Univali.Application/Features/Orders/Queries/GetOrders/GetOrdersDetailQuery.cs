using MediatR;

namespace Univali.Application.Features.Orders.Queries.GetOrders;

public class GetOrdersDetailQuery : IRequest<GetOrdersDetailQueryResponse>
{
    public int CustomerId { get; set; }
}