using Univali.Application.Models;

namespace Univali.Application.Features.Orders.Queries.GetOrders;

public class GetOrdersDetailDto 
{
    public int OrderId { get; set; }
    public ICollection<OrderItemDto> Items { get; set; } = new List<OrderItemDto>();
    public PaymentDto Payment { get; set; } = new();
}
