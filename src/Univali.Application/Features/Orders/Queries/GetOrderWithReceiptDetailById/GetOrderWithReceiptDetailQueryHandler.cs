using AutoMapper;
using MediatR;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;


namespace Univali.Application.Features.Orders.Queries.GetOrderWithReceiptDetail;

public class GetOrderWithReceiptByIdDetailQueryHandler : IRequestHandler<GetOrderWithReceiptDetailQuery, GetOrderWithReceiptDetailQueryResponse>
{
    private readonly IOrderRepository _orderRepository;
    private readonly IMapper _mapper;

    public GetOrderWithReceiptByIdDetailQueryHandler(IOrderRepository orderRepository, IMapper mapper)
    {
        _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    }

    public async Task<GetOrderWithReceiptDetailQueryResponse> Handle(GetOrderWithReceiptDetailQuery request, CancellationToken cancellationToken)
    {
        GetOrderWithReceiptDetailQueryResponse response = new();

        var customerFromDatabase = await _orderRepository.GetCustomerByIdAsync(request.CustomerId);

        if(customerFromDatabase == null)
        {
            response.Errors.Add(
                "Customer",
                new string[] {$"Customer with id  = {request.CustomerId} was not found in the database"}
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        var orderFromDatabase = await _orderRepository.GetOrderWithReceiptByIdAsync(request.CustomerId, request.OrderId);

        if(orderFromDatabase == null)
        {
            response.Errors.Add(
                "Order",
                new string[] {$"Order with id  = {request.OrderId} was not found in the database"}
            );
            response.ErrorType = Error.NotFoundProblem;
            return response;
        }

        response.Order = _mapper.Map<GetOrderWithReceiptDetailDto>(orderFromDatabase);

        return response;
    }
}