using Univali.Application.Models;

namespace Univali.Application.Features.Orders.Queries.GetOrderWithReceiptDetail;

public class GetOrderWithReceiptDetailDto
{
    public int OrderId { get; set; }
    public ICollection<OrderItemDto> Items { get; set; } = new List<OrderItemDto>();
    public PaymentDto Payment { get; set; } = new();
    public ReceiptDto Receipt { get; set; } = new();
}

