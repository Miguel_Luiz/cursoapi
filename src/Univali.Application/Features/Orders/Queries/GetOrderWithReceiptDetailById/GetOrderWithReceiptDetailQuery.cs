using MediatR;

namespace Univali.Application.Features.Orders.Queries.GetOrderWithReceiptDetail;

public class GetOrderWithReceiptDetailQuery : IRequest<GetOrderWithReceiptDetailQueryResponse>
{
    public int OrderId {get; set;}
    public int CustomerId {get; set;}
}