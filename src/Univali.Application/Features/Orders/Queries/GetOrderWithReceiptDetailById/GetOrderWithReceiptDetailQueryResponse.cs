using Univali.Application.Features.Common;


namespace Univali.Application.Features.Orders.Queries.GetOrderWithReceiptDetail;

public class GetOrderWithReceiptDetailQueryResponse : BaseResponse
{
    public GetOrderWithReceiptDetailDto Order { get; set; } = default!;
}