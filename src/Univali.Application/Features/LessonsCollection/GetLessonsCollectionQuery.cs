using MediatR;

namespace Univali.Application.Features.LessonsCollection.Queries;

public class GetLessonsCollectionQuery : 
    IRequest<GetLessonsCollectionResponse>
{
    public GetLessonsCollectionQuery(string title, string searchQuery, int pageNumber, int pageSize)
    {
        Title = title;
        SearchQuery = searchQuery; 
        PageNumber = pageNumber;
        PageSize = pageSize;
    }

    public string Title { get; set; }

    public string SearchQuery { get; set; }

    public int PageNumber { get; set; }

    public int PageSize { get; set; }
}