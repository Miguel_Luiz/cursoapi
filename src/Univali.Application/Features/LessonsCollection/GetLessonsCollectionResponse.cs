using Univali.Application.Features.Common;
using Univali.Application.Models;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.LessonsCollection.Queries;

public class GetLessonsCollectionResponse : BaseResponse
{
    public List<LessonDto> Lessons {get; set;} = new();

    public PaginationMetadata? Pagination {get; set;}
}