using AutoMapper;
using MediatR;
using Univali.Domain.Entities;
using Univali.Application.Models;
using Univali.Application.Contracts.Repositories;

namespace Univali.Application.Features.LessonsCollection.Queries;

public class GetLessonsCollectionHandler : 
    IRequestHandler<GetLessonsCollectionQuery, GetLessonsCollectionResponse>
{
    private readonly IPublisherRepository _publisherRepository;
    
    private readonly IMapper _mapper;

    public GetLessonsCollectionHandler(
        IPublisherRepository publisherRepository, 
        IMapper mapper
    ) {
        _publisherRepository = publisherRepository;

        _mapper = mapper;
    }

// APENAS PARA TESTES

    public async Task<GetLessonsCollectionResponse> Handle(
        GetLessonsCollectionQuery getLessonsCollectionQuery, 
        CancellationToken cancellationToken
    ) {
       var (lessons, paginationMetadata) = await _publisherRepository
            .GetLessonsCollectionAsync(
                getLessonsCollectionQuery.Title, 
                getLessonsCollectionQuery.SearchQuery, 
                getLessonsCollectionQuery.PageNumber, 
                getLessonsCollectionQuery.PageSize
            );

       var getLessonsCollectionResponse = new GetLessonsCollectionResponse();

       getLessonsCollectionResponse.Lessons = _mapper.Map<List<LessonDto>>(lessons);

       getLessonsCollectionResponse.Pagination = paginationMetadata;

       return getLessonsCollectionResponse;
    }
}