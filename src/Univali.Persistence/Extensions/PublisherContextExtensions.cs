//pablo1107 - o conteúdo de courses, lessons, modules tinha repetições e informações desleixadas
// Category de curso agora valida
using Microsoft.EntityFrameworkCore;
using Univali.Domain.Entities;

namespace Univali.Persistence.Extensions;

public static class PublisherContextExtensions
{
    public static void PublisherInitFluentApi(this ModelBuilder modelBuilder)
    {
        var publisher = modelBuilder.Entity<Publisher>();

        publisher.UseTptMappingStrategy();


        //discriminator é uma coluna que indica a hierarquia de cada entity

        // publisher
        //     .ToTable("Publishers")
        //     .HasDiscriminator<int>("PublisherType")
        //     .HasValue<Publisher>(1)
        //     .HasValue<NaturalPublisher>(2)
        //     .HasValue<LegalPublisher>(3);

        //======================================================================================
        //caso duas entidades na mesma hierarquia tenha um atributo repetente,
        //é possível adiciona-las na mesma coluna(tem de ser feito manualmente),
        //não é o nosso caso, pois as subclasses criadas estão herdando os atributos e não os repetem
        //exemplo de como seria feito abaixo:

        // modelBuilder.Entity<NaturalPublisher>()
        //     .Property(p => p.Name)
        //     .HasColumnName("Name");

        // modelBuilder.Entity<LegalPublisher>()
        //     .Property(p => p.Name)
        //     .HasColumnName("Name");
        //======================================================================================

        //======================================================================================
        //também é possível transformar o discriminator em uma propriedade na entity,
        //porém acho que não vamos usar
        //exemplo:

        // publisher
        //     .HasDiscriminator(p=>p.PublisherType);

        // publisher
        // .Property(e => e.PublisherType)
        // .HasMaxLength(200)
        // .HasColumnName("PublisherType");
        //======================================================================================

        publisher
            .HasMany(a => a.Courses)
            .WithOne(c => c.Publisher)
            .HasForeignKey(c => c.PublisherId);

        publisher
            .Property(p => p.Name)
            .HasMaxLength(50)
            .IsRequired();
    }

    public static void NaturalPublisherInitFluentApi(this ModelBuilder modelBuilder)
    {
        var naturalPublisher = modelBuilder.Entity<NaturalPublisher>();

        naturalPublisher
            .Property(p => p.CPF)
            .HasMaxLength(11)
            .IsRequired();
    }

    public static void LegalPublisherInitFluentApi(this ModelBuilder modelBuilder)
    {
        var legalPublisher = modelBuilder.Entity<LegalPublisher>();

        legalPublisher
            .Property(p => p.CNPJ)
            .HasMaxLength(14)
            .IsRequired();
    }

    public static void AuthorInitFluentApi(this ModelBuilder modelBuilder)
    {
        var author = modelBuilder.Entity<Author>();

        author
            .HasMany(a => a.Courses)
            .WithMany(c => c.Authors)
            .UsingEntity<AuthorCourse>(
                ac => ac.ToTable("AuthorsCourses")
               .Property(ac => ac.CreatedOn).HasDefaultValueSql("NOW()")
            );

        author
            .Property(a => a.FirstName)
            .HasMaxLength(100)
            .IsRequired();

        author
            .Property(a => a.LastName)
            .HasMaxLength(100)
            .IsRequired();
    }

    public static void CourseInitFluentApi(this ModelBuilder modelBuilder)
    {
        var course = modelBuilder.Entity<Course>();

        course
            .HasMany(c => c.Modules)
            .WithOne(m => m.Course)
            .HasForeignKey(m => m.CourseId);

        course
            .HasMany(c => c.Students)
            .WithMany(s => s.Courses)
            .UsingEntity<CourseStudent>(
                cs => cs.ToTable("CoursesStudents")
               .Property(cs => cs.CreatedOn).HasDefaultValueSql("NOW()")
            );

        course
            .Property(c => c.Title)
            .HasMaxLength(100)
            .IsRequired();

        course
            .Property(c => c.Category)
            .HasMaxLength(100)
            .IsRequired(false);

        course
            .Property(c => c.Price)
            .HasPrecision(5, 2)
            .IsRequired();

        course
            .Property(c => c.Description)
            .IsRequired(false);
        
        course
            .Property(c => c.TotalDuration)
            .IsRequired();

        course
            .Property(c => c.ModuleAmount)
            .HasDefaultValue(0);


    }

    public static void ModuleInitFluentApi(this ModelBuilder modelBuilder)
    {
        var module = modelBuilder.Entity<Module>();

        module
            .HasMany(m => m.Lessons)
            .WithOne(l => l.Module)
            .HasForeignKey(l => l.ModuleId);

        module
            .Property(m => m.Title)
            .HasMaxLength(100)
            .IsRequired();

        module
            .Property(m => m.Description)
            .IsRequired();

        module
            .Property(m => m.Number)
            .IsRequired();

        module
            .Property(m => m.TotalDuration)
            .IsRequired();
    }

    public static void LessonInitFluentApi(this ModelBuilder modelBuilder)
    {
        var lesson = modelBuilder.Entity<Lesson>();

        lesson
            .HasMany(c => c.Questions)
            .WithOne(q => q.Lesson)
            .HasForeignKey(q => q.LessonId);

        lesson
            .Property(l => l.Title)
            .HasMaxLength(100)
            .IsRequired();

        lesson
            .Property(l => l.Description)
            .IsRequired();

        lesson
            .Property(l => l.Duration)
            .IsRequired();
    }

    public static void StudentInitFluentApi(this ModelBuilder modelBuilder)
    {
        var Student = modelBuilder.Entity<Student>();

        Student
            .HasMany(s => s.Questions)
            .WithOne(q => q.Student)
            .HasForeignKey(q => q.StudentId);
        
        Student
            .Property(p => p.Name)
            .HasMaxLength(80)
            .IsRequired();

        Student
            .Property(p => p.CPF)
            .HasMaxLength(11)
            .IsFixedLength();
        
        Student
            .Property(p => p.Age)
            .HasMaxLength(3)
            .IsFixedLength();
    }

    public static void QuestionInitFluentApi(this ModelBuilder modelBuilder)
    {
        var Question = modelBuilder.Entity<Question>();

        Question
            .HasMany(q => q.Answers)
            .WithOne(an => an.Question)
            .HasForeignKey(an => an.QuestionId);

        Question
            .Property(q => q.Questioning)
            .IsRequired();

        Question
            .Property(q => q.CreationDate)
            .IsRequired();
        
        Question
            .Property(q => q.Category)
            .IsRequired();
        
        Question
            .Property(q => q.LessonId)
            .IsRequired();
        
        Question
            .Property(q => q.StudentId)
            .IsRequired();
    }

    public static void AnswerInitFluentApi(this ModelBuilder modelBuilder)
    {
        var Answer = modelBuilder.Entity<Answer>();

        Answer
            .Property(an => an.QuestionId)
            .IsRequired();
        
        Answer
            .Property(an => an.Body)
            .IsRequired();
        
        Answer
            .Property(an => an.QuestionId)
            .IsRequired();
        
        Answer
            .Property(an => an.AuthorId)
            .IsRequired();
    }

    public static void PublisherContextSeedData(this ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<LegalPublisher>().HasData(
            new LegalPublisher
            {
                PublisherId = 1,
                Name = "LegalPublisher1",
                CNPJ = "64167199000120"
            },
            new LegalPublisher
            {
                PublisherId = 2,
                Name = "LegalPublisher2",
                CNPJ = "94333690000144"
            }
        );

        modelBuilder.Entity<NaturalPublisher>().HasData(
            new NaturalPublisher
            {
                PublisherId = 3,
                Name = "NaturalPublisher1",
                CPF = "07382817946"
            },
            new NaturalPublisher
            {
                PublisherId = 4,
                Name = "NaturalPublisher2",
                CPF = "12345678912"
            }
        );

        modelBuilder.Entity<Author>().HasData(
            new Author
            {
                AuthorId = 1,
                FirstName = "Grace",
                LastName = "Hopper",
            },
            new Author
            {
                AuthorId = 2,
                FirstName = "John",
                LastName = "Backus",
            },
            new Author
            {
                AuthorId = 3,
                FirstName = "Bill",
                LastName = "Gates",
            },
            new Author
            {
                AuthorId = 4,
                FirstName = "Jim",
                LastName = " Berners-Lee",
            },
            new Author
            {
                AuthorId = 5,
                FirstName = "Linus",
                LastName = "Torvalds",
            }
        );

        modelBuilder.Entity<Course>().HasData(
            new Course
            {
                CourseId = 1,
                Title = "ASP.NET Core Web Api",
                Price = 97.00m,
                Description = "In this course, you'll learn how to build an API with ASP.NET Core that connects to a database via Entity Framework Core from scratch.",
                Category = "Backend",
                TotalDuration = new TimeSpan(4,0,0),
                ModuleAmount = 1,
                PublisherId = 1
            },
            new Course
            {
                CourseId = 2,
                Title = "Entity Framework Fundamentals",
                Price = 197.00m,
                Description = "In this course, Entity Framework Core 6 Fundamentals, you’ll learn to work with data in your .NET applications.",
                Category = "Backend",
                TotalDuration = new TimeSpan(1,35,0),
                ModuleAmount = 1,
                PublisherId = 1
            },
            new Course
            {
                CourseId = 3,
                Title = "Getting Started with Linux",
                Price = 47.00m,
                Description = "You've heard that Linux is the future of enterprise computing and you're looking for a way in.",
                Category = "Operating Systems",
                TotalDuration = new TimeSpan(0,10,0),
                ModuleAmount = 1,
                PublisherId = 2
            },
            new Course
            {
                CourseId = 4,
                Title = "Getting Started with WINDOWNS",
                Price = 47.00m,
                Description = "You've heard that WINDOWNS is the future of enterprise computing and you're looking for a way in.",
                Category = "Operating Systems",
                TotalDuration = new TimeSpan(0,0,0),
                ModuleAmount = 0,
                PublisherId = 2
            },
            new Course
            {
                CourseId = 5,
                Title = "Getting Started with MACOS",
                Price = 47.00m,
                Description = "You've heard that MACOS is the future of enterprise computing and you're looking for a way in.",
                Category = "Operating Systems",
                TotalDuration = new TimeSpan(0,0,0),
                ModuleAmount = 0,
                PublisherId = 2
            },
            new Course
            {
                CourseId = 6,
                Title = "Front Angular",
                Price = 47.00m,
                Description = "Angular course description",
                Category = "Frontend",
                TotalDuration = new TimeSpan(0,0,0),
                ModuleAmount = 0,
                PublisherId = 2
            },
            new Course
            {
                CourseId = 7,
                Title = "Typescript",
                Price = 47.00m,
                Description = "Typescript course description",
                Category = "Frontend",
                TotalDuration = new TimeSpan(0,0,0),
                ModuleAmount = 0,
                PublisherId = 2
            }
        );

        modelBuilder.Entity<AuthorCourse>().HasData(
            new AuthorCourse { AuthorId = 1, CourseId = 1 },
            new AuthorCourse { AuthorId = 1, CourseId = 3 },
            new AuthorCourse { AuthorId = 2, CourseId = 1 },
            new AuthorCourse { AuthorId = 2, CourseId = 2 },
            new AuthorCourse { AuthorId = 4, CourseId = 1 },
            new AuthorCourse { AuthorId = 5, CourseId = 3 }
        );

        modelBuilder.Entity<Module>().HasData(
            new Module
            {
                ModuleId = 1,
                Title = "Everything about Patch",
                Description = "In this module we are gonna learn every detail about Patch",
                Number = 2,
                TotalDuration = new TimeSpan(4,0,0),
                LessonAmount = 1,
                CourseId = 1
            },
            new Module
            {
                ModuleId = 2,
                Title = "Validating",
                Description = "In this module we are gonna learn how to validate inputs in our database",
                Number = 4,
                TotalDuration = new TimeSpan(1,35,0),
                LessonAmount = 2,
                CourseId = 2
            },
            new Module
            {
                ModuleId = 3,
                Title = "Installing",
                Description = "This moudule has the necessary information for you to install your Linux",
                Number = 1,
                TotalDuration = new TimeSpan(0,10,0),
                LessonAmount = 1,
                CourseId = 3
            }
        );

        modelBuilder.Entity<Lesson>().HasData(
            new Lesson
            {
                LessonId = 1,
                Title = "How json works in Patch",
                Description = "In this lesson you will be learning about how json works inside Patch",
                Duration = new TimeSpan(4,0,0),
                ModuleId = 1
            },
            new Lesson
            {
                LessonId = 2,
                Title = "Data Notation",
                Description = "In this lesson you will be learning about how Data Notation works",
                Duration = new TimeSpan(0,15,0),
                ModuleId = 2
            },
            new Lesson
            {
                LessonId = 3,
                Title = "Fluent Validation",
                Description = "In this lesson you will be learning about how Fluent Validation works",
                Duration = new TimeSpan(1,20,0),
                ModuleId = 2
            },
            new Lesson
            {
                LessonId = 4,
                Title = "Grub",
                Description = "In this lesson you will be learning about how Grub works",
                Duration = new TimeSpan(1,20,0),
                ModuleId = 3
            }
        );

        modelBuilder.Entity<Student>().HasData(
            new Student
            {
                StudentId = 1,
                Name = "Steven Spielberg Production Company",
                CPF = "12345678980",
                Age = 21,
                RegistrationDate = DateTime.UtcNow
            },
            new Student
            {
                StudentId = 2,
                Name = "Pedro Certezas",
                CPF = "09876543212",
                Age = 30,
                RegistrationDate = DateTime.UtcNow
            },
            new Student
            {
                StudentId = 3,
                Name = "Casimiro Miguel",
                CPF = "02036032907",
                Age = 21,
                RegistrationDate = DateTime.UtcNow
            }
        );

        modelBuilder.Entity<Question>().HasData(
            new Question
            {
                QuestionId = 1,
                Questioning = "What status code should I return in the get method?",
                Category = "status-code",
                CreationDate = DateTime.UtcNow,
                LessonId = 1,
                StudentId = 1
            },
            new Question
            {
                QuestionId = 2,
                Questioning = "What is the function of a controller?",
                Category = "controller",
                CreationDate = DateTime.UtcNow,
                LessonId = 2,
                StudentId = 1
            },
            new Question
            {
                QuestionId = 3,
                Questioning = "Should I make a controller for each entity?",
                Category = "controller",
                CreationDate = DateTime.UtcNow,
                LessonId = 2,
                StudentId = 2
            },
            new Question
            {
                QuestionId = 4,
                Questioning = "How to set the precision of a decimal in FluentValidation?",
                Category = "FluentValidation",
                CreationDate = DateTime.UtcNow,
                LessonId = 3,
                StudentId = 2
            },
            new Question
            {
                QuestionId = 5,
                Questioning = "How to validate a text field to ensure it is not empty?",
                Category = "FluentValidation",
                CreationDate = DateTime.UtcNow,
                LessonId = 3,
                StudentId = 3
            },
            new Question
            {
                QuestionId = 6,
                Questioning = "Como validar um campo de data para garantir que esteja em um intervalo específico?",
                Category = "FluentValidation",
                CreationDate = DateTime.UtcNow,
                LessonId = 3,
                StudentId = 3
            }
        );

        modelBuilder.Entity<Answer>().HasData(
            new Answer {
                AnswerId = 1,
                Body = "Resposta do universo",
                QuestionId = 1,
                AuthorId = 1,
            },
            new Answer {
                AnswerId = 2,
                Body = "Resposta da vida",
                QuestionId = 1,
                AuthorId = 2,
            },
            new Answer {
                AnswerId = 3,
                Body = "Resposta de tudo",
                QuestionId = 2,
                AuthorId = 2,
            },
            new Answer {
                AnswerId = 4,
                Body = "Resposta de nada",
                QuestionId = 3,
                AuthorId = 1,
            },
            new Answer {
                AnswerId = 5,
                Body = "Nem resposta isso é",
                QuestionId = 3,
                AuthorId = 1,
            },
            new Answer {
                AnswerId = 6,
                Body = "Sim, mas na verdade não",
                QuestionId = 3,
                AuthorId = 1,
            }
        );

        modelBuilder.Entity<CourseStudent>().HasData(
            new CourseStudent { StudentId = 1, CourseId = 1 },
            new CourseStudent { StudentId = 1, CourseId = 3 },
            new CourseStudent { StudentId = 2, CourseId = 1 },
            new CourseStudent { StudentId = 2, CourseId = 2 },
            new CourseStudent { StudentId = 3, CourseId = 1 },
            new CourseStudent { StudentId = 3, CourseId = 3 }
        );
    }
}
