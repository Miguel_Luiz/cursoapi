using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Univali.Application.Contracts.Repositories;
using Univali.Persistence.Repositories;

namespace Univali.Persistence.DbContexts;

public static class PersistenceServiceRegistration
{
    private const string HOST_STRING = "localhost";
    private const string DATABASE_NAME = "Univali";
    private const string POSTGRES_USERNAME = "postgres";
    private const string POSTGRES_PASSWORD = "123456";

    private const string DATABASE_INIT_STRING = $"Host={HOST_STRING};Database={DATABASE_NAME};Username={POSTGRES_USERNAME};Password={POSTGRES_PASSWORD}";
    // private const string DATABASE_INIT_STRING = "Host=localhost;Database=Univali;Username=postgres;Password=123456";

    public static IServiceCollection AddPersistenceServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddRepositoryServices();
        services.AddDbContexts();

        return services;
    }

    private static void AddRepositoryServices(this IServiceCollection services)
    {
        services.AddScoped<ICustomerRepository, CustomerRepository>();
        services.AddScoped<IPublisherRepository, PublisherRepository>();
        services.AddScoped<IOrderRepository, OrderRepository>();
        services.AddScoped<IUserRepository, UserRepository>();
    }

    private static void AddDbContexts(this IServiceCollection services)
    {
        services.AddDbContext<CustomerContext>(options =>
            options.UseNpgsql(DATABASE_INIT_STRING)
        );

        services.AddDbContext<PublisherContext>(options =>
            options.UseNpgsql(DATABASE_INIT_STRING)
        );

        services.AddDbContext<OrderContext>(options =>
            options.UseNpgsql(DATABASE_INIT_STRING)
        );

        services.AddDbContext<UserContext>(options =>
            options.UseNpgsql(DATABASE_INIT_STRING)
        );
    }
}
