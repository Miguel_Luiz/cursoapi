using Microsoft.EntityFrameworkCore;
using Univali.Domain.Entities;

namespace Univali.Persistence.Extensions;

public static class CustomerContextExtensions
{
    public static void CustomerInitFluentApi(this ModelBuilder modelBuilder)
    {
        var customer = modelBuilder.Entity<Customer>();

        customer
            .ToTable("Customers")
            .HasDiscriminator(c => c.Type)
            .HasValue<Customer>(1)
            .HasValue<NaturalCustomer>(2)
            .HasValue<LegalCustomer>(3);

        customer
            .HasMany(c => c.Addresses)
            .WithOne(a => a.Customer)
            .HasForeignKey(a => a.CustomerId);

        customer
            .HasMany(c => c.Orders)
            .WithOne(o => o.Customer)
            .HasForeignKey(o => o.CustomerId);

        customer
            .Property(customer => customer.Name)
            .HasMaxLength(50)
            .IsRequired();

        customer
            .Property(customer => customer.Type)
            .HasMaxLength(100)
            .IsRequired();

        // customer
        //     .Property(customer => customer.Cpf)
        //     .HasMaxLength(11)
        //     .IsRequired();
    }

    public static void LegalCustomerInitFluentApi(this ModelBuilder modelBuilder)
    {
        var legalCustomer = modelBuilder.Entity<LegalCustomer>();

        legalCustomer
            .Property(a => a.CNPJ)
            .IsFixedLength()
            .HasMaxLength(14)
            .IsRequired();
    }

    public static void NaturalCustomerInitFluentApi(this ModelBuilder modelBuilder)
    {
        var naturalCustomer = modelBuilder.Entity<NaturalCustomer>();

        naturalCustomer
            .Property(a => a.CPF)
            .IsFixedLength()
            .HasMaxLength(11)
            .IsRequired();
    }

    public static void AddressInitFluentApi(this ModelBuilder modelBuilder)
    {
        var address = modelBuilder.Entity<Address>();

        address
            .Property(address => address.Street)
            .HasMaxLength(50)
            .IsRequired();

        address
            .Property(address => address.City)
            .HasMaxLength(50)
            .IsRequired();

        address
            .HasOne(a => a.Customer)
            .WithMany(c => c.Addresses);
    }

    public static void CustomerContextSeedData(this ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<LegalCustomer>().HasData(
            new LegalCustomer()
            {
                Id = 1,
                Name = "Linus Torvalds",
                CNPJ = "14698277000144"
            }   
        );

        modelBuilder.Entity<NaturalCustomer>().HasData(
            new NaturalCustomer
            {
                Id = 2,
                Name = "Bill Gates",
                CPF = "95395994076"
            }
        );

        modelBuilder.Entity<Address>().HasData(
            new Address
            {
                Id = 1,
                Street = "Verão do Cometa",
                City = "Elvira",
                CustomerId = 1
            },
            new Address
            {
                Id = 2,
                Street = "Borboletas Psicodélicas",
                City = "Perobia",
                CustomerId = 1
            },
            new Address
            {
                Id = 3,
                Street = "Canção Excêntrica",
                City = "Salandra",
                CustomerId = 2
            }
        );
    }
}
