using Microsoft.EntityFrameworkCore;
using Univali.Domain.Entities;

namespace Univali.Persistence.Extensions;

public static class OrderContextExtensions
{
    public static void OrderInitFluentApi(this ModelBuilder modelBuilder)
    {
        var order = modelBuilder.Entity<Order>();

        order
            .HasOne(o => o.Customer)
            .WithMany(c => c.Orders);

        order
            .HasMany(o => o.Items)
            .WithOne(i => i.Order)
            .HasForeignKey(i => i.OrderId);

        order
            .HasOne(o => o.Payment)
            .WithOne(p => p.Order)
            .HasForeignKey<Payment>(p => p.OrderId);

        order
            .HasOne(o => o.Receipt)
            .WithOne(r => r.Order)
            .HasForeignKey<Receipt>(r => r.OrderId);
    }

    public static void OrderItemInitFluentApi(this ModelBuilder modelBuilder)
    {
        var orderItem = modelBuilder.Entity<OrderItem>();

        orderItem
            .HasOne(i => i.Order)
            .WithMany(o => o.Items);

        orderItem
            .Property(o => o.Title)
            .HasMaxLength(60)
            .IsRequired();

        orderItem
            .Property(o => o.Price)
            .HasPrecision(5, 2)
            .HasColumnName("Price")
            .IsRequired();

        orderItem
            .Property(o => o.Description)
            .IsRequired(false);
    }

    public static void ReceiptInitFluentApi(this ModelBuilder modelBuilder)
    {
        var receipt = modelBuilder.Entity<Receipt>();

        receipt
            .HasOne(r => r.Order)
            .WithOne(o => o.Receipt);

        receipt
            .Property(r => r.CustomerName)
            .HasMaxLength(50)
            .IsRequired();

        receipt
            .Property(r => r.CustomerIdentifier)
            .HasMaxLength(14)
            .IsRequired();

        receipt
            .Property(r => r.PaymentType)
            .HasConversion<int>()
            .IsRequired();

        receipt
            .Property(r => r.Timestamp)
            .IsRequired();

        receipt
            .Property(r => r.TotalPrice)
            .HasPrecision(6, 2) // cursos ate 999,99, pedido total ate 9999,99
            .IsRequired();
    }

    public static void PaymentInitFluentApi(this ModelBuilder modelBuilder)
    {
        var payment = modelBuilder.Entity<Payment>();

        payment
            .HasOne(p => p.Order)
            .WithOne(o => o.Payment);

        payment
            .Property(p => p.Type)
            .HasConversion<int>()
            .IsRequired();

        payment
            .Property(p => p.Description)
            .IsRequired(false);
    }

    

    public static void OrderContextSeedData(this ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Order>().HasData(
            new Order
            {
                OrderId = 1,
                CustomerId = 1,
            }
        );

        modelBuilder.Entity<OrderItem>().HasData(
            new OrderItem
            {
                OrderItemId = 1,
                Title = "ASP.NET Core Web Api",
                Price = 97.00m,
                Description = "In this course, you'll learn how to build an API with ASP.NET Core that connects to a database via Entity Framework Core from scratch.",
                OrderId = 1
            },
            new OrderItem
            {
                OrderItemId = 2,
                Title = "Getting Started with Linux",
                Price = 47.00m,
                Description = "You've heard that Linux is the future of enterprise computing and you're looking for a way in.",
                OrderId = 1
            }
        );

        modelBuilder.Entity<Payment>().HasData(
            new Payment
            {
                PaymentId = 1,
                Type = PaymentType.CreditCard,
                Description = "Forca Jovem",
                OrderId = 1
            }
        );

        modelBuilder.Entity<Receipt>().HasData(
            new Receipt
            {
                ReceiptId = 1,
                CustomerName = "Linus Torvalds",
                CustomerIdentifier = "14698277000144",
                PaymentType = PaymentType.CreditCard,
                Timestamp = DateTime.UtcNow,
                TotalPrice = 144.00m,
                OrderId = 1
            }
        );
    }
}