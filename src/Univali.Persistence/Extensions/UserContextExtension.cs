using System.Security.Cryptography;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Univali.Domain.Entities;

namespace Univali.Persistence.Extensions;

internal static class UserContextExtension
{
    public static void UserInitFluentApi(this ModelBuilder modelBuilder)
    {
        var user = modelBuilder.Entity<User>();

        user.Property(c => c.Name)
            .HasMaxLength(100)
            .IsRequired();

        user.Property(c => c.UserName)
            .HasMaxLength(100)
            .IsRequired();

        user.Property(c => c.Password)
            .HasMaxLength(100)
            .IsRequired();

        user.Property(c => c.ClearanceLevel)
            .IsRequired();
    }

    public static void UserContextSeedData(this ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<User>()
            .HasData(
                new User()
                {
                    Id = 1,
                    Name = "Teste1",
                    UserName = "Teste1",
                    Password = HashPassword("teste1"),
                    ClearanceLevel = User.Clearance.Admin
                },
                new User
                {
                    Id = 2,
                    Name = "Teste",
                    UserName = "Teste2",
                    Password = HashPassword("teste2"),
                    ClearanceLevel = User.Clearance.Publisher
                });
    }

    private static string HashPassword(string password)
    {
        using var hmac = new HMACSHA256(Encoding.UTF8.GetBytes("SecretKey"));
        byte[] hashBytes = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
        return Convert.ToBase64String(hashBytes);
    }
}