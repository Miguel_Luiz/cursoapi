﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Univali.Persistence.Migrations.Publisher
{
    /// <inheritdoc />
    public partial class InitialMigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Authors",
                columns: table => new
                {
                    AuthorId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FirstName = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    LastName = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Authors", x => x.AuthorId);
                });

            migrationBuilder.CreateTable(
                name: "Publishers",
                columns: table => new
                {
                    PublisherId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Publishers", x => x.PublisherId);
                });

            migrationBuilder.CreateTable(
                name: "Students",
                columns: table => new
                {
                    StudentId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(80)", maxLength: 80, nullable: false),
                    CPF = table.Column<string>(type: "character(11)", fixedLength: true, maxLength: 11, nullable: false),
                    Age = table.Column<int>(type: "integer", fixedLength: true, maxLength: 3, nullable: false),
                    RegistrationDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Students", x => x.StudentId);
                });

            migrationBuilder.CreateTable(
                name: "Courses",
                columns: table => new
                {
                    CourseId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Title = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Price = table.Column<decimal>(type: "numeric(5,2)", precision: 5, scale: 2, nullable: false),
                    Category = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    TotalDuration = table.Column<TimeSpan>(type: "interval", nullable: false),
                    ModuleAmount = table.Column<int>(type: "integer", nullable: false, defaultValue: 0),
                    PublisherId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Courses", x => x.CourseId);
                    table.ForeignKey(
                        name: "FK_Courses_Publishers_PublisherId",
                        column: x => x.PublisherId,
                        principalTable: "Publishers",
                        principalColumn: "PublisherId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LegalPublisher",
                columns: table => new
                {
                    PublisherId = table.Column<int>(type: "integer", nullable: false),
                    CNPJ = table.Column<string>(type: "character varying(14)", maxLength: 14, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LegalPublisher", x => x.PublisherId);
                    table.ForeignKey(
                        name: "FK_LegalPublisher_Publishers_PublisherId",
                        column: x => x.PublisherId,
                        principalTable: "Publishers",
                        principalColumn: "PublisherId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "NaturalPublisher",
                columns: table => new
                {
                    PublisherId = table.Column<int>(type: "integer", nullable: false),
                    CPF = table.Column<string>(type: "character varying(11)", maxLength: 11, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NaturalPublisher", x => x.PublisherId);
                    table.ForeignKey(
                        name: "FK_NaturalPublisher_Publishers_PublisherId",
                        column: x => x.PublisherId,
                        principalTable: "Publishers",
                        principalColumn: "PublisherId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AuthorsCourses",
                columns: table => new
                {
                    AuthorId = table.Column<int>(type: "integer", nullable: false),
                    CourseId = table.Column<int>(type: "integer", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: false, defaultValueSql: "NOW()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthorsCourses", x => new { x.AuthorId, x.CourseId });
                    table.ForeignKey(
                        name: "FK_AuthorsCourses_Authors_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Authors",
                        principalColumn: "AuthorId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AuthorsCourses_Courses_CourseId",
                        column: x => x.CourseId,
                        principalTable: "Courses",
                        principalColumn: "CourseId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CoursesStudents",
                columns: table => new
                {
                    CourseId = table.Column<int>(type: "integer", nullable: false),
                    StudentId = table.Column<int>(type: "integer", nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: false, defaultValueSql: "NOW()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoursesStudents", x => new { x.CourseId, x.StudentId });
                    table.ForeignKey(
                        name: "FK_CoursesStudents_Courses_CourseId",
                        column: x => x.CourseId,
                        principalTable: "Courses",
                        principalColumn: "CourseId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CoursesStudents_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "StudentId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Modules",
                columns: table => new
                {
                    ModuleId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Title = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    Description = table.Column<string>(type: "text", nullable: false),
                    Number = table.Column<int>(type: "integer", nullable: false),
                    TotalDuration = table.Column<TimeSpan>(type: "interval", nullable: false),
                    LessonAmount = table.Column<int>(type: "integer", nullable: false),
                    CourseId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Modules", x => x.ModuleId);
                    table.ForeignKey(
                        name: "FK_Modules_Courses_CourseId",
                        column: x => x.CourseId,
                        principalTable: "Courses",
                        principalColumn: "CourseId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Lessons",
                columns: table => new
                {
                    LessonId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Title = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    Description = table.Column<string>(type: "text", nullable: false),
                    Duration = table.Column<TimeSpan>(type: "interval", nullable: false),
                    ModuleId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lessons", x => x.LessonId);
                    table.ForeignKey(
                        name: "FK_Lessons_Modules_ModuleId",
                        column: x => x.ModuleId,
                        principalTable: "Modules",
                        principalColumn: "ModuleId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Questions",
                columns: table => new
                {
                    QuestionId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Questioning = table.Column<string>(type: "text", nullable: false),
                    CreationDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    LastUpdate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Category = table.Column<string>(type: "text", nullable: false),
                    LessonId = table.Column<int>(type: "integer", nullable: false),
                    StudentId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Questions", x => x.QuestionId);
                    table.ForeignKey(
                        name: "FK_Questions_Lessons_LessonId",
                        column: x => x.LessonId,
                        principalTable: "Lessons",
                        principalColumn: "LessonId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Questions_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "StudentId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Answers",
                columns: table => new
                {
                    AnswerId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Body = table.Column<string>(type: "text", nullable: false),
                    QuestionId = table.Column<int>(type: "integer", nullable: false),
                    AuthorId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Answers", x => x.AnswerId);
                    table.ForeignKey(
                        name: "FK_Answers_Authors_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Authors",
                        principalColumn: "AuthorId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Answers_Questions_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "Questions",
                        principalColumn: "QuestionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Authors",
                columns: new[] { "AuthorId", "FirstName", "LastName" },
                values: new object[,]
                {
                    { 1, "Grace", "Hopper" },
                    { 2, "John", "Backus" },
                    { 3, "Bill", "Gates" },
                    { 4, "Jim", " Berners-Lee" },
                    { 5, "Linus", "Torvalds" }
                });

            migrationBuilder.InsertData(
                table: "Publishers",
                columns: new[] { "PublisherId", "Name" },
                values: new object[,]
                {
                    { 1, "LegalPublisher1" },
                    { 2, "LegalPublisher2" },
                    { 3, "NaturalPublisher1" },
                    { 4, "NaturalPublisher2" }
                });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "StudentId", "Age", "CPF", "Name", "RegistrationDate" },
                values: new object[,]
                {
                    { 1, 21, "12345678980", "Steven Spielberg Production Company", new DateTime(2023, 11, 24, 18, 18, 9, 516, DateTimeKind.Utc).AddTicks(3514) },
                    { 2, 30, "09876543212", "Pedro Certezas", new DateTime(2023, 11, 24, 18, 18, 9, 516, DateTimeKind.Utc).AddTicks(3522) },
                    { 3, 21, "02036032907", "Casimiro Miguel", new DateTime(2023, 11, 24, 18, 18, 9, 516, DateTimeKind.Utc).AddTicks(3524) }
                });

            migrationBuilder.InsertData(
                table: "Courses",
                columns: new[] { "CourseId", "Category", "Description", "ModuleAmount", "Price", "PublisherId", "Title", "TotalDuration" },
                values: new object[,]
                {
                    { 1, "Backend", "In this course, you'll learn how to build an API with ASP.NET Core that connects to a database via Entity Framework Core from scratch.", 1, 97.00m, 1, "ASP.NET Core Web Api", new TimeSpan(0, 4, 0, 0, 0) },
                    { 2, "Backend", "In this course, Entity Framework Core 6 Fundamentals, you’ll learn to work with data in your .NET applications.", 1, 197.00m, 1, "Entity Framework Fundamentals", new TimeSpan(0, 1, 35, 0, 0) },
                    { 3, "Operating Systems", "You've heard that Linux is the future of enterprise computing and you're looking for a way in.", 1, 47.00m, 2, "Getting Started with Linux", new TimeSpan(0, 0, 10, 0, 0) }
                });

            migrationBuilder.InsertData(
                table: "Courses",
                columns: new[] { "CourseId", "Category", "Description", "Price", "PublisherId", "Title", "TotalDuration" },
                values: new object[,]
                {
                    { 4, "Operating Systems", "You've heard that WINDOWNS is the future of enterprise computing and you're looking for a way in.", 47.00m, 2, "Getting Started with WINDOWNS", new TimeSpan(0, 0, 0, 0, 0) },
                    { 5, "Operating Systems", "You've heard that MACOS is the future of enterprise computing and you're looking for a way in.", 47.00m, 2, "Getting Started with MACOS", new TimeSpan(0, 0, 0, 0, 0) },
                    { 6, "Frontend", "Angular course description", 47.00m, 2, "Front Angular", new TimeSpan(0, 0, 0, 0, 0) },
                    { 7, "Frontend", "Typescript course description", 47.00m, 2, "Typescript", new TimeSpan(0, 0, 0, 0, 0) }
                });

            migrationBuilder.InsertData(
                table: "LegalPublisher",
                columns: new[] { "PublisherId", "CNPJ" },
                values: new object[,]
                {
                    { 1, "64167199000120" },
                    { 2, "94333690000144" }
                });

            migrationBuilder.InsertData(
                table: "NaturalPublisher",
                columns: new[] { "PublisherId", "CPF" },
                values: new object[,]
                {
                    { 3, "07382817946" },
                    { 4, "12345678912" }
                });

            migrationBuilder.InsertData(
                table: "AuthorsCourses",
                columns: new[] { "AuthorId", "CourseId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 1, 3 },
                    { 2, 1 },
                    { 2, 2 },
                    { 4, 1 },
                    { 5, 3 }
                });

            migrationBuilder.InsertData(
                table: "CoursesStudents",
                columns: new[] { "CourseId", "StudentId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 1, 2 },
                    { 1, 3 },
                    { 2, 2 },
                    { 3, 1 },
                    { 3, 3 }
                });

            migrationBuilder.InsertData(
                table: "Modules",
                columns: new[] { "ModuleId", "CourseId", "Description", "LessonAmount", "Number", "Title", "TotalDuration" },
                values: new object[,]
                {
                    { 1, 1, "In this module we are gonna learn every detail about Patch", 1, 2, "Everything about Patch", new TimeSpan(0, 4, 0, 0, 0) },
                    { 2, 2, "In this module we are gonna learn how to validate inputs in our database", 2, 4, "Validating", new TimeSpan(0, 1, 35, 0, 0) },
                    { 3, 3, "This moudule has the necessary information for you to install your Linux", 1, 1, "Installing", new TimeSpan(0, 0, 10, 0, 0) }
                });

            migrationBuilder.InsertData(
                table: "Lessons",
                columns: new[] { "LessonId", "Description", "Duration", "ModuleId", "Title" },
                values: new object[,]
                {
                    { 1, "In this lesson you will be learning about how json works inside Patch", new TimeSpan(0, 4, 0, 0, 0), 1, "How json works in Patch" },
                    { 2, "In this lesson you will be learning about how Data Notation works", new TimeSpan(0, 0, 15, 0, 0), 2, "Data Notation" },
                    { 3, "In this lesson you will be learning about how Fluent Validation works", new TimeSpan(0, 1, 20, 0, 0), 2, "Fluent Validation" },
                    { 4, "In this lesson you will be learning about how Grub works", new TimeSpan(0, 1, 20, 0, 0), 3, "Grub" }
                });

            migrationBuilder.InsertData(
                table: "Questions",
                columns: new[] { "QuestionId", "Category", "CreationDate", "LastUpdate", "LessonId", "Questioning", "StudentId" },
                values: new object[,]
                {
                    { 1, "status-code", new DateTime(2023, 11, 24, 18, 18, 9, 516, DateTimeKind.Utc).AddTicks(3564), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, "What status code should I return in the get method?", 1 },
                    { 2, "controller", new DateTime(2023, 11, 24, 18, 18, 9, 516, DateTimeKind.Utc).AddTicks(3568), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, "What is the function of a controller?", 1 },
                    { 3, "controller", new DateTime(2023, 11, 24, 18, 18, 9, 516, DateTimeKind.Utc).AddTicks(3570), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, "Should I make a controller for each entity?", 2 },
                    { 4, "FluentValidation", new DateTime(2023, 11, 24, 18, 18, 9, 516, DateTimeKind.Utc).AddTicks(3572), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, "How to set the precision of a decimal in FluentValidation?", 2 },
                    { 5, "FluentValidation", new DateTime(2023, 11, 24, 18, 18, 9, 516, DateTimeKind.Utc).AddTicks(3574), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, "How to validate a text field to ensure it is not empty?", 3 },
                    { 6, "FluentValidation", new DateTime(2023, 11, 24, 18, 18, 9, 516, DateTimeKind.Utc).AddTicks(3576), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, "Como validar um campo de data para garantir que esteja em um intervalo específico?", 3 }
                });

            migrationBuilder.InsertData(
                table: "Answers",
                columns: new[] { "AnswerId", "AuthorId", "Body", "QuestionId" },
                values: new object[,]
                {
                    { 1, 1, "Resposta do universo", 1 },
                    { 2, 2, "Resposta da vida", 1 },
                    { 3, 2, "Resposta de tudo", 2 },
                    { 4, 1, "Resposta de nada", 3 },
                    { 5, 1, "Nem resposta isso é", 3 },
                    { 6, 1, "Sim, mas na verdade não", 3 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Answers_AuthorId",
                table: "Answers",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Answers_QuestionId",
                table: "Answers",
                column: "QuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorsCourses_CourseId",
                table: "AuthorsCourses",
                column: "CourseId");

            migrationBuilder.CreateIndex(
                name: "IX_Courses_PublisherId",
                table: "Courses",
                column: "PublisherId");

            migrationBuilder.CreateIndex(
                name: "IX_CoursesStudents_StudentId",
                table: "CoursesStudents",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Lessons_ModuleId",
                table: "Lessons",
                column: "ModuleId");

            migrationBuilder.CreateIndex(
                name: "IX_Modules_CourseId",
                table: "Modules",
                column: "CourseId");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_LessonId",
                table: "Questions",
                column: "LessonId");

            migrationBuilder.CreateIndex(
                name: "IX_Questions_StudentId",
                table: "Questions",
                column: "StudentId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Answers");

            migrationBuilder.DropTable(
                name: "AuthorsCourses");

            migrationBuilder.DropTable(
                name: "CoursesStudents");

            migrationBuilder.DropTable(
                name: "LegalPublisher");

            migrationBuilder.DropTable(
                name: "NaturalPublisher");

            migrationBuilder.DropTable(
                name: "Questions");

            migrationBuilder.DropTable(
                name: "Authors");

            migrationBuilder.DropTable(
                name: "Lessons");

            migrationBuilder.DropTable(
                name: "Students");

            migrationBuilder.DropTable(
                name: "Modules");

            migrationBuilder.DropTable(
                name: "Courses");

            migrationBuilder.DropTable(
                name: "Publishers");
        }
    }
}
