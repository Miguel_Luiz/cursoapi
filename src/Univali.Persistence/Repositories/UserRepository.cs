using Microsoft.EntityFrameworkCore;
using Univali.Persistence.DbContexts;
using Univali.Application.Contracts.Repositories;
using Univali.Domain.Entities;

namespace Univali.Persistence.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly UserContext _userContext;

        public UserRepository(UserContext userContext)
        {
            _userContext = userContext;
        }

        public async Task<User> GetUserByIdAsync(int userId)
        {
            return await _userContext.Users.FirstOrDefaultAsync(u => u.Id == userId)
                ?? throw new InvalidOperationException($"User with ID {userId} not found.");
        }

        public async Task<User> GetUserByUserNameAsync(string userName)
        {
            return await _userContext.Users.FirstOrDefaultAsync(u => u.UserName == userName)
                ?? throw new InvalidOperationException($"User with username '{userName}' not found.");
        }

        public async Task<IEnumerable<User>> GetAllUsersAsync()
        {
            return await _userContext.Users.ToListAsync();
        }

        public async Task AddUserAsync(User user)
        {
            _userContext.Users.Add(user);
            await _userContext.SaveChangesAsync();
        }

        public async Task UpdateUserAsync(User user)
        {
            _userContext.Users.Update(user);
            await _userContext.SaveChangesAsync();
        }

        public async Task DeleteUserAsync(User user)
        {
            _userContext.Users.Remove(user);
            await _userContext.SaveChangesAsync();
        }
    }
}
