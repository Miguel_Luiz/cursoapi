using System.Collections.ObjectModel;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Univali.Persistence.DbContexts;
using Univali.Domain.Entities;
using Univali.Application.Features.Authors.Commands.UpdateAuthor;
using Univali.Application.Features.Publishers.Commands.UpdatePublisher;
using Univali.Application.Features.Courses.Commands;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;
using Univali.Application.Features.Lessons.Commands.UpdateLesson;

namespace Univali.Persistence.Repositories;

public class PublisherRepository : IPublisherRepository
{
    private readonly PublisherContext _context;
    private readonly IMapper _mapper;

    public PublisherRepository(PublisherContext publisherContext, IMapper mapper)
    {
        _context = publisherContext;
        _mapper = mapper;
    }



    public async Task<IEnumerable<Publisher>?> GetAllPublishersWithCoursesAsync()
    {
        return await _context.Publishers
            .Include(p => p.Courses)
            .ToListAsync();
    }

    public async Task<(IEnumerable<NaturalPublisher>, PaginationMetadata)> GetNaturalPublisherCollectionAsync(
        string? name, string? cpf, string? searchQuery, int pageNumber, int pageSize
    ) {   
        var collection = _context.Publishers.OfType<NaturalPublisher>();

        if (!string.IsNullOrWhiteSpace(name))
        {
            name = name.Trim();

            collection = collection.Where(c => c.Name == name);
        }

        if (!string.IsNullOrWhiteSpace(cpf))
        {
            cpf = cpf.Trim();

            collection = collection.Where(c => c.CPF == cpf);
        }

        if (!string.IsNullOrWhiteSpace(searchQuery))
        {
            searchQuery = searchQuery.Trim();

            collection = collection.Where(
                c => c.Name.Contains(searchQuery) 
                || c.CPF.Contains(searchQuery)
            );
        }

        var totalItemCount = await collection.CountAsync();

        var paginationMetadata = new PaginationMetadata(
            totalItemCount, pageSize, pageNumber
        );

        var publishersToReturn = await collection
            .OrderBy(c => c.PublisherId)
            .Skip(pageSize * (pageNumber - 1))
            .Take(pageSize)
            .ToListAsync();

        return (publishersToReturn, paginationMetadata);
    }

    public async Task<(IEnumerable<LegalPublisher>, PaginationMetadata)> GetLegalPublisherCollectionAsync(
        string? name, string? cnpj, string? searchQuery, int pageNumber, int pageSize
    ) {   
        var collection = _context.Publishers.OfType<LegalPublisher>();

        if (!string.IsNullOrWhiteSpace(name))
        {
            name = name.Trim();

            collection = collection.Where(c => c.Name == name);
        }

        if (!string.IsNullOrWhiteSpace(cnpj))
        {
            cnpj = cnpj.Trim();

            collection = collection.Where(c => c.CNPJ == cnpj);
        }

        if (!string.IsNullOrWhiteSpace(searchQuery))
        {
            searchQuery = searchQuery.Trim();

            collection = collection.Where(
                c => c.Name.Contains(searchQuery) 
                || c.CNPJ.Contains(searchQuery)
            );
        }

        var totalItemCount = await collection.CountAsync();

        var paginationMetadata = new PaginationMetadata(
            totalItemCount, pageSize, pageNumber
        );

        var publishersToReturn = await collection
            .OrderBy(c => c.PublisherId)
            .Skip(pageSize * (pageNumber - 1))
            .Take(pageSize)
            .ToListAsync();

        return (publishersToReturn, paginationMetadata);
    }

    public async Task<Publisher?> GetPublisherByIdAsync(int publisherId)
    {
        return await _context.Publishers
            .FirstOrDefaultAsync(p => p.PublisherId == publisherId);
    }

    public async Task<Publisher?> GetPublisherWithCoursesByIdAsync(int publisherId)
    {
        return await _context.Publishers
            .Include(c => c.Courses)
            .FirstOrDefaultAsync(p => p.PublisherId == publisherId);
    }

    public void AddPublisher(Publisher publisher)
    {
        _context.Publishers.Add(publisher);
    }

    public void UpdatePublisher(Publisher publisher, UpdatePublisherCommand newPublisher)
    {
        _mapper.Map(newPublisher, publisher);
    }

    public void DeletePublisher(Publisher publisher)
    {
        _context.Publishers.Remove(publisher);
    }



    public async Task<IEnumerable<Author>?> GetAllAuthorsWithCoursesAsync()
    {
        return await _context.Authors
            .Include(c => c.Courses)
            .ToListAsync();
    }

    public async Task<(IEnumerable<Author>, PaginationMetadata)> GetAuthorsCollectionAsync(
        string? searchQuery, int pageNumber, int pageSize
    ) {   
        var collection = _context.Authors as IQueryable<Author>;

        // Pesquisa - contem
        if (!string.IsNullOrWhiteSpace(searchQuery))
        {
            searchQuery = searchQuery.Trim();

            collection = collection.Where(
                c => c.FirstName.Contains(searchQuery) 
                || c.LastName.Contains(searchQuery)
            );
        }

        var totalItemCount = await collection.CountAsync();

        var PaginationMetadata = new PaginationMetadata(
            totalItemCount, pageSize, pageNumber
        );

        var auhtorsToReturn = await collection
            .OrderBy(c => c.AuthorId)
            .Skip(pageSize * (pageNumber - 1))
            .Take(pageSize)
            .ToListAsync();

        return (auhtorsToReturn, PaginationMetadata);
    }
    
    public async Task<Author?> GetAuthorByIdAsync(int authorId)
    {
        return await _context.Authors.FirstOrDefaultAsync(a => a.AuthorId == authorId);
    }

    public async Task<Author?> GetAuthorWithCoursesByIdAsync(int authorId)
    {
        return await _context.Authors
            .Include(a => a.Courses)
            .FirstOrDefaultAsync(a => a.AuthorId == authorId);
    }

    public async Task<Author?> GetAuthorWithAnswersByIdAsync(int authorId)
    {
        return await _context.Authors.Include(q => q.Answers).FirstOrDefaultAsync(author => author.AuthorId == authorId);
    }

    public void AddAuthor(Author author)
    {
        _context.Authors.Add(author);
    }

    public void UpdateAuthor(Author author, UpdateAuthorCommand newAuthor)
    {
        _mapper.Map(newAuthor, author);
    }

    public void DeleteAuthor(Author author)
    {
        _context.Authors.Remove(author);
    }

    public async Task<bool> AuthorExistsAsync(int authorId)
    {
        return await _context.Authors
            .AnyAsync(a => a.AuthorId == authorId);
    }



    public async Task<IEnumerable<Course>> GetCoursesAsync()
    {
        return await _context.Courses.ToListAsync();
    }

    public async Task<IEnumerable<Course>?> GetAllCoursesWithAuthorsAsync(int publisherId)
    {
        return await _context.Courses
            .Include(c => c.Authors)
            .Include(c => c.Publisher)
            .Where(p => p.PublisherId == publisherId)
            .ToListAsync();
    }

    public async Task<(IEnumerable<Course>, PaginationMetadata)> GetCoursesCollectionAsync(
        string? category, string? searchQuery, int pageNumber, int pageSize
    ) {   
        // pablo1107 - include authors
        var collection = _context.Courses.Include(c => c.Authors) as IQueryable<Course>;

        // Filtro - exado
        if (!string.IsNullOrWhiteSpace(category))
        {
            category = category.Trim();

            collection = collection.Where(c => c.Category == category);
        }

        // Pesquisa - contem
        if (!string.IsNullOrWhiteSpace(searchQuery))
        {
            searchQuery = searchQuery.Trim();

            collection = collection.Where(
                c => c.Category.Contains(searchQuery) 
                || c.Title.Contains(searchQuery)
                || c.Description.Contains(searchQuery)
            );
        }

        var totalItemCount = await collection.CountAsync();

        var PaginationMetadata = new PaginationMetadata(
            totalItemCount, pageSize, pageNumber
        );

        var coursesToReturn = await collection
            .OrderBy(c => c.CourseId)
            .Skip(pageSize * (pageNumber - 1))
            .Take(pageSize)
            .ToListAsync();

        return (coursesToReturn, PaginationMetadata);
    }

    public async Task<Course?> GetCourseByIdAsync(int courseId)
    {
        return await _context.Courses.FirstOrDefaultAsync(c => c.CourseId == courseId);
    }

    public async Task<Course?> GetCourseByIdAsync(int publisherId, int courseId)
    {
        return await _context.Courses.FirstOrDefaultAsync(c => c.CourseId == courseId && c.PublisherId == publisherId);
    }

    public async Task<Course?> GetCourseWithAuthorsByIdAsync(int publisherId, int courseId)
    {
        return await _context.Courses
            .Include(c => c.Authors)
            .FirstOrDefaultAsync(c => c.CourseId == courseId && c.PublisherId == publisherId);
    }

    public void AddCourse(Course course)
    {
        _context.Courses.Add(course);
    }

    public void AddCourse(Publisher publisher, Course course)
    {
        publisher.Courses.Add(course);
    }

    public void UpdateCourse(Course course, UpdateCourseCommand newCourse)
    {
        _mapper.Map(newCourse, course);
    }

    public void DeleteCourse(Course Course)
    {
        _context.Courses.Remove(Course);
    }

    public async Task<bool> AuthorsExistAsync(IEnumerable<int> authors)
    {
        foreach (var authorId in authors)
        {
            if (!await _context.Authors.AnyAsync(a => a.AuthorId == authorId))
                return false;
        }

        return true;
    }

    public async Task<List<Author>> GetAuthorsAsync(IEnumerable<int> authors)
    {
        var authorsFromDatabase = await _context.Authors
            .Where(a => authors.Contains(a.AuthorId))
            .ToListAsync();

        return authorsFromDatabase;
    }



    public async Task<IEnumerable<Module>> GetModulesAsync()
    {
        return await _context.Modules.ToListAsync();
    }

    public async Task<IEnumerable<Module>?> GetModulesAsync(int courseId)
    {
        return await _context.Modules.Where(m => m.CourseId == courseId).ToListAsync();
    }

    public async Task<(IEnumerable<Module>, PaginationMetadata)> GetModulesAsync(
            string? title, 
            string? searchQuery, 
            int pageNumber, int pageSize
        )
    {        
        var collection = _context.Modules as IQueryable<Module>;

        if(!string.IsNullOrWhiteSpace(title))
        {
            title = title.Trim();
            collection = collection.Where(c => c.Title == title);

        }

        if(!string.IsNullOrWhiteSpace(searchQuery)){
            searchQuery = searchQuery.Trim();
            collection = collection.Where(m => m.Title.Contains(searchQuery) 
                || m.Description.Contains(searchQuery)
            );
        }

        var totalItemCount = await collection.CountAsync();

        var PaginationMetadata = new PaginationMetadata(
            totalItemCount, pageSize, pageNumber
        );

        var modulesToReturn = await collection
            .OrderBy(l => l.ModuleId)
            .Skip(pageSize * (pageNumber - 1))
            .Take(pageSize)
            .ToListAsync();

        return (modulesToReturn, PaginationMetadata);

    }

    public async Task<Module?> GetModuleByIdAsync(int moduleId)
    {
        return await _context.Modules.FirstOrDefaultAsync(m => m.ModuleId == moduleId);
    }

    public async Task<Module?> GetModuleFromCourseByIdAsync(int courseId, int moduleId)
    {
        return await _context.Modules.FirstOrDefaultAsync(m => m.ModuleId == moduleId && m.CourseId == courseId);
    }
    
    public async Task<Module?> GetModuleFromCourseWithLessonsByIdAsync(int moduleId, int courseId)
    {
        return await _context.Modules.Include(m => m.Lessons).Where(m => courseId == m.CourseId).FirstOrDefaultAsync(m => m.ModuleId == moduleId);
    }

    public void AddModule(Module module)
    {
        _context.Modules.Add(module);
    }

    public void DeleteModule(Module module)
    {
        _context.Modules.Remove(module);
    }

    public async Task<bool> ModuleExistsAsync(int moduleId)
    {
        return await _context.Modules.AnyAsync(m => m.ModuleId == moduleId);
    }



    public async Task<IEnumerable <Lesson>> GetLessonsAsync()
    {
        return await _context.Lessons
            .OrderBy(l => l.LessonId)
            .ToListAsync();
    }

    public async Task<IEnumerable<Lesson>?> GetLessonsAsync(int moduleId)
    {
        return await _context.Lessons.OrderBy(l => l.LessonId).Where(l => l.ModuleId == moduleId).ToListAsync();
    }

    public async Task<(IEnumerable<Lesson>, PaginationMetadata)> GetLessonsCollectionAsync(
        string? title, string? searchQuery, int pageNumber, int pageSize
    )
    {        
        var collection = _context.Lessons as IQueryable<Lesson>;

        if(!string.IsNullOrWhiteSpace(title))
        {
            title = title.Trim();
            collection = collection.Where(c => c.Title == title);

        }

        if(!string.IsNullOrWhiteSpace(searchQuery)){
            searchQuery = searchQuery.Trim();
            collection = collection.Where(l => l.Title.Contains(searchQuery) 
                || l.Description.Contains(searchQuery)
            );
        }

        var totalItemCount = await collection.CountAsync();

        var PaginationMetadata = new PaginationMetadata(
            totalItemCount, pageSize, pageNumber
        );

        var lessonsFromCollection = await collection
            .OrderBy(l => l.LessonId)
            .Skip(pageSize * (pageNumber - 1))
            .Take(pageSize)
            .ToListAsync();

        return (lessonsFromCollection, PaginationMetadata);

    }
    
    public async Task<Lesson?> GetLessonByIdAsync(int lessonId)
    {
        return await _context.Lessons.FirstOrDefaultAsync(l => l.LessonId == lessonId);
    }

    public async Task<Lesson?> GetLessonByIdAsync(int moduleId, int lessonId)
    {
        return await _context.Lessons.FirstOrDefaultAsync(l => l.LessonId == lessonId && l.ModuleId == moduleId);
    }

    public void AddLesson(Lesson lesson)
    {
        _context.Lessons.Add(lesson);
    }

    public void UpdateLesson(Lesson lesson, UpdateLessonCommand newLesson)
    {
        _mapper.Map(newLesson, lesson);
    }

    public void DeleteLesson(Lesson lesson)
    {
        _context.Lessons.Remove(lesson);
    }

    public async Task<(IEnumerable<Student>,PaginationMetadata)> GetStudentsAsync(int pageNumber, int pageSize)
    {
        var collection = _context.Students
         as IQueryable<Student>;

        var totalItemCount = await collection.CountAsync();

        var paginationMetadata = new PaginationMetadata (totalItemCount, pageSize, pageNumber);

        var studentToReturn = await collection
            .OrderBy(c => c.StudentId)
            .Skip(pageSize * (pageNumber - 1))
            .Take(pageSize)
            .ToListAsync();

        return (studentToReturn, paginationMetadata);  
    }

    public async Task<(IEnumerable<Student>,PaginationMetadata)> GetStudentsCollectionAsync(string? searchQuery, int pageNumber, int pageSize)
    {
        var collection = _context.Students
         as IQueryable<Student>;

        if(!string.IsNullOrWhiteSpace(searchQuery))
        {
            searchQuery = searchQuery.Trim();
            collection = collection.Where(c => c.Name.Contains(searchQuery));
        }

        var totalItemCount = await collection.CountAsync();

        var paginationMetadata = new PaginationMetadata (totalItemCount, pageSize, pageNumber);

        var studentToReturn = await collection
            .OrderBy(c => c.StudentId)
            .Skip(pageSize * (pageNumber - 1))
            .Take(pageSize)
            .ToListAsync();

        return (studentToReturn, paginationMetadata);  
    }

    public async Task<Student?> GetStudentByIdAsync(int studentId)
    {
        return await _context.Students.FirstOrDefaultAsync(s => s.StudentId == studentId);
    }

    public async Task<Student?> GetStudentWithQuestionsByIdAsync(int studentId)
    {
        return await _context.Students.Include(s => s.Questions).FirstOrDefaultAsync(s => s.StudentId == studentId);
    }

    public async Task<Student?> GetStudentWithCoursesByIdAsync(int studentId)
    {
        return await _context.Students.Include(s => s.Courses).FirstOrDefaultAsync(s => s.StudentId == studentId);
    }

    public void AddStudent(Student student)
    {
        _context.Students.Add(student);
    }

    public void DeleteStudent(Student student)
    {
        _context.Students.Remove(student);
    }

    public async Task<bool> StudentExistsAsync(int studentId)
    {
        return await _context.Students
            .AnyAsync(s => s.StudentId == studentId);
    }



    public async Task<Lesson?> GetLessonWithQuestionsByIdAsync(int lessonId)
    {
        return await _context.Lessons.Include(l => l.Questions).FirstOrDefaultAsync(l => l.LessonId == lessonId);
    }

    public async Task<bool> LessonExistsAsync(int lessonId)
    {
        return await _context.Lessons
            .AnyAsync(c => c.LessonId == lessonId);
    }



    public async Task<(IEnumerable<Question>,PaginationMetadata)> GetQuestionsAsync(string? category, string? searchQuery, int pageNumber, int pageSize)
    {
        var collection = _context.Questions
         as IQueryable<Question>;

        if(!string.IsNullOrWhiteSpace(category))
        {
            category = category.Trim();
            collection = collection.Where(c => c.Category == category);
        }

        if(!string.IsNullOrWhiteSpace(searchQuery))
        {
            searchQuery = searchQuery.Trim();
            collection = collection.Where(c => c.Questioning.Contains(searchQuery));
        }

        var totalItemCount = await collection.CountAsync();

        var paginationMetadata = new PaginationMetadata (totalItemCount, pageSize, pageNumber);

        var questionsToReturn = await collection
            .OrderBy(c => c.QuestionId)
            .Skip(pageSize * (pageNumber - 1))
            .Take(pageSize)
            .ToListAsync();

        return (questionsToReturn, paginationMetadata);  
    }

    public async Task<(IEnumerable<Question>,PaginationMetadata)> GetQuestionsFromLessonAsync(int lessonId, string? category, string? searchQuery, int pageNumber, int pageSize)
    {
        var collection = _context.Questions
         as IQueryable<Question>;

        if(!string.IsNullOrWhiteSpace(category))
        {
            category = category.Trim();
            collection = collection.Where(c => c.Category == category && c.LessonId == lessonId);
        }

        if(string.IsNullOrWhiteSpace(searchQuery)) collection = collection.Where(q => q.LessonId == lessonId);

        if(!string.IsNullOrWhiteSpace(searchQuery))
        {
            searchQuery = searchQuery.Trim();
            collection = collection.Where(c => c.Questioning.Contains(searchQuery) && c.LessonId == lessonId);
        }

        var totalItemCount = await collection.CountAsync();

        var paginationMetadata = new PaginationMetadata (totalItemCount, pageSize, pageNumber);

        var questionsToReturn = await collection
            .OrderBy(c => c.QuestionId)
            .Skip(pageSize * (pageNumber - 1))
            .Take(pageSize)
            .ToListAsync();

        return (questionsToReturn, paginationMetadata);  
    }

    public async Task<(IEnumerable<Question>,PaginationMetadata)> GetQuestionsFromStudentInLessonAsync(int studentId, int lessonId, string? category, string? searchQuery, int pageNumber, int pageSize)
    {
        var collection = _context.Questions
         as IQueryable<Question>;

        if(!string.IsNullOrWhiteSpace(category))
        {
            category = category.Trim();
            collection = collection.Where(c => c.Category == category && c.StudentId == studentId && c.LessonId == lessonId);
        }

        if(string.IsNullOrWhiteSpace(searchQuery)) collection = collection.Where(q => q.StudentId == studentId && q.LessonId == lessonId);

        if(!string.IsNullOrWhiteSpace(searchQuery))
        {
            searchQuery = searchQuery.Trim();
            collection = collection.Where(c => c.Questioning.Contains(searchQuery) && c.StudentId == studentId && c.LessonId == lessonId);
        }

        var totalItemCount = await collection.CountAsync();

        var paginationMetadata = new PaginationMetadata (totalItemCount, pageSize, pageNumber);

        var questionsToReturn = await collection
            .OrderBy(c => c.QuestionId)
            .Skip(pageSize * (pageNumber - 1))
            .Take(pageSize)
            .ToListAsync();

        return (questionsToReturn, paginationMetadata);  
    }

    public async Task<(IEnumerable<Question>,PaginationMetadata)> GetQuestionsByLessonIdAsync(int lessonId, int pageNumber, int pageSize)
    {
        var collection = _context.Questions.Where(q => q.LessonId == lessonId)
         as IQueryable<Question>;

        var totalItemCount = await collection.CountAsync();

        var paginationMetadata = new PaginationMetadata (totalItemCount, pageSize, pageNumber);

        var questionsToReturn = await collection
            .OrderBy(c => c.QuestionId)
            .Skip(pageSize * (pageNumber - 1))
            .Take(pageSize)
            .ToListAsync();

        return (questionsToReturn, paginationMetadata);
    }

    public async Task<(IEnumerable<Question>,PaginationMetadata)> GetLessonQuestionsAsync(int lessonId, string? category, string? searchQuery, int pageNumber, int pageSize)
    {
        var collection = _context.Questions
         as IQueryable<Question>;

        if(!string.IsNullOrWhiteSpace(category))
        {
            category = category.Trim();
            collection = collection.Where(c => c.Category == category && c.LessonId == lessonId);
        }

        if(string.IsNullOrWhiteSpace(searchQuery)) collection = collection.Where(q => q.LessonId == lessonId);

        if(!string.IsNullOrWhiteSpace(searchQuery))
        {
            searchQuery = searchQuery.Trim();
            collection = collection.Where(c => c.Questioning.Contains(searchQuery) && c.LessonId == lessonId);
        }

        var totalItemCount = await collection.CountAsync();

        var paginationMetadata = new PaginationMetadata (totalItemCount, pageSize, pageNumber);

        var questionsToReturn = await collection
            .OrderBy(c => c.QuestionId)
            .Skip(pageSize * (pageNumber - 1))
            .Take(pageSize)
            .ToListAsync();

        return (questionsToReturn, paginationMetadata);  
    }

    public async Task<Question?> GetQuestionByIdAsync(int questionId)
    {
        return await _context.Questions.FirstOrDefaultAsync(question => question.QuestionId == questionId);
    }

    public async Task<Question?> GetQuestionWithAnswersByIdAsync(int questionId)
    {
        return await _context.Questions.Include(q => q.Answers).FirstOrDefaultAsync(question => question.QuestionId == questionId);
    }

    public void AddQuestion(Question question)
    {
        _context.Questions.Add(question);
    }

    public void DeleteQuestion(Question question)
    {
        _context.Questions.Remove(question);
    }

    public async Task<bool> QuestionExistsAsync(int questionId)
    {
        return await _context.Questions
            .AnyAsync(q => q.QuestionId == questionId);
    }



    public async Task<(IEnumerable<Answer>,PaginationMetadata)> GetAnswersAsync(string? searchQuery, int pageNumber, int pageSize)
    {
        var collection = _context.Answers
         as IQueryable<Answer>;

        if(!string.IsNullOrWhiteSpace(searchQuery))
        {
            searchQuery = searchQuery.Trim();
            collection = collection.Where(c => c.Body.Contains(searchQuery));
        }

        var totalItemCount = await collection.CountAsync();

        var paginationMetadata = new PaginationMetadata (totalItemCount, pageSize, pageNumber);

        var answersToReturn = await collection
            .OrderBy(c => c.AnswerId)
            .Skip(pageSize * (pageNumber - 1))
            .Take(pageSize)
            .ToListAsync();

        return (answersToReturn, paginationMetadata);  
    }

    public async Task<(IEnumerable<Answer>,PaginationMetadata)> GetAnswersByAuthorIdAsync(int authorId, int pageNumber, int pageSize)
    {
        var collection = _context.Answers.Where(a => a.AuthorId == authorId)
         as IQueryable<Answer>;

        var totalItemCount = await collection.CountAsync();

        var paginationMetadata = new PaginationMetadata (totalItemCount, pageSize, pageNumber);

        var answersToReturn = await collection
            .OrderBy(c => c.AnswerId)
            .Skip(pageSize * (pageNumber - 1))
            .Take(pageSize)
            .ToListAsync();

        return (answersToReturn, paginationMetadata);
    }

    public async Task<(IEnumerable<Answer>,PaginationMetadata)> GetAnswersByQuestionIdAsync(int questionId, int pageNumber, int pageSize)
    {
        var collection = _context.Answers.Where(a => a.QuestionId == questionId)
         as IQueryable<Answer>;

        var totalItemCount = await collection.CountAsync();

        var paginationMetadata = new PaginationMetadata (totalItemCount, pageSize, pageNumber);

        var answersToReturn = await collection
            .OrderBy(c => c.AnswerId)
            .Skip(pageSize * (pageNumber - 1))
            .Take(pageSize)
            .ToListAsync();

        return (answersToReturn, paginationMetadata);
    }

    public void AddAnswer(Answer answer)
    {
        _context.Answers.Add(answer);
    }

    public void DeleteAnswer(Answer answer)
    {
        _context.Answers.Remove(answer);
    }

    public async Task<bool> AnswerExistsAsync(int answerId)
    {
        return await _context.Answers
            .AnyAsync(a => a.AnswerId == answerId);
    }



    public async Task<bool> SaveChangesAsync()
    {
        return await _context.SaveChangesAsync() > 0;
    }

}