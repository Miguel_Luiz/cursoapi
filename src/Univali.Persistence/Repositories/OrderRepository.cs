using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Univali.Persistence.DbContexts;
using Univali.Domain.Entities;
using Univali.Application.Features.Orders.Commands.UpdateOrder;
using Univali.Application.Features.OrderItems.Commands.UpdateOrderItem;
using Univali.Application.Features.Payments.Commands.UpdatePayment;
using Univali.Application.Contracts.Repositories;
using Univali.Application.Features.Common;

namespace Univali.Persistence.Repositories;

public class OrderRepository : IOrderRepository
{
    private readonly OrderContext _context;
    private readonly IMapper _mapper;

    public OrderRepository(OrderContext orderContext, IMapper mapper)
    {
        _context = orderContext ?? throw new ArgumentNullException(nameof(orderContext));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    }



    public async Task<Customer?> GetCustomerByIdAsync(int customerId)
    {
        return await _context.Customers.FirstOrDefaultAsync(c => c.Id == customerId);
    }



    public async Task<bool> CoursesExistAsync(IEnumerable<int> courses)
    {
        foreach (var courseId in courses)
        {
            if (!await _context.Courses.AnyAsync(c => c.CourseId == courseId))
                return false;
        }

        return true;
    }

    public async Task<Course?> GetCourseWithoutTrackingByIdAsync(int courseId)
    {
        return await _context.Courses
            .AsNoTracking()
            .FirstOrDefaultAsync(c => c.CourseId == courseId);
    }

    public async Task<List<Course>> GetCoursesWithoutTrackingAsync(IEnumerable<int> courses)
    {
        var coursesFromDatabase = await _context.Courses
            .AsNoTracking()
            .Where(c => courses.Contains(c.CourseId))
            .ToListAsync();

        return coursesFromDatabase;
    }

    public async Task<Course?> GetCourseWithoutTrackingFromItemAsync(OrderItem item)
    {
        return await _context.Courses
            .AsNoTracking()
            .FirstOrDefaultAsync(c => 
                c.Title == item.Title &&
                c.Description == item.Description &&
                c.Price == item.Price
            );
    }




    public async Task<IEnumerable<Order>?> GetOrdersAsync(int customerId)
    {
        var ordersFromDatabase = await _context.Orders
            .Include(o => o.Items)
            .Include(o => o.Payment)
            .Where(o => o.CustomerId == customerId)
            .ToListAsync();

        return ordersFromDatabase;
    }
    public async Task<IEnumerable<Order>> GetOrdersWithFilterAsync(int customerId, string? itemTitle, string? searchQuery, int pageNumber, int pageSize)
    {
        var collection = _context.Orders
        .Include(o => o.Items)
        .Include(o => o.Payment)
        .Where(o => o.CustomerId == customerId) as IQueryable<Order>;

        if (!string.IsNullOrWhiteSpace(itemTitle))
        {
            itemTitle = itemTitle.Trim();

            foreach (var order in collection)
            {
                foreach (var item in order.Items)
                {
                    if (item.Title == itemTitle)
                    {
                        collection = collection.Where(o => o.OrderId == order.OrderId);
                    }
                }
            }
        }

        if (!string.IsNullOrWhiteSpace(searchQuery))
        {
            searchQuery = searchQuery.Trim();
            collection = collection.Where(
                o => o.Items.Any(item => item.Title.Contains(searchQuery) || item.Description.Contains(searchQuery))
            );
        }

        var totalItemCount = await collection.CountAsync();

        var ordersToReturn = await collection
        .OrderBy(o => o.OrderId)
        .Skip(pageSize * (pageNumber - 1))
        .Take(pageSize)
        .ToListAsync();

        return (ordersToReturn);
    }

    public async Task<IEnumerable<Order>?> GetOrdersWithReceiptAsync(int customerId)
    {
        var ordersFromDatabase = await _context.Orders
            .Include(o => o.Items)
            .Include(o => o.Payment)
            .Include(o => o.Receipt)
            .Where(o => o.CustomerId == customerId)
            .ToListAsync();

        return ordersFromDatabase;
    }

    public async Task<Order?> GetOrderByIdAsync(int customerId, int orderId)
    {
        return await _context.Orders
            .Include(o => o.Items)
            .Include(o => o.Payment)
            .FirstOrDefaultAsync(o => o.CustomerId == customerId && o.OrderId == orderId);
    }

    public async Task<Order?> GetOrderWithReceiptByIdAsync(int customerId, int orderId)
    {
        return await _context.Orders
            .Include(o => o.Items)
            .Include(o => o.Payment)
            .Include(o => o.Receipt)
            .FirstOrDefaultAsync(o => o.CustomerId == customerId && o.OrderId == orderId);

    }

    public async Task<Order?> GetOrderInFullByIdAsync(int customerId, int orderId)
    {
        return await _context.Orders
            .Include(o => o.Customer)
            .Include(o => o.Items)
            .Include(o => o.Payment)
            .Include(o => o.Receipt)
            .FirstOrDefaultAsync(o => o.CustomerId == customerId && o.OrderId == orderId);
    }

    public void AddOrder(Customer customer, Order order)
    {
        customer.Orders.Add(order);
    }

    public void UpdateOrder(Order order, UpdateOrderCommand newOrder)
    {
        _mapper.Map(newOrder, order);
    }

    public void DeleteOrder(Order order)
    {
        _context.Orders.Remove(order);
    }



    public async Task<IEnumerable<OrderItem>?> GetOrderItemsAsync(int orderId)
    {
        var orderFromDatabase = await _context.Orders
            .Include(o => o.Items)
            .FirstOrDefaultAsync(o => o.OrderId == orderId);

        return orderFromDatabase?.Items;
    }
    
    public async Task<(IEnumerable<OrderItem>?, PaginationMetadata)> GetOrderItemsWithFilterAsync(int orderId, int pageNumber, int pageSize)
    {
        var orderFromDatabase = await _context.Orders
            .Include(o => o.Items)
            .FirstOrDefaultAsync(o => o.OrderId == orderId);


        var totalItemCount = orderFromDatabase!.Items.Count; // só é chamado quando já se sabe que customer existe (verificar GetOrderItemsHandler)

        var paginationMetadata = new PaginationMetadata(totalItemCount, pageSize, pageNumber);

        var collection = orderFromDatabase.Items.AsEnumerable();

        if (collection == null) return (null, paginationMetadata);

        var orderItemsToReturn = collection
            .OrderBy(o => o.OrderId)
            .Skip(pageSize * (pageNumber - 1))
            .Take(pageSize);
        // .ToListAsync();

        return (orderItemsToReturn, paginationMetadata);
    }

    public async Task<OrderItem?> GetOrderItemByIdAsync(int orderId, int itemId)
    {
        return await _context.OrderItems
            .FirstOrDefaultAsync(i => i.OrderId == orderId && i.OrderItemId == itemId);
    }

    public void AddOrderItem(Order order, OrderItem item)
    {
        order.Items.Add(item);
    }

    public void UpdateOrderItem(OrderItem item, Course newItem)
    {
        _mapper.Map(newItem, item);
    }

    public void DeleteOrderItem(OrderItem item)
    {
        _context.OrderItems.Remove(item);
    }



    public async Task<Receipt?> GetReceiptFromOrderById(int orderId)
    {
        var orderFromDatabase = await _context.Orders
            .Include(o => o.Receipt)
            .FirstOrDefaultAsync(o => o.OrderId == orderId);

        return orderFromDatabase?.Receipt;
    }



    public async Task<Payment?> GetPaymentFromOrderByIdAsync(int orderId)
    {
        var orderFromDatabase = await _context.Orders.FirstOrDefaultAsync(o => o.OrderId == orderId);

        return orderFromDatabase?.Payment;
    }

    public void AddPayment(Order order, Payment payment)
    {
        order.Payment = payment;
    }

    public void UpdatePayment(Payment payment, UpdatePaymentCommand newPayment)
    {
        _mapper.Map(newPayment, payment);
    }

    public void DeletePayment(Payment payment)
    {
        _context.Payments.Remove(payment);
    }



    public async Task<bool> SaveChangesAsync()
    {
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> CustomerExists(int customerId)
    {
        return await _context.Customers.AnyAsync(customer => customer.Id == customerId);
    }
}
