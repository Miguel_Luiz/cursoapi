using AutoMapper;
using Univali.Application.Contracts.Repositories;
using Univali.Domain.Entities;
using Univali.Persistence.DbContexts;
using Microsoft.EntityFrameworkCore;
using Univali.Application.Features.Addresses.Commands.UpdateAddress;
using Univali.Application.Features.Customers.Commands.UpdateCustomer;
using Univali.Application.Features.Customers.Commands.UpdateCustomerWithAddresses;
using Univali.Application.Models;
using Univali.Application.Features.Common;
using Univali.Application.Features.Customers.Commands.PartiallyUpdateCustomer;

namespace Univali.Persistence.Repositories;

//Implementa a interface ICustomerRepository
public class CustomerRepository : ICustomerRepository
{
    private readonly CustomerContext _context;
    private readonly IMapper _mapper;

    public CustomerRepository(CustomerContext customerContext, IMapper mapper)
    {
        _context = customerContext ?? throw new ArgumentNullException(nameof(customerContext));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    }



    public async Task<(IEnumerable<Customer>, PaginationMetadata)> GetCustomersAsync(string? type, string? searchQuery, int pageNumber, int pageSize)
    {
        IQueryable<Customer> collection = _context.Customers;

        if(type == "LegalCustomer")
        {
            collection =  _context.LegalCustomers;
        }

        if(type == "NaturalCustomer")
        {
            collection = _context.NaturalCustomers;
        }
        

        if (!string.IsNullOrWhiteSpace(searchQuery))
        {
            var name = searchQuery.Trim();
            collection = collection.Where(

                c => c.Name.Contains(searchQuery)
            );
        }


        var totalItemCount = await collection.CountAsync();

        var paginationMetadata = new PaginationMetadata(totalItemCount, pageSize, pageNumber);

        var costumerToReturn = await collection
        .OrderBy(c => c.Id)
        .Skip(pageSize * (pageNumber-1))
        .Take(pageSize)
        .ToListAsync();

        return (costumerToReturn, paginationMetadata);
    }

    public async Task<IEnumerable<Customer>> GetCustomersAsync()
    {
        return await _context.Customers.OrderBy(c => c.Id).ToListAsync();
    }

    public async Task<IEnumerable<Customer>> GetCustomersWithAddressesAsync()
    {
        return await _context.Customers
            .OrderBy(c => c.Id)
            .Include(c => c.Addresses)
            .ToListAsync();
    }

    public async Task<Customer?> GetCustomerByIdAsync(int customerId)
    {
        return await _context.Customers.FirstOrDefaultAsync(c => c.Id == customerId);
    }

    public async Task<Customer?> GetCustomerWithAddressesByIdAsync(int customerId)
    {
        return await _context.Customers
            .Include(c => c.Addresses)
            .FirstOrDefaultAsync(c => c.Id == customerId);
    }

    public async Task<Customer?> GetCustomerByCpfAsync(string customerCpf)
    {
        return await _context.NaturalCustomers.FirstOrDefaultAsync(customer => customer.CPF == customerCpf);
    }

    public async Task<Customer?> GetCustomerByCnpjAsync(string customerCnpj)
    {
        return await _context.LegalCustomers.FirstOrDefaultAsync(customer => customer.CNPJ == customerCnpj);
    }

    public void AddCustomer(Customer customer)
    {
        _context.Customers.Add(customer);
    }

    public void UpdateCustomer(Customer customer, UpdateCustomerCommand newCustomer)
    {
        _mapper.Map(newCustomer, customer);
    }

    public void UpdateCustomer(Customer customer, UpdateCustomerWithAddressesCommand newCustomer)
    {
        _mapper.Map(newCustomer, customer);
    }

    public void UpdateCustomer(Customer customer, NaturalCustomer newCustomer)
    {
        _mapper.Map(newCustomer, customer);
    }

    public void UpdateCustomer(Customer customer, LegalCustomer newCustomer)
    {
        _mapper.Map(newCustomer, customer);
    }

    public void DeleteCustomer(Customer customer)
    {
        _context.Customers.Remove(customer);
    }

    public void PatchCustomer(Customer customer, CustomerForPatchDto newCustomer)
    {
        _mapper.Map(newCustomer, customer);
    }

    public void PatchCustomer(Customer customer, PartiallyUpdateCustomerCommand newCustomer)
    {
        _mapper.Map(newCustomer, customer);
    }



    public async Task<(IEnumerable<Address>?, PaginationMetadata)> GetAddressesAsync(int customerId, string? city, string? searchQuery, int pageNumber, int pageSize)
    {
        IQueryable<Address> collection = _context.Customers.Where(c=> c.Id == customerId).SelectMany(c => c.Addresses);
        
        if(!string.IsNullOrWhiteSpace(city))
        {
            collection = collection.Where(a => a.City == city);
        }
        
        if (!string.IsNullOrWhiteSpace(searchQuery))
        {
            collection = collection.Where(
                a => a.Street.Contains(searchQuery.Trim())
                || a.City.Contains(searchQuery.Trim())
            );
        }


        var totalItemCount = await collection.CountAsync();

        var paginationMetadata = new PaginationMetadata(totalItemCount, pageSize, pageNumber);

        var addressToReturn = await collection
        .OrderBy(a => a.Id)
        .Skip(pageSize * (pageNumber-1))
        .Take(pageSize)
        .ToListAsync();

        return (addressToReturn, paginationMetadata);
    }
    
    public async Task<IEnumerable<Address>?> GetAddressesAsync(int customerId)
    {
        var customerFromDatabase = await GetCustomerWithAddressesByIdAsync(customerId);
        return customerFromDatabase?.Addresses;
    }

    public async Task<Address?> GetAddressByIdAsync(int customerId, int addressId)
    {
        return await _context.Addresses.FirstOrDefaultAsync(a => a.CustomerId == customerId && a.Id == addressId);
    }

    public void AddAddress(Customer customer, Address address)
    {
        customer.Addresses.Add(address);
    }

    public void UpdateAddress(Address address, UpdateAddressCommand newAddress)
    {
        _mapper.Map(newAddress, address);
    }

    public void DeleteAddress(Address address)
    {
        _context.Addresses.Remove(address);
    }



    public async Task<bool> SaveChangesAsync()
    {
        return await _context.SaveChangesAsync() > 0;
    }
}
