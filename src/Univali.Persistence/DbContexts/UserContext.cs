using Microsoft.EntityFrameworkCore;
using Univali.Domain.Entities;
using Univali.Persistence.Extensions;

namespace Univali.Persistence.DbContexts;
public class UserContext : DbContext
{
    public UserContext(DbContextOptions<UserContext> options) : base(options)
    {
    }

    public DbSet<User> Users { get; set; } = null!;

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Chame os métodos de extensão para configurar a entidade User
        modelBuilder.UserContextSeedData();

        base.OnModelCreating(modelBuilder);
    }
}
