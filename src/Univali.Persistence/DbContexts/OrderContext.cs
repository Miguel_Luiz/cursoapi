using Microsoft.EntityFrameworkCore;
using Univali.Domain.Entities;
using Univali.Persistence.Extensions;

namespace Univali.Persistence.DbContexts;

public class OrderContext : DbContext
{
    public DbSet<Customer> Customers { get; set; } = null!;
    public DbSet<Course> Courses { get; set; } = null!;
    public DbSet<Order> Orders { get; set; } = null!;
    public DbSet<OrderItem> OrderItems { get; set; } = null!;
    public DbSet<Receipt> Receipts { get; set; } = null!;
    public DbSet<Payment> Payments { get; set; } = null!;

    public OrderContext(DbContextOptions<OrderContext> options)
    : base(options)  { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Customer>().ToTable(nameof(Customers), t => t.ExcludeFromMigrations());
        modelBuilder.Entity<Course>().ToTable(nameof(Courses), t => t.ExcludeFromMigrations());
        modelBuilder.Entity<CourseStudent>().ToTable("CoursesStudents", t => t.ExcludeFromMigrations()); // criada em PublisherContext

        modelBuilder.CustomerInitFluentApi();
        modelBuilder.CourseInitFluentApi();

        modelBuilder.OrderInitFluentApi();
        modelBuilder.OrderItemInitFluentApi();
        modelBuilder.ReceiptInitFluentApi();
        modelBuilder.PaymentInitFluentApi();

        modelBuilder.OrderContextSeedData();

        modelBuilder.Ignore<Address>();
        modelBuilder.Ignore<Author>();
        modelBuilder.Ignore<Publisher>();
        modelBuilder.Ignore<Module>();
        modelBuilder.Ignore<Student>();

        base.OnModelCreating(modelBuilder);
    }
}