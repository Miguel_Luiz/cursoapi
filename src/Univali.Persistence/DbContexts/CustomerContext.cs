using Microsoft.EntityFrameworkCore;
using Univali.Domain.Entities;
using Univali.Persistence.Extensions;

namespace Univali.Persistence.DbContexts;

public class CustomerContext : DbContext
{
    public DbSet<Customer> Customers { get; set; } = null!;
    public DbSet<Address> Addresses { get; set; } = null!;
    //TPT
    public DbSet<NaturalCustomer> NaturalCustomers { get; set; } = null!;
    public DbSet<LegalCustomer> LegalCustomers { get; set; } = null!;

    public CustomerContext(DbContextOptions<CustomerContext> options)
    : base(options) { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.CustomerInitFluentApi();
            modelBuilder.NaturalCustomerInitFluentApi();
            modelBuilder.LegalCustomerInitFluentApi();
        modelBuilder.AddressInitFluentApi();

        modelBuilder.CustomerContextSeedData();

        modelBuilder.Ignore<Order>();

        base.OnModelCreating(modelBuilder);
    }
}